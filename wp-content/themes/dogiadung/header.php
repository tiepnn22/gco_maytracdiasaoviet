<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="canonical" href="<?php echo get_page_link_current(); ?>" />
    <title><?php echo title(); ?></title>
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>


<script type="text/javascript">
    var ajax_url = '<?php echo admin_url('admin-ajax.php'); ?>';
    // var path_dist = '<?php echo get_template_directory_uri(); ?>/dist/';
</script>

<?php
    //field
    $customer_slogan = get_field('customer_slogan', 'option');
    $customer_phone = get_field('customer_phone', 'option');
    $customer_email = get_field('customer_email', 'option');

    $h_service = get_field('h_service', 'option');
?>

<div class="wrapper index">

<header class="top">
    <div class="b1 s14 t3 top-contact">
        <div class="container">
            <div class="d-flex align-items-center justify-content-between">
                <a href="javascript:void(0)" class="" title=""><?php echo $customer_slogan; ?></a>
                <div class="d-flex align-items-center tcontact-r">
                    <!-- <div class="tregis">
                        <i class="far fa-user"></i> <a href="#" data-toggle="modal" data-target="#regis">Đăng ký</a><a href="#" data-toggle="modal" data-target="#login">Đăng nhập</a>
                    </div> -->
                    <div class="s12 d-flex align-items-center justify-content-center menu-r">
                        <div class="menu-r-top">
                            <i class="fas fa-search d-md-none d-inline-block search-open"></i>

                            <?php get_template_part("resources/views/wc/wc-info-cart"); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="">
        <div class="container">
            <div class="w-100 d-flex align-items-center justify-content-between top-menu">
                <div class="d-flex justify-content-center align-items-center top-menu-btn">
                    <a id="nav-icon" href="#menu" class="d-xl-none">
                        <span></span>
                        <span></span>
                        <span></span>
                    </a>
                    <?php get_template_part("resources/views/logo"); ?>
                </div>

                <div class="d-flex justify-content-center align-items-center top-service">
                    <?php echo $h_service; ?>
                </div>

                <div class="d-flex align-items-center top-mid">
                    <?php get_template_part("resources/views/search-form-by-category"); ?>

                    <a class="d-flex align-items-center tphone" href="tel:<?php echo str_replace(' ','',$customer_phone);?>" title="">
                        <img src="<?php echo asset('images/phone.png'); ?>" title="" alt="">
                        <span class="pl-3 text-uppercase d-lg-inline-block d-none">
                            <span class="bold d-block">Hotline</span>
                            <?php echo $customer_phone; ?>
                        </span>
                    </a>
                </div>
            </div>
        </div>
        <div class="container d-lg-block d-none">
            <div class="d-flex w-100 align-items-center justify-content-between header-top-menu">

                <div class="position-relative tcate">
                    <h2 class="s13 text-uppercase tcate-tit"><i class="fas fa-bars mr-3"></i> Danh mục sản phẩm</h2>
                    <?php get_template_part("resources/views/menu-category-product"); ?>
                </div>
                <?php get_template_part("resources/views/menu"); ?>
            </div>
        </div>
    </div>
</header>

<main class="">