<?php get_header(); ?>

<h1 style="display: none;"><?php echo get_option('blogname'); ?> - <?php echo get_option('blogdescription'); ?></h1>

<?php
    //field
    $home_slide 				= get_field('home_slide');
    $home_slide_image_ads 		= get_field('home_slide_image_ads');
    $home_slide_image_ads_url 	= get_field('home_slide_image_ads_url');

    $image_ads_one        	= get_field('image_ads_one');
    $image_ads_one_url      = get_field('image_ads_one_url');
    $image_ads_two  		= get_field('image_ads_two');
    $image_ads_two_url		= get_field('image_ads_two_url');

    $home_product_select_cat = get_field('home_product_select_cat');

    $image_ads_three 		= get_field('image_ads_three');
    $image_ads_three_url 	= get_field('image_ads_three_url');
    $image_ads_four 		= get_field('image_ads_four');
    $image_ads_four_url     = get_field('image_ads_four_url');

    $home_product_buy_hot_title     = get_field('home_product_buy_hot_title');
    $home_product_buy_hot_select    = get_field('home_product_buy_hot_select');
?>

<?php if(!empty( $home_slide )) { ?>
<section class="b3 slider-area">
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div id="slider">

			        <?php
			            foreach ($home_slide as $foreach_kq) {

			            $post_image = $foreach_kq["image"];
			            $post_link  = $foreach_kq["url"];
			        ?>
						<a href="<?php echo $post_link; ?>" title="">
							<img class="slider-img" src="<?php echo $post_image; ?>" alt="" />
						</a>
			        <?php } ?>

                </div>
            </div>
            <div class="col-md-3 text-center">

            	<?php if(!empty( $home_slide_image_ads )) { ?>
                <a href="<?php echo $home_slide_image_ads_url; ?>" title="">
                	<img class="w-100" src="<?php echo $home_slide_image_ads; ?>" alt="">
                </a>
                <?php } ?>

            </div>
        </div>
    </div>
</section>
<?php } ?>

<section class="sale">
    <div class="container">
        <h1 class="s16 t1 text-center text-uppercase py-4 sale-tit">Sản phẩm nổi bật</h1>
        <div class="sale-slider">

			<?php
			    $tax_query[] = array(
			        'taxonomy' => 'product_visibility',
			        'field'    => 'name',
			        'terms'    => 'featured',
			        'operator' => 'IN',
			    );
			    $query =  new WP_Query( array(
			        'post_type'             => 'product',
			        'post_status'           => 'publish',
			        'ignore_sticky_posts'   => 1,
			        'showposts'             => -1,
			        'orderby'               => 'date',
			        'order'                 => 'DESC',
			        'tax_query'             => $tax_query
			    ) );

                if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();
            ?>

                <?php get_template_part('resources/views/content/home-product-featured', get_post_format()); ?>

            <?php endwhile; wp_reset_postdata(); else: echo ''; endif; ?>

        </div>
    </div>
</section>

<section class="b3 tabs-wrap">
    <div class="container">

        <div class="row">
            <div class="col-sm-6 text-center">
                <a class="link-ef" href="<?php echo $image_ads_one_url; ?>" title="">
                	<img src="<?php echo $image_ads_one; ?>" alt="">
                </a>
            </div>
            <div class="col-sm-6 text-center">
                <a class="link-ef" href="<?php echo $image_ads_two_url; ?>" title="">
                	<img src="<?php echo $image_ads_two; ?>" alt="">
                </a>
            </div>
        </div>

        <?php if(!empty( $home_product_select_cat )) { ?>
		<?php
			$i = 0;
			foreach ($home_product_select_cat as $foreach_kq) {

			$taxonomy_product = $foreach_kq["taxonomy_product"];
			$taxonomy_product_image_ads_one 	= $foreach_kq["taxonomy_product_image_ads_one"];
			$taxonomy_product_image_ads_one_url = $foreach_kq["taxonomy_product_image_ads_one_url"];
			$taxonomy_product_image_ads_two 	= $foreach_kq["taxonomy_product_image_ads_two"];
			$taxonomy_product_image_ads_two_url = $foreach_kq["taxonomy_product_image_ads_two_url"];

			$term_id = $taxonomy_product->term_id;
			$taxonomy_slug = $taxonomy_product->taxonomy;
			$term_name = get_term( $term_id, $taxonomy_slug )->name;
			$term_link = get_term_link(get_term( $term_id ));
			$term_excerpt = cut_string( $taxonomy_product->description ,300,'...');

			// child cat
			$term_childs = get_term_children( $term_id, $taxonomy_slug );
			$count = count($term_childs);
		?>

	        <div class="d-flex align-items-center justify-content-between flex-wrap bg-white tab-header">
	            <h2 class="t3 b2 text-center text-uppercase"><?php echo $term_name; ?></h2>
	            <div class="d-flex flex-wrap align-items-center tab-header-r">
	                <ul class="nav nav-pills justify-content-center justify-content-lg-center wel-tabs" role="tablist">

						<?php
							$j = 1;
							foreach ($term_childs as $foreach_kq) {
							
							$term_childs_id = $foreach_kq;
							$term_childs_name = get_term( $term_childs_id, $taxonomy_slug )->name;
						?>
					            <li class="nav-item">
					                <a class="<?php if($j == 1){ echo 'active'; } ?>" data-toggle="pill" href="#sp_<?php echo $i; ?>_<?php echo $j; ?>">
					                    <?php echo $term_childs_name; ?>
					                </a>
					            </li>
						<?php
							$j++; }
						?>

	                </ul>
	                <a href="<?php echo $term_link; ?>" class="t1 bold px-4 tab-link" title="">Xem tất cả</a>
	            </div>
	        </div>
	        <div class="">
	            <div class="row">
	                <div class="col-lg-2">
	                    <div class="text-center d-flex flex-wrap align-items-stretch justify-content-between h-100 tabs-ads">
	                        <a href="<?php echo $taxonomy_product_image_ads_one_url; ?>" class="link-ef" title="">
	                        	<img src="<?php echo $taxonomy_product_image_ads_one; ?>" alt="" title="">
	                        </a>
	                        <a href="<?php echo $taxonomy_product_image_ads_two_url; ?>" class="link-ef" title="">
	                        	<img src="<?php echo $taxonomy_product_image_ads_two; ?>" alt="" title="">
	                        </a>
	                    </div>
	                </div>
	                <div class="col-lg-10">
	                    <div class="tab-content">

			            	<!--Nếu ko có cat con-->
							<?php if($count == 0) { ?>
							    <div class="tab-pane fade show active" id="">
							        <div class="row">

										<?php
											$query = query_post_by_taxonomy_paged('product', $taxonomy_slug, $term_id, 8);

											if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();

							                $post_id 		= get_the_ID();
							                $post_title 	= get_the_title($post_id);
							                $post_date 		= get_the_date('Y/m/d',$post_id);
							                $post_link 		= get_post_permalink($post_id);
							                $post_image 	= getPostImage($post_id,"p-product");
							                $post_excerpt 	= cut_string(get_the_excerpt($post_id),80,'...');
							                $post_author 	= get_the_author_meta( 'nicename', get_the_author_meta( get_the_author() ) );
							                $post_tag 		= get_the_tags($post_id);
										?>

			                                <div class="col-lg-3 col-md-6 col-sm-6" data-productid="<?php echo $post_id; ?>">
			                                    <article class="bg-white sale-item woocommerce">
											        <div class="text-center sale-img">
											        	<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
											        		<img src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>">
											        	</a>
											        </div>
			                                        <div class="bg-white sale-item-info">
											            <h3 class="sale-name">
											            	<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
											            		<?php echo $post_title; ?>
											            	</a>
											            </h3>
											            <?php echo show_rating($post_id); ?>
											        	<?php echo show_price_old_price($post_id); ?>
			                                            <ul class="list-unstyled ap-action">
			                                                <li><?php echo show_add_to_cart_button($post_id); ?></li>
			                                                <li><a href="javascript:void(0)" class="vk-shop-item__btn hover-product" data-productid="<?php echo $post_id; ?>" title="Xem thêm"><i class="fas fa-search"></i></a></li>
			                                            </ul>
			                                        </div>
			                                    </article>
			                                </div>
											
										<?php endwhile; wp_reset_postdata(); else: echo ''; endif; ?>

									</div>
							    </div>
							<?php } ?>

							<!--Nếu có cat con-->
							<?php
								$j = 1;
								foreach ($term_childs as $foreach_kq) {
								
								$term_childs_id = $foreach_kq;
								$term_childs_name = get_term( $term_childs_id, $taxonomy_slug )->name;
							?>

					                <div class="tab-pane fade <?php if($j == 1){ echo 'show active'; } ?>" id="sp_<?php echo $i; ?>_<?php echo $j; ?>">
							            <div class="row">

											<?php
												$query = query_post_by_taxonomy_paged('product', $taxonomy_slug, $term_childs_id, 8);

												if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();

						                        $post_id 		= get_the_ID();
						                        $post_title 	= get_the_title($post_id);
						                        $post_date 		= get_the_date('Y/m/d',$post_id);
						                        $post_link 		= get_post_permalink($post_id);
						                        $post_image 	= getPostImage($post_id,"p-product");
						                        $post_excerpt 	= cut_string(get_the_excerpt($post_id),80,'...');
						                        $post_author 	= get_the_author_meta( 'nicename', get_the_author_meta( get_the_author() ) );
						                        $post_tag 		= get_the_tags($post_id);
											?>

				                                <div class="col-lg-3 col-md-6 col-sm-6" data-productid="<?php echo $post_id; ?>">
				                                    <article class="bg-white sale-item woocommerce">
												        <div class="text-center sale-img">
												        	<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
												        		<img src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>">
												        	</a>
												        </div>
				                                        <div class="bg-white sale-item-info">
												            <h3 class="sale-name">
												            	<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
												            		<?php echo $post_title; ?>
												            	</a>
												            </h3>
												            <?php echo show_rating($post_id); ?>
												        	<?php echo show_price_old_price($post_id); ?>
				                                            <ul class="list-unstyled ap-action">
				                                                <li><?php echo show_add_to_cart_button($post_id); ?></li>
				                                                <li><a href="javascript:void(0)" class="vk-shop-item__btn hover-product" data-productid="<?php echo $post_id; ?>" title="Xem thêm"><i class="fas fa-search"></i></a></li>
				                                            </ul>
				                                        </div>
				                                    </article>
				                                </div>
												
											<?php endwhile; wp_reset_postdata(); else: echo ''; endif; ?>

										</div>
					                </div>

							<?php
								$j++; }
							?>

	                    </div>
	                </div>
	            </div>
	        </div>
		
		<?php
			$i++; }
		?>
        <?php } ?>

        <div class="row">
            <div class="col-sm-6 text-center">
                <a class="link-ef" href="<?php echo $image_ads_three_url; ?>" title="">
                	<img src="<?php echo $image_ads_three; ?>" alt="">
                </a>
            </div>
            <div class="col-sm-6 text-center">
                <a class="link-ef" href="<?php echo $image_ads_four_url; ?>" title="">
                	<img src="<?php echo $image_ads_four; ?>" alt="">
                </a>
            </div>
        </div>

        <div class="bg-white hpro">
            <h2 class="s16 t1 text-uppercase hp-tit"><?php echo $home_product_buy_hot_title; ?></h2>

			<?php if(!empty( $home_product_buy_hot_select )) { ?>
            <div class="hpro-slider">

				<?php
				    foreach ($home_product_buy_hot_select as $foreach_kq) {

					$post_id 			= $foreach_kq->ID;
					$post_title 		= get_the_title($post_id);
					$post_date 			= get_the_date('d/m/Y',$post_id);
					$post_link 			= get_permalink($post_id);
					$post_image 		= getPostImage($post_id,"p-product");
					$post_excerpt 		= cut_string(get_the_excerpt($post_id),200,'...');
					$post_author 		= get_the_author_meta( 'nicename', get_the_author_meta( get_the_author() ) );
					$post_tag 			= get_the_tags($post_id);

					// $product = new WC_product($post_id);
				?>

	                <article class="bg-white sale-item woocommerce" data-productid="<?php echo $post_id; ?>">
				        <div class="text-center sale-img">
				        	<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
				        		<img src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>">
				        	</a>
				        </div>
	                    <div class="bg-white sale-item-info">
				            <h3 class="sale-name">
				            	<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
				            		<?php echo $post_title; ?>
				            	</a>
				            </h3>

				            <?php echo show_rating($post_id); ?>

				        	<?php echo show_price_old_price($post_id); ?>

	                        <ul class="list-unstyled ap-action">
	                            <li>
	                            	<?php echo show_add_to_cart_button($post_id); ?>
	                            </li>
	                            <li><a href="javascript:void(0)" class="vk-shop-item__btn hover-product" data-productid="<?php echo $post_id; ?>" title="Xem thêm"><i class="fas fa-search"></i></a></li>
	                        </ul>
	                    </div>
	                </article>

				<?php } ?>

            </div>
            <?php } ?>

        </div>

    </div>
</section>

<?php get_footer(); ?>

