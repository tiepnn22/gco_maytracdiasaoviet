<?php get_header(); ?>

<?php
	$page_id       = get_the_ID();
	$page_name     = get_the_title();
	$page_content  = get_the_content(); //woo phải dùng the_content()
?>

<?php get_template_part("resources/views/page-banner"); ?>

<div class="container">
    <div class="" style="padding: 35px 0px 50px;">
        <h1 class="bold s30 t1 pb-4 about-tit">
            <?php echo $page_name; ?>
        </h1>
        <div class="<?php if( is_cart() || is_checkout() ) {} else { echo 'wp-editor-fix'; } ?>">
            <?php the_content(); ?>
        </div>
    </div>
</div>

<?php get_footer(); ?>