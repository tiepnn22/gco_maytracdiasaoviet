<?php
/**
 * The Template for displaying all single products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce\Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'shop' ); ?>

<?php
    $product_id = get_the_ID();

	$terms = wp_get_object_terms($post->ID, 'product_cat');
	if (!is_wp_error($terms) && !empty($terms) && is_object($terms[0])) $term = $terms[0];
    $term_id        = $term->term_id;
    $term_name      = $term->name;
    $term_excerpt   = wpautop($term->description);
    $term_link      = esc_url(get_term_link($term_id));
    $taxonomy_name  = $term->taxonomy;
    $thumbnail_id        = get_term_meta( $term_id, 'thumbnail_id', true ); // woo
    $term_image_check    = wp_get_attachment_url( $thumbnail_id ); // woo
    $term_image          = (!empty($term_image_check)) ? $term_image_check : ''; // woo

    //woocommerce
    $product = new WC_product($product_id);

    //gallery
    $single_product_gallery = $product->get_gallery_image_ids();
    
    //info product
    $single_product_title       = get_the_title($product_id);
    $single_product_date        = get_the_date('d/m/Y', $product_id);
    $single_product_link        = get_permalink($product_id);
    $single_product_image       = getPostImage($product_id,"full");
    $single_product_excerpt     = cut_string(get_the_excerpt($product_id),300,'...');
    $single_recent_author       = get_user_by( 'ID', get_post_field( 'post_author', get_the_author() ) );
    $single_product_author      = $single_recent_author->display_name;
    $single_product_tag         = get_the_tags($product_id);

    //field
    $single_product_parameter       = get_field('single_product_parameter');
    $single_product_form_quote_id   = get_field('single_product_form_quote', 'option');
    $single_product_form_quote      = do_shortcode('[contact-form-7 id="'.$single_product_form_quote_id.'"]');
?>

<?php get_template_part("resources/views/page-banner"); ?>

<section class="pdetail b3">
    <div class="container">
        <div class="pro-wrap">
            <div class="row">

                <div class="col-lg-9 col-md-8">
                    <div class="row">
                        <div class="col-lg-7">
                            <div class="pdetail-slider">
                                
                                <a class="MagicZoomPlus" id="shoes" href="<?php echo $single_product_image; ?>">
                                	<img src="<?php echo $single_product_image; ?>" />
                                </a>

                                <div class="MagicScroll" data-mode="scroll" data-options="speed: 600; loop: rewind; height:80px; items: 4; arrows:true; scrollOnWheel:true;" data-mobile-options="items: 3;">
                                    <a data-zoom-id="shoes" href="<?php echo $single_product_image; ?>" data-image="<?php echo $single_product_image; ?>">
                                    	<img src="<?php echo $single_product_image; ?>" />
                                    </a>

	                                <?php if(!empty( $single_product_gallery )) { ?>
	                                <?php
	                                    foreach( $single_product_gallery as $foreach_kq ){

	                                    $post_image = wp_get_attachment_url( $foreach_kq );
	                                ?>
	                                    <a data-zoom-id="shoes" href="<?php echo $post_image; ?>" data-image="<?php echo $post_image; ?>">
	                                    	<img src="<?php echo $post_image; ?>" />
	                                    </a>
	                                <?php } ?>
	                                <?php } ?>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-5">
                            <div class="pdetail-r">
                                <h1 class="bold s24 pdetail-tit"><?php echo $single_product_title; ?></h1>

					            <?php echo show_rating($product_id); ?>

				            	<?php echo show_price_old_price($product_id); ?>

                                <?php if(!empty( $single_product_parameter )) { ?>
                                <div class="pdetail-info">
                                    <h2 class="bold pb-2">Thông số cơ bản</h2>
                                    <table class="table pdetail-tbl">

						                <?php
						                    foreach ($single_product_parameter as $foreach_kq) {

						                    $post_title = $foreach_kq["title"];
						                    $post_desc  = $foreach_kq["desc"];
						                ?>
	                                        <tr>
	                                            <td><?php echo $post_title; ?></td>
	                                            <td>: <?php echo $post_desc; ?></td>
	                                        </tr>
						                <?php } ?>
                				
                                    </table>
                                </div>
                                <?php } ?>

                                <div class="d-flex flex-wrap pdetai-act">
                                    <?php echo show_add_to_cart_button_quantity($product_id); ?>
                                </div>

                                <?php get_template_part("resources/views/socical-bar"); ?>
                            </div>
                        </div>
                    </div>

                    <div class="bg-white">
                        <nav>
                            <div class="nav nav-tabs pdetail-tabs" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link active" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Thông tin</a>
                                <a class="nav-item nav-link" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Đánh giá</a>
                            </div>
                        </nav>
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                                <div class="pdetail-content wp-editor-fix">
                                    <?php the_content(); ?>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                                <?php
                                    if ( comments_open() || get_comments_number() ) {
                                        comments_template();
                                    }
                                ?>
                            </div>
                        </div>
                    </div>

                    <div class="single-product-form-quote">
                        <?php if(!empty( $single_product_form_quote )) { ?>
                        <div class='contact-frm'>
                            <?php echo $single_product_form_quote; ?>
                        </div>
                        <?php } ?>
                    </div>
                </div>

                <div class="col-lg-3 col-md-4">
                    <aside class="aside paside">
                    	<?php dynamic_sidebar( 'sidebar-product' ); ?>
                    </aside>
                </div>

            </div>
        </div>
    </div>
</section>

<?php
get_footer( 'shop' );

/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */
