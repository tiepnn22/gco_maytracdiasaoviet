<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

get_header( 'shop' ); ?>

<?php
    $term_info         = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
    $term_id           = $term_info->term_id;
    $term_name_check   = $term_info->name;
    $term_name		   = (!empty($term_name_check)) ? $term_name_check : 'Sản phẩm';
    // $term_excerpt   = wpautop($term_info->description);
    // $term_link      = esc_url(get_term_link($term_id));
    $taxonomy_name  = $term_info->taxonomy;
    $thumbnail_id   = get_term_meta( $term_id, 'thumbnail_id', true );
    $term_image     = wp_get_attachment_url( $thumbnail_id );
?>

<div class="bg-white">
    <?php get_template_part("resources/views/page-banner"); ?>
</div>

<section class="pro b3">
    <div class="container">
        
        <div class="pbanner text-center">
            <?php if(!empty( $term_image )) { ?>
            <a href="javascript:void(0)" title="">
                <img src="<?php echo $term_image; ?>" alt="<?php echo $term_name; ?>">
            </a>
            <?php } ?>
        </div>

        <div class="pro-wrap">
            <div class="row">

                <div class="col-lg-3 col-md-4">
                    <aside class="aside paside">
                        <h2 class="s16 text-white b2 text-uppercase s16 text-center py-3 aside-tit">Danh mục</h2>
                        <div class="vk-sidebar">
                            <div class="vk-sidebar__box">
                                <ul class="vk-sidebar__list vk-sidebar__list--style-1" data-sidebar="list">

                                <?php
                                    if(!empty( $term_id )) {
                                        // nếu có cấp con
                                        $term_childs = get_term_children( $term_id, $taxonomy_name );
                                        $count = count($term_childs);
                                        if($count > 0) {

                                            foreach ( $term_childs as $foreach_kq ) {
                                                $term = get_term_by( 'id', $foreach_kq, $taxonomy_name );
                                                $f_term_id    = $term->term_id;
                                                $f_term_name  = $term->name;
                                                $f_term_link  = esc_url(get_term_link($f_term_id));
                                            ?>
                                                <li><a href="<?php echo $f_term_link; ?>"><?php echo $f_term_name; ?></a></li>
                                            <?php
                                            }
                                        } else {
                                            // nếu ko có cấp con, sẽ đọc cấp con của cha
                                            $term_parent = $term_info->parent;

                                            $term_childs = get_term_children( $term_parent, $taxonomy_name );
                                            $count = count($term_childs);
                                            if($count > 0) {

                                                foreach ( $term_childs as $foreach_kq ) {
                                                    $term = get_term_by( 'id', $foreach_kq, $taxonomy_name );
                                                    $f_term_id    = $term->term_id;
                                                    $f_term_name  = $term->name;
                                                    $f_term_link  = esc_url(get_term_link($f_term_id));
                                                ?>
                                                    <li><a href="<?php echo $f_term_link; ?>"><?php echo $f_term_name; ?></a></li>
                                                <?php
                                                }
                                            }
                                        }
                                    } else {
                                        // nếu là danh mục cấp 1
                                        $terms_info = get_terms( 'product_cat', array(
                                            'parent'       => 0,
                                            'hide_empty'   => true
                                        ) );

                                        foreach ( $terms_info as $foreach_kq ) {
                                            $f_term_id    = $foreach_kq->term_id;
                                            $f_term_name  = $foreach_kq->name;
                                            $f_term_link  = esc_url(get_term_link($f_term_id));
                                        ?>
                                            <li><a href="<?php echo $f_term_link; ?>"><?php echo $f_term_name; ?></a></li>
                                        <?php
                                        }
                                    }
                                ?>

                                </ul>
                            </div>
                        </div>
                    </aside>
                </div>

                <?php
                    if ( woocommerce_product_loop() ) {
                ?>

                    <div class="col-lg-9 col-md-8">
                        <div class="bg-white d-flex align-items-center justify-content-between flex-wrap pcontrol">
                            <h2 class="text-uppercase"><?php echo $term_name; ?></h2>
                            <!--order-->
                            <?php do_action( 'woocommerce_before_shop_loop' ); ?>
                        </div>
                        <div class="pwrap">
                            <div class="row">

                                <?php
                                    // woocommerce_product_loop_start();
                                    if ( wc_get_loop_prop( 'total' ) ) {
                                        while ( have_posts() ) {
                                            the_post();

                                            // do_action( 'woocommerce_shop_loop' );
                                            // wc_get_template_part( 'content', 'product' );
                                            get_template_part('resources/views/content/category-product', get_post_format());
                                        }
                                    }
                                    // woocommerce_product_loop_end();
                                ?>

                            </div>

                            <!--pagination-->
                            <?php do_action( 'woocommerce_after_shop_loop' ); ?>

                        </div>
                    </div>

                <?php
                        // do_action( 'woocommerce_after_shop_loop' );
                    } else {
                        do_action( 'woocommerce_no_products_found' );
                    }
                ?>

            </div>
        </div>
    </div>
</section>

<?php

get_footer( 'shop' );
