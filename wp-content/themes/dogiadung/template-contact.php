<?php
	/*
	Template Name: Mẫu Liên hệ
	*/
?>

<?php get_header(); ?>

<?php
    $page_id        = get_the_ID();
    $page_name      = get_the_title();
    $page_content   = wpautop(get_the_content());

    //field
    $customer_address           = get_field('customer_address', 'option');
    $customer_phone             = get_field('customer_phone', 'option');
    $customer_email             = get_field('customer_email', 'option');

    $f_socical_facebook   = get_field('f_socical_facebook', 'option');
    $f_socical_insta      = get_field('f_socical_insta', 'option');
    $f_socical_twitter    = get_field('f_socical_twitter', 'option');
    $f_socical_youtube    = get_field('f_socical_youtube', 'option');

    $contact_info_title = get_field('contact_info_title');

    $contact_contact_title      = get_field('contact_contact_title');
    $contact_contact_form_id    = get_field('contact_contact_form');
    $contact_contact_form       = do_shortcode('[contact-form-7 id="'.$contact_contact_form_id.'"]');

    $contact_map = get_field('contact_map');
?>

<div class="b3">
    <?php get_template_part("resources/views/page-banner"); ?>
</div>

<div class="container">
    <div class="maps">
        <?php echo $contact_map; ?>
    </div>
    <div class="contact">
        <div class="row justify-content-between">
            <div class="col-lg-4 col-md-6 col-sm-6">
                <h1 class="bold s24 pb-5 contact-tit"><?php echo $contact_info_title; ?></h1>
                <ul class="list-unstyled s15 contact-add">
                    <li><i class="fas fa-map-marker-alt"></i> Địa chỉ: 
                        <?php echo $customer_address; ?>
                    </li>
                    <li><i class="fas fa-envelope"></i> Email: 
                        <a href="mailto:<?php echo $customer_email; ?>" title=""><?php echo $customer_email; ?></a>
                    </li>
                    <li><i class="fas fa-phone"></i> Hotline: 
                        <a href="tel:<?php echo str_replace(' ','',$customer_phone);?>" title=""><?php echo $customer_phone; ?></a>
                    </li>
                </ul>
                <ul class="list-unstyled pt-4 social">
                    <li>
                        <a href="<?php echo $f_socical_facebook; ?>" title="" target="_blank">
                            <i class="fab fa-facebook-f"></i>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo $f_socical_insta; ?>" title="" target="_blank">
                            <i class="fab fa-instagram"></i>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo $f_socical_twitter; ?>" title="" target="_blank">
                            <i class="fab fa-twitter"></i>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo $f_socical_youtube; ?>" title="" target="_blank">
                            <i class="fab fa-youtube"></i>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-sm-6">
                <h2 class="bold s24 pb-5 contact-tit"><?php echo $contact_contact_title; ?></h2>

                <?php if(!empty( $contact_contact_form )) { ?>
                <div class='contact-frm'>
                    <?php echo $contact_contact_form; ?>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>