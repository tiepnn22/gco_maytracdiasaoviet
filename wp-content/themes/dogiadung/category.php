<?php get_header(); ?>

<?php
	$category_info 	= get_category_by_slug( get_query_var( 'category_name' ) );
	$cat_id 		= $category_info->term_id;
	$cat_name 		= get_cat_name($cat_id);
	$cat_excerpt 	= wpautop(category_description($cat_id));
	$cat_link 		= esc_url(get_term_link($cat_id));
?>

<div class="bg-white">
    <?php get_template_part("resources/views/page-banner"); ?>
</div>

<section class="pdetail b3">
    <div class="container">
        <div class="pro-wrap">
            <div class="row">
                <div class="col-lg-9 col-md-8">
                    <div class="pb-4 blog">

                        <?php
                            $query = query_post_by_category_paged($cat_id, 6);
                            $max_num_pages = $query->max_num_pages;
                            $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

                            if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();
                        ?>

                            <?php get_template_part('resources/views/content/category-post', get_post_format()); ?>

                        <?php endwhile; wp_reset_postdata(); else: echo ''; endif; ?>

                    </div>

                    <div class="bg-white d-flex align-items-center justify-content-between s14 t4 flex-wrap pagi-wrap">
                        <span class="">Trang <?php echo $paged; ?>/<?php echo $max_num_pages; ?></span>
                        <?php echo paginationCustom( $max_num_pages ); ?>
                    </div>
                </div>

                <?php get_sidebar(); ?>

            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>