<?php get_header(); ?>

<?php
	$post_id = get_the_ID();

	$get_category = get_the_category($post_id);
	foreach ( $get_category as $get_category_kq ) {
	    $cat_id   = $get_category_kq->term_id;
	}
	$cat_name = get_cat_name($cat_id);
	
	//info post
	$single_post_title 		= get_the_title($post_id);
	$single_post_date 		= get_the_date('d m Y', $post_id);
	$single_post_link 		= get_permalink($post_id);
    $single_post_image 		= getPostImage($post_id,"full");
	$single_post_excerpt 	= cut_string(get_the_excerpt($post_id),300,'...');
	$single_recent_author 	= get_user_by( 'ID', get_post_field( 'post_author', get_the_author() ) );
	$single_post_author 	= $single_recent_author->display_name;
    $single_post_tag 		= get_the_tags($post_id);
?>

<div class="b3">
    <?php get_template_part("resources/views/page-banner"); ?>
</div>

<section class="bg-white bdetail-wrap">
    <div class="container">
        <div class="pro-wrap">
            <div class="row">
                <div class="col-lg-9 col-md-8">
                    <div class="pb-5 bdetail">
                        <h1 class="bold s30 bdetail-tit"><?php echo $single_post_title; ?></h1>
                        <h3 class="blog-time t4 s12"><?php echo $single_post_date; ?></h3>

                        <div class="wp-editor-fix">
                            <?php the_content(); ?>
                        </div>
                    </div>

                    <div class="bdetail-cm pt-5">
                        <h2 class="pb-4 text-uppercase">Viết bình luận</h2>

                        <div class="fb-comments" data-href="<?php the_permalink();?>" data-width="100%" data-numposts="5" data-colorscheme="light"></div>
                    </div>

                    <?php get_template_part("resources/views/template-related-post"); ?>
                </div>

                <?php get_sidebar(); ?>

            </div>
        </div>
    </div>
</section>

<!--connect.facebook-->
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v10.0&appId=182467580352571&autoLogAppEvents=1" nonce="VEdeRuel"></script>

<?php get_footer(); ?>