</main>

<?php
    //field
    $customer_address           = get_field('customer_address', 'option');
    $customer_phone             = get_field('customer_phone', 'option');
    $customer_email             = get_field('customer_email', 'option');
    $customer_address_children  = get_field('customer_address_children', 'option');

    $f_service = get_field('f_service', 'option');

    $f_form_title         = get_field('f_form_title', 'option');
    $f_form_id            = get_field('f_form', 'option');
    $f_form               = do_shortcode('[contact-form-7 id="'.$f_form_id.'"]');

    $f_socical_facebook   = get_field('f_socical_facebook', 'option');
    $f_socical_insta      = get_field('f_socical_insta', 'option');
    $f_socical_twitter    = get_field('f_socical_twitter', 'option');
    $f_socical_youtube    = get_field('f_socical_youtube', 'option');

    $f_bottom_copyright   = get_field('f_bottom_copyright', 'option');
?>

<footer class="b3 ft">

    <?php if(!empty( $f_service )) { ?>
	<section class="intro">
	    <div class="container">
	        <div class="row justify-content-center intro-row">

                <?php
                    foreach ($f_service as $foreach_kq) {

                    $post_image = $foreach_kq["image"];
                    $post_title = $foreach_kq["title"];
                    $post_desc  = $foreach_kq["desc"];
                ?>
                    <div class="col-lg-4 col-md-6 col-sm-6 d-flex align-items-center justify-content-center">
                        <div class="d-flex align-items-center intro-item">
                            <img src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>">
                            <div class="intro-item-info">
                                <h2 class="s14 bold text-uppercase "><?php echo $post_title; ?></h2>
                                <h3 class=""><?php echo $post_desc; ?></h3>
                            </div>
                        </div>
                    </div>
                <?php } ?>

	        </div>
	    </div>
	</section>
    <?php } ?>

	<section class="b1 py-5 regis">
	    <div class="container">
	        <div class="row">
	            <div class="col-sm-6"><h2 class="t3 bold s30 regis-tit"><?php echo $f_form_title; ?></h2></div>
	            <div class="col-sm-6">

                    <?php if(!empty( $f_form )) { ?>
                    <div class='d-flex align-items-center regis-frm'>
                        <?php echo $f_form; ?>
                    </div>
                    <?php } ?>

	            </div>
	        </div>
	    </div>
	</section>

    <div class="ft-1 t4">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <h2 class="bold s16 t5 pb-3 ft-tit"><?php echo wp_get_nav_menu_name("intro" ); ?></h2>
                    <?php
                        if(function_exists('wp_nav_menu')){
                            $args = array(
                                'theme_location'    =>  'intro',
                                'container'         =>  'nav',
                                'container_class'   =>  'nav-intro',
                                'container_id'      =>  'nav-intro',
                                'menu_class'        =>  'list-unstyled ft-list',
                            );
                            wp_nav_menu( $args );
                        }
                    ?>
                </div>
                <div class="col-lg-3 col-md-6">
                    <h2 class="bold s16 t5 pb-3 ft-tit"><?php echo wp_get_nav_menu_name("policy" ); ?></h2>
                    <?php
                        if(function_exists('wp_nav_menu')){
                            $args = array(
                                'theme_location'    =>  'policy',
                                'container'         =>  'nav',
                                'container_class'   =>  'nav-policy',
                                'container_id'      =>  'nav-policy',
                                'menu_class'        =>  'list-unstyled ft-list',
                            );
                            wp_nav_menu( $args );
                        }
                    ?>
                </div>
                <div class="col-lg-3 col-md-6">
                    <h2 class="bold s16 t5 pb-3 ft-tit">Hotline liên hệ</h2>
                    <a class="d-flex align-items-center s18 bold ft-phone" href="tel:<?php echo str_replace(' ','',$customer_phone);?>" title="">
                        <img src="<?php echo asset('images/phone2.png'); ?>" title="" alt="">
                        <?php echo $customer_phone; ?>
                    </a>

                    <h2 class="bold s16 t5 pb-3 ft-tit">Kết nối với chúng tôi</h2>
                    <ul class="list-unstyled text-sm-left text-center social">
                        <li>
                            <a href="<?php echo $f_socical_facebook; ?>" title="" target="_blank">
                                <i class="fab fa-facebook-f"></i>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo $f_socical_insta; ?>" title="" target="_blank">
                                <i class="fab fa-instagram"></i>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo $f_socical_twitter; ?>" title="" target="_blank">
                                <i class="fab fa-twitter"></i>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo $f_socical_youtube; ?>" title="" target="_blank">
                                <i class="fab fa-youtube"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-6">
                    <h2 class="bold s16 t5 pb-3 ft-tit">Địa chỉ</h2>
                    <ul class="list-unstyled s15 ft-add">
                        
                        <?php if(!empty( $customer_address_children )) { ?>
                        <?php
                            foreach ($customer_address_children as $foreach_kq) {

                            $post_title = $foreach_kq["title"];
                            $post_desc = wpautop($foreach_kq["desc"]);
                        ?>
                            <li><?php echo $post_title; ?></li>
                            <li><?php echo $post_desc; ?></li>
                        <?php } ?>
                        <?php } ?>

                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="b5 t3 ft-last">
        <div class="container text-center">
            <div class="py-2"><?php echo $f_bottom_copyright; ?></div>
        </div>
    </div>
</footer>

</div>

<!-- quickview -->
<div class="modal qv-modal" id="qv" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

        </div>
    </div>
</div>
<div class="ajaxLoad"></div>

<?php get_template_part("resources/views/socical-footer"); ?>

<?php wp_footer(); ?>
</body>
</html>