<?php get_header(); ?>

<?php
	$s = $_GET['s'];
    $cate = $_GET['cate'];

	$page_name = __('Tìm kiếm', 'text_domain');
    $cat_id = (!empty($cate)) ? $cate : '';
?>

<div class="bg-white">
    <?php get_template_part("resources/views/page-banner"); ?>
</div>

<section class="pro b3">
    <div class="container">
        <div class="pro-wrap">
            <div class="row">

                <div class="col-lg-3 col-md-4">
                    <aside class="aside paside">
                        <h2 class="s16 text-white b2 text-uppercase s16 text-center py-3 aside-tit">Danh mục</h2>
                        <div class="vk-sidebar">
                            <div class="vk-sidebar__box">
                                <ul class="vk-sidebar__list vk-sidebar__list--style-1" data-sidebar="list">

                                <?php
                                    if(!empty( $cat_id )) {
                                        $term_id = $cat_id;

                                        $term_childs = get_term_children( $term_id, 'product_cat' );
                                        $count = count($term_childs);
                                        if($count > 0) {

                                            foreach ( $term_childs as $foreach_kq ) {
                                                $term = get_term_by( 'id', $foreach_kq, 'product_cat' );
                                                $term_id    = $term->term_id;
                                                $term_name  = $term->name;
                                                $term_link  = esc_url(get_term_link($term_id));
                                            ?>
                                                <li><a href="<?php echo $term_link; ?>"><?php echo $term_name; ?></a></li>
                                            <?php
                                            }
                                        }
                                    } else {
                                        $terms_info = get_terms( 'product_cat', array(
                                            'parent'       => 0,
                                            'hide_empty'   => true
                                        ) );

                                        foreach ( $terms_info as $foreach_kq ) {
                                            $term_id    = $foreach_kq->term_id;
                                            $term_name  = $foreach_kq->name;
                                            $term_link  = esc_url(get_term_link($term_id));
                                        ?>
                                            <li><a href="<?php echo $term_link; ?>"><?php echo $term_name; ?></a></li>
                                        <?php
                                        }
                                    }
                                ?>

                                </ul>
                            </div>
                        </div>
                    </aside>
                </div>

                <div class="col-lg-9 col-md-8">
                    <div class="pwrap">
                        <div class="row woocommerce">

                            <?php
                                if( !empty($cat_id) ) {
                                    $query = query_search_post_by_taxonomy_paged($s, $cat_id, 'product', 'product_cat', 3);
                                } else {
                                    $query = query_search_post_paged($s, array('product'), 3);
                                }
                                $max_num_pages = $query->max_num_pages;
                                $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

                                if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();
                            ?>

                                <?php get_template_part('resources/views/content/category-product', get_post_format()); ?>

                            <?php endwhile; wp_reset_postdata(); else: echo ''; endif; ?>

                        </div>

                        <div class="bg-white d-flex align-items-center justify-content-between s14 t4 flex-wrap pagi-wrap">
                            <span class="">Trang <?php echo $paged; ?>/<?php echo $max_num_pages; ?></span>
                            <?php echo paginationCustom( $max_num_pages ); ?>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>