$(document).ready(function($) {

    // Ajax Readmore_post Product
    jQuery(".hover-product").click(function(e) {
        var data_productid = jQuery(this).data('productid');
        console.log(data_productid);

        jQuery.ajax({
            type: "POST",
            url: ajax_url,
            data: {
                action: 'Readmore_post',
                data_productid: data_productid
            },
            beforeSend:function(){
                $('.ajaxLoad').show();
            },
            success: function(response) {
                var data = jQuery.parseJSON(response);
                jQuery(".modal-content").html(data.result);
                $('.ajaxLoad').hide();

                $('.modal.qv-modal').addClass('d-block');
                jQuery(".modal-content button.close").click(function(e) {
                    $('.modal.qv-modal').removeClass('d-block');
                });
                // data_slider();
            }
        });
    });

    // Ajax Delete_product_cart
    jQuery(".cart-top-item .trash").click(function(e) {
        var data_productid = jQuery(this).data('productid');
        console.log(data_productid);

        jQuery.ajax({
            type: "POST",
            url: ajax_url,
            data: {
                action: 'Delete_product_cart',
                data_productid: data_productid
            },
            success: function(response) {
                data = jQuery.parseJSON(response);

                if (data.result == 2) {
                    location.reload();
                }
            }
        });
    });

    if ($('header.top').length) {
        $(window).scroll(function() {
            /*var anchor = $('header.top').offset().top;*/
            var anchor = $('header.top').offset().top;
            /*console.log(anchor);*/
            /*if(anchor >= 130){*/
            /*$('header.top').addClass('cmenu');
            $('.cate-list').removeClass('on');*/
            // $('.tcate-list').slideUp();
            /*}
            else{
                $('header.top').removeClass('cmenu');
            }*/
        });
    }

    // new WOW().init();

    // if($('.to-top').length){
    //   $('.to-top').on('click',function(event){
    //       event.preventDefault();
    //   $('body, html').stop().animate({scrollTop:0},800)});
    //   $(window).scroll(function(){
    //       var anchor=$('.to-top').offset().top;
    //       if(anchor>1000){
    //           $('.to-top').css('opacity','1')
    //       }
    //       else{
    //           $('.to-top').css('opacity','0')
    //       }
    //   });
    // }

    $("#menu").mmenu({
        "extensions": [
            "pagedim-black",
            "shadow-panels"
        ]
        // options
        /*"offCanvas": {
                "position": "right"
            }*/
    }, {
        // configuration
        clone: true
    });

    //Tooltip
    // $('[data-toggle="tooltip"]').tooltip();

    /* nivoSlider */
    // $("#slider").nivoSlider({
    //     effect: 'random', // Specify sets like: 'fold,fade,sliceDown' 
    //     slices: 15, // For slice animations 
    //     boxCols: 8, // For box animations 
    //     boxRows: 4, // For box animations 
    //     animSpeed: 1300, // Slide transition speed 
    //     pauseTime: 5000, // How long each slide will show 
    //     startSlide: 0, // Set starting Slide (0 index) 
    //     directionNav: true, // Next & Prev navigation 
    //     prevText: '<i class="far fa-caret-square-left" style="font-size: 48px;"></i>', // Prev directionNav text 
    //     nextText: '<i class="far fa-caret-square-right" style="font-size: 48px;"></i>',
    //     controlNav: false, // 1,2,3... navigation 
    //     controlNavThumbs: false, // Use thumbnails for Control Nav 
    //     pauseOnHover: true, // Stop animation while hovering 
    //     manualAdvance: false, // Force manual transitions 
    //     randomStart: true, // Start on a random slide 
    //     beforeChange: function() {}, // Triggers before a slide transition 
    //     afterChange: function() {}, // Triggers after a slide transition 
    //     slideshowEnd: function() {}, // Triggers after all slides have been shown 
    //     lastSlide: function() {}, // Triggers when last slide is shown 
    //     afterLoad: function() {} // Triggers when slider has loaded 
    // });

    $('.slider-area #slider').slick({
        dots: false,
        arrows: false,
        autoplay: true,
        infinite: true,
        // speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
    });

    $('.sale-slider').slick({
        dots: true,
        arrows: false,
        autoplay: true,
        infinite: true,
        speed: 300,
        slidesToShow: 3,
        slidesToScroll: 1,
        rows: 2,
        responsive: [{
                breakpoint: 1200,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 576,
                settings: {
                    slidesToShow: 1
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });

    var slider2 = $('.hpro-slider').slick({
        dots: true,
        arrows: false,
        infinite: false,
        autoplay: true,
        autoplaySpeed: 1000,
        pauseOnFocus: false,
        pauseOnHover: false,
        pauseOnDotsHover: false,
        slidesToShow: 5,
        slidesToScroll: 1,
        fade: false,
        responsive: [{
                breakpoint: 1200,
                settings: {
                    slidesToShow: 4
                }
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 576,
                settings: {
                    slidesToShow: 1
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });

    slider2.on('afterChange', function(event, slick, currentSlide) {
        // slick - is a slider object with a lot of useful info
        // currentSlide - is a current slide index (starts from 0)
        /*console.log(slick.slideCount, currentSlide)*/
        if (slick.slideCount === currentSlide + 5) {
            /*alert('Instead of showing alert goto slide 1.');*/
            slideIndex = $(this).attr('data-slick-index');
            /*console.log(slideIndex);*/
            $('.hpro-slider').slick('slickGoTo', (parseInt(slideIndex)));
            /*$('.hpro-slider').slickGoTo(0);*/
        }
    });
    var blogslider = $('.blog-reslider').slick({
        dots: true,
        arrows: false,
        infinite: false,
        autoplay: true,
        autoplaySpeed: 1000,
        pauseOnFocus: true,
        pauseOnHover: true,
        pauseOnDotsHover: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        fade: false,
        responsive: [{
                breakpoint: 1200,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 576,
                settings: {
                    slidesToShow: 1
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });
    blogslider.on('afterChange', function(event, slick, currentSlide) {
        // slick - is a slider object with a lot of useful info
        // currentSlide - is a current slide index (starts from 0)
        /*console.log(slick.slideCount, currentSlide)*/
        if (slick.slideCount === currentSlide + 3) {
            /*alert('Instead of showing alert goto slide 1.');*/
            slideIndex = $(this).attr('data-slick-index');
            /*console.log(slideIndex);*/
            $('.blog-reslider').slick('slickGoTo', (parseInt(slideIndex)));
            /*$('.hpro-slider').slickGoTo(0);*/
        }
    });

    $('.search-open').on('click', function(event) {
        event.preventDefault();
        $('.search-frm').toggleClass('on');
    });

    // $(".button").on("click", function() {
    //   var $button = $(this);
    //   var oldValue = $button.parent().find("input").val();
    //   if ($button.text() == "+") {
    //     var newVal = parseFloat(oldValue) + 1;
    //   } else {
    //     // Don't allow decrementing below zero
    //     if (oldValue > 0) {
    //       var newVal = parseFloat(oldValue) - 1;
    //     } else {
    //       newVal = 0;
    //     }
    //   }
    //   $button.parent().find("input").val(newVal);
    // });
    // $('.nav-link .custom-control.custom-radio').on('click', function(e) {
    //     e.preventDefault();
    //     var a = $(this).children('.custom-control-input').prop('value');
    //     $(this).children('.custom-control-input').prop('checked', true);
    // });

    /*if($("[data-fancybox]").length){
      $("[data-fancybox]").fancybox({});
      if($('.linkyoutube').length) {
        var url = $('.linkyoutube').attr('href').replace('watch?v=', 'embed/');
        $('.linkyoutube').attr('href', url);
      }
      
    }*/

    /*slider range*/
    if ($("#range").length) {
        $("#range").ionRangeSlider({
            hide_min_max: true,
            keyboard: true,
            min: 1000,
            max: 10000000,
            from: 400000,
            to: 7000000,
            type: 'double',
            step: 1000,
            prefix: "",
            postfix: " vnđ",
            grid: true
        });
    }


    $('.paside-chkbox li').on('click', function(event) {
        event.preventDefault();
        $(this).toggleClass('active').children('.custom-control-input').prop('checked', !($(this).children('.custom-control-input').is(':checked')));
    });
    $('.paside-list li.active').children('.custom-control-input').prop('checked', 'true');

    $('.paside-radio li').on('click', function(event) {
        event.preventDefault();
        $('.paside-radio li').removeClass('active');
        $(this).toggleClass('active').children('.custom-control-input').prop('checked', true);
    });
    /*$('.paside-list li.active').children('.custom-control-input').prop('checked', 'true');*/

    $('.top-cate h2').on('click', function(event) {
        event.preventDefault();
        $(this).next('.top-cate-list').slideToggle();
    });
    $('.search-open').on('click', function(event) {
        event.preventDefault();
        $('.search-dropdown').toggleClass('on');
    });
    $('.pdetail-seemore').on('click', function(event) {
        event.preventDefault();
        console.log('click');
        $(this).parent('.pdetail-wrap').toggleClass('open');
    });
    // filter
    // $('[data-sidebar="list"]').on('click','li',function(e){
    //     e.preventDefault();
    //     var el = $(this);
    //     var data = el.data('value');

    //     el.siblings().removeClass('active');
    //     el.toggleClass('active');

    //     if(el.hasClass('active')){
    //         console.log(data);
    //     }

    //     return false;
    // });

    // /* 1. Visualizing things on Hover - See next part for action on click */
    // $('#stars li').on('mouseover', function() {
    //     var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on

    //     // Now highlight all the stars that's not after the current hovered star
    //     $(this).parent().children('li.star').each(function(e) {
    //         if (e < onStar) {
    //             $(this).addClass('hover');
    //         } else {
    //             $(this).removeClass('hover');
    //         }
    //     });

    // }).on('mouseout', function() {
    //     $(this).parent().children('li.star').each(function(e) {
    //         $(this).removeClass('hover');
    //     });
    // });


    // /* 2. Action to perform on click */
    // $('#stars li').on('click', function() {
    //     var onStar = parseInt($(this).data('value'), 10); // The star currently selected
    //     var stars = $(this).parent().children('li.star');

    //     for (i = 0; i < stars.length; i++) {
    //         $(stars[i]).removeClass('selected');
    //     }

    //     for (i = 0; i < onStar; i++) {
    //         $(stars[i]).addClass('selected');
    //     }
    // });
    // $('.star').each(function(index, el) {
    //     var rate = $(this).data('rate');
    //     console.log(rate);
    //     $(this).children('li').each(function(index) {
    //         if (index < rate) {
    //             $(this).addClass('rated')
    //         }
    //         // console.log( index + ": " + $( this ).html() );
    //     });
    // });

    // $('.acc-change').on('click', function(event) {
    //     event.preventDefault();
    //     $(this).prev('input').removeAttr('readonly').focus();
    // });

    /*$('input[type="file"]').change(function(e){
        var fileName = e.target.files[0].name;
        alert('The file "' + fileName +  '" has been selected.');
        $('.acc-img-wrap img').attr('src', fileName);
    });*/
    // $('.tcate-tit').on('click', function(event) {
    //   event.preventDefault();
    //   $(this).next().slideToggle();
    // });
    $('.tcate').click(function() {
        $('.tcate-list').slideToggle();
    });
});
/*
http://jsfiddle.net/LCB5W/
https://stackoverflow.com/questions/152975/how-do-i-detect-a-click-outside-an-element

https://codepen.io/altro-nvp2/pen/MmQBVd
http://www.landmarkmlp.com/js-plugin/owl.carousel/demos/transitions.html
https://codepen.io/radimby/pen/YpEJQP
*/