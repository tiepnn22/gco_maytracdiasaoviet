<?php
class show_post_category extends WP_Widget {
    function __construct() {
        parent::__construct(
            'show_post_category',
            'Core - Hiển thị bài viết category',
            array( 'description'  =>  'Hiển thị bài viết category' )
        );
    }
    function form( $instance ) {
        $default = array(
            'title' => 'Hiển thị bài viết category',
            'id_category' => 1,
            'number_post' => 4
        );
        $instance = wp_parse_args( (array) $instance, $default );
        $title = esc_attr($instance['title']);
        $id_category = esc_attr($instance['id_category']);
        $number_post = esc_attr($instance['number_post']);

        echo '<p>';
            echo 'Tiêu đề :';
            echo '<input type="text" class="widefat" name="'.$this->get_field_name('title').'" value="'.$title.'"/>';
        echo '</p>';

        echo '<p>';
            echo 'Chọn category :';
            echo '<select class="widefat" name="'.$this->get_field_name('id_category').'">';
                //
                $categories = get_categories( array(
                    'orderby' => 'name',
                    'parent'  => 0
                ) );
                foreach ( $categories as $category ) {
                    // if($category->term_id == 1) {} else {

                        if($id_category == $category->term_id) {
                            echo '<option name="'.$this->get_field_name('$category->term_id').'" selected value="'.$category->term_id.'">'.$category->name.'</option>';
                        }else{
                            echo '<option name="'.$this->get_field_name('$category->term_id').'" value="'.$category->term_id.'">'.$category->name.'</option>';
                        }
                    // }
                    $team_lg = $category->term_id;
                    //
                    $child_cats = get_term_children( $category->term_id, 'category' );
                    $count = count($child_cats);
                    if($count > 0) {
                        foreach ($child_cats as $childs) {
                            $child_cat = get_term_by( 'id', $childs, 'category' );

                            if($child_cat->parent == $team_lg){

                                if($id_category == $child_cat->term_id) {
                                    echo '<option name="'.$this->get_field_name('$child_cat->term_id').'" selected value="'.$child_cat->term_id.'">&nbsp;&nbsp;&nbsp;'.$child_cat->name.'</option>';
                                }else{
                                    echo '<option name="'.$this->get_field_name('$child_cat->term_id').'" value="'.$child_cat->term_id.'">&nbsp;&nbsp;&nbsp;'.$child_cat->name.'</option>';
                                }
                                //
                                $child_cats = get_term_children( $child_cat->term_id, 'category' );
                                $count = count($child_cats);
                                if($count > 0) {
                                    foreach ( $child_cats as $childs ) {
                                        $child_cat = get_term_by( 'id', $childs, 'category' );

                                        if($id_category == $child_cat->term_id) {
                                            echo '<option name="'.$this->get_field_name('$child_cat->term_id').'" selected value="'.$child_cat->term_id.'">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$child_cat->name.'</option>';
                                        }else{
                                            echo '<option name="'.$this->get_field_name('$child_cat->term_id').'" value="'.$child_cat->term_id.'">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$child_cat->name.'</option>';
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            echo '</select>';
        echo '</p>';

        echo '<p>';
            echo 'Số lượng bài viết hiển thị :';
            echo '<input type="number" class="widefat" name="'.$this->get_field_name('number_post').'" value="'.$number_post.'" />';
        echo '</p>';
    }
    function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['id_category'] = strip_tags($new_instance['id_category']);
        $instance['number_post'] = strip_tags($new_instance['number_post']);
        return $instance;
    }
    function widget( $args, $instance ) {
        extract($args);
        $title = apply_filters( 'widget_title', $instance['title'] );
        $id_category = $instance['id_category'];
        $number_post = $instance['number_post'];

        echo $before_widget; ?>
        <div class="bg-white apdetail-sale">
            <h2 class="s16 text-white b2 text-uppercase s16 text-center py-3 aside-tit"><?php echo $title; ?></h2>

            <?php
                $query = query_post_by_category($id_category, $number_post);
                if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();
            ?>

                <?php get_template_part('resources/views/content/widget-show-post', get_post_format()); ?>

            <?php endwhile; wp_reset_postdata(); else: echo ''; endif; ?>

        </div>
        <?php echo $after_widget;
    }
}
function create_showpostcategory_widget() {
    register_widget('show_post_category');
}
add_action( 'widgets_init', 'create_showpostcategory_widget' );
?>