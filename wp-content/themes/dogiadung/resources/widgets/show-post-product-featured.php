<?php
class show_post_product_featured extends WP_Widget {
    function __construct() {
        parent::__construct(
            'show_post_product_featured',
            'Core - Hiển thị bài viết sản phẩm nổi bật',
            array( 'description'  =>  'Hiển thị bài viết sản phẩm nổi bật' )
        );
    }
    function form( $instance ) {
        $default = array(
            'title' => 'Hiển thị bài viết sản phẩm nổi bật',
            'number_post' => 4
        );
        $instance = wp_parse_args( (array) $instance, $default );
        $title = esc_attr($instance['title']);
        $number_post = esc_attr($instance['number_post']);

        echo '<p>';
            echo 'Tiêu đề :';
            echo '<input type="text" class="widefat" name="'.$this->get_field_name('title').'" value="'.$title.'"/>';
        echo '</p>';

        echo '<p>';
            echo 'Số lượng bài viết hiển thị :';
            echo '<input type="number" class="widefat" name="'.$this->get_field_name('number_post').'" value="'.$number_post.'" />';
        echo '</p>';
    }
    function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['number_post'] = strip_tags($new_instance['number_post']);
        return $instance;
    }
    function widget( $args, $instance ) {
        extract($args);
        $title = apply_filters( 'widget_title', $instance['title'] );
        $number_post = $instance['number_post'];
        $tax_query[] = array(
            'taxonomy' => 'product_visibility',
            'field'    => 'name',
            'terms'    => 'featured',
            'operator' => 'IN',
        );
        $query =  new WP_Query( array(
            'post_type'             => 'product',
            'post_status'           => 'publish',
            'ignore_sticky_posts'   => 1,
            'showposts'             => $number_post,
            'orderby'               => 'date',
            'order'                 => 'DESC',
            'tax_query'             => $tax_query
        ) );
        
        echo $before_widget; ?>
        <div class="bg-white my-5 apdetail-sale">
            <h2 class="s16 text-white b2 text-uppercase s16 text-center py-3 aside-tit"><?php echo $title; ?></h2>

            <?php
                if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();
            ?>

                <?php get_template_part('resources/views/content/widget-show-product-featured', get_post_format()); ?>

            <?php endwhile; wp_reset_postdata(); else: echo ''; endif; ?>
            
        </div>
        <?php echo $after_widget;
    }
}
function create_showpostproductfeatured_widget() {
    register_widget('show_post_product_featured');
}
add_action( 'widgets_init', 'create_showpostproductfeatured_widget' );
?>