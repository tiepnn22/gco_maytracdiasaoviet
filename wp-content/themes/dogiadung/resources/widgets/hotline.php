<?php
class hotline extends WP_Widget {
    function __construct() {
        parent::__construct(
            'hotline',
            'Core - Hiển thị hotline',
            array( 'description'  =>  'Hiển thị hotline' )
        );
    }
    function form( $instance ) {
        $default = array(
            'title' => 'Hiển thị hotline',
            'hotline' => '0911077422',
            'policy_title' => 'Chính sách',
            'policy_value' => 'Nội dung chính sách',
        );
        $instance = wp_parse_args( (array) $instance, $default );
        $title = esc_attr($instance['title']);
        $hotline = esc_attr($instance['hotline']);
        $policy_title = esc_attr($instance['policy_title']);
        $policy_value = esc_attr($instance['policy_value']);

        echo '<p>';
            echo 'Tiêu đề :';
            echo '<input type="text" class="widefat" name="'.$this->get_field_name('title').'" value="'.$title.'"/>';
        echo '</p>';

        echo '<p>';
            echo 'Hotline :';
            echo '<input type="text" class="widefat" name="'.$this->get_field_name('hotline').'" value="'.$hotline.'"/>';
        echo '</p>';

        echo '<p>';
            echo 'Chính sách :';
            echo '<input type="text" class="widefat" name="'.$this->get_field_name('policy_title').'" value="'.$policy_title.'"/>';
        echo '</p>';

        echo '<p>';
            echo 'Nội dung chính sách :';
            echo '<input type="text" class="widefat" name="'.$this->get_field_name('policy_value').'" value="'.$policy_value.'"/>';
        echo '</p>';
    }
    function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['hotline'] = strip_tags($new_instance['hotline']);
        $instance['policy_title'] = strip_tags($new_instance['policy_title']);
        $instance['policy_value'] = strip_tags($new_instance['policy_value']);
        return $instance;
    }
    function widget( $args, $instance ) {
        extract($args);
        $title = apply_filters( 'widget_title', $instance['title'] );
        $hotline = $instance['hotline'];
        $policy_title = $instance['policy_title'];
        $policy_value = $instance['policy_value'];

        echo $before_widget; ?>
        <div class="bg-white py-4 px-2 d-flex align-items-center apdetail-item">
            <img src="<?php echo asset('images/phone2.png'); ?>" alt="">
            <div class="bold s16 pl-3 apdetail-info">
                <h2 class="t5 bold">Hotline hỗ trợ</h2>
                <a class="d-flex align-items-center t1 ft-phone" href="tel:<?php echo $title; ?>" title="">
                    <?php echo $hotline; ?>
                </a>
            </div>
        </div>
        <div class="bg-white py-4 px-2 d-flex align-items-center apdetail-item">
            <img src="<?php echo asset('images/return.png'); ?>" alt="">
            <div class="bold pl-3 s16 apdetail-info">
                <h2 class="t5"><?php echo $policy_title; ?></h2>
                <p><?php echo $policy_value; ?></p>
            </div>
        </div>
        <?php echo $after_widget;
    }
}
function create_hotline_widget() {
    register_widget('hotline');
}
add_action( 'widgets_init', 'create_hotline_widget' );
?>