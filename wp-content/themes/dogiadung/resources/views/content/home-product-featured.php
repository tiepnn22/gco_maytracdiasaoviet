<?php
	$post_id            = get_the_ID();
	$post_title 		= get_the_title($post_id);
	// $post_content 		= wpautop(get_the_content($post_id));
	$post_date 			= get_the_date('d/m/Y',$post_id);
	$post_link 			= get_permalink($post_id);
	$post_image 		= getPostImage($post_id,"p-product");
	$post_excerpt 		= cut_string(get_the_excerpt($post_id),200,'...');
	$post_author 		= get_the_author_meta( 'nicename', get_the_author_meta( get_the_author() ) );
	$post_tag 			= get_the_tags($post_id);
?>

<article class="bg-white sale-item woocommerce" data-productid="<?php echo $post_id; ?>">
    <div class="row">
        <div class="col-5">
	        <div class="text-center sale-img">
	        	<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
	        		<img src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>">
	        	</a>
	        </div>
        </div>
        <div class="col-7">
            <div class="bg-white sale-item-info">
	            <h3 class="sale-name">
	            	<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
	            		<?php echo $post_title; ?>
	            	</a>
	            </h3>
            	
            	<?php echo show_rating($post_id); ?>

        		<?php echo show_price_old_price($post_id); ?>

                <ul class="list-unstyled ap-action">
                    <li>
                    	<?php echo show_add_to_cart_button($post_id); ?>
                    </li>
                    <li>
                    	<a href="javascript:void(0)" class="vk-shop-item__btn hover-product" data-productid="<?php echo $post_id; ?>" title="Xem thêm">
                    		<i class="fas fa-search"></i>
                    	</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</article>