<?php
	$post_id            = get_the_ID();
	$post_title 		= get_the_title($post_id);
	// $post_content 		= wpautop(get_the_content($post_id));
	$post_date 			= get_the_date('d m Y',$post_id);
	$post_link 			= get_permalink($post_id);
	$post_image 		= getPostImage($post_id,"p-post");
	$post_excerpt 		= cut_string(get_the_excerpt($post_id),220,'...');
	$post_author 		= get_the_author_meta( 'nicename', get_the_author_meta( get_the_author() ) );
	$post_tag 			= get_the_tags($post_id);
?>

<article class="sale-item">
    <div class="row">
        <div class="col-4 d-flex align-items-center justify-content-center">
            <div class="text-center sale-img">
            	<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
            		<img src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>">
            	</a>
            </div>
        </div>
        <div class="col-8">
            <div class="bg-white">
                <h3 class="sale-name">
                	<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
                		<?php echo $post_title; ?>
            		</a>
            	</h3>
            </div>
        </div>
    </div>
</article>