<?php
    $terms = get_terms('product_cat', array(
        'parent'=> 0,
        'hide_empty' => false
    ) );
?>

<div class="search-dropdown">
    <form action="<?php echo esc_url( home_url( '/' ) ); ?>" class="trans s14 d-flex align-items-center search-frm">
        <select name="cate" id="">
            <option value="">Tất cả</option>

			<?php
			    foreach ($terms as $foreach_kq) {
				    $post_id = $foreach_kq->term_id;
				    $post_title = $foreach_kq->name;
		    ?>
		    	<option <?php if($_GET['cate'] == $post_id) { echo 'selected'; }?> value="<?php echo $post_id; ?>">
		    		<?php echo $post_title; ?>
	    		</option>
		    <?php
				}
			?>

        </select>
        <input type="text" class="form-control light s14 search-ip" required="required" placeholder="<?php _e('Nhập từ khoá...', 'text_domain'); ?>" name="s" value="<?php echo get_search_query(); ?>">
        <button type="submit" class="btn search-btn">Tìm kiếm</button>
    </form>
</div>