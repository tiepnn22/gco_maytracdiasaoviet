<?php
    if(function_exists('wp_nav_menu')){
        $args = array(
            'theme_location' 	=> 	'category_product',
            'container'         =>  'nav',         // bao ngoài
            'container_class'   =>  'nav-menu-category-product',
            'container_id'      =>  'nav-menu-category-product',
            'menu_class'		=>	'list-unstyled tcate-list',        // class ul
            // 'items_wrap'        =>  '<ul id="menu" class="menu">%3$s</ul>'     // thay đổi html ul
        );
        wp_nav_menu( $args );
    }
?>