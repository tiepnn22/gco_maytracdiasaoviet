-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Dec 30, 2021 at 03:18 AM
-- Server version: 5.7.30
-- PHP Version: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gco_dogiadung`
--

-- --------------------------------------------------------

--
-- Table structure for table `wp_actionscheduler_actions`
--

CREATE TABLE `wp_actionscheduler_actions` (
  `action_id` bigint(20) UNSIGNED NOT NULL,
  `hook` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `scheduled_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `scheduled_date_local` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `args` varchar(191) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `schedule` longtext COLLATE utf8mb4_unicode_520_ci,
  `group_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `attempts` int(11) NOT NULL DEFAULT '0',
  `last_attempt_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_attempt_local` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `claim_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `extended_args` varchar(8000) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_actionscheduler_actions`
--

INSERT INTO `wp_actionscheduler_actions` (`action_id`, `hook`, `status`, `scheduled_date_gmt`, `scheduled_date_local`, `args`, `schedule`, `group_id`, `attempts`, `last_attempt_gmt`, `last_attempt_local`, `claim_id`, `extended_args`) VALUES
(1, 'wc-admin_import_customers', 'complete', '2021-12-30 03:16:09', '2021-12-30 03:16:09', '[1]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1640834169;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1640834169;}', 2, 1, '2021-12-30 03:17:01', '2021-12-30 10:17:01', 0, NULL),
(2, 'woocommerce_admin/stored_state_setup_for_products/async/run_remote_notifications', 'complete', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[]', 'O:28:\"ActionScheduler_NullSchedule\":0:{}', 0, 1, '2021-12-30 03:17:42', '2021-12-30 10:17:42', 0, NULL),
(3, 'woocommerce_admin/stored_state_setup_for_products/async/run_remote_notifications', 'complete', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[]', 'O:28:\"ActionScheduler_NullSchedule\":0:{}', 0, 1, '2021-12-30 03:17:42', '2021-12-30 10:17:42', 0, NULL),
(4, 'woocommerce_admin/stored_state_setup_for_products/async/run_remote_notifications', 'complete', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[]', 'O:28:\"ActionScheduler_NullSchedule\":0:{}', 0, 1, '2021-12-30 03:17:42', '2021-12-30 10:17:42', 0, NULL),
(5, 'woocommerce_admin/stored_state_setup_for_products/async/run_remote_notifications', 'complete', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[]', 'O:28:\"ActionScheduler_NullSchedule\":0:{}', 0, 1, '2021-12-30 03:17:42', '2021-12-30 10:17:42', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wp_actionscheduler_claims`
--

CREATE TABLE `wp_actionscheduler_claims` (
  `claim_id` bigint(20) UNSIGNED NOT NULL,
  `date_created_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_actionscheduler_groups`
--

CREATE TABLE `wp_actionscheduler_groups` (
  `group_id` bigint(20) UNSIGNED NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_actionscheduler_groups`
--

INSERT INTO `wp_actionscheduler_groups` (`group_id`, `slug`) VALUES
(1, 'action-scheduler-migration'),
(2, 'wc-admin-data');

-- --------------------------------------------------------

--
-- Table structure for table `wp_actionscheduler_logs`
--

CREATE TABLE `wp_actionscheduler_logs` (
  `log_id` bigint(20) UNSIGNED NOT NULL,
  `action_id` bigint(20) UNSIGNED NOT NULL,
  `message` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `log_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `log_date_local` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_actionscheduler_logs`
--

INSERT INTO `wp_actionscheduler_logs` (`log_id`, `action_id`, `message`, `log_date_gmt`, `log_date_local`) VALUES
(1, 1, 'hành động được tạo', '2021-12-30 03:16:04', '2021-12-30 03:16:04'),
(2, 1, 'action started via WP Cron', '2021-12-30 03:17:01', '2021-12-30 03:17:01'),
(3, 1, 'action complete via WP Cron', '2021-12-30 03:17:01', '2021-12-30 03:17:01'),
(4, 2, 'hành động được tạo', '2021-12-30 03:17:09', '2021-12-30 03:17:09'),
(5, 3, 'hành động được tạo', '2021-12-30 03:17:19', '2021-12-30 03:17:19'),
(6, 4, 'hành động được tạo', '2021-12-30 03:17:25', '2021-12-30 03:17:25'),
(7, 5, 'hành động được tạo', '2021-12-30 03:17:34', '2021-12-30 03:17:34'),
(8, 2, 'action started via WP Cron', '2021-12-30 03:17:41', '2021-12-30 03:17:41'),
(9, 2, 'action complete via WP Cron', '2021-12-30 03:17:41', '2021-12-30 03:17:41'),
(10, 3, 'action started via WP Cron', '2021-12-30 03:17:42', '2021-12-30 03:17:42'),
(11, 3, 'action complete via WP Cron', '2021-12-30 03:17:42', '2021-12-30 03:17:42'),
(12, 4, 'action started via WP Cron', '2021-12-30 03:17:42', '2021-12-30 03:17:42'),
(13, 4, 'action complete via WP Cron', '2021-12-30 03:17:42', '2021-12-30 03:17:42'),
(14, 5, 'action started via WP Cron', '2021-12-30 03:17:42', '2021-12-30 03:17:42'),
(15, 5, 'action complete via WP Cron', '2021-12-30 03:17:42', '2021-12-30 03:17:42');

-- --------------------------------------------------------

--
-- Table structure for table `wp_commentmeta`
--

CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_commentmeta`
--

INSERT INTO `wp_commentmeta` (`meta_id`, `comment_id`, `meta_key`, `meta_value`) VALUES
(1, 2, 'rating', '4'),
(2, 2, 'verified', '1');

-- --------------------------------------------------------

--
-- Table structure for table `wp_comments`
--

CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'comment',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_comments`
--

INSERT INTO `wp_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 312, 'WooCommerce', 'woocommerce@wordpress.local', '', '', '2021-08-24 17:46:40', '2021-08-24 10:46:40', 'Thực hiện thanh toán khi giao hàng Trạng thái đơn hàng đã được chuyển từ Chờ thanh toán sang Đang xử lý.', 0, '1', 'WooCommerce', 'order_note', 0, 0),
(2, 310, 'admin', 'tiepnguyen220194@gmail.com', 'http://wordpress.local/GCO/maytracdiasaoviet', '127.0.0.1', '2021-08-25 16:29:40', '2021-08-25 09:29:40', '4 sao', 0, '1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', 'review', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `wp_links`
--

CREATE TABLE `wp_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_options`
--

CREATE TABLE `wp_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_options`
--

INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://wordpress.local/GCO/maytracdiasaoviet', 'yes'),
(2, 'home', 'http://wordpress.local/GCO/maytracdiasaoviet', 'yes'),
(3, 'blogname', 'Đồ gia dụng', 'yes'),
(4, 'blogdescription', 'dogiadung', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'tiepnguyen220194@gmail.com', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '1', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '1', 'yes'),
(22, 'posts_per_page', '1', 'yes'),
(23, 'date_format', 'd/m/Y', 'yes'),
(24, 'time_format', 'H:i', 'yes'),
(25, 'links_updated_date_format', 'F j, Y g:i a', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%category%/%postname%.html', 'yes'),
(29, 'rewrite_rules', 'a:182:{s:24:\"^wc-auth/v([1]{1})/(.*)?\";s:63:\"index.php?wc-auth-version=$matches[1]&wc-auth-route=$matches[2]\";s:22:\"^wc-api/v([1-3]{1})/?$\";s:51:\"index.php?wc-api-version=$matches[1]&wc-api-route=/\";s:24:\"^wc-api/v([1-3]{1})(.*)?\";s:61:\"index.php?wc-api-version=$matches[1]&wc-api-route=$matches[2]\";s:11:\"cua-hang/?$\";s:27:\"index.php?post_type=product\";s:41:\"cua-hang/feed/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?post_type=product&feed=$matches[1]\";s:36:\"cua-hang/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?post_type=product&feed=$matches[1]\";s:28:\"cua-hang/page/([0-9]{1,})/?$\";s:45:\"index.php?post_type=product&paged=$matches[1]\";s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:17:\"^wp-sitemap\\.xml$\";s:23:\"index.php?sitemap=index\";s:17:\"^wp-sitemap\\.xsl$\";s:36:\"index.php?sitemap-stylesheet=sitemap\";s:23:\"^wp-sitemap-index\\.xsl$\";s:34:\"index.php?sitemap-stylesheet=index\";s:48:\"^wp-sitemap-([a-z]+?)-([a-z\\d_-]+?)-(\\d+?)\\.xml$\";s:75:\"index.php?sitemap=$matches[1]&sitemap-subtype=$matches[2]&paged=$matches[3]\";s:34:\"^wp-sitemap-([a-z]+?)-(\\d+?)\\.xml$\";s:47:\"index.php?sitemap=$matches[1]&paged=$matches[2]\";s:46:\"(tintuc)/(?:feed/)?(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:29:\"(tintuc)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:11:\"(tintuc)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:55:\"(tin-tuc-noi-bat)/(?:feed/)?(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:38:\"(tin-tuc-noi-bat)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:20:\"(tin-tuc-noi-bat)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:29:\"tag/([^/]+)/wc-api(/(.*))?/?$\";s:44:\"index.php?tag=$matches[1]&wc-api=$matches[3]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:54:\"(chua-phan-loai)/(?:feed/)?(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_cat=$matches[1]&feed=$matches[2]\";s:37:\"(chua-phan-loai)/page/?([0-9]{1,})/?$\";s:51:\"index.php?product_cat=$matches[1]&paged=$matches[2]\";s:19:\"(chua-phan-loai)/?$\";s:33:\"index.php?product_cat=$matches[1]\";s:59:\"(may-thuy-binh/leica)/(?:feed/)?(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_cat=$matches[1]&feed=$matches[2]\";s:42:\"(may-thuy-binh/leica)/page/?([0-9]{1,})/?$\";s:51:\"index.php?product_cat=$matches[1]&paged=$matches[2]\";s:24:\"(may-thuy-binh/leica)/?$\";s:33:\"index.php?product_cat=$matches[1]\";s:87:\"(may-toan-dac-dien-tu/leica-may-toan-dac-dien-tu)/(?:feed/)?(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_cat=$matches[1]&feed=$matches[2]\";s:70:\"(may-toan-dac-dien-tu/leica-may-toan-dac-dien-tu)/page/?([0-9]{1,})/?$\";s:51:\"index.php?product_cat=$matches[1]&paged=$matches[2]\";s:52:\"(may-toan-dac-dien-tu/leica-may-toan-dac-dien-tu)/?$\";s:33:\"index.php?product_cat=$matches[1]\";s:59:\"(may-dinh-vi-gps-rtk)/(?:feed/)?(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_cat=$matches[1]&feed=$matches[2]\";s:42:\"(may-dinh-vi-gps-rtk)/page/?([0-9]{1,})/?$\";s:51:\"index.php?product_cat=$matches[1]&paged=$matches[2]\";s:24:\"(may-dinh-vi-gps-rtk)/?$\";s:33:\"index.php?product_cat=$matches[1]\";s:53:\"(may-thuy-binh)/(?:feed/)?(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_cat=$matches[1]&feed=$matches[2]\";s:36:\"(may-thuy-binh)/page/?([0-9]{1,})/?$\";s:51:\"index.php?product_cat=$matches[1]&paged=$matches[2]\";s:18:\"(may-thuy-binh)/?$\";s:33:\"index.php?product_cat=$matches[1]\";s:60:\"(may-toan-dac-dien-tu)/(?:feed/)?(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_cat=$matches[1]&feed=$matches[2]\";s:43:\"(may-toan-dac-dien-tu)/page/?([0-9]{1,})/?$\";s:51:\"index.php?product_cat=$matches[1]&paged=$matches[2]\";s:25:\"(may-toan-dac-dien-tu)/?$\";s:33:\"index.php?product_cat=$matches[1]\";s:59:\"(may-thuy-binh/myzox)/(?:feed/)?(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_cat=$matches[1]&feed=$matches[2]\";s:42:\"(may-thuy-binh/myzox)/page/?([0-9]{1,})/?$\";s:51:\"index.php?product_cat=$matches[1]&paged=$matches[2]\";s:24:\"(may-thuy-binh/myzox)/?$\";s:33:\"index.php?product_cat=$matches[1]\";s:57:\"tu-khoa-san-pham/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_tag=$matches[1]&feed=$matches[2]\";s:52:\"tu-khoa-san-pham/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_tag=$matches[1]&feed=$matches[2]\";s:33:\"tu-khoa-san-pham/([^/]+)/embed/?$\";s:44:\"index.php?product_tag=$matches[1]&embed=true\";s:45:\"tu-khoa-san-pham/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?product_tag=$matches[1]&paged=$matches[2]\";s:27:\"tu-khoa-san-pham/([^/]+)/?$\";s:33:\"index.php?product_tag=$matches[1]\";s:36:\"san-pham/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:46:\"san-pham/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:66:\"san-pham/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:61:\"san-pham/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:61:\"san-pham/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:42:\"san-pham/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:25:\"san-pham/([^/]+)/embed/?$\";s:40:\"index.php?product=$matches[1]&embed=true\";s:29:\"san-pham/([^/]+)/trackback/?$\";s:34:\"index.php?product=$matches[1]&tb=1\";s:49:\"san-pham/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?product=$matches[1]&feed=$matches[2]\";s:44:\"san-pham/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?product=$matches[1]&feed=$matches[2]\";s:37:\"san-pham/([^/]+)/page/?([0-9]{1,})/?$\";s:47:\"index.php?product=$matches[1]&paged=$matches[2]\";s:44:\"san-pham/([^/]+)/comment-page-([0-9]{1,})/?$\";s:47:\"index.php?product=$matches[1]&cpage=$matches[2]\";s:34:\"san-pham/([^/]+)/wc-api(/(.*))?/?$\";s:48:\"index.php?product=$matches[1]&wc-api=$matches[3]\";s:40:\"san-pham/[^/]+/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:51:\"san-pham/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:33:\"san-pham/([^/]+)(?:/([0-9]+))?/?$\";s:46:\"index.php?product=$matches[1]&page=$matches[2]\";s:25:\"san-pham/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:35:\"san-pham/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:55:\"san-pham/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:50:\"san-pham/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:50:\"san-pham/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:31:\"san-pham/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:27:\"comment-page-([0-9]{1,})/?$\";s:39:\"index.php?&page_id=12&cpage=$matches[1]\";s:17:\"wc-api(/(.*))?/?$\";s:29:\"index.php?&wc-api=$matches[2]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:26:\"comments/wc-api(/(.*))?/?$\";s:29:\"index.php?&wc-api=$matches[2]\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:29:\"search/(.+)/wc-api(/(.*))?/?$\";s:42:\"index.php?s=$matches[1]&wc-api=$matches[3]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:32:\"author/([^/]+)/wc-api(/(.*))?/?$\";s:52:\"index.php?author_name=$matches[1]&wc-api=$matches[3]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:54:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/wc-api(/(.*))?/?$\";s:82:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&wc-api=$matches[5]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:41:\"([0-9]{4})/([0-9]{1,2})/wc-api(/(.*))?/?$\";s:66:\"index.php?year=$matches[1]&monthnum=$matches[2]&wc-api=$matches[4]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:28:\"([0-9]{4})/wc-api(/(.*))?/?$\";s:45:\"index.php?year=$matches[1]&wc-api=$matches[3]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:25:\"(.?.+?)/wc-api(/(.*))?/?$\";s:49:\"index.php?pagename=$matches[1]&wc-api=$matches[3]\";s:28:\"(.?.+?)/order-pay(/(.*))?/?$\";s:52:\"index.php?pagename=$matches[1]&order-pay=$matches[3]\";s:33:\"(.?.+?)/order-received(/(.*))?/?$\";s:57:\"index.php?pagename=$matches[1]&order-received=$matches[3]\";s:25:\"(.?.+?)/orders(/(.*))?/?$\";s:49:\"index.php?pagename=$matches[1]&orders=$matches[3]\";s:29:\"(.?.+?)/view-order(/(.*))?/?$\";s:53:\"index.php?pagename=$matches[1]&view-order=$matches[3]\";s:28:\"(.?.+?)/downloads(/(.*))?/?$\";s:52:\"index.php?pagename=$matches[1]&downloads=$matches[3]\";s:31:\"(.?.+?)/edit-account(/(.*))?/?$\";s:55:\"index.php?pagename=$matches[1]&edit-account=$matches[3]\";s:31:\"(.?.+?)/edit-address(/(.*))?/?$\";s:55:\"index.php?pagename=$matches[1]&edit-address=$matches[3]\";s:34:\"(.?.+?)/payment-methods(/(.*))?/?$\";s:58:\"index.php?pagename=$matches[1]&payment-methods=$matches[3]\";s:32:\"(.?.+?)/lost-password(/(.*))?/?$\";s:56:\"index.php?pagename=$matches[1]&lost-password=$matches[3]\";s:34:\"(.?.+?)/customer-logout(/(.*))?/?$\";s:58:\"index.php?pagename=$matches[1]&customer-logout=$matches[3]\";s:37:\"(.?.+?)/add-payment-method(/(.*))?/?$\";s:61:\"index.php?pagename=$matches[1]&add-payment-method=$matches[3]\";s:40:\"(.?.+?)/delete-payment-method(/(.*))?/?$\";s:64:\"index.php?pagename=$matches[1]&delete-payment-method=$matches[3]\";s:45:\"(.?.+?)/set-default-payment-method(/(.*))?/?$\";s:69:\"index.php?pagename=$matches[1]&set-default-payment-method=$matches[3]\";s:31:\".?.+?/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:42:\".?.+?/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";s:36:\".+?/[^/]+.html/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:46:\".+?/[^/]+.html/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:66:\".+?/[^/]+.html/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:61:\".+?/[^/]+.html/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:61:\".+?/[^/]+.html/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:42:\".+?/[^/]+.html/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:27:\"(.+?)/([^/]+).html/embed/?$\";s:63:\"index.php?category_name=$matches[1]&name=$matches[2]&embed=true\";s:31:\"(.+?)/([^/]+).html/trackback/?$\";s:57:\"index.php?category_name=$matches[1]&name=$matches[2]&tb=1\";s:51:\"(.+?)/([^/]+).html/feed/(feed|rdf|rss|rss2|atom)/?$\";s:69:\"index.php?category_name=$matches[1]&name=$matches[2]&feed=$matches[3]\";s:46:\"(.+?)/([^/]+).html/(feed|rdf|rss|rss2|atom)/?$\";s:69:\"index.php?category_name=$matches[1]&name=$matches[2]&feed=$matches[3]\";s:39:\"(.+?)/([^/]+).html/page/?([0-9]{1,})/?$\";s:70:\"index.php?category_name=$matches[1]&name=$matches[2]&paged=$matches[3]\";s:46:\"(.+?)/([^/]+).html/comment-page-([0-9]{1,})/?$\";s:70:\"index.php?category_name=$matches[1]&name=$matches[2]&cpage=$matches[3]\";s:36:\"(.+?)/([^/]+).html/wc-api(/(.*))?/?$\";s:71:\"index.php?category_name=$matches[1]&name=$matches[2]&wc-api=$matches[4]\";s:40:\".+?/[^/]+.html/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:51:\".+?/[^/]+.html/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:35:\"(.+?)/([^/]+).html(?:/([0-9]+))?/?$\";s:69:\"index.php?category_name=$matches[1]&name=$matches[2]&page=$matches[3]\";s:25:\".+?/[^/]+.html/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:35:\".+?/[^/]+.html/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:55:\".+?/[^/]+.html/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:50:\".+?/[^/]+.html/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:50:\".+?/[^/]+.html/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:31:\".+?/[^/]+.html/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:38:\"(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:33:\"(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:14:\"(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:26:\"(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:33:\"(.+?)/comment-page-([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&cpage=$matches[2]\";s:23:\"(.+?)/wc-api(/(.*))?/?$\";s:54:\"index.php?category_name=$matches[1]&wc-api=$matches[3]\";s:8:\"(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:5:{i:0;s:34:\"advanced-custom-fields-pro/acf.php\";i:1;s:37:\"breadcrumb-navxt/breadcrumb-navxt.php\";i:2;s:36:\"contact-form-7/wp-contact-form-7.php\";i:3;s:55:\"remove-taxonomy-base-slug/remove-taxonomy-base-slug.php\";i:4;s:27:\"woocommerce/woocommerce.php\";}', 'yes'),
(34, 'category_base', '/chuyen-muc', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '7', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(40, 'template', 'dogiadung', 'yes'),
(41, 'stylesheet', 'dogiadung', 'yes'),
(42, 'comment_registration', '0', 'yes'),
(43, 'html_type', 'text/html', 'yes'),
(44, 'use_trackback', '0', 'yes'),
(45, 'default_role', 'subscriber', 'yes'),
(46, 'db_version', '49752', 'yes'),
(47, 'uploads_use_yearmonth_folders', '1', 'yes'),
(48, 'upload_path', '', 'yes'),
(49, 'blog_public', '0', 'yes'),
(50, 'default_link_category', '2', 'yes'),
(51, 'show_on_front', 'page', 'yes'),
(52, 'tag_base', '', 'yes'),
(53, 'show_avatars', '1', 'yes'),
(54, 'avatar_rating', 'G', 'yes'),
(55, 'upload_url_path', '', 'yes'),
(56, 'thumbnail_size_w', '0', 'yes'),
(57, 'thumbnail_size_h', '0', 'yes'),
(58, 'thumbnail_crop', '', 'yes'),
(59, 'medium_size_w', '0', 'yes'),
(60, 'medium_size_h', '0', 'yes'),
(61, 'avatar_default', 'mystery', 'yes'),
(62, 'large_size_w', '0', 'yes'),
(63, 'large_size_h', '0', 'yes'),
(64, 'image_default_link_type', '', 'yes'),
(65, 'image_default_size', '', 'yes'),
(66, 'image_default_align', '', 'yes'),
(67, 'close_comments_for_old_posts', '0', 'yes'),
(68, 'close_comments_days_old', '14', 'yes'),
(69, 'thread_comments', '1', 'yes'),
(70, 'thread_comments_depth', '5', 'yes'),
(71, 'page_comments', '0', 'yes'),
(72, 'comments_per_page', '50', 'yes'),
(73, 'default_comments_page', 'newest', 'yes'),
(74, 'comment_order', 'asc', 'yes'),
(75, 'sticky_posts', 'a:0:{}', 'yes'),
(76, 'widget_categories', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(77, 'widget_text', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(78, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'uninstall_plugins', 'a:0:{}', 'no'),
(80, 'timezone_string', '', 'yes'),
(81, 'page_for_posts', '0', 'yes'),
(82, 'page_on_front', '12', 'yes'),
(83, 'default_post_format', '0', 'yes'),
(84, 'link_manager_enabled', '0', 'yes'),
(85, 'finished_splitting_shared_terms', '1', 'yes'),
(86, 'site_icon', '0', 'yes'),
(87, 'medium_large_size_w', '768', 'yes'),
(88, 'medium_large_size_h', '0', 'yes'),
(89, 'wp_page_for_privacy_policy', '3', 'yes'),
(90, 'show_comments_cookies_opt_in', '1', 'yes'),
(91, 'admin_email_lifespan', '1656386173', 'yes'),
(92, 'disallowed_keys', '', 'no'),
(93, 'comment_previously_approved', '1', 'yes'),
(94, 'auto_plugin_theme_update_emails', 'a:0:{}', 'no'),
(95, 'initial_db_version', '48748', 'yes'),
(96, 'wp_user_roles', 'a:7:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:127:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;s:18:\"bcn_manage_options\";b:1;s:26:\"wpcf_custom_post_type_view\";b:1;s:26:\"wpcf_custom_post_type_edit\";b:1;s:33:\"wpcf_custom_post_type_edit_others\";b:1;s:25:\"wpcf_custom_taxonomy_view\";b:1;s:25:\"wpcf_custom_taxonomy_edit\";b:1;s:32:\"wpcf_custom_taxonomy_edit_others\";b:1;s:22:\"wpcf_custom_field_view\";b:1;s:22:\"wpcf_custom_field_edit\";b:1;s:29:\"wpcf_custom_field_edit_others\";b:1;s:25:\"wpcf_user_meta_field_view\";b:1;s:25:\"wpcf_user_meta_field_edit\";b:1;s:32:\"wpcf_user_meta_field_edit_others\";b:1;s:18:\"manage_woocommerce\";b:1;s:24:\"view_woocommerce_reports\";b:1;s:12:\"edit_product\";b:1;s:12:\"read_product\";b:1;s:14:\"delete_product\";b:1;s:13:\"edit_products\";b:1;s:20:\"edit_others_products\";b:1;s:16:\"publish_products\";b:1;s:21:\"read_private_products\";b:1;s:15:\"delete_products\";b:1;s:23:\"delete_private_products\";b:1;s:25:\"delete_published_products\";b:1;s:22:\"delete_others_products\";b:1;s:21:\"edit_private_products\";b:1;s:23:\"edit_published_products\";b:1;s:20:\"manage_product_terms\";b:1;s:18:\"edit_product_terms\";b:1;s:20:\"delete_product_terms\";b:1;s:20:\"assign_product_terms\";b:1;s:15:\"edit_shop_order\";b:1;s:15:\"read_shop_order\";b:1;s:17:\"delete_shop_order\";b:1;s:16:\"edit_shop_orders\";b:1;s:23:\"edit_others_shop_orders\";b:1;s:19:\"publish_shop_orders\";b:1;s:24:\"read_private_shop_orders\";b:1;s:18:\"delete_shop_orders\";b:1;s:26:\"delete_private_shop_orders\";b:1;s:28:\"delete_published_shop_orders\";b:1;s:25:\"delete_others_shop_orders\";b:1;s:24:\"edit_private_shop_orders\";b:1;s:26:\"edit_published_shop_orders\";b:1;s:23:\"manage_shop_order_terms\";b:1;s:21:\"edit_shop_order_terms\";b:1;s:23:\"delete_shop_order_terms\";b:1;s:23:\"assign_shop_order_terms\";b:1;s:16:\"edit_shop_coupon\";b:1;s:16:\"read_shop_coupon\";b:1;s:18:\"delete_shop_coupon\";b:1;s:17:\"edit_shop_coupons\";b:1;s:24:\"edit_others_shop_coupons\";b:1;s:20:\"publish_shop_coupons\";b:1;s:25:\"read_private_shop_coupons\";b:1;s:19:\"delete_shop_coupons\";b:1;s:27:\"delete_private_shop_coupons\";b:1;s:29:\"delete_published_shop_coupons\";b:1;s:26:\"delete_others_shop_coupons\";b:1;s:25:\"edit_private_shop_coupons\";b:1;s:27:\"edit_published_shop_coupons\";b:1;s:24:\"manage_shop_coupon_terms\";b:1;s:22:\"edit_shop_coupon_terms\";b:1;s:24:\"delete_shop_coupon_terms\";b:1;s:24:\"assign_shop_coupon_terms\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:34:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}s:8:\"customer\";a:2:{s:4:\"name\";s:8:\"Customer\";s:12:\"capabilities\";a:1:{s:4:\"read\";b:1;}}s:12:\"shop_manager\";a:2:{s:4:\"name\";s:12:\"Shop manager\";s:12:\"capabilities\";a:92:{s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:4:\"read\";b:1;s:18:\"read_private_pages\";b:1;s:18:\"read_private_posts\";b:1;s:10:\"edit_posts\";b:1;s:10:\"edit_pages\";b:1;s:20:\"edit_published_posts\";b:1;s:20:\"edit_published_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"edit_private_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:17:\"edit_others_pages\";b:1;s:13:\"publish_posts\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_posts\";b:1;s:12:\"delete_pages\";b:1;s:20:\"delete_private_pages\";b:1;s:20:\"delete_private_posts\";b:1;s:22:\"delete_published_pages\";b:1;s:22:\"delete_published_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:19:\"delete_others_pages\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:17:\"moderate_comments\";b:1;s:12:\"upload_files\";b:1;s:6:\"export\";b:1;s:6:\"import\";b:1;s:10:\"list_users\";b:1;s:18:\"edit_theme_options\";b:1;s:18:\"manage_woocommerce\";b:1;s:24:\"view_woocommerce_reports\";b:1;s:12:\"edit_product\";b:1;s:12:\"read_product\";b:1;s:14:\"delete_product\";b:1;s:13:\"edit_products\";b:1;s:20:\"edit_others_products\";b:1;s:16:\"publish_products\";b:1;s:21:\"read_private_products\";b:1;s:15:\"delete_products\";b:1;s:23:\"delete_private_products\";b:1;s:25:\"delete_published_products\";b:1;s:22:\"delete_others_products\";b:1;s:21:\"edit_private_products\";b:1;s:23:\"edit_published_products\";b:1;s:20:\"manage_product_terms\";b:1;s:18:\"edit_product_terms\";b:1;s:20:\"delete_product_terms\";b:1;s:20:\"assign_product_terms\";b:1;s:15:\"edit_shop_order\";b:1;s:15:\"read_shop_order\";b:1;s:17:\"delete_shop_order\";b:1;s:16:\"edit_shop_orders\";b:1;s:23:\"edit_others_shop_orders\";b:1;s:19:\"publish_shop_orders\";b:1;s:24:\"read_private_shop_orders\";b:1;s:18:\"delete_shop_orders\";b:1;s:26:\"delete_private_shop_orders\";b:1;s:28:\"delete_published_shop_orders\";b:1;s:25:\"delete_others_shop_orders\";b:1;s:24:\"edit_private_shop_orders\";b:1;s:26:\"edit_published_shop_orders\";b:1;s:23:\"manage_shop_order_terms\";b:1;s:21:\"edit_shop_order_terms\";b:1;s:23:\"delete_shop_order_terms\";b:1;s:23:\"assign_shop_order_terms\";b:1;s:16:\"edit_shop_coupon\";b:1;s:16:\"read_shop_coupon\";b:1;s:18:\"delete_shop_coupon\";b:1;s:17:\"edit_shop_coupons\";b:1;s:24:\"edit_others_shop_coupons\";b:1;s:20:\"publish_shop_coupons\";b:1;s:25:\"read_private_shop_coupons\";b:1;s:19:\"delete_shop_coupons\";b:1;s:27:\"delete_private_shop_coupons\";b:1;s:29:\"delete_published_shop_coupons\";b:1;s:26:\"delete_others_shop_coupons\";b:1;s:25:\"edit_private_shop_coupons\";b:1;s:27:\"edit_published_shop_coupons\";b:1;s:24:\"manage_shop_coupon_terms\";b:1;s:22:\"edit_shop_coupon_terms\";b:1;s:24:\"delete_shop_coupon_terms\";b:1;s:24:\"assign_shop_coupon_terms\";b:1;}}}', 'yes'),
(97, 'fresh_site', '0', 'yes'),
(98, 'widget_search', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'widget_recent-posts', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(100, 'widget_recent-comments', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(101, 'widget_archives', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(102, 'widget_meta', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(103, 'sidebars_widgets', 'a:4:{s:19:\"wp_inactive_widgets\";a:0:{}s:15:\"sidebar-product\";a:4:{i:0;s:9:\"hotline-2\";i:1;s:8:\"policy-2\";i:2;s:28:\"show_post_product_featured-2\";i:3;s:13:\"media_image-3\";}s:12:\"sidebar-news\";a:2:{i:0;s:20:\"show_post_category-2\";i:1;s:13:\"media_image-2\";}s:13:\"array_version\";i:3;}', 'yes'),
(104, 'cron', 'a:17:{i:1640834318;a:1:{s:26:\"action_scheduler_run_queue\";a:1:{s:32:\"0d04ed39571b55704c122d726248bbac\";a:3:{s:8:\"schedule\";s:12:\"every_minute\";s:4:\"args\";a:1:{i:0;s:7:\"WP Cron\";}s:8:\"interval\";i:60;}}}i:1640834502;a:1:{s:33:\"wc_admin_process_orders_milestone\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1640834526;a:1:{s:29:\"wc_admin_unsnooze_admin_notes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1640835883;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1640837590;a:1:{s:32:\"woocommerce_cancel_unpaid_orders\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:2:{s:8:\"schedule\";b:0;s:4:\"args\";a:0:{}}}}i:1640838100;a:1:{s:28:\"woocommerce_cleanup_sessions\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1640857483;a:4:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:32:\"recovery_mode_clean_expired_keys\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1640858187;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1640858190;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1640859703;a:1:{s:14:\"wc_admin_daily\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1640859710;a:2:{s:30:\"woocommerce_tracker_send_event\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:33:\"woocommerce_cleanup_personal_data\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1640870500;a:1:{s:24:\"woocommerce_cleanup_logs\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1640873292;a:1:{s:18:\"wp_https_detection\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1640883600;a:1:{s:27:\"woocommerce_scheduled_sales\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1641289483;a:1:{s:30:\"wp_site_health_scheduled_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"weekly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:604800;}}}i:1641464560;a:1:{s:25:\"woocommerce_geoip_updater\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:11:\"fifteendays\";s:4:\"args\";a:0:{}s:8:\"interval\";i:1296000;}}}s:7:\"version\";i:2;}', 'yes'),
(105, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(108, 'widget_media_image', 'a:3:{i:2;a:15:{s:4:\"size\";s:4:\"full\";s:5:\"width\";i:281;s:6:\"height\";i:562;s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:9:\"link_type\";s:6:\"custom\";s:8:\"link_url\";s:18:\"https:facebook.com\";s:13:\"image_classes\";s:0:\"\";s:12:\"link_classes\";s:0:\"\";s:8:\"link_rel\";s:0:\"\";s:17:\"link_target_blank\";b:0;s:11:\"image_title\";s:0:\"\";s:13:\"attachment_id\";i:301;s:3:\"url\";s:84:\"http://wordpress.local/GCO/maytracdiasaoviet/wp-content/uploads/2021/08/banner13.jpg\";s:5:\"title\";s:12:\"Quảng cáo\";}i:3;a:15:{s:4:\"size\";s:4:\"full\";s:5:\"width\";i:281;s:6:\"height\";i:562;s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:9:\"link_type\";s:6:\"custom\";s:8:\"link_url\";s:0:\"\";s:13:\"image_classes\";s:0:\"\";s:12:\"link_classes\";s:0:\"\";s:8:\"link_rel\";s:0:\"\";s:17:\"link_target_blank\";b:0;s:11:\"image_title\";s:0:\"\";s:13:\"attachment_id\";i:301;s:3:\"url\";s:84:\"http://wordpress.local/GCO/maytracdiasaoviet/wp-content/uploads/2021/08/banner13.jpg\";s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(109, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(110, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(111, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(112, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(113, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(115, 'recovery_keys', 'a:0:{}', 'yes'),
(117, 'theme_mods_twentytwenty', 'a:2:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1607335044;s:4:\"data\";a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:3:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";}s:9:\"sidebar-2\";a:3:{i:0;s:10:\"archives-2\";i:1;s:12:\"categories-2\";i:2;s:6:\"meta-2\";}}}}', 'yes'),
(143, 'WPLANG', 'vi', 'yes'),
(144, 'new_admin_email', 'tiepnguyen220194@gmail.com', 'yes'),
(147, 'current_theme', 'Dogiadung', 'yes'),
(148, 'theme_mods_corewordpress', 'a:4:{i:0;b:0;s:18:\"nav_menu_locations\";a:1:{s:7:\"primary\";i:7;}s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1629711705;s:4:\"data\";a:3:{s:7:\"sidebar\";a:0:{}s:19:\"wp_inactive_widgets\";a:0:{}s:7:\"adv-ads\";a:0:{}}}}', 'yes'),
(149, 'theme_switched', '', 'yes'),
(152, 'finished_updating_comment_type', '1', 'yes'),
(170, 'recently_activated', 'a:0:{}', 'yes'),
(175, 'widget_show_post_category', 'a:2:{i:2;a:3:{s:5:\"title\";s:21:\"Tin tức nổi bật\";s:11:\"id_category\";s:1:\"2\";s:11:\"number_post\";s:1:\"4\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(213, 'acf_version', '5.8.7', 'yes'),
(223, 'widget_bcn_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(224, 'wpcf7', 'a:2:{s:7:\"version\";s:5:\"5.4.1\";s:13:\"bulk_validate\";a:4:{s:9:\"timestamp\";i:1607433912;s:7:\"version\";s:5:\"5.3.1\";s:11:\"count_valid\";i:1;s:13:\"count_invalid\";i:0;}}', 'yes'),
(229, 'wpcf-version', '2.3.5', 'yes');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(230, 'wp_installer_settings', 'eJzs/etyI1mWHoj+lsz0Dm6QuiJCIkiC1whWZrYxGZdidVxYQUZm1ZkZQzkBJ+gZgDvKHQgGc6bN+tc8wJw/R2YjMz2LHqWf5Kzr3mu7bwfAqMzs6u7UaFQZBLB9+76s67e+lZ7sn/yf9clg76RXZfOyzhdllWd177fpyR5+cHDSu5vPpvjvgfx7nC5S+vch/gF+mI97v61P9p/yV/uL26x/V1bjeZXVdX+2nC7yaV5Mlum0P58uJ3mB34ZxinSW0X/unvS+v3jzOuknV7dZ8j389AJ/mrwxP00u3E9xrvOqHC9Hi74bg4egeZz0ltWUvnh00rtdLOb1yc4Ozmy7rCb49z34NrxoNvyY3dfDWVqkk2yWFYuh/G5/0P7dTjoalctisYM/rHd0HvVyPi+rxXCRTmpZo/xk1yzeygnuP4s86KaslrN6Z1HO81EfxqVP4IH/+I/1CSzxPB19hPnS0/bxaXsH+0dHR/vwz+NgP9zOdc1jAK85zupRlc8XeclLuwtPwE0YlbP5NFtkiTxuO7nIqptstEhgekla3Cd2XxNclC348zhJp3WZ5MVouoShwy99X5Zn5WyWVaMskXXbxofCIuQzeIYu/1Fs+e/m/VFZLGCXdpbzaZmO65293cHBzu5T+k5/Wk7KQX93sD0vaI8PT3pwjrKq91vcDxgUV66Cn+sfnrpDREt5QGsHT86KcX9ZZ1W/Xl6btYGt3W9uqT+3i43ObXu9YQSZ7LzKR5nObXCMJ8s/fri4n9OHg8H+AL+wtxf5wnCRfV70+NVewFZ8qPH1I6PlxU1JH8DK/ylLq+l9Er4sPOBp7AHZX5b5p3RKq+gmP9jF14LLPkoXGZy8a3oFc8iPI9v59+l43F+U/VFaLb4+PBzsH+0PZOqwSdkdHCNc892T/5NP/XI+qdJxZv4Io7I04Xt3zBcPZn7ElzsifUazmhYRv7UvwqpeVPBpf1GlRT1N+fXhTuFXjlWe+c/6Xljg1/ZlK+hrs2ycp82BDkTc3JXlSM5+MCf8ziF+B44AvOGnfHEPF2xWt750JDPCS5CC4MNv9Y9bXzsWsUczKvLih5S+Se/9VGQWfXY3d39/JutxvRyP79vLRku2y6c0Hd3M+A+4iKAAeLB0Ou3nM7zQ9Bmt3kCXJc2no1v4FCcCj6Vv7Mvpg1tdZ2X7cQf8ODx0vGWHco5lz/LRx/s+/OAjf3pk3wx2GdTCJxCYJFrG5V1B8kKPzj/yo4OTAXtajO6jt3xPJGZ4q0/lB5ve6sHhs5X3+ngweLr2XuO0u2bxM91xePmj473BT3PPWVP9es9/vee/1D1/2rjn+Ju9+DUfxK752ZvLze/48corfnQ8eLb+hh81brhO4G9dhfPV3v31av96tf+FVPg1eABdd3svdre/hR9sfrn31l3uDdT3ceNyuxn8q7jdh8/+mtv90Mu98krurT/u+92H+2Dd2T5cfbSPgpPddUT/8R/hSYeDXTgYTc8cb3f7xYMj26kIfvXZvc9O6yyrfNRe5eOIhGguc+eVbK/z4R4v83Va56OE5MmnrKrh061kUpZjWuVg8fCB9d/YckWOK8X12rp4/VVNUec9wIzCsBs8o7eBFIifdLwVfNIrOOU03WSxrIraBF/sWNvJ+SIBQVZTgIY3zoiSBJe7Kqe13IJpeVf7z4tJItuhWzi6TYtJJidk72gPL95XtwffHGwfbD/7agf+66vb/W9e5p+zGv61/81/+o9fLafw//6Hr6Y5/Xmc4KLjyOVN8v7F5VUCUy6XcLnq5O42K3jyfVwYfPZNPllWPNO7fHGbjPObmww3OJmm+H4T+FleJ8s6G29/tQPPkEedjsfwKHzlKpuVoA/wcV+BmVQWk2/y0XTIFtPwOl2MbnGu8hEtg/2eWauhOzLm6zWImmqBo9Oev8/qDP5V0KN1nKuqXF5Ps/q2LHFN/a/nKKLstN/jZGHi8D52whm8gP8V7Lh+4BZh+wc/qWBEXvM0gVsAU5jxKs7LelEnRQknYz4H5UrbUST1At5zJJ/i3HhH8I0WZXKd0S90QVIYLvKkAob4lLndIYnwbVZM0mke+TZtDj69LmdZghHk5KYqZ8EJpTmACqTjWFY5nHjYTpwkLBP+TRfjD0swqJIX8M34Uuji0kaN0nl6DZdkkevBS3HVq0d1Atchw0PFZ32M757yHxe36QJ+WcBCwDGGD0HakSmdlHRDa33eVzt06vH/w/sA+mMOL0PPu++6F+lyUc5oA+zbwykgC5E3brSs4UvJPKtmKf4ZPvvoz9nO36F5Mymr+7/b+TtcIBRJf7ez8mAU8Kb1MuPhX4DRNR3DEpx+ytPk22U+BdnKq4Pnmdb8Do58XsC6gbC4p32BYejXr5YgKa6zahJ5zms9D5fwVbhxFZ4q3NGaTsiLKXkj8F/wnFnWtY4f6nT1GlYwn4rUPZ4iOsKwgHyg6TX4xPCXYJ3o4ePsJgWh485s8HAVbk83EG7JDVyLaZJVVSmrVmV3VU7mSJXO8EzdgKGR4Q1OeTXRgoo+79g/L0tBwLtHRt66XtxPRaKeTdMaRTzeAphEXsyXiy2YSz66pZ27rsqPMC+YgFcXh9tHa0+G21sdGV6vygIpd0n+DL2Wl1U303RCD0bRcZ2xRpnnfKv0nMO/8FeRSch0V9xwuXl2d6/M9XnjPN2OI2V3M1jaU9SFuLjpJ1nbUVl+RGmBZ+by8h0tTLlc+AWA23YJu23lO267uz+k1MZlVhePFiTiLy9fR7SWjvfDX5ZZdd9f5n2w/0Hl+nHBlcmKMQY+aTLuB/UOGwvr1AFOBjYE/Do5rEalXKpJcnIymlM+cLgoh/kY/2szSUIng4WW3eQEdmFZw92rWek0NDaPQkOIsKvQlavwcOAyglVf4drjttCG0L6jYi+8vDAjfQK1hjqKrhi+4MXvLp6/O+tQtyIpdBHu5kO2rYYgKvy6g0ECOrKIndS7tCqcOAOX+RO/MZwQML1mafURlwg+JeWFRgY8uKpy2PAiGO0SlS1sjxNK5SjFlyhhFYbnb2n4i2XxAwhCVNnghsK7waSmYMKJERLb8FCjsPFDZooxTdrmyBtwZ92a4NDD0+u6nIIsGH6Ac3GBtn8N1/fkZDi8Sz9my/njJ+b0w8ggimgvF7cVbxudt1Xrl17jneJ1aq4jrAIfANzM5CkaLMU4rcax/b8GDxAGHZNP6JQvz4Z/XWUse2BUHxtwr/s+u5mCkoAVu9Djd3IyyRYkYR8/kTmttjTcGQX7DY81aaW0mixRINVsTtzhbfFHvco+5eWynt5HBMMMboc1eDoEDk6VzokKrEDzqf08BmMDbgWYEt5Yo8XxdkidT4p+XjzAqIkoJ1xr2Vyxs8Zj+xJF+mmIksG/RMbGAAkPuAmLDHx62JqoWRturlVYZTmBe4PLk/xDvhBLlowPfDoOnI5nsADu5eFxPbhGvcjCg9ae4nmC2zhB+6wk6Y1j3OVjOBN8BQsMEKhVg3/J0jHeNvzeZFpeU8BhnF2nDeMHP3ezqMU+2tACiiy46ExjFNgb7Jf5n//v/8d7KOnnsihn91ZQBA5H1FpozO304rx7Vup/TPjNm7o/ycdqhauNq+ZRcpuy81rD0U7QpEVB6r63kKlHraijzVzS0NbRRRHjbVgtp9ZDFCcAXU6YBel0OBAgQe8o2qEeM0pXODYkVFvHIR0tMCigznVs6ofRqevkE/g/XfN3GhbXmZwd8SCsI4WzwGunzw4HXKM8G0/Xrfgj+lXks6PAy8esb3hm2ec5SlN4cppcvfjj1en7F6d2VfDfHcMHL7ecjzkwQVHRZLycg44RxcY+GGtgNvnFQksXi3R0S0K36xUC/1gFLRzQ+hb9wAqGoCXLirELXIBB1Bzt51aY4ax/Gp0Zjvkzq03/sjy/5iQerDm9wnRKtGOLQa31m3YQXl5zOVhH4j6fjj+hPB8HzgS7PvHhbZRY12J23xfInH9tfhrcqDPQQ1mdtbzOVYcf9HFZBRbAx3zufGv/EFEvrZiO3gezDJtdCG8Xz6ZDF30wD7zlw0oqEf/kQxQbje/iQCJJbvJsOobLNirJVtD3waiEFzISGGw+gFV2qn7C767gB+yCmbAmHMsZ+CI+uLpumuIGGrWFQ/DAOSwtRnXRwXeKSpSWC1jh1zdaC32Sty6igYn1saWWlP7JbKX4e6DUhP/xsRy8mnXsjPEWwWRuNaMSeoq0/XRL/GCyFoi83eSqnH5Kx+mjOnmN0ciMYxYYg4ClhqOJ2UQ5sjVLq657EL9/cLAzMLSylO8ymalGUGQrBIV/I71UF3g3/V2SFwW7IS9MBJSCWfYh3tSsNUTrA84+Rpv02Vg1v8xvkvRTmk/pY5iIM29JpIfBMO/gyIus2HlQjvMyLxbhw2CmoPrzG5BCo7SWmPLiDkymfAZzqNzPajASch8G0ke8+LzAYAdHLfD200pcS3BS7A20wOQkUQJhPtVTBA5MHw72LClHo2UFrs8oa+31+QyOxSd5xF16T/87ApOB4sCTrMgquvLX96IiymLKXzp/vsm5ScUIIOmPU5asmQ8NfXhPUm0BLhnatv+NfK/40Pj1s7KCOYGohGN4dnlJxn5m1SIe9Sq7AYv9FqMqrSMaFSXx8KpXzfC8SiagJ+252F9G+VwvFzCvIDD1PK/BhbsnmegEqLF71NSvo5bwwWZGPFonEtRiAcZylAxgd1eusxtMnIGplNck3K7ewBb9CAZPLH2EoSU8Wq+qLPsYaE2Ky7vki/3peTECuYDXYZZ+zmfLWVIsZ9dwUn0gGo8EnYRgRNq8lCaHrzNeVlbh4GmBmRw24gywrGiljCmKk/xlWS40eobn1P8JTsDlH16TNZCJCVWCEChzY0WiTME0DCzWch4LqSzoPuCpVjcm9KDVd6XT5wz19AbjEc5QJw+AXT6wwRa3Jv4QPvMyBcnNuRqJY6Qo6OASx0IBTjR/fzH8A8ZN/Z7TaaBYKlkYnCLJs7g5RPIqg6sINvK9fzWM2ImTjL+0VmFb/P408jaezpQbij8loynz7hcbTZz5oBtlX0sMFUxviYnNiTZrotCv8E3xrzxaROGs0Dcds1cDBK44gizgna/RWCpuynhMx2kJa1QH2XTMKGgeWa40nRScvHPe8CzfSPLkyxJzV97ay8ehJXxa3KMJD0v07iYIEKN/XRaRF7PJoRuKucNBlNz2CL2jzMsATBDg+zVi5JyfC7Nz/aShcTQYwZZOKECyzwu5j/B1uqHZlh4Nr39eoe0On8+m4DTViYSofpPU97Prchp5NfCEXarLqbR0PKaAERwXUu8kzUo2m9MKTMtPGatyOnH415zCOrSoIGDSgqWDNZyDc/L7S9L+IFbALOQX9+FN0bC1iqo6Q7d0D+TudQU3e32MHC/opALnDfZ+JjEcY7xZI2RtPCySp6wptYL/gbefFk2PlsuV4lWRTGVuI1DhRYzqzP3NdKZNt1BMA+YM9w+TCTSl+3JZkW2CMEs4mu2r/7wRUK6DiHKIqzA+tjMJ66z65Mw48ejiZsDeulc65QMHr3XOGRHK5qNJcKMhIrpfLhU3SgvUVPDsAnPG8FaUPimriU2Z+ljaDOVMCdeHBqI7AzdtOY0lslqp4AWrH5LbkqSWALcK5IULwsZPPGXtGF7ROKv4yWvYw2Va5GnbTQuWcbBiGZPGOo7KepZh+IAlC5xHt7KS5ssxJUaba+6LpDRg2RZsfJJE/wqc+eyby7P35xdXw+cvvv3w6qsd+hOtshyrRbX03mVjNiTQ0cp7S1ZejRKGxyeFso3BV5EDGsXCSxsJZE2mSweegp1AFVOMRVGKxgheNpyR0QqieSf0Ldi4fDFtCoqLqkzC30eRF8H36wwFpHqCdHzwEvrb5+0SFJku6xPd8N2NkAQSNdEoKj4kK9Bg2cFwMoWCbfgEUxYlLh9ufZVNwG8TVdZKImoiHWMBCxu8upv3aYwQD6XhT5MTmYMnk3/GZcVVoOVwn20onAxaLC5Uo/k7PJ4iCTjlTkayVS0+L8JfS8X3EmeUlvQe5UZ0/LtsOsJDrW6LNTBewxkoElTkjST6OE/DcGLEUjz9Mauu0/wHkActadAwKBlbkFyXnyV7kIERMKV7xesa+KkIkcLsBcWYazfZb6vyrraZRJVxBgkiZsRNXqFtms/CCTkXHNQHPRezaeqRsJ/BmRT8W+68nGvw/b1/hbPrMvU6gR5euBrbmrFfLHLd8JKYJAwD+rM828iJj1nrfgs9loYDNKM0BI+ZWR3sHogOKUMXPovcumYM+wtugd5+cQLgMPOBdtYd5cKMN6O7yqGVdqgykgHUHBWuIE6VvI4M1tjFKdj/WnVw1cafV2CbgeDNi3H2OQg5IHaTsRPDWTrv9EsS0FcgXAp6OIxx4gbw0FwKrvgl4ssaeUG3Ud9f9M9en2O8dYYLmX0Gp9VDWJ3p42UHfks83UhgO4aX5OyXyWs5NctZVPeBBgM41EbZU4qy1WFW9waufH1fL7KW4eHPYK3aFXVqRRaMhK00nDWX/FaxoOgZTDTyDkHKLoiJNMOoP5TXNM04oIuNuVuMElxnGb49iAwUXOwQBk9+zjpsbGIVC9CFNdkverfoDk3zeuGk+Agm+pHOeUyKB8daMUuwlWAQgESdUJxwSXngSEChXl6rwUw7dsnhmkuEXHBQgaTs+ieamwTam+7RmDCBlPcBJ79gYA+DDlddrabIaZitlGnL7sjemAgWNmdJUoFPybGlqKPWsoVFgbjwnIs80QmMCUL7lsk1TP9jUo8q3HYq7EgqxS+FrhSJ5vnt3EDnyozBiHQLXGQUUaerokoB0PFhSTdn1HRl3mJGYV1KLKC4N1A6mMQtrpPup9EGijrAo8uO/ZjEigTky6b2cOPEDljK0jU1JpgDFmHsAQ5vX/BDMDI5ju+zv4AnGUPGNYCBuoof6mwNDtemRDCIrsvql+AOnS/GYsDHnHhUy8MnTirYmHufkKxpjRqWosFLr4phmTPszFmLZBEYIa218+ZYTNUspcYZ1ieNG+aghHEJLasq9bQC62G06qryC7s9wtlzarkqYaHSlQoEhOUtinSKAjqz2i0eX/nrhlm3+mJevnjn1vMWTHb8lTUtisYB9iHJltnO6YLXaGMukkZ+jMCTEmLiMFIdHOSHwtTWWGss9UKnrsM5D2K+5BiTKyCJRlEhtL+UnUyOFRYGLwvCISoQurO23198m35Er/MCF1PDgXTaUFSTUSD+qaSHBGDFwXebivFAmRVpzJbvi8AGBPIvb9A7I2HDjpqzk9esWUXn3pszjQklb+ivVvuXzRDclwA3Op1wSujqQIqkb4/Dj1SBa7J2i3TStFV1NLckcLYwphYYeltO3NfZ9KZ/WyJECATMOCsTNiBhnTFAjxFD/jMWoVwHAn1tGrr7sLpSoGmuSx9WgjTinJ0LaB/uAf9u4KCORNePJGMZlBhcg+H+ka3iINcLN6laoE8evymKeIyHVd6/w8rG0UdKiMVSof8hSDZb77csDECXKwRdEKCR+1tQsSGK8nyKl+O2LD+G6BlcjfthDWbr0Fvpw5RBCAZlqPEXgargYtg8DwjNexDh8OX8R+Ojp4HtjxEReFBXaVyQSyIVxDltDHfz+U/On5O88o4CxcIdRLdlthN2cLqM+Wy6Cme88f1LTOXB4/sXJZjZ9/7Vb7OUJFmJmdCGtctZzX4tZrIG69SAxKvDfmGJ96QKxEPo/bLx/SUw2zOtCzOnXbJliJbE9fJnmeBHdKC5joxTMCBo9MJ2yxk3N3fGm/rY1DthERUsTV7fJo8//MMTOi/q9bEf3PWuK2G7JFIvziVHg+YsPkkOd3CuJ9liiOYVVoug4LGVk6tMKmNAgOXkXctAPqm3ViQOqWzXSyWLxCtIzVKEfrC7u6uWWB6FF+xvDzasW/2B08qaLj+holPvaVTl3CKmHbBVvwDXsgqNGI8zxmjgafA528pFljVTgDwXTtGECEyZ382yGHW5F7/oK7hgzmI5b2AswvVfW1onKQAuHmtZQyIZLEL90KvyduGhwoTRH8a4YnxSxyvh1zb3WTC0GXflNht9xEDqdXabfspNxEQWnqOKHHq5wHnGn33UFbGP4/pcYI3OjFOSUuVTczpGI2adZoI3D0nDoVXuRqIQGeW1QV2P2ZHJVFOKcybpFCHBaMiajnTTShgklxOyx2arBzHahKtNLqMHFulRjACMNoJddj8IE+p2MoovZ8iSx+10VUfrY89YstkoFA71PP+UOxPnw7nbtoZ+WAU+jnsJMMBL0A43dCAXd7hpfypTgXisHtapOET8IKUGansXstMajhGYdWAEUC4VXchr2IjpXXpfi/5V41cde8phaTZQcRSrS5KMOKnFIOrCd8OeoF2RY5mNmBaxqg4BAnQeDjZWVAu18628/x8+VjBV42Ovgv8167bQN/xkMiMisbvCYOYOJevRvjx/BUPiXF+CIZtcLrJ5OzMZf3n749cl7HWWfK91Tmt/H/z6dAl+ZZV8S8klHCEu8FbXujTSwA1opUhxzaGZ6gQbs5vm159nU5s3C2Ag4XQEcNiqEsIoWG1KZjEXfbh9pEIe5odghrHfUAq36xRgPV6AYd46eOGzFbhx4JbioLXFHyS+1zC8mzkBxJ0F+C50B9ASHJvQC9OsdF+qVuz2JhNYeKokO82YfddQCv/COYFo9agMrqqTuFmA14wPpUYPmX62tBC9LQldOeoMNEWVFmGIPp3bgC1v2IzHSF8cfoy4J7WgjZmFz6i3V4ChzeH0LprWHGDZS4ob49QV/HGoscJhPo5Fqv+CkLfgB3m3cnueLVjJyDLXIrS7bgI5CphmTvUKwezoaJTLBZ2zlhw6aAXV2mc0dn7M3XRSBJ4zvod7gujwsBqujXp2ahNep60orR7tHoNdMf0pXWgxYf54kRo1sYOO2vp6pbbADKDZWEglr9pxlu1P8Q1gPa4wGaEBvU30C725B4QiRFCjKy02FcyprRuT7MiOyIuHl+vaaVRHH0mYKdB5n6nCyv9QkCJLvE+4uF2lXySyEDCPPF4WJCkRnRSNMbL04+9hCidcsZWllzDH2Lr5kSNcViM9eRhobwSo6CVqB7e0Jih8gC5+3XKGRmJ1tc1vI5InFQKnKvZgn+5S/eFKiWMVHbrhFDEy9gV760hwhkUBmH9eaHESbq6oCR2B/nh9H1hecZghqKu9Na5KI+IE+1DnsSi7ygBf+cMh8qPtvRYcLD6WBV2TiU0h9UaWjDNya0dvxfF9zP61jIun+bJROrTa2VmRLGAMzj//03+viRbiBqs1KQ3kEefso5QLQh0FgcAYuAwfIHwsr9lnkUCZr40ltwUX7Z//6f/9awDr//xP/6OjHq87B5iLlBBkGF6hrGFkO2QAYczuXVU4l8hyPCj2JMqvSTDJmEKEb/Cyqvf9rebifEHCliTnwomkKPKm054jmhGwosNsoEe8j6tEYSddCuPlxc7HCjt+A4ygx8lRIRLVksjNEdyqCmlH5kBsTc6n7ToznHEBtVXCkZhKkQoVFQmFzwPGCyefzeZwT7/NUkT7umRVq/5Fsin8XqhCJUM2K8fLabb+MY0H8HFLOQoug7h3sb665HSqzGWfmKOJrA0+MwSl6GMBeKO8z+InnT06A/3kkpweRIE/bWTZWsP7+sXk5ZLMt6YtYF+fc0ytGhxSTHJpBEdKKikc47GUfmJZHZZ/ztJ5/YQKCjw1hUFYBhmcHVqcdt3rqALBHC8xpmf+Q5bNo5gxZZujrC6a9GrbNFaaitmDiAyz02HSs5JUGJlEcnaypgHWnb+6JbBpnRH48CbOzKG6HNkTXO1ErqASDgV23S8f+GGd7ANK4cElVaDEJ/XOqJwuZ0W9g+H/HT7Fa96Gzn1kzOA6hRFCQhOYwthIpBCMR10WzeB7aa7Qo65311gdDOAkGc0Jgz5gWRHiEya2+pZrprQo77Zg3sldibUyC4RWST2QWNEE6TFlojY+RkFNMXvC78k+8iFYRGeSYPGSZNjlVhO3gAhDRM2g2PTy49ry6DnVB5dx3ClBHQ3Cw+zgQBJh8gznRtfFgrejlS10YeScde2gwAgCwr5H/pyuuPPi6zgqNa/KaAaPHFJBsLARjdlRnNp5lzXoZur94kg8bxSMY2WqPTqTkfBjaAgPmjEj3kIsws246LiRrnnkghJ4XB6JRIgPvrtBhqqRBmiILTLi4WIbKDeDyEJV2PF2z+IBMYx/vfT+/UnyoRily8ntInnB/z5DAhxKe84yAlG7yBjoo8dPcEOuSzjgKSOOv2fdeQLncZhWVXoPX2GJWhsA2p6Ie/pGUFOEsVPBnUZjqPo6qxJO3QcKBALJlyVWC6F2r5hyhLKiUjv0iB2DK3gpsBERPnFxLiVO8al00jza2RCrjIg1Dl9jTBqDS6E8rxsmjaq0FRwmndj7OiiiTgXjaf3oxm+Lsug38L/NSh6SRBxwxEER7caVyd+9EgvjwZ5U6nF2WlP4F06Dhu6TIMy6rrirTktdJaLxGq+XXDRGpYkVfout6/UGBeF5RtMSAyWnZy8F2RJXK51AL/xhi8AkFcgBCvK532rJV/hkaMxWbdbLU76GYSvX986+42y+SqmGfOKB3oGwnuU/Zo2qDcoQDbnQs2CoKRU78hDR12coYzr+AdQAOWzoviXu4bkBDnnyW0FWzucZ4/dVAtRN6yjgd1AJgU+wvAwd22JoBYiTqdDKfONdRtBEdhBkaNAqPRXLFAfGXLF9NfIOLJYx2C/8xhWlHHkh5SWaN9wg5J+n9e11iUFI+PtFWsBXMYXLIJno5XQ4OsJ0qBIZstQpq6G4Ej41LN5L1eSPbC+iSHddRVd+7upOBeRBWXQdlzzsS4mRdo9/9u79pU1SKMtZanmiCf6lGD4mlFI8PGcew7FfsLrnQJkrigP93UcBhpKCLMc37yi4QGwGleCWbuHYTzM5bVqTwak9sbhRgnCe2coZrmTv4fjUMoOn1SMyaiwx1Ps528ATFtp7vMBhZWkg2DtoQbqHtSX5yodO2kZp9p316yt5rLkVRe59Wk6RYkVFXxF3vL4gjCK+IutM8Q1cCE+eY++OuzBdIhotwQJDgqTmRm6ySBpzl3FyjP6ArhKC5/iYtaFiUs/mI+xMyYpB9dbdtPm+MaV3lBmc3qTDwW2nJpoVnFcc37tJBJJXx5K0vNAXXr536mKrAxpONZya8Qx58iOH933GqseR8DLxDliMZSNBu94REGIpRx2pSN3CY9pjTkpgkq1B3wT+GbkNJMCkShZsCVEW1E9h3ISSSlqaxNo3TrB9k5zDd+B/vN52zAhywJgzw7JfBdQ9K5FrqcTKp94Jd2GCguppUDAiuiu/XnLfBYU0O0EfubdqPFi/IrBQ6B1kMKtXpF2VTQ6Z1OAlf3jVlkob8p41bBytFhe9Ey1AOkO56zgOj7cPomKqG5/k4Gaax2iCmSPj4ZNsJluX8/GfP3D1Dfo32J93yAV77C+V1z+QP5TXxpn885NAq17d5ZIQr8BZwbG630o+cU+P4GWDyBeReK84DLrKZi3tmN+fJe9IS16ysmdjJe72Hm6eW0oJj1alySOvTr+HKeDmXr15ZM42HLBvGV7erDaw1Nkb+B/vKEK9XMyXC2/m0ylHr8bXf8OaIQb4MBR6zylsBfIhOXt+enXKGLP7KfXA8JnU5pXqOlWhlvpE7EdaZYpigwzlGVx7NpZFLIJFgYP+WSjeUWrm4z97w+yUiC9InPd/M1n8NnlDBO6u+EwJoOKzaGsD4cwFE0aUGAYZb7z9l9dOigb7gK1mjk96gmWQtj/UcKbHLX8QmJyN5QPkJx5Iqx1Um9pEbG93DzsH9fd2k8HeyeHhyd6zsBP1wWGsB5jaOF8f7T59+huZw9fu6QP4/U2VZX3wfu/m1FxbenztwQeSNutHv4E/1cqOPjYeN/3Knp70rsFwdC2F1rYKosZ74AnKimDnstJ1XHYUF7hI3FwM+4IhC8aQrG1p4kbPVY57bcFlvkXIfm1xdH72enh5fvXiAvbvcvjdi/eX5+/eulZuKNmHQe8i7YnWeLJ2Q9JGbeGnugDyKbYODPk6hpbexT8E25frzg0bvxDCR30ad9sDZVIWaAJp83P3a2l4mKdwSdJr+2uZ1DF3RSqyqXaaE1NYH3JA+1ylQ/maaRCHZwBUH/LF9QskauOm6jiBX/so/dpH6dc+Sr/2UQqf86+2jxJ1iVzZrzbSSg+bF7fdgWYfvVWDRpvo7e9LE7322FrGoJ3wLEYJc3DVDVbJaRSH3x+2j5ZJUAOGEkiqam8l1sIgpe1ZyXCUbdyOjabhC5owPhqYhCJrCIvGvf6wk+U0g426L5dm9ngp+57KVsBrWruAAZF8bHZZiy9h0Jewt9nnFMsAtqS0fUIEyISBevGOvdJYg8DBYG8w4AaB+9uDdcmOMFTrzrWNtlAUA2NYAnmRMOjDXWKtFaVeTu3lxzdCA0zLWRTOEbmRFOsTPUxgiCDW89eQ4JqHOL4bj2002qu7r0f71TYSYppk51QKAkkIGGPbI63S/Zx+ymtG8YAPCv8RZcxJDdsI01LS8XEMYwbv2+4L8ohJ11uuI560eN+PRghHSBd8yeG4HC19oUuLWktF45d1pXLPc3Eukwx2eVFdYSqnRIJ09MyiTVkavPwmXjwUZigD4STcaeRlwohG8/QyfYYLptHnt4TuLWCdKhA75BHgUcxjzB9BI4wLLhYYvvg8yuaNV4lgpLq4qnWHNygZkQy868aAxzRSdUMdtaizRZCBi7yP8G1Q6qkUjKXerxGHf4IT1EGv8vtLTxV1SrUrLIw5XOac5CWcILCuSFjRU7gVxoYqPSLzfNaILDep3mgHQXF513JuXyLm+REWqNbCN/OIyZQwDbdoUVd/OEe51+zQYStapTSS9qDKax4prFLjg+KDIEpkwcbTX2Uz/iRkssEK7jcxE7+jpJCQKwdVNUQcKD3xXBk/SxIQcnzHHIGMWtNkctxMU/Ipiqxuld4Hs9mLIzhQ8i1NA0VF6JgSWS+F/XqQKWJLi6LPXMU0GhaAc6KNCKSsu3bmSfisKuDVd/eiw+nUH1ujqspLKgtcPQgpIp8zapSZRKyFtg+nLhKszSjw48QviwzirMuYnmSQp4PrMU9jaG0LlzptjTIBiJSyj/k2rT9GwPy4Xw8hCjWGiB4WKiQqK5dnITIWlG6XMm1epsTVlpKVUImZTTxuN2xRZTliiJj5ZC50P0TFFvfxiyVz1Y4zXtzgqcFPnHE5Kuf3FT1FdpV/c0Vy2C4Xk4zFB3Ed1lYP0dICugCdz2T2Vx8qwBxyv84KJJz6FKPWaJB5hCDtx1X2RO1+NaIkaV0LCPKeaRPEyETNFC/92pBL0xVIto+5weB3XSSffgrOOzcyiFwO6w0FfQlYq6IHLilIl7i+VN/iork3vnsDSZ2smCDI/kZLk13u1oa2ZuWQFnPIX2rawQ6DiYYGxWeUQ8YTGFJ9kTyDaLxW2WqE71PYQWCvBX12fYNRQg00CFqIA0W8nAdozshuNwgDNTIQcXYYSckNUgUswKxR9I+EHC5Q5zvyh52/MxiGv9uJX0JSx03DNYRS4QP3KzLYFwKfkAfzPnyJRWXcVLqs0cMuXJnTJbw1ueViGHEPw0Kq9D7HTOfAMI+MbJaaAr0U56XuB5b7BIn6iCw3Iu93XZ3zOgX9ELKR8AFxFvqfnUSDnu0r05INmCf+KuqA9eCEVrleQ2ilejRcPzEXoHX63UdS4y/cQiD/MlXr9Gw1HVZVrSsufZalmJO+WU7ZYSBqobLEpk0Og9lor7LIp1ORanAxRsQD0Fr8zauSvccvo8kucAF9m2UseNencbs+mYAPmMFLSWOoVt387188F52LutYF4qLP2AgVzIXeDWWPlXuFUw6CPhLFwr0K0KhQEFtkFdcDbtuJXRi7Rz4+C6qhEVTU+1EDRQ5OyJ5ozzi7fksaxMH22Sbj5YgNm56mS0K4eyMdtHNXY4AHDo3GZcNveWBlHu+tKQSh1a6xqUTT+21UH+IN1Mp8jJIpcWORHMPmxw7EBvEsL1vkbEnWw1t8L3NDAikrR+GcQqrznMxhPxKjySM6MgIbIKpdMq3qiHHtymO4/7CGSbCID6aBpIft6Ii2LrVniuK94yzWCuRBPn0cW/PSVCAMZ+DFjIZsWQ4n89HjJy1czRfCaNZgokydoVZpKRAQT+e7izNtPrdsijo9EvEA2IoiBoKMNE8mo+wihyI0t0MJ4KLjlieasF3DiD0sjl+Tefk+i+LnmyVnrlu94QinOkO2YhGa7+MSyoBLqQxC1M/KogHuDRbxYCOX13W+UxWZtgvYI2BO9fWvkg/nTeOV83R8X8AZQaZIk0dQT8xkf6djDjRXTRcudAeZfkmtbyrpfUfi3ggFadDn6vuCpLrJVxOhHla0LBJEKCzoKmvX7TXovDViytfJkhVHYEimvcDjfyH+0WuU3AG9JWVPOTV2l7pWZqucwvAINTFff7bQhOzP/iGhKNQyKQohN/NNepZswC+8L23+c1VKboPF56WvS8OJx9n2ZDvp3VT7w5fve09WQ7OZ+i2mDbUSnCVSu9iYwn4Glc7Jg6zSJSLXNPBJg7f2ZvZmgVY8d0Qmh4q5nPtVqTjQ1xO3NRu2UPBSM8uk30I0VmVwa8H8wL5Bva0EdbEWdsLBZ7Ck84sjJo+zYDdsFN/q4uazvT5I7PX5IQy9G9YjOjMbpGWNthFlpm2OJ65W9cJQQny08H28TwhLidwZMvL29navhcNEWFw04/dwM0aH/KrTXNk1vtcqBkN1CZoyiNlw2TvpW2HWM6LMxUDqnjax0ZLe1pzhYPNT8f8IBbsHOnKB3naUYaVrir4iZET1i8s5E5NLRZTUy5rApwsgd0RdNvNUdb+ELMEYZ9wwUcHMW5GgwRbXJsZLhTQmRfJCgtZa9yYUnnKLMLiCCxhfr2hZfVlSox4u+GNBZAGkoHOKbCqoVKE2QSZ85hom93iaVghExpVVo2gNIc4dl5o7teoboQoN1ITbddGiGIAIJb+9mrCi0bf9EWdler9uFqEtfqFhVxHDPa/zdP2nRuf1OHacvJVad40lUVpdJ3adoYSWoHvq8xosNbcsjytW4wamibJ2hm+xlm9w3vmC+Jft20U6GmlTGaO3fQMG11r5ZrrEBsVwFeb6RFNp0iE0GpVqvVM4abiK5u0oqKpZENXljsKlZEoZp3wxTMfxPOuIxLc2CDw2qh4VLCKNf9ptpj09eOquEWxlkd254wqvFgn3cUGZi4iTa0Y2yR1FTSlD4jw8s+Ou6y0KHS/wnqsIAqMLhrnNpnMmssonBaFpCwQ1VVSY0/DN6kUHFZXPRLxxAUnb3B2LU+U9Iy9IcFC3fby5zsLlC8AsG66ShamIEIZk8pLW8kEaUZqFEpmQ28CPiL/CK1UsGCnGYKBLk9jqKSsUCoaYYkzZ8tE20oaNLsfhnRxpACrQDavOnltLhyTx6Zyu1EqtZrNplO5NF7NjKOqb5/YyOBZ5EHrHHzVatnFsWts/K9XAlnAsUbsem4zR8H9nXYGVR6vjq1xVwd1vhZtyffaWWtq1sq/OntnbHriAXMya9WrONMpqnBcOdfCpHmUVMRxZh7FOHtdLBAHUxMLzjgTWE2MVU4aDXO67MKPCToJNqTyJv0E8ghSzMUx9tD3yMAPSWz7dYJoCcKPMtsAzjm6j9Job3/JKcewQHhBGDqXn3JB6zknMUJ0A170cNd4n4is0h9Cnyjy0QiJfQs2QnLKKkluh7bZsRFZf4sVn1rpFCU6JEi2Y5nniQjmDmVQ6boho9EDX1+uUfXTzDq1/ssqgjnZRpPmpMqT4FiMHuKVEyMG0UTg4bpLSY6iQ7r9hzOO/JVdv3IumoVHJBmUe2pNtGyoxIEO+QkJyhkjaApVVNtY6S2kBaFS5gBfzGU4/lGcLdDw/k+CFLz7/lr6b2xAtvIDAsUa3zMPvWabw0i4is6VenfBtpDqgZUakKvn0TgNpke/E3YXYW3uRAtMQtDC+FgpthWaC96guiKItcTG19ZcnKcmn95uY6LeZ6QEoZKfJ6Lass2JtLRZhe7+0Fmv/ZHf/ZHD0wFqsZ3uuFss9/ReqxVoNN/+JCrG0HKqjAEs/7iq1GiCQHt46o0cfwddOGUlOmHAbt2TbRMuEqOzwOsP4aa0B+kJoM7cJwv9r+da68q2j3QOp3voV5P4ryP1nALlzKc2xnH2zE/2Zi+dEy2lQDsbDP82SmnWDx8tq8OATseo9tTij7hAYoh1lOXsteYV+CxKHSgLMlrKSENri396V1ceE+m1LJ+Jc/nadEWIHkwWrI1ni1DIuBr2JWkvMeLQANwUj05DYHh4ZyzCkXKD5NOWW0ugeeKEJBv4WsYFxg1p2bjmtvEAgOJqfE7KLFS4Mk2BGR4vyYwrbaNnM0TFulTf/NgxO68mmrKswObYCovN89BGMr5lQIIZgC4yEEgW8tp9l82oJgxQIMWh2royZEsvCuA3SvMxVR4beiwNfbtazL4i9BEQoKMaODvogPsux8c+dlwDav6a6O0JQruu8mroexZnDmFMKJWxTeV64nfZiAd4+nwZdLZtt+riTfIa+dNza3ywp2bC+I2Q2USKoi5Kr/qklB7lH5YicAGVBMG2UjloqZ6PkXkAJ6Nq9PoQaMKgWhkvG1BfFcjpdJdm1rZW4w+TNiSWLl7/6iOuFPgTm9ShY7xCSHcXDIpaNRmDhMgT3zxTXSMV8ZEq2QdVfXTg0pgyEBrJNO7xGbPvyD69ZqcXyore+HQ/j7XGxcCEIDmZOj7XM1pcOhY0utMZVGL3CgFAaSx4HZkCB59kLFM9DIixk9KYkeyOkmf6YRiLJwWlVWiVfXq5GMXKmJ1eYcrCNZ90JdfIVS5jbYcEvr9Dp7KYJqgSEhuTL14hME3HH3rUlbACSUWHgSNWuqazpFrmhUNoYyqnEYmEC3vc+YaHjKYaIxZMcm62EglkKAFV3PzgiHOHTRIjpqFIET9SWZtTKhLSZ8O22iU4j9WqXdM4woWAuucJSG/3o/7DMln8lgLq7h6rcH+fYO4Ro6mvIpXI8ziIeMhDHd3bvgdaFj0qao++YGh222abUxvnYBWYfqPMf9PA1z1x/5huS0jcXCkq0Vl4fxdVKSZZl7AqLnkzXS30BMBroNHOln07bNghxflZwD9qnWwyvzm5/psvRXGG2dMS7WpNvonxQ8ZBSca2O2fpyirlUFkSM/30hyUO04gU71ietUqsI8dpKuRmQ3315dWAXXLyzXJCuYRyBstlFtLqaFLqwhyPv+RxbL1MRgAfZgu7SoDgVpdCnpnNFoHY5kxbrl+vm/pDKMW6AS6hPXcIRHx9ww6SruOtXSHgGcaxaTopv8hsxfsM6N/N8bczAU2Bqs6BkbZ1iNXBOau7r2Lo0jrqhat3AnlZLtF2YrvL+UtZiipDK/EbIN2sG9HbcZF/F8eYeryyYFhV6tcmj/+v/esSv8ejd+0fBr7HcF+NAXE9kqhnhZ22fg7cEw62giSIXCuwREHXT9DqbRjcvUKrmd+0NDkxMB1gUcRj6tqYzHb+Ss1kNXhCNBhdid6EMiiLgVrBQo+Trh/evKVhBPDZUsk5fDYy2L25Ljz3CW1C+wBNxAWgvXjwp76icOSTXFznYY1g/oviQdJucNrQj+aQZslsvUwztbWOAsP7R9UFt0DMhIi69yYbKPvOgMJ7RfQwy0Zasri0IB98CdKd695I2WW95yHGDSeNaSDgP13aqsSQMIBEuA1NFghtm3mC2xbUBauBrUZ9nmcWal7VbSMlMjERywIYSWhSVvFkry1yEZ4UdfGXMiTJeZX3VslabfceiFzwWmuiue06JXN6z7iOfNuXELNyT0/gK9JxMyE77otI+NqpWUOcqm7I1N7lBurojBLfBytPIyjdBeCLCQx9ba4RTOmdy6qicsO+iixZK3Arx1ej4eCjHl3kmKGHmTtEhNeVq0/dTjkU1odtmLIVnrkjtZyk5pAfEW2X87CWH+Oxnm+XovfhTmGJTylsj8n8zov7/WCfrz65edB4vkocsodtdLKURm+apjWXNPKL3jYIrr7o3UC0PAriGcSVmXFF+axNHzrFmKc2nYZjKmQH3jGOtqfVbGUb7WWhrhDe34zdvrBdTq1bM0BhL6UmTp7UVBm4O99PUoTb6YPJCirPmWnOMFNSwWix38ETgOY+3T4mi5W3LX+HFNpVRZN1zHL/WlQpKowSpO/rYaGMRTEeLJQ/stYuUnxJs1MXukb306v3521fDq/enby9fn16dv3s7fP6nt6dvzs+GZ+/eXr3445XJ9tEGtgPGpvq0u/I1FuaRoVpGnr3OiuiMwKp8HiQE4GoOtVkdFE5lRtGHUaadNhwFi4m9Ud/FRT/gNgnWfQ1PethEoUf5U4oT9xTe3zQqVZuzzstrhwz6kirVxv0RCwzft8fSqRf3W2jh6RBSpT4Y+j3i6UBCrZR+ZlyFeHlcIMEUiGA3Gr71x9fnL196MDhFRaQvxyajsslJkril/YMOv51hxjS5XU485sOA/bg2k9GTLpRsgQoEXxTmoEaEq/t5rm24IpSCU/8QT+Wv6AlmLp2PuhniUg6qUmzLyPSa20dGL8FGHOVNGGRw8LgA0/ZnBbuEqcPNIVEMtKUKQM5c8SqmNva3RQQCQYKb6+epEHBezpdB9jX6uGA/FXEvXSWb/GxNHswgDr3lWDA/IWpU/WGCYmGY+iavIq2ZNy5qJddPbjb8XXnpmKeFgCYiuueO56AVxVnbTTdVAzxs/SGkvkVTiDtYQEoFiovMRawEzECAA2ryiyUQaW5ZhBg10SczQvrBIIQpX5ilg5u0rhqQKMRW+X8oYlU2McfCcuTatvwF4y9qAyMXmdAB+V9YcsRm+a5PanKxHNaKSTa410oDozORPCIE8O9h2YZ/fPP6/cXZ8CWKi+HrcvKIHLob7FbY6xCMzmaKBQkNdoEEm0hRebe8+JRO87GURwQtyJpvxeYmYgPFJdSkqZW3lm3PFArK3i4Ye7w9QrLYjJhZYM45+Kqe+I39WHBg67b7ur4Ri2s6KLhzOZx0IJnNEYtwCgIfNNsfx178Np/cJu/hwmvNDufEkfDhf6c9u3ozRFPKVhqcnFjZ0IuKTQ9x2KhqtNFcQkhMXGuJoMJee3vPGesD4kFkK/JWvuJ2eVpt25X3sRLB5LZNIXK+oCp5wi+BcNeMDXGgRTsr2fBjsBT7G8XOL/CauIKLj9m9i1eDy8eJi0eMIlxUy1GXqexTfSsYoDX0MCqneHTZHdWacWF47I60h974Rq10+qBlGcsdyEZyrQJcAR9jdQw2DWqC9tKyDNNlZpVs5MjhnXSNJhLEOkPUxkIpD+B+Rl9408LXoBKMjMxWWK/Hcb0uJw0ZCtmMTlXaCrZXo7BBFfgGi0aHlyvq+ahpGNakzxQjTcdw+8EFcutmQBvcuG+tcoyCWdVuff8xPZ3EjqA+gwxWit2CUXruzsUGaIQgv24E4vSJb9zrrJj7I6nmIA1R50SKjtFfqS/kXcZvKK41VgqsT7TtpZRHpbdq4dQ87zk+BWrY4nju6F9/RVTmX7acN5LxpKhoQzCXKwMa3MLGA+GkORVWUGNtAEM8cZtArfdFbxJDgcSX3dHHVjXjfCy5hXpeFg+oB27AYgQkQCAYU33CioegFBind60xGCwD0r/Dr2/XdPh8BXvVnNhKY4AY4nYvEu583Y6aTdK8UAO2EAcUti15idcIpSUWmMNndaZyejo1uSEwwT0Ez0m7DIy6jcvIDWYilcigCY5LLgZpQl1l8Bm1ktQvwNyxd2VgnajoWH12wlMjVbM8M9d+1G2eA1vJErkyetU+6nPrFtym7N9Lgsii4x52KR7wvjhBiqxlXEGFCRZaOxKuuPngs1XKD0riHjt95zNXafTqzVU4vbVl42v5DMK1VNIlfP4fPDbvnOqgqJ1GLnpgNRDcZ7pW1C2TA1Cn2I/jxwAwYse+qMrP9+SOLQt1KNBSf3ihqrZCNjWyI1C37MfwdS18GtvCr2MaqykMbL9IQnUoCEawD7VhMlhhYFwHhl1Uv9sMGOdELdK299bQg/didwxdPnenwuFQpBBppoMKBplbOeTMVNY+h2yQPQ3Y+iQY400v502zqAWDdzlzG08hya4pg9re8kLWc4Eg/zHdJ8mbSztl/5DQyQrWUoEpgUINiBE9RT7LFmbTUYuHBQ8h1fB2kxQnlbbixARrdRyN66+2B0j2/P7y3dtEsfcUQ5M6TUc+pEz2wq2tTcU7/KNmVsRmESbIlvV50ezYG5+eifqGzD3GEYjgVR1PuLaUrtAjCPtKB9IDm3F/9kS/Qcz3Nmvl9zTQ2OL6VM2QY8uXcQ6r2E7VNR445o4wDdNC4l8loa1g8mkXmX4a+rrS3QFNVQRvjG7B6RnZtt/xWegiazAMoe5ZzRBAdFDhf1ATIR5v5mw1Npdt9MSG/9bTBpGziF2qFzOsERjSGwzz2hUPsCrz9eK0NzbvcptpA0fYFI5nSAMlG9DQzhUnJzzWEHRl8IxhI3LYWCO916TiRPDWzQvNMdUWNIWvM2qwu1SCp23sfFQyI5hOq/hZhlCiIS58Y9LgqKtnAKaYCGSDYMZcMGBnfIr+yHg4322uY+gNy9lZTSoKpLjn4BjnL5SUrodLR80r657vRkYViKGA4fKvquncsba6zei6UtqL4+Aoxc/47OZwqqkYCJsdeY6U0YOq5UPEbkM1kuJUVfzG2ptcrYXgBEJGdjoWIvgzB3py3W00CN2MUHedHuzYnUtahrPbd3pbzs/SQkj5kC5+InlH9vokv0uVER6O50v/UeB6Eq6Y492Y0YUlmuXZkV2HUNF5X13NSet3Ug1EDoLqT2f52EyPc3QiK2Q7m0THtwqwwwCFM4IIdI8ztOzch3YTgutx6BPa7ZRyRwsCNtNN5SyqkgrRYSutrrVpO3Lqazzzs0aaEONpq8aWlaZdshzWjS4iwksRmwntOWKPTZLSeI5gXPXUZvuiZwWL3ll0F2k2bCqBUKZXmfTxgx1mK5VV26qAjYkSgYsVJlOVPGJkgSL6fKNQYDQq8OS2crEL5f3vLe+fkgy1PkVTUGrqRNFAlKIj3+/L83SGX3BRqlImqae5ETMBTj/RhDHOXyOssSw8otM1V155fqOPVCR19OxGPf6/dhosITgI6SHipC5hPtQOk6Q2ySsE5vMZq+1F68pJaVpyJMHP1fHJIP7DSuIGzvltNn5Y+CP+tqvBpqbbPGUwcAquyJZDEMgu6z5zCOjwaD5mLAKqKOWswAAepQgpuMtO6TT/RCk189M1/Km4Ma4RNaMPHWIhAKn4LJpHGTe7Vjfzk4LKvZmmd/4I9EYmWNNL6nleFIhgHa2NRo3Xb4dov44BKsxtGNy8qEMq9XbQD6LQo+VG3/j8+QaoFB8BFltAJRUbJ44AGteLkHwo+8jBIkng9/JarYvMhlYjEzAKj1Fu+DTmz62TCUHJhIl1sLu7u/4IuD0vp10IFXjjG5YE8YxzwL9HygBF7CKo612tb+PqVBZOI6CwyOfIWVaAX/Xi83wKhkWVDAbxXYLDipaUZFJdpdCH96/XiY2ormy1NouR4wboCAdaIRlUCqLbCdAm/dQ69mQzWE52s8NE4xlGjxNLYBh6Zl+gzV50dNJjioW/gr7o6GRw2KAvOlpHX3Tg6Iv8438h/qL13B5/ixxGg93DJokRsn5ZaxT+5Kt1lJrRE22GmWWLGfC/EkvzV6qjzaiODp7ubtCo/ldekn/VvCRMN7Qn54965Kxv3A1H7Q1107my3wxJhlYMGeUXGuBpp9Rb2B3bV9l58pJX8BlWbmCXM+cEurLF7eRPUrgnbrIG2mYcI8DUpdKOEyIzHfl62Chlz9ExyidWkEcdZAmbd6pPl+O8pCOOBa6lNCbSMoWITj5aw8MfBecHthvD7LnHkqLiYu1L8NiBtlhStyHh6W1udHSCgwdPMLi9vAQBqFUdWjRjmiWvwaM3K09+gxRo9QyxABxVZz8R95/qPc8dtqJNOPZlRDkEZxPQmrVvvO+DFg76T5Tz5D95lK5mxvjvo1Toy11lEMdJtbnP+fOEwymNRXpwURiHFvGhvCf0GLk5tHzO5D7cTT7T/zv/HNmXw5WctH99MdjhBi1yTmmjYTzaV9e8R6JqhrZa/6RdFxAuomnEFm/SfnQ2PsLUSXHOdcl/A2zLOudVgqwdgzb9uWKe56IAfaekualgIqgJa1dsabMkkKlWs70isBebYI5ojW4x9cNrBA5ikebT2t3oTUTYoZOx3YHZRl4sK+pllblzhbECxhT6ILlrid2gYhfydTLHEuJBVqY5T/1aJ13VOYcrG5p0u59gXsgeyoVuwICY5uO7Vw2sdPDkeAVm+8kEqsK0F4Y8EFwioXoJfrS2pCtVddBOVHHe6PreUx0bK4CPBHW059i/K4WKDr6al6MTzkJA5fHSt1vn9xGqzTbgry6nn2SEz6PMtOR2VxsvNGN8mW6C4C9m6FZFd/AanTowhI+jAcdb81U2+2Z7Vn61A/8r3QThxT58rFDjpCbODjYAkkxnojXRllzU2uAIzVIYYPmRxulAP4TtWpHPvrX3PrrjKNMtBwHIFfwxGsbNdlpN9hA0rxYLMOi6bKl9W+DYtBeSALnM4kzBob8//aPM3KfGpMFkRONuZCE0osipSrTa1LeKVPCesCukQz/Z9QNhz4uqecHgtNGo4N3jIJD4cbcAYcZuGYayNkScN7G9t9K8h21PrBICmdaZ5+TbCyZf3Ux5hv5bjItN33ElF36nbOSQKj+fkasB51gjuIfBOYlgmju6aB5pwRuQjYcDM798PIpHuBcjrjD5Nc1rgyfmMcQSDAd5XtIiwdFHc9CD68yEYBPwZNxQbI4MfVcYTW0GtpJXVfoJr+hLkP51I6LuEXpBPZU1UtVYL617j4gCXJvoTokFJ06s7Jb8y5zK/xDI0FCceHGqpEI4+3xM6BB6EcT1kZjwG9XaAKedaBPyIuKPoBprpesy39s2+qM5d/cLsbbqlDJWMxYVMAkVpjfx5jgGWRRDogVkCoqVY9K7xNh+azIKDo3uxFpSUl/aoNx8n8V7MuI2RkgmCYd4p89VQkiKnXp+/CHmZHq0lz3qO5ti6G/o0jU9x0kijG6V507LuXlB9OU3Jj9MYQknDauSrp3WaLgiSgzCWKtEipTGcof1rLqttGosU0b2zoPh5ZWV0YzI18MQ2FnckSFI3iBs+ZbBcaQ7MD/nlL5E4eRNV59Qj3Iiwkyauh4Es071qMqkyazJ8lnXoIP3Vto2awMOTy0Q4TwnUECYrVt3rZqCSg1If8+iZ2YVraKV65hatimOR8qMuyy4v9HYtgycTYd4sGmjhuJXPH7yqNH9DV4EkSe8SbHJbeodNCdHB4t7PV8gwe7ofnjG9mjP12nGJ+PqvsvkgFsENiTswe5+0nsLg5wuF7ewtD9m4x6XiRo8ibOGeSvwjI74GfT8Azdu8LobGb/R3ikUw8XEMpOkeVQUV9bI0fWBxeCtkDRNxaz6nO2gmnyBxGWjtC6MNWvbb0pmOHVfc04fRRlbUA76RbLXHl8vKMMhHc2ccqKwmczvRiWNGXnrMwkbO/R4P637fvxw0DMh8sDoicdqtXIvEXX7PnMFgUY8XN8bOqjsjkdry1U3HV2duWm9hYNwzLLhDjYL3ser+J/E3WqbuaaODh3A5eR2JdHTv6795DXhMS6FiLl5Xg3GoL0zo9uyrBULIOYwU+7Xt6wRi3HTFHoo3gi1LzaVb5X1K2PpLJ+mFcP9J3KFMSyEAZZFVvgc/BShpdz7NxJgKxlyfS/NF0mqNbdAHfpAnBgF47MWmqv4lGd3EcPflcx7S/bROUfoT6sJoZle6CePFAlHF8WVrMnBSusVDkvLfpDjsgAlCmJWoBt8JBHS6ND8rA5zZSKOKMO9NQZU6OX9bO8bm1hcS8cnZtbm1N08DFyR6G6tDp4rikl6mBMt6WpAwiECEo629/8KPMLTk/0mHmF1O6Xjg2OLR5Cn/0JwhJVZwL9FJMLxfhOIIJKtqVOkHllKOuKJyF+BBhsBDQa7g02ABj9BPpWz3TC/u7KU98z6tk1NNN+9D29ssyxf1fO0+OaN+dlXO/SnZg585YM6uuzsIehi/g3lC37zn5/uDY5+i+eD/vP4t5ocn6UfMyKBtB1TsU0R5kXuw847WV9nIFzQrJy+SpPbKrv52suOshojkU5NAkSa++2YF9jpfWOW4audVNhC7UBGBvWwEOvrXlEyZUxPmjylGCjmhsAH3/wDqNkosVsYdEKnKZLnws6aOQYBtkCbVzk6flsJiRQseFYZ9URPAPy/L9I6YGRMPDYq6LO5ZfmjCI3iNKUZ7R+ybF57GKVvOSz2IZVo4r1GhiQJFpufX1IPJKKeYRtKeipx00PqasR2aF41MXQuD4Jf+UQxxnvuT0ROspAyXYOVTn9w60V18E5A2YUpuI25hg5TJYC3y06Et5KxV24U7jE5yrO6ac4dhDlnzaT64V7ABhU1K2/e+jm81Kxktjh4ofazwd6sGVSauR9vB9+z15IYmOhCaNRwKtNnnqzKjLIl1TvwyxM5n+4guhO+8/fomAzz8deDweDo2dGz9hk3U/m2LHH1azzzZqnjox3v7u6uHO2KYprvqVLpNp8jc+tGAx89GxyvHPhyee1k0GZzPXp2sL9yyAvJQ5+SSt9w0KcHB+sWQKuX7YBy3ObfvOblZvomLE8yu+vdroWQMhTj1qFA+fT34P5kUlstyMLIdI+Ojnef7bVmy1XFN8F5jM0h9uCGYPxA0OpzYfwJ78hLDFEssjk6vPS/ufkaEQ6K84NBv0YXtttyvgVKBBVwMikpg9chvncCXvodKapFguIfkNJzp0uz7azcxOB6viGoESkSSf/ORQqw78GNh+HLW0I7JnURKHWVSTebfcPhzbv8x7Qac0YzCBXwb1B6zLlBt+uFAP5FurBOECwPotQD8NyWYI25jBun3xeZd894RipvoPOmRC3+Ra7AgaEHwt8ZI4mjiF6XW1Krbie5X9wb1bOFtMxNBcTnCK3TZW3WTHvzUXEbQQ221u0w3JTlrN5ZlPN81Icnhcp+4200oZTlrHGOn4tZaY4vmTbikDRvSzAuQnrrttogqpd9DvzJuyPgj3B9xClAC/BgqwTWupxktI8uUBCJMSvLX0dkRFpIb5kchPK70HHt2or5sgJruY4se7AkZ28u5bpQWsUs9Ju8yGfLme9Xor23acYyLb8JnYtO9oqcsUXA92c7ofjWHWSdeHfCNINuqVGO9zVeRypG6Q4MfmtjvPqLyCbYX+xhRj/8Rcfe+F8ctZTH92IySU6EovREdyMG0daKY8pNAaiwJfWmuOxNDE+6t390KC0gD7YHAx+nSEC39G7QC+qxM/S//mfgDq2snCClpZKJwRMSOGdoICIEDJWbGco5EZLfUhjZ9RLJxjpJu3CI3zHnuC3GROwNHBGeQXlN2gJHpQqsdJ7zbKmHnVPX+N0IUwCtR5Dx7YVG5abrQzLkDPvCwxaWYwEuiH3WCmrRpgyauKM33LrO1yp3/HBNPsLEW11Nmg1Gz0pjIRjLWRoTkFbQDSKvZ8HmK5NXwn9ki9EGiNXWZARaE3my9nknmQAOU84cs3zgQLitBedLZRVN2bXz4vPRpPnS1Ggr6bJy8FoMYi6jr+4lVofUY7ZgC15qar8lCC510ULYf4E80TjATCSrFQCqvuCq7yMzYGSWfPJs+2RCGvgAv988ZizB/4L3sF2IK+rLBVZJycHsAPpBz7KTOgVn936Rj2pT04lEO05AOQClSTWrZ6gciNOwsRCfDX2DosRKgs9uhjv+A9YTAac6ClHkvcrubLtxrqx0mWvOHeeL5DFn71NnFbvGMaPZdChPHsKYQxlzOCd/OGxI8mSzw5jek0aYwAtTj4Z8MW0V4bWqRjjRJf66IsdD0cWZwWzhsIkuJuDFnB47MW/T1p66Mk9pUMd+g2npVaVM/Eccc3fT6IbGRtSKF3cHKzQKbMbCHqfxfZHO8hES5iAg/AG48nVNEGEHHCUHlv3P05ySQz1J65Dt/6lEnIKsto+/tzNYkd42yAqq1rU/IVwFgfAxQh9gsxlX9rHi1CAWgHllMsJpgVuI0o9gE9yCUkuufQcBBefg1kXuUyhkR0STTPbau3cvk34ScaVrbBaA8pmF8cpTTmhCDgEvYjyKvs5I49eueHdUzsOwCza8aXU5XJ1BVbpltgoZ1S7Cvx6BL1XlZXL+vI6MmCo6hU5kw9dSEnVf/CkOk8JVmMPY88I1WRbWN5BtnVvM/zngO/HjWXl+IQVEdK39MfveO1FtVHN3m6LYpQH7a+kbVWLQSsjCwIF3PMSNdXJk/vPmuZZFllOnUBz/Q+qWJ3RKXMFd+p6Ub+6xBxWz1Oibtp5gqgWE2LRNJ4+oIhvVWV+VfLB532wvjXzA0bS8ZiCgefrB9tP44zYt8mrzNFbZhEKLKDYzTwmFjHkYmkmnWS+UQT6CGlnL3196lnHdCnvA/vn//n88ESz+4wUJzLV7FJk4Q8paplSOyhoWvBCQAuJHXQsupb7wGiSFrZ9R09qGko2v84b9abXzibGZcC6viPCcdvU52KX1bdtuDB+3cR/On7aB1EG0j2aXJd4oe/FNmvSc0Avr8mq1yDUZq0K9tmHZWgcXu+T+7UU5UylB5KdMIh45VkEZLgY+ciJNvkHgGHPsi+JnpMqSvMONTLZYYF2GFMIAbyMoGi1vtDdhEGLU1kIsnFU+AR7SVOJKD6hAcPqGmSYA2H5EyyF9Ay+TjsuLdELl7n3PDRKE5zvluZe2sWb28WaLQX8/+Fb8C4v0YyacySzzRbKJGGDojJg42Wfirot5QFSa6AKX3FesVXTU/GHQkcAIFtccWZwYOIeeLspP34VkXZuf1CUH1Q5ZdwIIA4hJphBlpGRpX9pwWliKvF9DaE+Z05CK1v178GKTjsfGg5iLDshHMMm83jl2abrea6KFAQO417ap4QBkOeKZhNE0KBm0Yze2MgDIF04a19yh6QEmtnfkU9ol3ARiFmWCkZi6JNub0q70G2Z9p/QgkbmI/xV5KNJBiErCBRVuL2ccm7uBhp89WtzZKopk38gL6hRjVTZD77YSeeZonGrBlnjt/KDFXCEy9Ym1opoodq2HBS3dlWfL3S2+4rpmwwmXkAzRsB6iH2DsEKotx/aZpQ8wRvguu22UovTRijvK0yjPkIr55pGjdJcUw94If0PDLfffcEzEpG8xrXE/uy6n3dVdXc1awjL8Fj5QbAfxz2qRCk0Hm62GZ+0o5IdYhy3JLnx/9uY1im14ZjpqnZGGCqJTVo7oOhmC8wBmwb6UureO6DE603UwbF/ZKxhfdeg4vBFgrlDTMg+Bt0dJKtTrTOeN2Do7TWA9l17QoZj1p0U7A7CBoaz0aA2pM8aU1dECt+bzjMu8UStpodCi/sJqD2tLLD9h7lWzbgYqIjC22ccYLqw6Ho1ROh0tLQWDb6GeOsnbMbijnKANRaCuBjmN6tWOXeVy0S9v+qSR9HL2CFJPcRV/0akhHqvh1Segp0zdPkCw0T44kmTsyKINKtvRMIM+aJbrbNR/RS+RtJ1RceQp0ZskFx3Fx7aF5CirqF2fGI8ap+dWL44DmyXDkBxDycc0n+Er28lCoVAUyJIh/OyVqMqTEzRWeLkXQ1GgQ70Zj59stNYT7tAEK93oBdq10q7+s7b93Lk6Wri9I7qEWr1uyPGzVibgsWBzR7dQ3YvG9c8do3w0BLPuKrJRTdWTciD9pbbvxgrb4toprODCQZ5jOjR2179oCKv00TsK6HGIHI+WPbS0P3YuvCYBoH7DCSW/Tz+llzQytbpC7kxXBeZcLCmL2v6BFGdrcfkg+6C76lY+0uaQ+QzJJYkg7rIVUyMP6HYR9JJHCBu89CwjvkJM7IjVcp25s5+3de4b9/1GqNco26cb0zAEqwp3aJF+9vEkF1pG3BJdpY1Yg5lcdIKNEtFy6MGaE6M05vO1XEwcc8EhUVktnwymm+XjGq9rIZIE5MWAs1jfempGLCARO8fIXox6sJImWjGmv6VDkEd73oUeXcOBiYhtPDff+aATF4fczzdYJpfKOHNNq4x5TzzrdSwa0i4jDwOWGxBdmqIE5y0RaRC29nsuJrB+0kvGVTnvI/7bVY1uEFHufrq3rbWQqNWQhJ0ZqnzSZ1541tUHPo8reuSZRn5mleaYmQ2VV99903e/RRdA+OPZgxQqadoV/W+JAfl0Q9MVW8EPbBFmjXTMLL1nmG5nOniD8UP7yOtMNTUapewtcl4kWYU3khrdTGl9Hb3EBlNwviO55N7Zw3ZWPKN24m4zIdsIjLplUoByz3Zr6DUveNlSbR46FXunWMMirO4zGFnYHnBfQNqhlXGR3l/4GuEVC4TTePGJPL6zdIrFGfCYfITMGbRQUfswEAVIqoxhSayVjJUeRhTF8fazL2LDUGbXZVFkCFpHS9u2/UCU1SM2NK+47Sn8i4As2IW3igXfj7uJV+xs3mDkkZwBo1ZGqlak84nZcGbwo4DWZi6g5Rf2qHjCKvg4rhdYIp3QX70WfLGvd93k6rfegjUam+z4pDEVc3MkwzoVYRnhJpbsGmLdOOk2kdAV7L2RHS1gIyZvx6Cow/0dbw9oy7FvbLS7KntzTRHHy+FiqqzcxVeJDELeKJidM8ZjZKw62d/Bj5TbjAO0tm2PGeQUja8JweXJkSPrTlPoze2OQiVmjoEI65QMNwKZMYFi4XRIZBS6zxI09h4tudU+MIXRUvCwp9K0UEwcarG9c4vv2BH3VzAPqCcqYb+x2BXx07z6Z5dJSikJt1LEBgU73SeeKHf7SML8MDhlAh7JJSxZUchKdMzRZ1CQ+Nz6TtS5JJ32KaluzvqmrKw8vmbGlYrc31tBr7KhMStbBGCbqR0ft1UwjLXFCE/SYiqCN0VyCW/s6F6sMhgIUQxH4r3F8IhTYvdVDCcjuOKRz82spqDyQ0QSCHKQRNN7p+U4Gp8jcRCaqF4NygfeTHDTgcO9fgLOYiBqLaaBFGlLh9/GbaXTjA9Qk3iaZ0VH5WUzUU1Z6tj5CYLq5PU2XG+ak5c7tlVlAyBGiSdFpthasshzv9OoWLCgPey22WNRF6AHqPbRHCxZeZ8MklmbOx1o3tW0X1b2Wr+bXp1dgbOoK0AZGAwQLCl5QXWEMYHejGa0k0eBvnXg1lyJIMybmpgI42HvebiYgGw4OmhBoa6txNGkde5ydZqxnRC0siLPy0cKrRdu5VctXFtOYscC3VOBuP8k1ShxOBpLxJ5XOrjgFVjWC64y6XU+mLO1dPxSuhMN3G2eNaTGqpRSw4+y2XafnFqbmOIvCCAd9Xo+dvpbcy1derz3LflGRPx/XeJaMmSRXDE1aOJLiGsQqZnrUNPoeeLxw1kOGY1IObMgdq2/FWvH5yrzohXlEj4mrf70vkLcdlmbTYzcVqStZQHVvjKepcdlbqI7rEhQzi+jM+EsE8ZBcEwCs/G01K3Yuhnsz4TUZfJQEFNDWps/a1C5ia5tyTNpKd0wXAjIxxc5brKgifpWOFhx10ztITVHx1EfCUjmESEdqW9XxrckxW9FRnVRZUqL4FbX7Kd11EI0rT4brtb5c4SXGuq52OpF42gFcntDCmXBv7Tos8Xa69XY7wTb3H3Kx1h67l7ONyxDxkgWiKukgoboptkk1ZASvk6zJaa7g+hrr4TWckUBRqYWxIPti9mx/4P39UWUBnpVzAj3Dvu7Joby12XprUVAyIlLRk4Q99zb0v3TAMLjkB9Mc3pHrE6EOjZogYwH+Hj7gGkYkz+/9N8/8XQpL/jfSh+WSlzZs4cRIx6rpcdP8CIXy+n0z08YCsO5uOv7YOfmKecQtUTnGm0+sqTauWM6xczPnZdKIlcnj//80j5/lk7y0fAvSyR2GE7mI5gIXV8l9obpxN48fofoAbpeuNO6ThaujglvLe5xYdrk8g5ZvQQnd3FbLsrY9mz4jBBeuw461A51Ulkq5WiwVLdn/RqRSr0ttKT4aY0lCY7vWsRw6H7AqvedaS4ilh0KDRSGCd7mKwai6HCT4E2cZI3XQbAE7bDJ5cd87uGFlwpCVo2DN/4t/PLzGQLCHhzjeX/12vdRLPjAXLLbDsZBzEsRGCzL1Cn3w3ToIFLcQc0NOi9pSFNvRzvDHAZauNy27D7pXQhuE7TJbTkOard7Eapb9iDbdlj0aQoLcCKyDEINRVO6+6gMEoa5I0HpFLATx9x/Zcr0/CFy5ou96YYXaDRY7eflsWmWxb5hH2P8YTXAb0VgzusWp+opw4lhVm3PrB2t3RUJdYPAANU53KIFVg5m0O7Lbiauti9OthMzgAyd8XMLFqhZC7R482wctyCTVj2Yw3yF5SZ9JLDHOXyCY6ThHvYbLKccR4xWZnJJvl7rUygCwsp8pYPRvQ7cOGyWymr7LaIAZbN0zuy3MVidgKXP+63umZgO4Ref4gslynUVO+M9durxSMQce3p9gTQYv5SaD3WkPDY6kEGCPbVATZO34e1xG6bLZRcF7YSCmQnie+CqbrAHhhLm8XNJMDgjluHVRd+GO4hlqbkx8uhHNcdw+lmxyau3gy74ePc+31IZsrH7NNRmNdwmp4tFu0YyVSuOjdIOjfOm32dkXWf1UJBgMGpQmttN08o1MSQYr8LbWB1Iy0mmho08Nm4CrEhCG2tAbor3tQImRS2FdBEQJkKlG9wCfq3YSo8lM5Ukte6an8/iTsKK8vzguQ6fSz+y5V6MwLvja20AnU5kEvrYF435wSKreXpxvgI5IRadqb7UtIzv2MP9egIspkWsmRMdRDxQQbTdf7W1DjYN12n4m7U25QoEAsZb+2c0ykke/dlxQv1Z42Z/hqPGhIvRSaxsDtOyfuNOy2vknHd0jifJqXdXZrBARJhQcB2s0PH+71d3+WSoTsW5QllRUPNHl8T3pV/4CW5IG1vtjSIxAstG8E/PdRgMbcIlQ4PFZ/I8PEJM3ejyb1gX1ZGxDllNfW7PNlNrF7OX07F2d9rHxlprq1joobhSBBjvejxFoQ62B9u7mjqGf+374b/MRXclpa4jVjcM3Eg7KpvwtY8a/nIXNTqp1aWi75XCl9LWd/MhNi4Hm4iaVIOpJbzad/N+L7rVq+i6V6d0V/GeaaJN0ncreFE6XWMRdmNT/CQeiFPNFM+19cHBm3mUWtL0Gud+2eiq4eV2tdUiROeIDkfp4v+e1lJJQygydKgXJUjBcS0FSrPyE7w+tqjC8bTpoQDE5yIiki9AhntlhlU/XXF7Y5sokhrNizjNxMpnqsunoEEG3VGZTtDm10XzpZNy29JbAztle0BitBIfsHlr2DM4WYyRWJVoXLeAzr4QV08qOKak5hVOiE9xgKuw0TIaZI/qthsqj97sZbmo7Ga6rG91Vxri2MIS1wzWyaEXelbdYom9S2F+UsEcZATlt87vethb09qrnOQAf0c/He9hN2ILa6+F7KYKVN2eeRDkCEx2oSIyE1BH2jFlPuwNSey+3IxbpdfBqUJ+FomPkGPCRpRXzcjCbB2+lhnEUPpSR6OFjzcZeyX5QnwxYSYdeYtvCyn+InKnmfDJ+oursX1CIqDwZDcIDqiTomRc5wuyv2FVc04Y0ZcdSpf0DDVtCNHLqy6TRq08WqTgJJI6ok3nOjR2IlU4Yf3OA8/St6dnl05cBdSKsdsj/UT6CpkL4nVBQ3ZOn7qV4iLLh4kvdWtM/5IyuHeID3yguqEGn4WqD1YH4qZp/NWcNQWurRlVafq1TQ31OJTcrQTVfMWCT7oTh3OtUT0NWqYm5kbRDDJRzWs/cIPZPeU3VdnToiphm15uU4WdT6IOedJpG4awVbYLxSAUbiS2EpHPVk50yTB8TtmwG4FhiaqmbsZOdFL3Ow7ZuhlN/zZai8qixJ0wnKRWb1McpHL97la08GwVSFhD10nANijTzqfp6Df3hhimsrshp/+5/w5d16EPBYqJxHtX+oBZM18o1dXavU+InD1dlLPMI6R3bDgfdQB2NtAK3//p8vz7P70KUNDM4IEQHfx+rb3m5LNo1UMDfCoY3+Tqfp7VDxDsilajbFMLU+sihJa+kGNBBllRq+cxKmfXeWHKIlIq+k4qC740s1lhn4XxcDPhMxXc7wX8dm5pqODi1Y1YQayEju53QLeGZ+x3SBuhnMLjnmfbcQrh4TVqOOUhgW6HAkVtYm+DOGoIQe0x5beAdg0PeupBQ1PQM9NetBSgYf+gD0i1OM76scv/nrkJNVBC/UZdadb1fStts+qIXxiHhVSMgiTHzvBpwrUompHwDUSDRiqwPW3LrVALy1hEPLvRZEw1gj+zDsqfNcixyF3TTO/1cvqxQSnQ7n7cCm9a0q3nxNyRfJsWH/nw3GRVK4670Wsg+qkrhRA4IKiHCE3gIk8buwpILujaFwheV1cKbgoZgdftir54JN9k3EWayGJc42IsdDE2KMT0I4ldSUae4EaZa6tzcWLtdlbWBH6UIIIr1v+e0QF6eEfTUuLaGoHJi3F5t/Eaj8rlXJwZklNgB2jOkEAjHpuGkGXFQt1iw8h8s9tn2O4sWkfa7nGpwsU5lv0wBRwL9OwzXzY+BHVYr7LeuXtHQad0kuaujaUrVbsnUldMXn6TfIfwt3cOkF63vKGoONxM6jB2T5P3WhXtTxh25EiWhdzPzFTe+XQjLNQ8ZeIS9zW02NfL13AyHqOH666piZZazQMjuRl3CEy/SwsS2Mxh7BDOTWLajhJuInmIp4eD8SOqupZnaR6PH0GgvlsE8jg7uYPX8rlA+S+Y17LFCfctXGsqHWM28C6Np2wH9BRaP1e0rnwZsuctDrxNZFwwIojfW9XbinqIDWhhMetIOMjoxQSRIX33yANJa7t8DbzGxNCafn/mz7fAGtGMef5tVJOl/DC9K8NlPqQzOHQd3N1DWuXz3uB2veYQGqiI93l0QeWxRdjztBUzb56E36Sz+W9Bk+i5Em0FBsp73ACmEcxrZW7lY8zBCtjsktpVto9r1MJXaCf/az/4116b9QXt2RaVTIQbHy2d/2qf1+5BdnTSY9Lxhzch2+vvPkt2n53A/93fbzQhe7qyCdnR/vH+8a5rQ+Yn8IV9yAYP7EO2phOT7UR2wA25Ht6HDJ6J9RML8L9X9SPD9ljfnw0vXn94df52+PL89YuHtCd72mxP5vtc3COzVbtlDoo36WARadqzskcZvr9ZthX9yvCb5tEb9i5rvBuM8TbjS7X2rRi63eI+ZbX69/RSG/ZEQyb+4Nr2JYneFy7yPnVMg2XsU/RJP/4baJy2/2yXGqf92jYgVH7/Am0DuLMcyRGiQENJXW/QWg5k6yv+hbSr37i53OpnRbvLDYz4sPRTwQy2E9dNxTS0YMaShIidZ7gtdC+9SWkZkdBJIJ+g3RExeSvtWeDzGZjzRF+kVCn0HltkEdu+vtuxFhr7+8+OuIPGYPuwAzGzCvrhnYtL9H5w/mfSpUfZS9CMM+x4ltr0ixkM+Eo1qXYoAn16cXX2u1MOy7VMBnzJtZTEUfZYvys9LYHqKTsC2Wyy+bz3sed6KMoX5KoEhOaK4JVKU0LeJHxuMcT2qBZDEguiOSuMRQvRGa1scB5MhPwfihJ6Pqw0OO/q49cmFWojcY4oqGljvhBUWoQ6ic7VmdIGv8b6xJ6P8/XgoNGJorXobeYGm2wpJyyZ5817V1ThgY3lmZrOrvkUYVj8nkEBlhkUr150qTcmGjJs+eQK147aj9ptaTqP6EZsVkxCQQx81NA4Y6kCkiAzq4PNWr6HdOetlmCSi+EwuGa5em9RMPsq9Jet27i66MAz0jaEi2sBhh9iowO7KK6kUjAXLtzWIPMTuBEqDx4fu59tz0puenaTCx3Gh48ViNU8NfAzcILRAcKUzQ9LPurI+4TVDbRlONDyI40TWfD97cHKjEQDmvX826QG83CWmhDrgmwDvmKhwtuD4RsvKGCQUKlwwmmMPtgj7lMgVyDsihJOW9wsbcDiDov8od0M2nVX90lv16IvucCl/HaZI05ui5LhNSXDwexjOvRFGelW1USUwX/6hjArJyPlfQT1j8ZBKFCwrD5l99Rq8V0RXYTDcBEaDw2QbOBo1beKexSmHMryC/enf52AjJSmaNFWW0Lz2Ox0SMHkgpl6x0yY/uZ1/0z9lFT7RUbe4mDlW0QAc2JJ3jgme9E9KOQtSXmMLJ6e6L1y+qfowfk3X13r46+/YVRZeBMuS24UAYdbuxA3SSICCpstOkQYWg8aD0pBYNxIAa1CcoSjoU/CG9Rp6yvOKncVTnKIPhQ5wUfgwlaw1bG8JK3BYNOjxFNAx0D6rdam9RPLXJ5M1S6jfUdJ/hm128IRbvgxkensBjv0LPjXU/ev+HSDdsOWAkXVbkHZPJdANaQBbAWfCOaQN0uXM6UdvtMf2Pe6wMZF5ZJxDgXmYHn3/I+4Wa09HLHXZllMbRTFJTTkxXj23KJxowAbN7Ldy/DcnmLDRO5824q2uhv7h2X+o9xN+pFrkaoW+fZszOonD4r9bkqWlKpd8fUSbZIqumFb3+rIxbxYFNXlFBecne/louwTHXe1Pb+di/yjtQlWhiXd96ZvJIpt7BU50K8cBIeEr/ilSaphA07edYbtonUKiqvuJY+JaEJQKqR6nugwex3P3YNP9Kx2fkWnRl/4UGeMZUWwwAUL4MAeFZlJv+Qf7snj2115QkU72H62/Xkbdw/cfeoAg0dkux0hBM+SnKoHBgh3MUC4d5AMDk4OD092DxsBwsOVAcKnT5/uufCge/oXRgd3HxgdXOdK/yThQfgrYYdXxQZhH169pN16QEzw8Lg7JshaKKDaDZ39leG/vXBdVsX/9htxjC+KAB7sxyKA/A7WV+VH/HuJ8u3hmv8bjW1w2OyYIucLMDz6uNn94/WBswNY1zP+Dbvxx5tHztY9Lh47O9yNxs4ak7jhENq54WbSXgk+HiYnGpkL05EnTN3SKBt/XhbMLHP28rjtCs28EonGxw6e7Wt4zDVZCiJFYc9R5tq3W/e4o4kuuTzUGA3zktlflnmVjdtdF1mJ+U5zkrUX+8D5pUipcl1+7vi5NFxaVnqQOJ23KOfJdVo1ofuRSip8+cGaWEErXlSBIYZJ1h1i05Ha/lhoyKxKK0oUTGF9EIVvkICnrQ2mplPjmEWooTv0+O6DE32gxwf9vWcJiH9Qs4dPH5To2x8cHh09e2ZU+e5fleh7qCpff7d/0VwfzPz7C7jAw+9evL88f/f2AWr96OBBaj0qh1Zq94PmUq3S7wdNcftlCv5gjYIPn/HvRsNjGujfmXjmown3hnalyIsf0r5YmbE8Gaz5W/zOA7Nk8Qd0JMeOogrePPZfR2ps7+CYVT/ovkD1N6m0I6zYyQiOZtFfzlt6DAdrwVIc4Q8swTh1/CIgFyvfRYWWhchV6iYc1j2WP44+dLf1UO7ZpbGP9UqQ1uHhSnC/P9hL4Hbv7p8cPnuQEjzcP9g7eHrslKCbwC+jBLsO/S/mxuKIdG2GD/ZlDx+k9OzdXAdkaUqAbiCLGfXLtFwUyGK0nHnAvxcVd7SvGu6LpRBv8J6c7bv5Cm1BZtcDNUVr4A4tsRfVEvK4fyUa4ujo37SGWJH51X36GZXGAD0nuG57gwcqjaOnz54d/ospjcj5/0VdpUO6s+/ev7n8AmfpcPAgvaGnYJXOkGXq0BPy6YN1Axmyq3SDTO1BekEm829ExO/BONfL8fh+jrn+DTB0cKK+xe8zPnvjMOCqx3SEAJ/C77+af3PlU1zSt/IaQQGOLVSrU79Kk9squ/naX/uyGtPj6O6Ldt7xs9jpfePf5Kud9Buq3JWEUTCalSE9NIW/7hXlTclA+cmo9w3GknCE7eRcCtqktUuOXXP6VBGMriUBaOlfZg0F4C7FBcm3pbaLJI2DXyZMC3WtuAalwgXK1bII3l8pV+bfvMLOMeabo9tSCp3yhh4UVRksozROgYn4njDT+y0Onk5o6HDQJvmS5z6e3id+Vu/ZPaZk6In81eFBJcl2tP0Zl0lanpB2J+oqQVT/5j8/3Rsc/dbzv6Du5Y7axLVOnx//FoeIf7XEJZVvBcXc7P7XdldcI5Yl0t4ke9ufm6H6AxvjPAjeB5aubrrkQiCAtejmMVw69fgUER+gP7eSV6gDwLa4qEpK8uI+OZAOfvuJmbjaRqWUNIUBBUqrY1wVH03N2uY8aG2G+Icsm9fh71CD4PMK5VWf5qOPws1lTCVqH/YYGySN22TUMCX6Qny2XhhzhiQg2YlODslRq+Da4DW7wRvY0fkjeSzHm7TqlqTAsUAMzKE6yRaj7SftLSV1Iefh73Vf599ccOa/Xl7PwGTUcJHt0qS4h07BAWpnOav5f3Yy5t3py0jdUiWh76NscViKbTgPQkgNz62o2rd1o1BquiNc3piF2+LINxdiVWNeTMIOgjKXy1EJh5q9JKfUUgGeSPY0yqUtyX7dZSQS6sz19aVaLeYMaY1iCTdhYnfy5CT7PM+4CBYHSqlSvTkp+ER4VuaInGn24fpP/zFqgO+57MxRd4IiAKf4HJwiM/xSoUnazjscxc1iG1h8Wy4/ZenSE2wFkb7292nLnldIQsVCIYoZ5Qd35CPEeH6QVT3Y6+8eJ6CRwdza232QVb0HMnfv0OYj5Dm/UOHRaiPjl4vI7Fob6SFZiP2mYd3U8kldUguPIlsYM8M/bHVgZteuz6q4TOsFHmx6Hz6Nmd6BEmK8FHcQQmPcP/HfS5zm+EiwBj+RKKJlgy1NRzezjkw+1qQo3poJOBh2vbkp78eP2+0I8CG43D//0/8Pz9Y//9N/d0wE6ceMSK1gGXJWX/5seJyW9u7DYEY+ZuiqwEkDxzL+ItEgzLPDo4EqgafRis/ME7ozIk0KJKL0KfwNkiZEPGGaU5GlKiB0tKJI+7paQH6NLQEBMhUxUjvz3+PkkMgKOOfGAoFv5/pHCH2wYCKxhlZ7pcFUCQyaKwBFMP8RTfK0G+cfnMdvs8WCsl2jclLkqsWlTevp2Uvnq3EHYfwLYQnHSieJeApeIeaXUhbdaX5dpVXY88Eg/OfEa8zYTU25zdLqIw+MkTouL9eaDFduly4Xt0SSgrV1t8ubmykFyOZ4kCq06CJrf9Wxzvh4w/HA7JiKc8ZXfbWEkwuW2ST5lvagxXDKGJ9vl5NO6lFt8I2vg0PSZrb7zdC+nj/X8kFTNWjGatXX4YByW54riQz1baqxVWRW1E14cADTxRHCrqDNBQqpBzxdcrawNvLzEgkE3NX3lu3KJ1cZ+3riSK1oXijlN9n1csINxeKNlCwS1Q5y0XmzauQVcmdHtl6YD1hO4QKHW2+su1k+qTxVUOy8mIZfUpYvSiwZbDMwCW6qGvbBE+ReEl2ZlmxqryJikgom1ypQ+nDeCgZH2arCt3LHU6ctuHrTsRyR1njMiDJDD8B/Gb56cfW/PYIfjR/9H277WeRGDoGljeGJmx5ILVZw6rJq+9XTSN9x/UrOFDN6jLEONg2p0ku4WjnTiTUrF9hiQUG0yR0RBHOjFVecj8soFJYyolVSt2S204JfsVaLdCPRgz4y8SLG95m89bJAniaGsZMxhj8/iA568DN5UI4FPUK7bdbWisdV6jB6S+2XqDwEl1l75uBSEnsIt+aOTrEF7tMSJt7A27xQ5hBXzOCp2LBcgD3mBrMiGS7s5DnGkXASvKZa5Ig+sud3iSgzrZUipecnIHUW9Ka3dLrCVdpIB+Ligg79nKuh01xpPL5ILEmfI41QMOx7N9kGe9pI3w7Z1Zimh7pz8yNmVHBB1hjSoNNXmFqH8fufhSwRpUJ05zpsvgaQF1zzhat1S5VdmixJKTnT+4gdv/w9VOSgfkp7aT520iY6tzUVluG64Xq49dfO66vslalyJ1kVfFbO7/38kDenoCJSpSaV86zaRGhOG/mFUJtG2PCqjOrC9H7U6M+ipYFIqByJM6WxcVlht0DY5MK1AaJ4puN4NSd/DIe/XBM6QX8QWUTNwteBit7QTlvXDsDIJezYohW158/pqEo7R3n1+BLyj9t6Nt7izjCxhYyi+E5Cz8P3QKWeNKLoOiBNs9HVGKokedT5U4o91nONOodSADe2OafYKxHZbeOlFsw54biczLXCId/NJUrsdGAzrR+7BTEb+XJRzufZONDOWgws68dOIIr1mFKJ7VHInQdbVIzKea43QAvvrZLwCr9xP5N3MP2Wcdw4omt6OJgVo4ra+sTVfVuubidISqF8cdV2hipKzvMaDnwbFA2k7qpGNvC2uTQXwwVXoxdn7x0sgltQbaMIZJzGJ2fX2ZslDiOL7DT5/eW7t/1p/lE51KNz3ajOHXNSaORSESnuUUD+qoqjpjCdC1SQjx9Wjf4uBcFYeNKElGWcs9wDH8o+o1PYb0nZ6xz5qtEMITtOuz86KeodY7movKkBPWHaUAPnz8PJtwXuKktMD3ljUVxLaaIZjz2gk1yDexMx32vQVtU7zOsmvNqNFZOwdXkeQE5wbimhmRSCCRpwE+j6sw1Y+r7jrsUQ05uRyEq9cdkx2Q2rkSmyJsGCsEOx/WFMJnCjy7kK5VYkYuFbxgd7ocW62LYknrvY6MadaeNb47JVVLCKE7ql2ioMMWCvIm4kyPfA9NLoKMptnJGzl9TA+Y4awCNZxIhS2CCYazoJ9X2NDVi8pOzhqvS29Ma50KFcvTWO3wPO0vuGhW0Z8ePmgytyloscuAFdM5GD0LAtGcgwT0csZbm1RWR43y0PliZshNc4o8/9ophfGcIALMHBJAvcmcirySF26UWsIsaTFzvPOJTRx2+Xs6yCUUVO+15/QfTWlWKvIImmZ7xCXkLLdRCND7XXSiwCsQhJUZlmkOEzTAg0CCt8+QabEcsXs2s3sPiMDxnwIWWLXpALdsRDCTgGLstHDwrOYDj979lG9GhQ4z1TzyIXEwGViBTmnzITe8jcGZIIi3Y1WMF8Y97igoPbI4xC6O1bFmilZiYqZE8gWrci17s4j9PFIh3dMnUFdbkMjxXudE4RxiZ1C5lqUSIB/8KUeiAUUYgzinAddGBTrdpc5TvprDSQFlOg3qNUmxJr/xXJ8ZhljYPLtIpa9CC+M27Fk8irtGG2Meu8BlmADYczXfI8kAnu+K/XsCPpFI6NK6ibpOdyTUc3w1l6f50R/evFu8urx1ECEcsys5xNQQdpAMT1C5lGe0L+7Cpb1LCxuUE2Y7ilAlWwyFxBEloYQ9jxIV3I4aVEsE9OaFKcHh+K8/H4ScB2e52Jp0g4tqgh+Orth+TVxesEpGRW1FkQ4G1KfCsd7BEkT/FxvnAWPDx3jO8tvMUX1K+6SSm0Ad8L3etyRBCkgFCLI4vw2J0G6745rK3IlPoYRGm/0k7lhAjHUWKBxFgU8YKIJJ0wvkWZy7YPPyst4PQInfS1o7ZPFFbM3x3+7vz5i+HL8xevnw9P3759d3V6df7u7SXtZVki2hTmvIz5hLsaQG6lFJ0DlHPaELnpOK2IDQ6Imz0aQghL4JsiZ01WTFPLSTp2rx2ddUdwOjTZsfsfFZmXZRuyHy0OF+LtrldfVWTdHKRl8ptsrFDGRyumVyfD23Gdho8tMqunbozNOm9vbz9y/g02JOrIcHtLI3R1y6IVlufxX5fCaMddju5Scl696VIJo00kTmgj9arSLqowovi75QwhtlRFMc18GEKsX1jQoDHOmQAWQOTBxQOh2Czj1CyBTWSRj4DFp/bSRu7radMbkam08v8u+N8PPd2oLWUNcdfIxpvgEbO73zIEzKqZEpFnzfNkwRL2LNSSedM908S6jSe4MwbS2s685aJtlgvya+RSku4zglvTp/ZBOuEwMChhKhPSZeIJYeEIJ2+H419LrA81dJ+JJ2qzDi72gzPUSJhzpcLRPvvwJdFf8O3xFjDHRTnSkvPJDewFgQ3Dec0MXoR+toPb4H9pYpTuqqfWFe3ZmbngzirXqLH/4hWhXu7wjJq5Z2s4y4Mec5OrLW/RbZGpgKLoCUULgrbqi5Rib2WsvygHrdsH3HOd7WrvKCs8A5+vvh8Vbi9TpP+SV0NdOsZuJBzQ1NPDvblglj0MAPcCMmg/gxbY9NxTPgZpkCkaN7wy7VEOuxQJdtwWsmmz+Hi+bJp6AzqMpw+GnxLv/eBZMthH+oLDgwfBTwe7z44Pn1r46dO/Cn76wKIuC4z7xbCmiBg9e/kQjOnRiuItAdnBjS8mqqA6MHbdMFNc1FR+1meJ1OeRV0BO8UfxZ30Z/DRObmWic/GH/XtBnu4h4cmvAEhb63woJZkgDvooAatFJ4n89xcJXKHknL61edFz7AlRDO0x/EoGj9VmUWd3DfBgNQhVVtVJOK3fMF9VvCZi8HQgVcl723srKZNWwbD4JYiY6/I7VrOS9/SKeW2O1HVVame6BK8S64zZtaEdERU/086OVJu9aGBywKEzppbcAwQC2LuwEZ4N7ufCVTTiPeFTS49dv4R8R6SfsnT5VNeJ16cL39AJN/lQ5H/B2z/GxMZNzt3CKUAiNDW4A8TqTa15XMjc9CcS3m9mYeVXadkye40y9iRAIXAnu3muXMUwLtxCajoIV0DUfDLAMSTpc+P4clSWXYNp9ZEBQ8TZowb1KEXyz0d4aB+RuHpEC/coNsWmmaTz22xuu5Eh1YqMee5E7eoOHG+n/upYaZHVl/3MjoSzodPpaMm192eB9e6gUjxy8hglUZr3j59uJfqfgydYRaaP2qCiyc9ykUpHYUG2uSevPv3RlTm05yEgjBMQB+Pj5H+H1O3K5ZGG5c3Q94nEs6rd8Uxz8dhTW1EpESIgcatqOV+IXVwRnW67y2ce8yBaRvdedyAJV+UlnYMX2tTTSQWRA/GlfGsFCd3QO+5zKHmymxy8j9uQHFkPsNzyf8goFCj/Ovd33mScYq+yZ85w/PRyC3G+m0FnTnInUywEYGiHutceTSLPGHQ8QznSFWIeKr/0Gqk2dQzWb5K7oYbUFCJCGOmCUsfog/sruxt1Z0hRPpyj4ll/bx/dmf2nJ/tNlt7jle7MMQywN3DejHv+L0RREbdWrGNzhHY+yr4O12a327VRvocOr0Y//lI+Crmpai/lhVesPif9y/NRIOGSeiX5CvtOSqHMgf7XyFCBfeB+tSl/tSl/SZuSOU0GIrqQW2B0C19DJjKQhHGHDi7lG/jiGX5xc0KT7mfEyyLxjvz0ZZF+4nhwfNZfKGujrt/uQPmoIsbIG2oEIYUgSlmi8ok7dkcCki6xJf8adBel01MfpkYHT/u7T/t7u9gN8+DgZO9hJLmDg4O9vb1nhupJJvALkeSuOih/i9qUTuqaKnRm7Rils3maT4pa28n4rrEWIKxfA5MN/jT24I88YE77pZVxvANmkd3VU85H1zDn5dzSRXXctn+Fyvkw5MB96K3n3XqGJ7tfZ+UGtFGwd38qETZy+eLd5hGzridERezTPZGwPZav/6NBG8UU8CxhQ8wAMZrgxFTEejxInNLvGHs7akOQVf0Qu7FHCLFJ57WHY7u0E9KsEFyV+sxhlZZyHHmG8mab8RgGKu5p1sHvMToyHlXL2TVz/gUrE9gi4LGB9PoU8ejrWFe+1u+ELwiDs+6ZMaTFXhdGoqP1nw5v3iQXM6Wr21S85bM8gidOa4FWifaSAiNMKw/RZby+91/WGdCPomU77dZQgsr3ChtPn3qg8yovyQoYHESCR4MwYNtpbUUx9JhbFiMKbLrq3reJQpdkERQXtIui1p5xCjCBwvpMgOeb0OVnNBusHN2riG1dgMbKx67O27A94JrdZVqcsPl1CGBy/LOEHhJi4fzd4yZKpmD8IY8Ib7gFoZnDydeA0vIByeZG4whtJ9U/eOFXd9xU+p6g1tt3YcMCItejzyO1neyytG+8crHrFovw6s2IdNrjt0Z3Jl4nPHh4oQ6aJuLCiG1iQAC32MRrzsx19Jm83gPw+fo2d1ioRfJbhmJqPCluo4ZXilvxpzyKLgyryFo/8pvuytJb0I8YkDIFmYaaLK9nVOjOhQtK4ObZ8uCzpssZGTmGj9tglVrASUby6z3nEGa6rlKFWu5QfBMLPXs8XlccdhUWb51MM/JCVI7ZhThypHWpGerM7b4LrdMS3pO0ypQh5TFbYDbI4I5Ju63A2NERyw8Cggx/wMFNii/AmmYt9KCEuTrQA8D86rzvFEZ4damhIcc1Fo9qhze0c06ofh0hjqoxuQad+BiusxvqSzkeN9WQXlm5lsGIkvocu96neCeWcwEwsMEptu8DdFly+qkEPYT3mHkhaqcsLFAz9ToF3KolsnNmqrKCzUo68Jk8P9f/PaOQdWSf1qZn34CETkDp1oLUbW0MLos3v6kkss4QwL3IHtDdZu9LutQ96w8OKP69C37Lgxz3/cOjo8HxsYHz7P2SjepWOB+/fFcb2LYvIGo2HehbWB+sWPBHglBwVHsCgrhqkcKqxl9NLwczdZy7uG4rnHZ8W/90zsx8kRd/ZIA+WEIwQ0IM8uf88OrVOc82yAP8e4H8DKjvzd+Ol6pkdWRSUNDgqBE0oJBpWU4xCH7F32oy0S30z5F4wN4uLNplhuHqtnW01UgLGJzsfTsEQsTQ28mL4ofyXkCsHic1EcZeGjxA1xqSs/IO06miZre0mh2zy0gJhUhh6eKqUW4MCleEneoOQ+w9xT55qCb2tw8UG7CyXbAzBvGOtreba1aYBMUuWYjOWlFRpRkNsntH5XQ54yqSQgovNxiIq6RTYuvQpuM3bYg64Ym5MJbMU4uiB1kwgX2f4tTdyWWwGhb+lVSfRjQh0ymc7nYbRlm1i45mvt3LKoTZnW2AcS1GKVe3+YZQwaH5y5Jgyy2LDbe4Dfy1Veb2obBkONA9w6hR/CFtqNbgk//wmC2aEoQH24zOgiAM+BOBzvupeToRORujFMO5KdkZUxC9jbTSm1TKOcdKNbecw2kfZ40W9XS3E3i/BN0ROIU3WXWSvEVqZzKPHBoZK+yEdJwqGgt82ykVHzsg/t1tyYm4EZFEy4Sx0XCV1x8JJJ9yeI90HhbXCdv16H47uZxnI9I2xHnuUmgpBhHRh78XDjc8sH4tHahocVe6mhjuSoJ9OtOJlkVrr2G3IB4tSe6iGZIGAOvgnmeRjQ2GR5dR3xI1En4nThpTUQGpSY6GD6eEG7odj/8L/WHIf3giF5F7XcMj4XQhZirh6DhLzDf3l394jXWUnyjujz+oMhBxhYztymWmKP/w59sJhWhhFW+FbEKuurBG1z7t1zo1vADutHCr6+xzii/f/eadIQZ8xrcVKOMRlTMKOoQqdNPanRLYemOye26aa//LUijAR1m1UM5+5PuhA6s4NLz1mca4eIsFJHsDUsBdO+QOir4LvfYOszggJgnbv6bBS7mpUxhieu+PLw48pGhmEJDjJIbPg190Z69TuHhpewkDTjelA8d1fS+FpFoZpQlePhbZ5xGYbLUrjjTAhzEm1n0/14mgvNIErN38RzyMUyySAsG9KnBmdYhqjg66m6nvjWCZeTrFYlQuH24sl9syVeR9cl3SqyfovCDOmni+KlqYdtkePrSFWAse6m6PJ50EBQjvkTnzDAf5HCWTdCNFSst/+APqFfjxIfyY0FvSzx6T+TGOv7UjUntDopGU6vwFGH3Y1FC9+lBdfPlDru7gkN3L/6D9xVy0VTZJOS9JQUI9HIQzyKI1p6kjuaexYYPpmuOlaoiJvyxzeS9XXN19jFoJG8XpjcvREu0H3kcKIYc1vVbzsxdk7jgFPNmOHoZn7/uLIe+mI3/VqFOtZPw+rOyjgbSsm94/VV9iM5DwIsw/PNYgXYhOjEE7Gj01n07hCjZrOVXjp5ZZgno8EGgkd1XOyE9cdDzYG9yBfKANm68J88ekjC+hjNumNZe7jzDovCxwndH8+KTKTQzoLWY9TMd5GRdV5E1jBHBhgDiN6Gi9uJ9KHiZt9BxtxaU3tdDi9p06aa1R5AliZ+dx5FJ0PdHJ4sUL7iRmc1m56E6pprnAHRU3GheLkNjEf6lX4CaviBEMQy6jdsYU325F6hHN04hMpQpSskGnaTXJxFNcUaWO0GXc/MaBMSWhNl4xk8ixoGPp2K7KQHm5REpMu6GnZHX6Q88QWjZwMNNy3NhZ/9rs4i+Ul6wxa6FUJF9MguNSepMFdDnBIseSr6tSQXw5R7giPqbjeps4q4eA/ioTgnk60pfOSxmbZ6d2bZlfav27viqtSEB9klAcTsgpnWnRSrB4j4XbCev7NCMpLleMLImfm8JxbXZNltM2iLGGoFtTDjnxCUIZ8MJbwt43ookHz1e1xblO4uw0+iv46pV5OcH5yXWl+KZGrzHMBMewykGOZY7/j2R3j1ko3p6+eXGS8H9/d/r6w4selSrbP0R2eX970FK6q6wIa3scxsfb2zgMs3abNEXEWbKy/Mh8hpYR8zqfDElCDuv8x2y4uIXJgQc8ttyz00UWu4kw18ED5+ppA40bo/V2znrT04NpDRTDZDvTjQExmd84IlZ47+u8MDSomEfTCAo/RirN1weMmqe1lWxhYUdLd/2NWkOTpkX0+MlXO9ffJKcX54bBB260i6UI8CNuTKwPkIkRRJLCp86n6aRukg4FVRG4t/gHBBoj8hW+InRsV+1Ur4HLSECzz2wHdnkNaYCP8CkBedlsOmOVWnh2ycUWFWL3wzj4czUkUrUHxArxl4ntEXSdUTWO8grEBavpNUFHcjhBt2Hpk4BFwrjuOB87Wks+g07c0CbYRKNy7j8kcx7srs9Bx16PVbKJqgYWZxiV4bSwq2FbLsTuI1mfCWVECk+GM7idnJJ654tuj/dsbzasP+bzIQd23I85GUMnXQj76Uo2EAFyUjjGo14q2TvT7BMyAWn7s2arwMZajcH8Hy3Yn82xwd8cPlM9GvEEHtMBGpeejhSDd3atqPC38kDS2RPPA4k0kBRrzRxt3RWcalxTkKDsxzdTEdGIb4cgvMwnBUUIEYkdj/IGBFCGIicVG7FYUvM2uHoCSsmL8NQ+4ABeV+UdNlUiVYgpT990w5N8pcntMniupXZ0UsFZ1zETqWO9PmhUqGO1nC69E+SvWIm95+QVJtPsZoG4uvZhqHsuXrFiG+1SYChtaUMZifJdLVQ1zdCccDyH8zwLOfHzgB0EDyysiPKYKWcgWhUXGeOteo2dQhvPlJxgokrqzDKhvGnUdzToWcgD4IArmUBYIAebjr3uKORaG6GCjL0du/KiuMUzuSpX8X32iDADC0pVd+rFbDykzRC9CMd36fIY8P0eYVqu74dVOc169BUGt6S+MSVF1O/xB8LLivF5h4chf4jREwwDkrSETIgf0AsejhuB8urJdnJ+E34xmIn9vkFYbLGbkoFoQZilEh9re8hCrRx0Fh3gB2Pa6JWWU8LnsLzEvdSXiNtXuxvbVwqzw/Jayc2NM/gv4V7Ryh5LzYEzwzaQJB6u7vKJtlBSJmhZV6Kdua7YLpoR0nul4GJLieNCb8pCXScXN+gwBNiliNmyLjsCI3gPzvSJCpK87RD879NP6SVldlXDmnmu8ejaBeXnTIOZSZeAQggVUs6JBuCrm0yIKGWuEtaIuYv7ynule+0QVV2pQrZjmPaQ0LccRxUExtQtiusEJHvAi5R95ipsw4LYEAIMzeYcQ4DwjE+I247Skr56ceXZHem5H96/rp2+clRLcAlullO6BrdlbarVjUKbClFbvZUQ//rpLP0R3vdy/0lsh37qDRINwApCQYN388V3znvh1uTzZX3ryLGd5d9rnTxVRUiJxhEna0u4rZV4y1fovX5Dgn5Yp9gsDTw0JsNkEt+vdugbarlxd2CkvyL0nPor8lMHQQwY55UN2PW60I4LpeahJCvHJJG3bJT87orqycZiimNckQfaTqTTLcJG+KfqOOW1zpOb2ozKZZVORAzi63Z6CRGZUAvXq2/+BmfjfVp8TN6k8GeQD0F274mGBbHbsMuZ4WL4JHpWgDW6FBtDkhR+n7HBFdjL2SZ4W9OvmQLJ8G5kOMC4XUFdEbgL8twU/IhxXpw2/nBZMPxdUhpbnOrmoC9od+6WLSajT3twjpKc6xj/tBVrUU1gzSN10iJqpUOCnJU1iIB81CXEXLMQId7RBAd3JdZ1ah1Ld4EsZCNeq4Fy9enGOlTuOax1kWFeHBdAPE6h54wwERrzV1n/4QK/hT3vuN/6lHEFu0dS8TvMEkWo3lWCP0/r2+syrcbNgSQkQpzIGTXeoF97qsFp5kSvAtf6QsvUiIrbwNT+Fyu/m2VFcKVgtOj9jRsBa38XD9Z8oZiFR/Yog1f3FFyBeSJcrY/ZPXoceCYJncB1J94hNw0kfNsF+9QrxYJxZQCjfWUoMuvFCBQ8gywgKVxwNjCljVK0FTPcdwamGAhj3UnROQymWXvzv1cbWc+YtPUTPej4Y7e4mIhUD6JpyNd6nd6XSC+92TE9t/AlmZh3RYPmHxx5k1NL58qSm2MbEV4txpuAvlhKltI36/TffvXh/Lm3zrGWFHNlDkbDQpOfVi+v56g2iAWspIxuWoMfmLCXQwZwKl5004t0KSM8mOq2VLAbqes+pV4RwSgmy3w8zMeCZMiZSEjiViUxxeQlUzg6PmhyJE1YIi0YFKhAMIz3kfbX6ycmeWCmc90YemtVM1MdFZshGvBsAxNwAznhmoStDIzvN2/qD8ua9cS9Ur/P0sXo1sGhwhsiX4sceNGPqK3962tsheWigJb0tNSxU7il70Ffxkkw1D/4UZB348dpzgX3Ey1FwtF/lSa3VXbztUOyI3Bz2+GgCdMOQ2c7e7uDZzuD3Z3dZzuaXIWX6Sshfr+86V/nkz5b0P286Hss9WF/fwdEHAaPFl/3htdTMJR6qG6/7hVlibUzVe+bepSCzdFHhDFiS7/aSb9RR3BEfuAUMTVoEFjnj42xdf0dGj0oc9t/q+MQ2TrSXCMfCDoXAilMD0XLcxoezNrcCdsoQZBdusUhboDNY6/Zd0i6Y6wpplM4By+nTDtItUTxFnrWdz6czcaFJqUihoXvXuMC7JwJgZHvSUKpBSiDhNN61yGn6Q22JFLKosGn3Lh3mgktkXyrgt6sZE2DZU9DVYtomoUwlfA6hPfGsANLeO6mFbgiYfSMO2/Za8ciTAoMZIUZYrOdnIKI3gKpMEZhXql5owgcecUMjvhcERDXWPaVUkyaTGlS0eCOTvucLKEBVEkH78UVTbKFpD/AhyjhbghGgnK0bc4YAl5iqDVolWV/b2BA28lzZ16AdsJqYvKo+Ki0v8+P0N6f8QXusLliNyAlnQX2oo2UpIwuIDiIZfh0waQWCVeQ2Q20y9FDgh2IwW/eIdN6RR3Idg6nq5jR0B2zpy1RFNeF0zve6mL7bLJPI/PX8CBQXcrXj/IbLJfoE/79kf6SLVZE48LWzZcLt3r4G77JJtyOlpf0Mba2TiMLqO0sFdvj72bB7TJ5xzSGwkKMzqXgflAFgY1iUUDgUvuFZGvHvXTdiLbYaA+vQn+g75vSbHt5gcRxi/seI7tRDsqyrooR8xACfZ2X8+U0raSx53byu/IOCxa3OJgA//c2n9xynyEXTDAus9VTI6RJr1ol6ZtoCo6X0QYWHFuRewnCCnnxRMzfcOGiwrxR0FI6hNyP98o0yz5rzBNRY06rFd3SoyHrpF4zOdGF0RZtxU4ZS1cMAmG3Gpd0x+8YVCzFKbpDFB1hD2pkhHKVjMrQRXOIGqwFfo0y2yYjOOMUOPcyqZ6SoMMhJd+4B6ueTsvJ+tCLdSU6ijvCPmGUxF3Xp8sFKqaYlZ4iiL9Ga4icPhYd5o7hsGfBDY2PNsMQA5Vfs/6PZNd9MyHwcPMpx0PwEWh/0B2uW81EXcu32yqVEh246dOMLpIrcNLUhIgIZuQawjUqFsORlED+r//pKpr4LeNnbzLFcJcYKap4WgGcvJijz6jtl8mA8y3Y6KpICuDstkJvqIErts/UumWQ0GXcezl8iH55QVksK/lFRAnSqimeHLJDdOU87IijGStT9OEg4O2cNZm1YLkU9/1F2Z81c9UxubuxHLUtkqSuouNSUMMmrWwSjArMBT1kLjsXeeaMGHqRm3SUrdCsc3CUwa4gMAzB1kFxZp+alHqu/k0QOxnpDQ75+5aNNkStEMxonjvoKnqb+YJNyhtKBR0hX8QcPh1RDQxVfrvbJJP6AnXhkwU2jHiVF/dvzl40pTAr/qW0AApYJDoqILaTKEAXfqlIKEwAw7vWFu8z1QoEZmeAC7acEEpQFqG1iVTgrLRx1lkHc+QEDxECiHyKEf6TD2U+mg45Vz4c3QxxgpmzC4r4l9AMChMZTWvNBpAEDwKGb5E5zmbjGflKeo+E4ivmEKxNhDoc/TmVNHuPqyF0EFR9cy96OSW+AOyrQMQaRL1QBdgXD2qSyGBURMUIg1eB+fAggOpMMOOUHLMgFWhUHUCjGtQnfmxXiUT9liK95lCUUMwJlAB3Enr8pKfFcWwHhZyJXiafJGer8Lcuu6AxEDizNtHQnizTOuhleHF5RXLEgSnGmWqy4Ke/0w6QHFPnClI2NdruL9hsjfBnq2+WmY+gv0KXlLFJ7iqnXp+yh4ZRRU33P2YoAHaVZe2b17pMqShGSX9GQGfpdOFJf7wPUnvQG/KRR0g+TkTOR8IKcXxl0dbdoTBkYUsUF4H1F1pCjUQjeBc+rLLFpQlPNIThQCuoMvGOwmRuYwj7/XaJy6VPthEtBfio4THjwp14TiuaWZRDm1fjPkU1YH1vFnCT44B5Y3iTA4c4vD7IRfTECk0ZRRZf9pjKDOzikwGM+CB0HHoB8KiBQx1Ji9sQKt8mFAo6sapfb3MV0sGEPJVmTi2Ky+6I8EY3rN1K0xYbORvW2RNiirJC7vJFpAiUeE8xg8NtKDGC6ephl7UG2OkzLnH9DN/JClSKcB4JTAc3a4ZGLx1Y3LstQ/EAh4RPcpHVXFbjPtMCAV58zMcuRtvtVqKE4unuTxANclLpEuVhHtUrja0omDUwrazBTx5ZCyM8JT2qYtpgAuIYQmkYEFF1on8a5WspG1cpuYCCH11YGUoaWQqsZmL+SLFq8Cpx14PRSOKA0Ft02LeUcmshzyOXoOv8h89Vc851A8b1Z2GmMZywgkSwY6sQkqwfeCgQDqZHNcvkCcVCIpZzR3/RZkjApjwWt8vZdQEeZZ048U2ZKv27hkfvYye6dZ6N1d8oqEOYUd0IQzaVejj978RkNfXC1v5rBlfVr6RX5BZ8JXyOWxukuFhiCEYKLwJRtI6rdEJkK2OQwGEOdUt0eG4ZsPmSYq9WEF5cZNfVObxxLYPiTW7hTGErcjAVRkq6kD7T2JKLrXYVRD9GbDcIp+l0i39EORIOcdti7ScaI7/jxnLUttVGsGpDHSJj4+uRDZI3Ubj4iUNA4u29ueHXQGhNjtU2eL04aNMEU3UwW5wkuvNhASq5rni2ONvnIMgO767IcsYpLelWY3zgXvHlyWMEAtFfGEhPbpZ8+GTT2Z1rAhaNDMrGwmnqz7IZcZlJ8M7ntZlyUowrxppZN4WKMxp+A6ovF0WrVZwRVh70fCOdaXcRLYY3986PqXvN8tQXPPOy8nvc+55r7Utnm5A9auSRBt20kHy2SbJf5jUXrENia89QlT2qPR6R6kadKpJDp/XzYRLtQcUxLTEftXMbYB1H+LrlszVBviGaCorNAm3ivpOqRgNvsfG+xdU3t8wUylSctn6taThvIFwoieBRx10xHvXeVNyYDgLJG5DfU5f+JmsctVhEK3IIJRCLDwsZm7XSAhWqcqceQBiYCZRrHakbjSu86+UkiTTS9sYIV5J7J83CubiidZpN0tF9pP6S+80smJQFh2ufBSal1skwWi6KYdoWsuqOi8OOiau8LYROibUZs4B4piz2Ohd49RfylhzqzRfaS2dBCUe6BGSAJffNeE/0TJFTmX0WbgZTckAagUplMPMlwds47ikqG3wCpZ3+5rMYdphFyy3Iksk9IfJHzXvTfKLJb90Qkv8lhoaWE89jYzy1lo/WTrvbAnOB4HiHjtvQ5xOwSJtzaGxu6vSFuQqX+SLDawPy4J3w0JlSSPnpirSGUPuSoOYjSoDR2GIETUgKkMouabySeIAoBRqhAYZqC29NC4/R9dBYCWg3MpaqkAoKUbuyUH5DsC5TKj8pCzsaRbnYOrPZTI5x+1/LLyhSCzeb/S4p4+lcu8Ytwd80Mnytl9M5L6p8MqHs7Hp4Hh8YdHATjzQaw/pOo1kElk8h0dOKpAIhsPnL696xYM9M1MaMtYRREB2nUbro3NCiLETGUsKbOKXhTrewrNyTJCVNnfJF4MDtqyVYvGD1TbyEt9UdnvgpXjXnDQt0o4jRCUNl28lllrHM/l//UyVyE4s1gQGW19sgxHccxmxnovPZ4TO9MzjcPzpeg69CrjGqOkSo1RcMj3As5hUsKX3BNe1dObnwkCp2nFwlE7IUzlwPV+/eTktoRXQcuSd7Of2Ujpn88332qZwuaexLOP5Ztf50GdQzpsr6JOk9bV9zPrUVRrGL57zLSKlR/FY12QhixgHn+ToWR2i4PokczQXY1zIiuHAMVnpq9OnKqAVLLGZ7qSWdFnHI92JtxCPnIDDmRKUJY844k354vurV5oxCGFCbsLqD/AO8B+RsRDtfcjZcXPqArA1GYPMRhT5aNBEvbPWhTTaKH18nNkfKhTP8wMdPOJge/1g+DUNvERSez0iapOV2cr5g0njGArvKf/kRYuZqbA1jraroprbIGm349XqZTxd9OOs+Dmt9SgXkp4Vn5PHgDHbXa0kcTPOunmjOilWEO5tqyDsMR2OZqcyTY0FyJE7QY7JN1N+h1wTN92zVRK916FowjTdpAV+Zmahzo5pwgu9PVhxaRLieh3INJOHr0XPeJSXbli0E7PysviJckmnOhSmd2WHdtRaV2xsUNwZqIQHBlT5gwOvyKU/5/S8XnIjzyxCPzjejyj/v1ppz5eyx9y9fSZDrNgfbC1UNKBiPXNdk9ooBvaEoRi2NqQ4jVUA0UEIarwigkKmLmF1pmldRG3jE8BoZPWXTZu9MHt65gRwhoLALJ0gddknqljnUx/wGAXsUeUcB8xQPEHv0GxKCGE71yPaAIiXwnHRNTBXMurBh7KEudepEi9aEeAY3ylGa6mUtOaoySbcYW41//AUBp7XHbO4gFoJ1mLL/YkSztCAs+tMm7IzNg7xiaxSkMYuDzjvSUYgU3P0o3z1RKj+Q737Q392D/5vsPj05PDg5fGCjuv2nh892jxzfvZvAF/LdD7r47uFVRXNGaO8tqTZ8Ma/7sGiZPtEy3yMJva7sNBOm84fz4P+kzeueHTQZ7+Et+xsQgLt/OshcnMjrX6JRHfxQrIRmpXwpiOnY60WIzR/CdH8AL6Hc9tiwcIzJ1BJjon1+WJ/qfft4U4n0/m+A4v7gCCnufyVA/7dHgM637lhaaYJjM/p436dal2i7wQFcnkv6UvKavxT2FYwPFO8icHSAbQWzxI6ndZYfs4ySQYK34+IbUpmYr3IAavIpXBcnCnjCqUb1ir/IxoLrgwGpcwVxm+O95eHx3iNNOTF/8COWc4wLURPehCrjUNgJgwnaxSmBr2CP4DKBo4wWT1Zh1x0Wa1QwQ/YTrgjIiugjiJAhn1GcapFN77f/03+MdiE4PNzXZoiHmzZDxEUQMKBDIErNvvP+WuY5ju/bTCWRPlPwfyQaNeLr3SAD47yHnu7LUVp4njzCN8o/3uP2LfxnRLQ+RghydFK++a3O6aA1pzOOMGTKikhtq1rMT8ksrai+o3ZzkdDEcFEO+bi6aTkp0noEf9EXg7kzUbeiNk3z2nTdQ4GmCuc7y4QcvPxgxY43C8mUg0Ds30b3L7X5HMiWoOLi2gkSLHILo7Na1TOJZ/Uay1pcoCNVdqrfXyLGD67hKJM6PanoCJ7sE9GRpzs+8BVPf08Gl4DWs9k327Pyqx34Xy4oQ9v8w8cqhZVITY89cBMQooPBMSmpxV5ZSLHCzRFwoOVHGic6rfjlbJ7U92BGVhNMg8v8pEV3sQxDFIRld2kfmynm72K8X6vd+VeyhxQWi05wfRO597A3009Zs4+coZ7QFT29rjGYmdGG8doS4q4FTnZ12S4fCqYKSApGS0evvGMK7i5QaK+q9FGUhNVIC6dxcS9wd79lX3aLSNWZowxshonWUrajB7F5bcLoGdbEYAzJTSpHy5EzYd609/iyssgc6S/NvH9GXdu4/XOfyoLbc9qEBcsBdU3HNbTevSCz0CFT1xrtfRggXYvRLaah0bXH0JA3ikzQV7raCQcbCMJcaNbtSLyFoSAIeRvYpyY2lCYAOSCMf3/12rnRCClz0kTkc4PGgTaqyBYENr0Du685uJQiSUBBYP9VZlNtpp1pyiedzIycHYv2tu1vD545tUb/fBr+08esg3vJ/a/A1ukzsn57fjuX5CE9JHwEx0i/h/eqzXVgoLB+57DzO3vuOwfh3FiOPPdXjTwxTX8J0dMUwzK4W0JOOc7SqeacQPDOljMhYfZ45G33hL2OSe2ZSQ06vzNw39nt+o5+gzfBWNZcUAqnlJhhnNJ8gbnF+taESnQE3rdLlWls2OmHxx3PH2w/BXUhX+raJfxSR6/Awy/pFfi0v7eb7D47OTw6ORw0YieHK2MnR7vPdk2jwMNftFFglzdhgyWwECwhOwIkuz9bgET85Z83YsEP2TC0IMP+CwcLsF38T++nsKO6JydiNKv7Rfop7qPCf5+9uUzepp/ySerCLsZLbY0SdVAHGPRABzUczPErjsGrKNyf0WKaiivOIe97aZSMVdekfCim7sjn8kXWBN+mtoU7erL5dCtBBC6Tj6D1J72nQVVhHxf/+Hgfu8NnR96DXGU7Gyy7sh2LHYELBef2EzxOD483Mphx5iOBqWwzMew2S8Yj6XXO1mqXLj/lmHvxy7m5MUvdPGFZZJ/ngt3VmmVF9roeVPJIwyv3/UWUwN44s+ZBS0TNmR2nqijUoGGyV2MMGOHoZnCI+o4HtgNx23d2LMI2qoSBJITKsfk2urVcDJTwG6XX05h7tolz2AzJlZ4yKDP+4eOzy8ud318+sU2c/SzUeog7EKuO0L+gj7i3+uj9DTiJe6Zxd9i3ujnJeEVxz1GzWMKantYWh5HQdqPo5kMcfLUqP6L0dBeFeU2abdTPLq7YhrpN66E2wlbGyPj7Nny6vxlfc29jx+4XdDYDr+Ug9FoOnNfSOe1A/tDc//xfUIj1v6GKdQQ0/5kLhDzciTOXpPAYB8NuF8au2llJxMAxwDk3RO1Jj1wmxHn1yOcT3GG1LIpc+0EpgotWEbR9/+3pd+jO1tzKUqKbc+U7h3+0H2+7i6C/+UhunfcUpbLa9W4PV5ddAbIHxKLyaEIhwRtnJJiL5Xwbj+ZlJvAJVfnbszFLMG3zJZ1vSz6Rrl0Tqh3lcxTVte3m4WEaD/I+4XXcUlBxZSJkM2/uzy9P34CUn3Bhuu2eROJoNB2CHYYmxpC1DV3npkt7oK7oCpf2QP3TFS7tgesjo/8c/Lwu7cEKV9RP6lm3R+vGebrao9U+xVF/9GClq3nQ7Wwefklj+t1n6Gwe7J8cHj7U2TwKnM3DX9DZjDgFfxt+ZqMve6v1PBrb3qp2vbCZ+tmal8Zz+GXS7f8KndfB4WCl9/ov6SPBlv0jzHtvv7Xw+lB8uX1yhx8OMkjxVPuTBo51w6/GlSFrwdgUSjNuQ8ghKY0GbLjfBK7wSW+fDh/e3sod8K6vjLO1X8kXq7+Ca7HoX69/Vlav/Uq1XPuVdP2Dbte8FBzrH2+RwXT9hH5IV31F7jgLNqUjqDfYbPyVY7nRX7UeMVi/kYP1GzlYv5GDjTZysH4jB+s3crB+IwfrN3Kw4UYO1m/kgDfycM/Dwghw19cbLn1A+mykFaClEEyqH2+w23sYMguwfCu3fW/9tu+t3/a99du+t9G2763f9r312763ftv31m/73obbvrd+2/dU3vO+z7KmHqzdVsLzpLtJ02Bo7zV895V+t/XMXT5qYKlwxef64eCrr+WrcQmEIghslnp57ayamnqmu7FRbWef53llcXo5nNn9wa4+Ec2zcXpfDwWW3TBT5K9Dyde5T/nx8FuK00jtvDEB4BMChqA7MhzNw0/ElKj7s5Q+2dvV2ZJkfIP4HTU76OrN9C/7MKXT8Sf0bMdBdUmdvDH9/2TJ0tENo/z2YDFfVaD84QJS/5TWt2HhJ/pl+G/bWzTy1bsRf9UJczAcsV4fjEy3cCQyptN+7v+8J9+WuHeXdbw30IWwLQ3hRxKKFqVTuVyOYPVD9JjL8fBL7buvNQMlwZcXaoGuqH+wP1jMaFv27Ub1TT3AjiBewKuN7ygeYdqnHV6x0Y1+127gPpr4vIEYToWTY5am8QyzlWit3ZWlWLtZ+Ct6nvnU/F73d7+9kTuNf7s3i+/53n64v831iGz/AawRGpHMpG6njN/acR/pGM0Tsfc0kt1rPDc8HPsH7hd4OFbsX3hO9vVJ5gdY+ynnpOO3eGT+kZb5Nq1vaRxYhWe7R4dPx8ejNN2/fnYNrtTus+MsTZ8+2z1Mr0eDY5afFgguQo5dZRZ6hyJRc3KoDwbu231rs3uaerHe9VAb6Qs+dk+RZP3kwljjpp9d+Gsj2fpGJ1x54Lr319GpV39dpohFmSp+cI+HHzOQyn41h/JT3KzIT3fSEXGp0vmod3RCEkoeItVB6KPPwbnHeBou3DGqhf3DZ4OjXf6XWcXBU2PmciIure4by7WnsrtPrHzB1yIZQJkFLAJ3ApZXO9qPv9rdvC8dxneYIqfG3gR7O7sHO0z31J+Wk3KwPWf15ZUl6jpWpfO0om3KT/YOd48GT2UNRBF5jdnxsik6vKF+xq86Q37dCw9QgDEE/r85/s8qQx592jU+WTSaFMclfyqXzAF9jSgZdm1D7PxCeKIR9Ej/aRrUSZ2K0mEhoZ8rnZQ1mlf5yJVJUJDEWBISY8lPDo8GR79lW6n1+XCRfV40tr+9Gs2RsZRYtcyl+UCRIQmZLPcqytpPxXjgp3Qq106NlV1cdM1Z9n57Ta/lb9x+/Mbt/D0GnxZlf5RWi6/3DwdHHFp7iqGAIrtLbWAD/qj92vwfj11AQKw4V3ciVSlSS7Inf+jzlpAMfOZDMmZEjApykyA74J6RZkxnXPWptNdWqzS+QHbaP8KZfzp49vSwdbVh6lPurNO4zvDBa/9B5DzjTX2eYVFpgiQRVSat23zBdvEpK3LpOmUYrzyxXFQAdMi2FQJAXmCFBNjfVALg6uNiHQ4OdttyEOOUsnNNO/3U/T2yVIg1P5PbeEekCo6kAOt9a4IyyDUtDZ4+vkA/g4Tcffj67D971lofWBIQT+PG6sCTyODuWJxjKn9BJgTK7yVk3HHQy/PRjF1tpbyj9g6W3i+uFRsNFF22wYOXDd9lxaLtfcmiHbcWDcakfiHtVftO/xxbtQN3+xwFV9AhTNcJk1TT7DNnKcHJIxGLgEts8IsLevG7i/g523vIgg12YM3oPX4KRexWbPdor30NbbVfuGJX+uco5AhdGqFZraWjDLvFFO1HIvic2trQPWQcQIJXkbS0w09sCabI0xMzVxlnDGeZ1dvcxdYrb6xvus2mc4IbLInFdLbEhhU237+c/VT7wcqmcz+efsF2wMcRFWIqM0MVYizf2JYcwFf41osKucuuWYu4Et5WQ/rI8WWo1xy25xZzuw7zdcVFhXSPttjr3xLTa0vJx9in5Xp4UXg/gdg93tk90q/RBmzXn1rrf3DY3IDd9tqTP4N6fVlMufiD0wtxgxS+90G/R+xOXbdBTCZrA+6BRF9hBR4f7j1dawV2TaDLAEQn6E9ZinwH9vNNDT949cPj/eNNjD90qdcaf4Pdp8/AUPprrL9Bt/l34I0zLlSnVRfbmZQN6TNQZ30uucfP99VwlF/O0jn97iBqUIKxznOHYylaJT85kh2wYRD34fFvOVtHz9ePVTpY6zA/weOBwQydSipRuD73dXaWBLqvsxyWaZH1VTn1zTikB3Dnb7BdM9YH9HMlFPnFbGJ6uf0Vdwk/fugVGhysu0LHa69Q47k/68159hPfnN1fb86/h5sD4w3o5kQvDn5KB3jze3O07tqsjz+ET/2Zbs2GgYbNb8zxs7/qxuz+m74x//IHHe8sGGMIzrstq354ZGL2FyzuRVUmp/T9B5heq/XG4dGzDUyv9rP/ZW/BRuG248HB091fDa5/F2oDZ7DpTcKQ2gMv0WC1/wKXaL3xFT7257S9DnZ/yjs0eLa/6R1iyMFqwXa8kVzDQqG9tAkb3GSz6BUHT4+jq9yQel8sUHBgF6oZUkZB4aT/+KsU+VcqRQh00nyQ4nQa+2n3MlbLaPK7VyaeqEhdG2aMBfV3UZS4LuMBx9U0w7asGFVEJOo6Gi9LB2CbX5exANh2chqkVlxjMXgmP1LzftR9YkGkRRRNK4uMGSC3bb3LOP/EFBNf96hfGhyxWVb2qM9f9nUPBWM/neaT4iTBRplZ9dveN1/ldASSuhp52mIqqqu26ed0LZFbudwZHB8d7D09PB70kARgcft17whEX3KbYVfTr3v7R/APGu2a4mJf9+DfVICFpRTMVvh1r/EHmMEOTwH+A14A3+P24Juzd28uXr+4epE8f3f24c2Lt1enV+fv3m4lF+++f/H+5YfXyNO6lVye47eowe3Ld++Tt+/e9s/ePX/x/lKqaM5vcBX/+Z/+OzOAY6VilWeENkImGMn4Y9EHfw3DvHOU4Eg4pE2Eyhn85TYraowoO4bnnb/HNMUwH399tOeomm9KfLneN3IWYI5Iu0xVKC6OfJNTXcQizZEOYbLMx8TzEhv5YHC8OzhujQ7n0PTAXEO5pkfPpVdwRibufRKW/MVmcfjsYLDfmsWZq05xOQtin4A/TQM+uBqfGHY66HjIYNOHNF89kTq2jR+1e9T9KO3Ah+y9oFeJAXXjcZ/Fx83MflDr3fZ8pd6PWKA2PgpI5u2OmJFfyWw5uoVHjW4LqoatF8ubGzwKWLBa/L0H3V9poVOLutziWJqPFQlJnOICPPEyi8q3BTiwsDLPZAMw8m95tr3s25b7/+Hy6t2b5OX5i9fPL+lyn717ewVyYCu5Ov3ju7fv3vwpuXrx/s1lcvr2efLh0t95J7VbQlvJYMtK6N0fz7KUq9v8JOmTLeFCMcJcc3B0055shftZO75nyRhREniMNXlanbdQwsQwX+xOmLz36dnZi8tLetn3717Ti/MayNt94Jbp7QNyuHuw+/TgP4sV0twwTpTw1b+PIVFsRlvJyiKJbTSK6F3ReJFczV16v+WK+JkbHevwAjpVU6uI11f6PdC4XFqIZ/CebC7Xo5qZfnlR8Xi/xC5Mn1M8VtJjx5+2NJmhxquYFxfO2ZYjD3C5psYbM1md+Zl067vRZk+oY9228u9rTgzmlf2dCl3zlrKZ71+8Pj/9FhTU5YeLi3fvr/SEcvLQlgVLdmvOBY0/lJJTXHUpdyi9WO+09trcepuIJDHh05maItXvcEsY3CCyL5aV144wMUx04sHDFR9n0xzJBSco2ORSI1lSLs0ZRmXFQ8WucnBJXXLPLkVKBEO+8DJoZdRQVsqjRzqnT60I0cgxxbRWZMsfCYa76ZeF9dILzMdeVHyX14jk5f4bT6I/1zbum8yImbvPXJPT1b95jw1Q+Xqt/uJzx4txmU1hX1Z/+yXWxXLyc/UXzzGhapalsaNpUpXXy1o60vZhvfIf5XgI+SZXusaX7TkWZPqxzai/v8Ra46w/z0cfs45FfzED22r17N9SG7jV37m4BQmw+iuXH+GtV3/lw/vXq79wCmZYufor36H1vforL7Q/MiGxV3/3rJyWFS/gmhVg9mHpj2E2hHFhvOUXlPFOduDkoiQOu155vOuT0MxpN+ikUntpS55id4hxQyv3sRiCq563UM9Ir4xRVda1rbVjevbiniEcWDsowOXEk1hN75lzVJ6Mmgt0g743NylynOD+JoYS7eLd5VVy9aeLF2yEiF1y/qLDEgHRnmOUKVvE6KCt0Z5nxowQkKjrPr6QiyZdNJzpIcaUFFM42kpUdfQsvH/SxJDq6xuylEhW1AzAFnTy5ZCUQmh267pkF8m4uLaDXosIz/eFp5jEOHxSgyRVUE/c3k9tkNaQyhQMS/3th/PXzxPQtuQknr4Gv/Cqaxe4i0Qy11M7ap3aRMrEog+nfWFPMdhPEBWuUxJ19exqncT7xVbLvLzLKrB8WInK4Xrz4fXV+evzt68+wIu8f3H6/E+hzqyFm2J6H4k8mBMi6GGkOIX51szKnwT1B2iv3BM3MC44wYpv2AigzQysD1OJ3rI4ECvszAteHGNpOYoTE8WwwCtUwjx76fwtjkETLaS2WN/BBrkfJbi1xIPqz8IV2c2XV6ffwkJetdYPW8JQ09HkJp3hFsJ/S7jJzosQ0PhQ8Xa2nEU0li3OiXcCOTXu24t1d7cNU7vNJmV9XxOXTMyTeleAB/aqvOSv8CK+W1auYJiOCN+Sw93drd3d3cQXe/O52dIWJ369asLsigN29uYSSZpRsd8hSRAYfVr7Tt1cPyL5CjHhguDlssYtS3S0ZVrpUmm8EidQKyyEboGtuU01U6WZ+QTjDmCeTG4bIZSF9sL+w2ljPBDM93Vea18pbcJKRnGcuGvvKTGuU+sUJXb4lX/93yD/um5xq6eRo+YiJhfzUFgy1zppXqLAwBuTiurCZyWPlYUVeyhzP0DtWYVRgCckYULhrZBFjQqk2M+YuVcJKRf2AXmTSksU12zYNVPyHa+xYymJJng/ZkavU9A7J8lbuJc59UuxfAjOSkLftsC3RTp34d7BD+5uS2QcYApyN2HSQ3n9kXwq4fEh1h6kXYezhI3VR6AMLsE2IHIKspF8WzOQv1PSF4nkFcZ2LZ3OXNyVrnceqwFsgpROLDkymjJuQfjsKgOXHZI97gVYAmFfQvyeLqO+pQq7eBuWZifzxsNBNjGn6eP/Qn8Y8h+eBM1s4ZFwurDzH9oS1AkGJ/3m/vIPrxPqVVxpC2kwCKpCxsYEBWm0KQpy/LlETWYYM+d+UXLVfR9CJaBqnRpeAHdamKtIYiLdbx7pPuOf8W0Frh7Ma2ENhrBfnzTEbHbDvfa/pA1HS1MCOxwUEPI/bS/ErEV5YTpbSmnTDUgBd+2S8+fxXaTX3iGtTGUnGJpIg5dyUydmpem9P7448JD9B9PTXAoAfW3whYB1o883DeX9Enqle7h9tGWaenb1W+djkX0eZdmYTTnqg6A2DeVViBQZ7hre1Qkb8vD827LKf8TDOMXOTyC41/Y5koOlmiMQsqJGMCRc1hrVEm2zWixG5XKra1mnXG7LVG3Afl3SqyfIU5QVY+0bTunY6EMPVj7U3R4WWHjeQAEu0PpXii8c5LMnojLL6UYKLw/tubBZ7m8fwo9TF/Rgmjh7HjYekbpm2f5cGOdD4lLPmGnVxZc/5OoODtm9/A8GONlHrrIJKbKJdImWw4F9smzbheA6BHzivhde3RQT1JqbA8dKkt99jPa7WhaOy9GSqhBpH6kGOpiVNVRKbn9s7ji+uvZqDM/e9xdD3k2kvyp8T1IhoSMydt++zPEo0rJuev9UfYnN0Gq+63MDoGC5naYy0/lPMR4aPPB07DhyVS75sDZ1uc5r3X90doqOB/vyjEA+0IbZ/hKbShnfRbWrIzMxLmJL0mRZ4DqX7BewchMDmrMCFQU5o6KKuLao0wg2oYm3D8W8t3TVwO7uiwV3ivZZJ2kDQpGGDS20uH2nkIHWKPIEsbPz67iejq0nBqF48YI7iY5w2AZQNQ1F64TyBBeLyPqDFoI3eYURWQR6FKNYz9CDCPmuW000TyMyFcVqRjboFBs+SiwjsiGqeJVAsnFgcPkoEzX3BW94pYWZNZ/k5FbgS67oGmvkUkChmIYBNOltzwaONIXpOETcKJmjgWguhLMWelDyxYQ4VhoXZkFb7mCRB5FFFo+9007DfA2syJ9KNLguX7xTrVV7q0e7PXH0xM5T1Ur3pYzNs1O7tswvtf5dyKTdt+4kITY+zv950yKgpvVNt9FjYaLIJmuv4xk2TTrIPokehs4lleV0i4gOtjEE3Zpy+RafIJQBL7wlbFo+tzoLq9pyDZAC/RV89cq8HKeA9LoSyyHRmfIZTOAYVjnIMeUyHbPs7lFqbfj29M2LE06zDb87ff3hRQ8f3LN/iOyya1YR2+iIFWFtjzZ/975vTbFJGGbtNmkwiRs1leVHpgcmQmoZ8TqfDLm6D1NMw8UtTA774vp0BbMQxue6SbuaZhMpvlLGjUGbNbDe9PQgJSqKYWkzz82A8xtHKgzvfQ1izdMlYETf99mgztAcDV8fMGqeVtudyDRCpqW7/sZ2rg7u6eMnX+1cfxO0waYb7WIp1/fMRxs1JtYHyMQIIknh7vLNNJ3UQv/rOd2Jx1vkPqUP4Q+f0iovlzWD32qnmWPPJaNPUBp9VFrjYHk1zYDBXxfhU7wCvLSNlIdKLTy75GJrXzGzH8bBn6shkao90BHiRteZOIzzCsQFq+k1QUdyOEG3YTpA+qY10ynj4pGfCFKHqLihTbDdnjVrFD6SuTwRYoFiJ69nZBVzD93G7tJx64zgs0o2UdXA4gyjMtwvSWMHIPTE7iNZn1FUEQPdNUjVMeIoUb0r3ag/3rO92bD+mM+HHNhxP2aqVjrpkvujK2nfy4cyOcajXirZO9KgGVVUhr5MM7UYrhU2rxdC5BvQ9GU1h89Uj0Y8gcd0gMal27dmEomTspV4WOjDPUE6d+46npuOYtIc/QpONa4pSFD245t946IR3w5BeJlPCooQYmIsHuXlnZZgutfqsC5sIxaUfserR5K7brUhfMABvK7KOwRSCYlElv1oec0R+In/f3K7DJ4rlyMUus66jplIHev1QaNCHavldCnSVIkzwk0AyCtMptnNgtI7rcOAKC6JV6zYRrsUGEpb2lAGT+FCCdlBE8zQnBDCfTC1sSk9N6zg85fbjgR0YHPEjUguOXdWeu8iYxBdr7FTaOMZECXir+GwYoE/mLDLaebhd9G0t2LRUpEgNUKTTfNtOOOGsKtsJAn8rrwobvFMrspVfJ89+pSx2z/WHhcRvZiNh7QZohfh+C5dHgO+zwwA1/fDqpxmPfqKNHJn7mlsIUcR9Xv8AX+EX/X9JsgfYtp+7kQpaQmZkFAMBA/XFupPtpPzm/CLwUzs99EUQZY0+Cdj47RJCy0jShoJHWgonp1FOa0c00avtJxyEtW1cNeXiNtXm3Ro0GYj3DMCsWCSm9NqGlRNrDgp0u9Q8DizTIEvV3c5to69Bn18vyUrLevK1B0V20XwczyBqwQXW0ocF3pTFuo6ubhBhyHALkXMlnXZERjBe3CG36vRVrsZgv99+im9pIIF1bBmnms8uiRwSHAG51rp4dlrhWibrhV3K2DReJMtOFUnc9Xio+hmP1vXJqSRKmQ7ppxrwELiqIlQtLpFcVgW2QNeJK54oUYOHUKATpvkGMyedE2I3FNe0lcvrhAlAMKHpA48l3rdqb5S7fEYLgHCN6j5UMkh8pZCQ0ZVUi5bzDR/Okt/hPe93H8S26GfeoNEA7CCkP3v3c0X3znvhahUtufL+lbFROos/17r5KkqQtAjR5ysLeG2VuItX6H3+g0J+mGdYldd8NBo8YaM9fhqh76hlptr2MP9aFwPMP6pxJtCTKiCRhyRuhwoMtQ4DyVZOQR6FGhP4bC/uyKqv7GY4hhX5IG2kwsG4SLVPf9UHSdYHplnTiilUbms0omIQXzdTi8hIhMw+2w9XDob79PiY/IGu1uBfAiye080LAhaJHU5M1wMn0TPCrBGl2JjSJLC73Py4Rzt5WyxCYoB/l8+VhxIhncjwwHG7QrqisBdkOem7YQwzptKg5Nlgc1lxprS2OJUNwd9QbsvKYQoJqNpFEY5SnKuXwQBtbZYi2oCax6pkxZRKx0S5KysQQTkoy4h5toKcctV2wW48uvUOpbuAlnIhrKeteXq0411qNxzWOsiw7w4LoB4nNyZ1Ti5ll1MzV+ZG17gt7DnHfdbnzKuYPdIKhIrk23qrQarSPDnrrqjMZCERGC7x9j2GPaXfq1eP3XpUdGr3ZEFKN2MitvA1P4XK7+bZUVlB8Fo0fsbNwLW/i4erPlCMQuP1IJMBVcobv5jdo8eB55JQieQ5DAOuaGXSxOhBQ+eeqUFLuRl5Ddk4chQZNaLESh4BllAUrjgbGBKG6VoK2a47wxMMRDGupNBI+O1N/97tZH1jH3LYfdG5+JsC9YZrFxSPYimIV9LyME2PKbnFr4kE/OuKEVrTW4jdaeWzlWBUdt0dEvW5flzWS3Gm4C+WEqW0pn85tuvPpw/99Y5VlVgrszBaFho8tPq5TU1RaQKvJIyumkNfmDCXg4ZwKl40U0v0qWM8GCq21LBbvjmU+oVEYwCSxaH+ViQDGjo4EZL762qWrI9h+EIbjOmjqQJS6QFF/coEIzKTfG99PqJSR6Y6dLSsBCfyd7/qNgM0YBnG5iAG8gJ6cK4JjC+37yp0glRE5LU6mYxunVwqPCGyNciB170I2pr//oaW2G5KKAlPS117BRu+W6SKQXW6Lg2fhTk3fhxmnPB/URLEYduFylhDdi2Y+Wm3lVIZo5ces92Brs7u892NLmK9OQk/PE/ypv+dT7pswXdzwvD7H3Y398BEYfBo8XXveH1FAwlh6UFJQIOQ++bGltMjrnWhUo3sNqN7ZIR+YFTxNSgQWCdPzbGRBK4KtkOg8Aw6K+zCWxclSr8uLpp6sqyMD0kAfAu+2Oz3AnbKEGQ3RURqXnsNfsOSXeqQo3oFM7ByymTmFBbFG+hZ33nw9lsXGhSKmJYEFQ7DLBzJgRGvicJpRagDBJO612HnKY32JJIKYsGn3IjBIMNLZF8qzKhC80qtabBsqehqkU0zUKYSngdvCIUdmAJ3yeLPXBFwugZZaqCa8cijN1bzRwyxAbh03WJZS1jFOaVmjeKwJFXzOCIzxUBAWuOZTL4amRKk4rGOso+J0toAFXSwXs9p8FkC298xZdiJEyJtcZJVT1RzarqJIpN298bGNB24rqconZaTMWj4qPS/j4/QpmB4wvcYXPFbkBKOgvsRRspSRldwNB/TeItDQLGiTuXmbKZ3UC7HD0k2HGZLVp3CGOZmogRB7Kdw2m3L3aVNCanBApEoiiaOjaOt7rYPpvs08j8NTwI1LXu60f5DbZ46xP+/ZH+ki3WnPD9i/ly4VYPf2PaDXO4nXqT0roGtk4jC6itohXb4+8mXuJsLDumMRQWYnQuBfdD1SBFgAICl9ovJFs77qXrRrTFRnt4FfoDfd+UZtvLi5sc+Xx7jOxGOSjLuipGzEMI9HVezpfTFBNuoGzAUv5deYdFDVscTID/e5tPbhGnaoIJxmW2egqE6Sirii/RFFfSWAA2sODYitxLEFb1reDxUHzkyHSiMG8UtJQOIffjvVYBss8a80TUmBPTwS898XOo1GsmJ7ow2qKt2Clj6YpBINecSNWSQcVSnKI7RNER9kCHgOQqGZWhi+YQNdhF+TXKbJuM4IxT4NzLpHovfUkWM3/DqqfYenZt6MW6Eh3FHd7nXWjfa9vZZ1WcpJ5iVnqKIP4arSFy+lh0mDuGw54FNzQ+mnSRqlX/R7Lrvq0veLj51BX3MwuGVHRFpTu8/G2VSokO3HRkrkgMb4+mJpSug+Y0pPYbw5F0O/1f/9PRNPBbxs/eZIrhLjFSVPG0Ajh5MUefUSulyIDjjA2dnMKnAM5uK/SGGrhi+8waxQAsE0joMu69HD5Ev7ygLJaV/CKiBGnVFE8O2SG6kutLHIhUM1am6MNBwNs5ay7a7yx4jMndjeWomyhF1KiuouNScGNQqWwSjArMBT1kbjAt8swZMb51RbdmnYOjDHYFgWEItg6KM/tkFJO1abcUsZOR3uCQvyX/8CFqhWBG89zNdvEa9+K8oZTuEfJFzGFtQFNW5jbJpL5AXfhkgQ0jXuXF/ZuzF00pzIp/yeGjVGJFxaoKiO0kCtCFXyoSSmqDa4v3mWoFAskbKSxEoSOL0NpE6mWcuI4+3lkHc+QEDxECiHyKEf6TDyX22eZc+XB0MyTmLGcXFPEvoRkUJjKa1poNIAkeBAxf6vON3mngGUnO8jozSCi+Yg7B2kSoKzmS8bgaQgdB1Tf3opdTtK+IJyzBTuXcqDzAvnhQk0QGoyKqBRBdA2PFgwCqk8j7k2MWpAKNqgNoFD64WdlAY7tKJByuB+4PFXOPvZhAUUIxJ1ACzC/2+ElPi+OkAJj7vLXQoifJ2Sr8rcsuaAwEzqxNNLQnS56ouwwvLq9IjjgwxThTTRb89He5QMo5ps4VpGxqtN1fsNka4U8X94zMR9BfoUvK2CR3lVOvT9lDw6iipvsfMxSgRpeFtG9e6zKlohgl/RkBnaVTRkAtcvQFnQ9Se9AbKHtvptuNeWmRkDasEMdXFm3dHQpDFrafynwcWn+hJdQkn0ke+7DKFpcmPNEQhgOtoMrEOwqTuY0h7PfbJS6XPtmG4TTwBObhMePCnXhOK5pZlEObV+M+RTVgfW8WcJPjgHljeJMDhzi8PshF9MQKTRlFFl/2mMoM7OKTAYz4IHQcegHwqIFDJRkE+jWEygeTc5V8ThaqX29zFdI6lDyVZk4tisvuiPBGN8yWRCStYiNnwzp7QkxRVshdvogUgaYfSZtlswSZ3BKMYLp62GWtAXb6jEtcP8N3sgKVIpxHAtPBzZqh0UsHFvduy1sMQmkFHxRadO8+0wIBXnzMxy5G20+iKzBorcDqICeVLlEe5lG90tiKglkD08oa/OSRtTDCROTkxLTBBMQxhAjAjKs60T+N8jVpIZSSCyj40YWVoaSRpcBqJuZPqtyR5lXirgejkcQBobfosG8p5dZCnkcuQdf5D5+r5pwWhtD6szDTGE5YQSLYsVUISdYPPBQIB5LOAmkjmTyhWEjEcn4Sn2MzJGBTHovb5ey6AI+yTpz4pkyV/l3Do/exE63nWdlS7/KPyAhWL3Z7MUegUWOHyKO6EZls6vnwjb4TK9aUEFuTsBlvVVeT3hppRuAb8DnudpD1YiHi2LGuqS690aEvSKtuiVrH1XKCgO8t6O6SyD6I2qSsNrmpQT1nckfKAyNZ5HMqspTUI32m4SYXbu2qkX6McG+QV9PpFv+I0iYc9bb12080bH6XUWTrlmwnE9Qyvad0bHw9MkvyJjCXeHkUFIkX+uaGXwPRNjkW4OCN4zhOE1/VQXZxkujOhzWp5M3i2eIEoEMlOwi8gs0ZurSki650KAQ5Tx4rTaFg68nzkg+fbDq7c83Jot1BCVo4Tf1ZBvvvWTp8qpv2U+0thp9Zz4XqNRquBGo0F1irVcIRfB5UfyPDaXcRjYg39573tdesWH3BMy8rv8e977n8vnTmCpmoRkRpHE5ry2eb5P9lXnOBPyS2HA2126PaQxSZtEq1kxw6LakP82oPqpdpSf6o6dvA78wVOLLlEzhBCiKaHYrNAs3kvhO0RilvsT2/xQU5WHcl/dfCkrYWkeN64UJ5BQ9E7gr7qEOn4ob/xcssbeEsjQcqtoii5KhKIBYfFkU2a6U1K1T4jo2aKVYT6Ns6Ukoa14HXywnP32EpGvYJF5d7v80ivLjIdZpN0tF9pCSTLH8Ss2JWtc8CN/PUyTCALgpr2pZ2nR0Xh30V3wNUGJZYmzExCBOQouBhR3SBV38hb8nR33whr5QvKAdJl4BssuS+GQKKninyM7PPQtdgqhBII1D1DCbDJJ4bh0JFZYPPqbQz4nwWg/IdMuaCxJncE0TNuFQ4zSeaD9cNIflfYrRoOfHUNsZ5a7lt7Uy8rTkXVI738ahUP8knYKQ259DY3NTpC3MVsIcTXhuQB+9YAWamOlJ+uiLTQbJFlDgfUcKQxhbDKhi45lnl8sgruQiIZaARLWD0tlDZtCAaXQ+NVYV2g2UzZqTHqLWrFOU3BOsypYqUsrCjUeCLrTOb4OSwt/+1/IKCt3Cz2RWTyp7OtWvcEvxNI+nXejmd86LKJxNK2K5H7PGBYYZoBz4aw/pOo4kFlk8h99OKPAOBsvnL696xYGdN1MaMtYRREB2nkS+YI4VkGUs5cKKxgzvdgrdi3wQKY4CmTvkicCz31RIsXrD6Jl7C24IPzwUVL6TzhgV6VkTyhNGz7eQyy1hm/6//qRK5Cc+awADLa6KQdrCznYnOZ4fP9M7gcP/oeA3kCunHqBAR0VdfMDzxx99QcIgyGlzm3pWmCw+pwsnJVTJRTOEO9Aj27u20HFfE0JF7/pfTT+k4pX19n30qp0sa+xKOf1atP10GCO2Jzz2TX3M+tRVGsYvnvMtI9VH8VjUJCmLGAaf+OhZHmLk+iRzNBevXMiK4loxJVA27xopABkssJoCpJcMW8dH3FL/ZpCJpnIPAmBOVJiQ644xC4akphLVppBAZxGy6KHtWsIvQEOUEaRzRzpc0DtebPiCRg0HZfETRkBZzxAtbkGjzj+LH14lNm3ItDT/w8ROOr8c/lk/DaFwEmOeTlCaPSRyxeMwEHuzIAORHCKOrsYG5taqim9rib7QRWaKo7cNZ96FZ61MqRj8tPEmPx2uwu15LLmGKHXXixFVqxSronU21Hsr/vFhmKvPkWJAciXP2mARUDv/1udfE0fdsIUWvdehayI03ji07OvEqm+D7kxWHFhGu56FcA8kBe0Cdd0nJtmULAR7tfEW4JNOca1U6E8a6ay12tzcobgz6QmKEK33AgOrlU57y+18uODfnlyEesG8Gmn/erTXnytlj71++kiDXbQ62F6qa27zwYHbNb68Y0BuKYtTSmOowUlFEAzik8YoAHZm6iNmVZn4VyIFHjMils0g+5SR5Z1Lzzg3kCAGFXThn6uBMUsrMoT6mPAgIpcg7CsioeIDYo9+QEMRwqge7B6wpgeeka2IKY9aFDWMPddlUJ1q0TMSTulHa0hQ0axVSlUkGxthq/OMvCDitPWZzh7oQ+MOU/RcjmmmPMQ0xbSLR2DzIK7ZGQRqzOOi8Ix21ScHdl/7VkoeUJmrEsizNFZHHGZvwUVu/w+0jabmG91UbK+7t7g36u3vwf5PdpyeHhyeDvV7YXfe4ow2g9oT7+ujw8Dcyh6/d07HdG1I9gGXVv5sjJflvpRXdHnwg4K9+6xsD+SkW/SM8pA+eX2maGj496WH0VZvK2aZx8N+0C43miFVGbNwoH3QK2OxuAvKOKUakA6L/0VHwKUlG+zE2UETg5DDoUKf99xpja3c7/XH4qb6GfLp3bKZLF3aINw6JPMKH7A98S75h4xdSK2SGDb7tqcmRcCXy7WMm8C6yqbaiZACIfg0bNMLuVOlQvmaaD+LOCRk4gpSlxx/xf//K+P1vj/Gb2lDGGnbG2j8e+PaP38rXgv6PsXGinSCfwv18nqEpC+o9BbcP6/tdEmLLNW4zGlpUr23kGGTgBN0Q468fHB49PWD++kEHL2A7/UgauMQoBNh4OB+nrj1QX2BVAtYhBSF4IqkvSEX5Rpkab7PA3nIFvvK4BsZt9S/xT78TpDGre7GAeRLkDKVgzM9jlhj++LLkVgcIW+MROr5InS0pmeUmHAMVom3ui3TiVdeDCPmhr1hLnt/DwYObfVkuK3SiXmAcluzwk+ZmIZJfT+aNaxRW0w/Z2hAk8DxHqCXODif8SXlYXGVSbLvsnM4ZCMH24ybT4PIkYoWnVoGfF20cTICcQPghL6tEzNQKfJ5/yqNGJ/+yMRUURTAajiQHgoqwAmm5fhRvbzWP14pDtZKTuk1IFjvnwRBKTm3y+jgSRgi4RnIOXjGml00fJOJyXDGmQmJ1cb/lkix+t1KRANyCD/kmtPYlausFXKjee/BVUGYXXEKFPpWEu6+Iom82+eHiDwvKGIRWYFq6AKYEWnkqfgIbjIl1qqevOET2qsrHMnNN2SCpejWPev30Kly8REyf6oCcvhdxLgG2f0jHFHajovqVM9L9oT5oMhGKxRbjcoZMFqxxv+vgz6YoBcN+NDs88+StZ7xEIOHlhmRMA1cLqnT9ZsvKYj6JgqugGorkmR5pUsmxfGz3gEE9Vj6SbRWHKF9Fl9i9oapPEA/sXU7pIuED7f6McEpgXuWW3I44wrqYqsOXUPPDnD1ViHIAJFnq+XI3eq8Ay84DKUyRIn4e7Kp0/mLlkSfZUCcRbbTfjsCYB8uQIBXx+iqLEkdnKt+/KnenYlqW8463oIAv9o702pREmqsidjJvI2OArwPSFSIra160AhlGAKFuwiaa/BMxq7uujp+e5OBc3VJ4dbp+r7vlzAL8vZwxXEWVKfQSxOSZMd6k802xwyH+QrhMmOeYnQGx3PFrXmxFt7mbc3mDPgRzzTcFTB+/dB+C4H26qYWb6pctC8l+PVwVf/ijOwFioyD/RXUfV1nBTzvWgVOY9T/k2hllM1NCTF6yp4Ir4r0y+bOSnLvEo6sKchyR2HHtvzK5UM8XVG48EYX62bVltzBHjCxyoATEB3it4ewsC/2hLQCiUpiOZexYwuVc2Pq6T6O/QCoTYN7XVfkR1qoZ5AxMB1ts2HXTUUthzZTlzPPYALZojf5eG7nl0s6AtqaXjEV6i03PIdO7lArkyUvjuyKijtSBJchROPfah2O1vrJq8gFyEf7eJX5IfqGvslSFSiRi4H5zprRHqeg6uCxy5WkTK84IpKoI8fO1U7NrSgMSRQhHxZmj1MQ3VFcy2afLS5DBSLnk8Ub7wPvGGdmWY2nW23Qbp7D52kPTflTTGvMoCWRb+K/csofrG1JhttDkSuN2/ZWP03zKPOXeHw2K8Y1GH6VB9aw5/lvSlUilBIOz6O3WjmyKvTn5SlC6khjvq1xxTozkU+Ei2Qr6vWheaUsW3KioHRlbtHBng9NA6ldOhKABl1OpUCJt3fvq655cgLzWW0N8jN32cWsO3YfRgQqNiN/AeG094Q3CY5Ln2SdMlNTUzpkTJ4G9or1VKWWKzEP0Vz6mnLeuPy7KucqAWabssGg+OKNHEz4RWMbKOcpWqoFFhJL+NmhAiuE8Gp/xkoNPwOnvT//4hVJgq+HrOXnsUkqm381PNheyWFShc6dVe6710fnCL8zPMQ35RUMQws5PsH2pYCBZ9qIjhXvjCDe8Abz2MaKfxWzn8Zx1wzMgbA34tutF3itK9Et8i/gnkLVn4hetaaEwJIp5qDGu5ZrF4IJ+iewAM+vTf8VK2LkxspI2CQR+9/eXzBrK6DuhunBTk2Mn5efhTdhoKiaqwaUIZIRln+dUiaBytKHjjBCcTvE+iGlHO7PRY3EYWHo8YzZm2mJfczlSE0GV5V+LWIBfEemqTt7yTSi+1i2kxinUPVv5FiEgWqxtdS3vFXvroCnMrW/Rw8HoykNtCwOYF8kkfiVGe6FhxhUuXauj0sooZKOcF+N8HeGBDR0ZH0AWUe/dgDM4yLOcCqpjRczB2AH1PuxpOf0UULxoPF/jGmHUA+0Nsgb9Lzq8AEd4hSMY46Tu1OcdwQyncgP5Yl//jXiIoSO0SiGbCQqSy4tbzzyxKpbN41R4xAQWXt75U9UODlisMdMRZdL8USSjCWVKZip6CDsyS5EuMQKZ00USOk3WLgVhzW0BfEBbHhQXxG+seZBC8cIiMoXjrYwLU41LQ1b5GIJsdCntqDYMTJrLc2+ilLdK//xtliJz6Ldd2Br/dnKQxCjBGxw9glIpL4xydhU63vi6HN8LbYP0rPHL1YUHokIDRrholSBXm60QI5FohvESg/hd7ihVpGdn7PA9qLsU7tg1Rm0KZ4mah5tu8kwKA3YNihWnyIy36aIqi/irdj3d98JstYptByRwd9tX0AWVm0ZxpNA9WHq7zvmPyPBwLaleuSnadkYYBglKSz028Xhx2YcIBu810qC5lk7L1OJzwP7tDQsq0EryGtf3cpzIjgUfmDtJ01GO9vhsP+UswhXlsoHCgBOEeeCpRAaFV33TJ+rd9gWWLuMix1ZvJu5j25d46Ghm+7xkilyJPfj/OiQyBadlmZlpilgaa6XWzcb+BvJqtOWbhZdeLvLRx/vktW2YGs7kp2v+Noi9VnC+wzxpy2e+vqfslla2BS1fywYNwrLutCGEosipahFis1Dfh+HiVaoGOb2IwMRmRMblIoov6MoOocWSCURZ+pFSkZlwhTF6mSqc7Bb6loRrzBGMmvKpEXAq9/niQEH+4wY/D9IiQT0r4yd9GrprmSSyhKpgwrUuo3LaUoirhghOQ9S6MT6rV6Kxk9g8hxbFEOhjekUeZ0sr7vmgcA2EdakaDDJmyGDmfsTI9/HbK57gDZnOZ/mH/Al+sbxGNPU4K7seZlrswhbxBZRWS41UWvQMBxe4JLDKtO0ZNjJ6JvjRPVhT0zQ4TMlbA7f8Vrrz/nUDUTBv9TAGpGHsntp7pVyo6i3N1WCqlmavW8lT9SuvmSgRRpZ+ANeLou8rUsjoQ6xb9/j+SoT4Br02GAskwiPactPTNmxm0LiPt8SWj6eUSaU02BMDp6xJi7vNaEicTqBWV+YWxctdip0NQgUVlsyx/ER3hR8gGtpdn6gtFoVmdEaYV8INGMXuKpCk5kjfMY+Ktk2weV1RD7TOfsyq0vngvhVAM9XdsVGCeqT3VXsPrkLlOJ3c4WWkK6aFyzAHHGuhvIIP7vuLF1gdTe3t0CacMl2BQKZa6U4j3SNehdm7T3mNtpC1zjTDJcl+30vHdJpZ+ybB0pvO0hJxuo7U2FvLsNvI6Bz5O5TrimtDnNncJ0/wKJPc3ww4FRVl2vpPe0eRkHQ5/rpxfmip1k5fclOpLWW5wmy4w0itW2ib6orgUUQ2rwyWrF8ACdXyOpSFEEsG2LRNbnxb/ThJF4BvRI+dP19viCEnLAUVPXhBTahmanDDxbQYNa8ogvLHZhZbenAjPpS5vNeDfRGHuMSbNEaIlWkDG3GbSYLYYsxoCJaqvccpkUK8KssJwWqKhQ/NE3XluCWgru/1/ERlR0eP+2g9DuUzpyRlG7ovohvp3cS4X6kYg8i4dGYyKlmKfVUv8FFdf8oNCXULpYWgU5+0SBuxwpXJAc+oAq/meIBVrXJZsKts9OBSG3pgb3UD7RC8D3WmUyr1VIlLG6HqYGc7w5wrnmLbvvNL7W0/jY6+26LBC5yKljyy36H2cIoQVLg+dbtrBFe2TGhFqvGonYNme2tN4WIXXgoKOes+0n7OTrCR2sK4ec3tkBiwRCekrhFu72JXaTLJsbcEyzDfM6qW+ItiS/DX4ZPbQOm+6h16tft6IShaXnS6SI1juXKkwOJNW3auZgXz4gdm8aKknGZD8jifR8T0s7lOulptacbxf0c44ML5IZZUcoXd/BiN+2d6yY2qcu4Y7nyDSVqPsIlWO7/SXW8UMgqFAIJlwe12kQvOtSQxLToJ5OJrQn1rO78/LHlCszYW1++eIHMH6gSdHCbZOP6EvqfQGBtn1qYAVj6ByTwCPHAriC5J5utMw230Iyp3gA0lioiIoNjtDj5YNks68E7vN2TBt2vcScwIV3nG7jeRPIldSPA+Cm+gi++shE4Q0iaRfx2naZ2iUFFAC/VG7cpcGOPGYRpsHq+18Jl0GQpBBRzfdsYQs5vx0N4I0DE6psJkAn4iQitTJKenf0yYycil86imhDmdijCnJ6ECOdMcEI5qN0vsGYkZuLzmosEhrEyiskAjtfsiD2moehc71J7DFtWkDxeSFJ7RPpzY+GK1h9aQNBJbcAlaXBII3Av9IlnFG9Nw3eVmOVu8NlXIL2Sq7UwFeYb9BA6jr7DC9cS38wDfOobwXWM6hO43L6xXQ0TvZgwLqXJdm3qEJ9gjIx1Lo0JmRbGgg6iy4f8cVbUY9pHuk577lgV+g5qLgjji2XC6xdH9F/dN9IedYVcFXTZuohyYFgGzOg0T6USLTWmAVdB+66spCidifEc80uAbQSlZ8Mkl/NnRiTMv0FwkIf9tp5HmDX7dGSjrgh+fggVYRv5uIgLh9BZplUhXsdjqgEKnvnjtzy6qcsJtpdVxiw3fWYSp5cP2nULmXF83aAr0JEaDfdWQG9IzA45dKJl+E56AV9xEiHl85NyYsBrRcPnEYTva5X4fuEVs6UrB25LmJTapb1kTqT94nX/KmjkmRj8vF0hutKV9GtJpEHjebMUi8UGzfKvWzQkhXuNpA9UTPoraSs0pqNKsaq3L5j0dpY7byuRfW7EOpWRF+cCwH7VlWY50zYctCsG4u1TFvZWv8pphgqSbroKKqr+AruIp0lXsH50cPA3pKg4H6+gqBnt7+8cH+46yws3gCykrdh9GWRGvc/+VvOJvk7xi/yn89ddy/2C8X7rcn2gm9mAvYanGfbV4++yBxMkm9j3ZBIWoGlwT3YNFGSeO4Osv0jp3zR+3WMQxgRaR+wYIZ+9h88tEeSX2Dp49Y16Jve2jNumasCPZKyTUVrXzgNAE12Cfp4/VkhNLFWyHDEhACgnhSQREInlaGdvROAdH6SGReQ5r2ANtNlnOpPEAq2stxPRV+7o+pB64CDwaT1YmZE9TZiC92KTQjUglycHytCxcXNgVVbkB5tiVapScKfcPiY/7C5SBNsIhfGXUhHYlq7ziDq+aS/l+B947yAP5ojAidfXbJo66O9TcQJF6suKqN9m6GxGuAFMbLFo8JXjiLEKkCUscaarrNFtKI2RNClLHGf5z56uemHAfwqweUS/3zLeNDRIzY9evCfVM7emv8XbEBpdrHyQSbWvdyE8E8tD7UGdkd3Gl0GtpGEQ9em12jnmRES3vpAk3zyF5RrQBVJV3g6NH73lK9p7WnuPF8Xcd/7KCdBW/gh/ExrV3rimRPMbEnJk4YPbERLtZGNvF7OPu9hFq1uM0koLOMCVK2x8HgAutimuf2+N7PcRi355qv8aDmYOz1+gNrSyM0YPckZ9iYstISWu9vJ7lC10aPLhhlIX+gy/hqmo2CwKXzRTUbSVwIemwbS9qURK1B/yVlMWK6ngYZaRIPg7o6JQcxbf1JIioVKsGHWQ/PNXSjFujVCGiI1jSrsRQJMzE9JXUgcT1aRdrnpJ7F7+7iD3iS1DOvhYhzgLqugUY3HMWIZAx5yPsAAbmFJ0NzUdSAZ4tq+8YyoJcm+EhPnmRJTjcfvrTYUlxuBYBMIfRbWGz4764ZZijXkLnljc65kQgdcp2qSgaytxgbZK70A9cwhCaJjgGHpHyZQ3jjvvV1yXHgMdtEg9umCeTcU1pOgoFjCxQG0r47e2NozBYqQYV23PRO7b6CXY4m7dsJ6a2/NO6QqMPmIIqZ2+ucBi/QxDa0snggLUurU2Xcjsg6qhkMqelNrLOKJ1DO0jGXIuWx+d9QzM7tD6JfrmqF1twQiTFSoFQEn+rIX/1PdhI1MzsJsscb6DO0BEP4tGSU8gRnGv0nfiPucuuORrsKMmEF+TWwuHS2vDt0kBj2w0puGSHtoVqF1EK9uflHVWfBij2u+wazakunDGK+88Lo4cdroEEK/NlhNAUX6jWAm9FbTDOFTm5ADcem2EGkS4HeQhOXUs2RMp3gzPYciHWlfQ1ilR0kzlVSeCLBVHBynkghIatZOlc1RXFgzlhdsa5exyPeusKIVkSzMpxOu0Y30Q02DOILpbgSaIJQc2/hoGMYDFjHWkz9AnneSUlQ/nMFiubTzChWo5tm2h2DXyDWkLHlVxWXnVg4YRO1HcKNdLWarz97QFf9rK6Mw2nzEgez5ovNFJtG3DoK1B/OPMBBxfQ3hR0lDBxE7WG2kxnz99GF7ADVrkKxGlfip6FTiKB3G3JKPYBQ+lRTbIYNH4FetOZMgqhaaM197vGCjHBHsj00sf96XtUmIzSdoyQV4bHO07lrhQXrlcXlKx7vXzzM/TV5XDI8Sax6GugSYFlUqfveNurrE9s82tK68lyptc04XNaQ7asM1f/j1aCK/dU9G1X6WqDyrzzAVsRKFFE628x46CKY/xljW0oRaZyEGNaTia8OvR5dCNaBv4Z900Kk/6kNm1pS1aAI8ru6lk6hwOXbv+n/9iXDuXqb1Zhpq/WmawwnwOEGdkHRtJwd1hse0ylPkzrDQKdJCkKqB/LYiP6PJY1oaVFkoYHnmVMoOBu0GSZaxE8OebMT7YmLMR3JaBJDoiqmqvzsEQ81qC/4Rr0kPA0D0rVRc1Eofv2HMQNOnHkaMe/LcsFWAjpPDnoNq00FuhjWLIK68rXbWvWGzokKFUaJ4ZAJdhIQ8ADauyPqHGNczo9v/saQwhnyrYKyE3cdDoGDFzyrUfSm2wRMZEFZDMutYtuF9FI9x5KADKgpmB3n1iRgsGeawkKN74VLg0kqeHgIHfcBSlRjsXX5dbX2BWbsbEUl9T4O8qTSInlXtsSiLLIIn0I/skrk8Ce0X7bWHtoPScLsG3LAH6SuA7ue+Zk4EsF44UOWHQKmzyy8XKcSEWnuIHBWv0uW6SuqepUnWf5LVPrYLwGD/VWIkBo8lcowvjh/euYhy3WAJ1LH2J33WicErmm4NkdlccQN5q1t4ntlltWEqUQ3h82+nUTW5b8OmyblD1I8ZUi6HKigkglZo8unAuFSJG76bi6wvNHUq4bBCchatpHLlGpE04hxIty5x764aliG9/E3bA2QjumYDxHgDcu8Lv6DmWgfpzWMbSrRBn74eps7RQaPPW+GZ3jFto47hSM29K7fmRnDXETsE3HV7JtGyUjKF+OrdGNbCwrd2EL0xNU4Q10bimQI1dGmvGtY1iNbF1gcEXFRRnoDo50UKzoBttcp25H/U82erKGzrj8AKuGOdM3Y0FMd4CcXN8bNeKxrX0UFnYZ9xs1Kx49VAEulhAQLUiWkOPw7Mv7Ht+rHsddoLxckLgdvWsqRY/uBLAfiAVg03zkgv9hqA+Hc30g63m2gcXtEYFEUqwXXoGcEgMZCdxDvTHD0rMFEv9jloSdfFQAP+DxYajeENDWS+4UpfKYOtczSmej/SQ7clpeY329y71+1o5zvvFduOx8anNuQ+ls+7i98aGoskleL0hyUDjQxoY0NHCZLZZz19WbFpIiI2URVS+cdY0Wi++B/xjPkK9GkHpmJVYkaq+KObTCCGpZ842WviUHG7gVuLY6cwFUodxkIoSFd2iiL9aRoW54EjYZ2kIJwzDozbHMIb8xLLvCbhe/sZ0uYvPozmhzDCOovGaAwGbVSeFm0zZzRN0FSLjnVzumhDHYmqNAKskZotLo99RhDNdw9vJRi15TS7VNtRVFF+AsgL8LflqtzRm1gsfs6qrlXOEHmugmedqeeAu3CLvFh0HabAaao5M9w5Z7xa5Si9xmowPQFVlq0yhGbi7mwPyhe4HFD3ytjElAHi2KfTKs1JjaaG4d9RqVduHkTDlXozl/SmEMNh4sD42VUkeqp6m6Omow20Z6Wm94/lztVt0ASSZlslzcFN5sc3eYQp4DdrtD7ouJJW8giB3KsHO7YX0o+4wYAchSSsETwajLNXVFLhqGXdNWgz9x/2+C8fpkDgEetkLCIc757+gvdvByu1jjmmBGO+Js6xLA6J+PcAFR7F8rlAwXwaUaOKq+pXQ8oulmZNOAKbpWQ6dqZnm4uvbhLlVbSx9oZ7ab875eGJCwcdVqpPZbVNnWkg6aQq+QMUQbwWsl943F2x0yzJjLxuHmh4iv2DUNjd4TLJ8uNfE2dnumMUzHkMUB26AIxddwSZjFlwt3mDtt1WjFESqXTDiyIwHNMFarvoUENqXA3mmmeXpPOm9SLhJV5xvvhTm8JvN6VyYEQU2WxR1c67FVBnoAv+BU2e2flrVfa81vB2teCd7OgTpa28uxPjeddJLGq2/M1TV7QITHHJAMIrq4KXBQkKwYTex7n6s181/zmOZ+I3KF+0Hqq6LY9gmC9rvp9uaVtzppi1mDPew1G0ebNLdJKfkghcVNyWZs5gaqsjeoiqqc+sRVvjFmpH1i+BD6G6thAWfe3xguWSVGQSs+jmta/9SIZpEz4NfQQvtEfsUqeL/jPBScKWtvsG6Sy+rqZSl/Qa3DfzTJvMDGWE2M5pnRY4EqjJ+tNAvPVbQF7+9LxlXiSf3TWNCheqI1l7vuMUGKM7LULZXVEcGKaD/Rvz765KNungJwyzqyG842omxiv3SNeeC+Z4vmgUjDyA/LXjwkLnMFS6hO1CpC5GAuDZzGxGOZfPpeIgTOxN94CezhWwVzielsD+c8sb/yoWU4QRa9F5aaNfc2/FTaY6aSJ7i6yylO7tIy9ttky5xZnHBt3y6soWqnerRbBvOSaF/NIgK+bl/INkMJp22U3IZTZ/5MEXoHJAQIYRjB5S6IHEGQzyvSTaHDadC08EgHvdZrSgl/iV3jkcC13GK4UkCd2TheIgM2CSXZw0mxT5YkOpIvHeVuwtJtO3Ly9mJsK99WEmiMtYwhRoUFKwFBXa4v86Iah4eWeQ36e4fJYPdk/+Dk8LjRlfjZ2jKvZ8/2jlyRl3v+L1PktarA5NdSr7/RUi98/q8VOD9ZBQ53B97z1Y6zdN7RG3jPl2u9oS+FnYFbY0SrtPYP91yZlpbAYp0FtZBH7gvfJQVVjLBV4fO2Ycbj/BNHBr/upVPCvuH/2xfB2SODWz7qfWN+S+t5+iOSr9A/wfj4REDsr9Lktspuvu7t/D3qpmE+/vrp0/2DweA/Y9y4j5yi5Bf0F7cgDSa38L9Z375mnxVIT6KjX/eG19MU6yHg5H7dK8oSmVewtUzFyLGq940M6jIi5pW/2km/kUTSPSZ8BMD5qBbH13NcYOdGRDV4iY+zga2FJfomWso2eIrChvXJblegHgehW2EyEOROCBMwEayWHLDyFHCSh7quyrta6W6+v4g2s8Nnd0M1f4qqrOBRKwrAUGZyrgveFYOeNZ4NR/WigBUxEzt49PU+ipdWZBlO7ezyEi/581w4XbjyM/pD5mLJitE9Y4Tg7p/eZVyC0BqBy5JmiOQSIS4ptogV0vv/lCAOptkngcsjLmWWVh/hBAqxJfsfDVQJrcTYWFIuiy7CnQbzC9VHg2qUYgsoTNS3CnLMjPyP8PU4cUgCeC7pYTrdYldRupimS/ykPr8fcrvDAQl6uUUPQUfIHC4NGcYuv/CGnpdclPMPc+nEQc0PTtdZajToi4KXDl5AEeFu+15K50Klr+lMiNgiEakgirAi8sJQDlDiFltUaEMG92hBBOb8bUF56ITuQXY/BokljJSBiwwiegZut4YPWNM9iczxco4UzTf3vqHPS+qlTCU3cyeZBPIND6DR4V3OQdB9nxdgPUS3SQxc8F7KYvKNcK+AoyJ/CAgsXssF4GZ8QZyfsqj03neYEva1YpwDD2Lu9mSdXpxvh14R/IdOBl68Yyaga3OWKbNMKRVvs+nccb+gTr9Z1g5rnDo2P9cFSPaT94xdEJxyxZF2EwOMMDyiOgapLRRjfC7Qt4D/fcHmRemgt5rNfozliLW7X/46Bq29UBfE9r8BqFYtMA5l1VYCCkQYJnw5BikQCd/Sixd4WNMf0s/y1+bbUX+cSFsUcsOsHtbH90W4dbLe7nnOn42OmhdbcIeE9QbLPxAuhbJBMgGtfYwkipiah8kIfXO5yYQTbv5BH77oJPrUI2M+wExazlyTQtqH87HwY1JWneog0zb/fzEOWkW33gyLFZFjE/6A8LZxls3ZKKFeaOQ34vgMhojQJbxH7AjxBlNrLomT4OIMeXGGuji+YAjpDuCU4uGlFt75whkLU9PY09p8cKGl4TcJg/Aov8GMfIZmtFjbmIhCqwwZYRA7gM+aIalyfsNq98198tqpXan/YZid6eYcDVbZ7l14sQbbT0/CaiF3iZ43LxGK9UZTQT71Qzz1Qz31wyod5yCDP7x/7Zdqy0H6hMmaGrtxrNL9MrrGRkRS5XJymHycNblg2Z668SUeuQ9M19gnj6qjOA4YVKOq9dLJiCulF9JTEh0QEqLTXO3dlyAJ6PhizjATSucx0zzCbH6owXIvP+ZZ1DP7nqUwGKiEv55KyBwbYSBDXV+juYgbpl7a2lzAmT/oWFEVPMG/t1x3SLszYFXgpg+ZGNnAKwnM4+g1L//wulEJSnO85NiwDMIkQZj6YBtouSgdagX9S4z0cBJ3yohVfOZ2cnWb1wmLelos1yyXhRhrlloioGwCIWSmoJuDiR+nhWJCNB4MvWN8lsg6zy3C8i1G3S1BVB8U9XIQ/kGXPWYIdphRrdFoAB+6E0F2m6Pb/RvYyfmSlYl7ajDcO7aKAuv195dBJxBD1fa0CybkfxwtRLOlN8FwLfflgznlTZ4phTjjCVXUNFdFxZ7aZn7Sh3abyy2D1vJ+MkjWMeYY+ebxp3qewwxi8PSOSiDZt/OxNVfs48uuLTQ0jI5f3gk/g5CuyFp2GD733VQ9jK5SnvVDM6lFrsA28DPy9Pm3aAC9uUcBoOSLx51NvVs81JHnSWGzLxfiblbyVUdFwTYXJtUlyh7dho5C/9D2c1kbjFXIKjm8HZfGaGwg4FS2T2pxQUc72yN2RM4O3GkQSztTDOaRnJNB+ddvltihu2OIhonK5bRjLmsy7lxjHgFBsooiKzEaNBGMQPa1PZHek9bZCFh1kTJ4zlXq0fq92ONjA3VXAHIUrOns8GYcdwuc55edv2nJCyekuqplW0KanRh+tUj72YbckGu0Yk1ZhyIuEKOeBarFekaskqEclzhubuNP+TSWg8EX7aQjvrtFKhGqq8wIeSmlZLWqs1VIyLBOmS9RM+4zztNpOXGF4nd0mwPGhVn6MfPnWzpt8Un0mDJqcb0WWqVG6ZxNt1YQg0SllpE4W45atlPSeUz8v1R9KGKPcCZZRRUGyJP+u6uri8s185Cmez0O/4Gg82tCCeOYQdlzQf20psDE5eVrS/hDV5GlojK8Uaognd4hFXvvbZmcUyvAl9h1q7ehMX/MFwujf4gIT3n0/EeRGBdvXznHfLQJmoSbk3AlEEUpOQxpop2ORgX5ern++TafUANHOiHEtQ7n+91bLvTlsXjc8PnPM9jlEd1WpJ/pLwsKSjh0ffjt70BxlRg8wrskliVVKvsAiEM+0sZQLOgmbTjD/1BQcRK+8Ym/jw1ALFmLuLhwyPDyDZihDzeXAo0MIHGxQ3gqXdHt5HflHfqXW7C1CYUPhDmnRHAYUlMxGgSOJa85UfXiE7UfK41ji8q3+Ovu+cynTmkLbKUXlRUt/cmyYlpy27qHCIj34I8g/ASDuu+JQEHDvioXVCIU+QjW5ENUTHf2XIglzd+kHjXe0IaX373CakFjTvr4gKgYccJBHnMsK6vnGeYIp/zdczwUBUiWF5/hRFYw48Gg42GSd+C7JEkOV4GXF04vKGKMAlJ0LJXHvuHzBEk9f2rjuthcdRZIcS9WTLLHfPdE2tYs5J5E5aIzCVnU4jejMwjFIb57w4IJEJwcjGAglsCrGjV/gbB2EY6YAdjF6FIEZB5IYO0kKTbdQ+dPsFYSkDCRBMmk2LTGy3ZiRI+lD0vccACz3eZU+5g2X2/NeFqWSaDD3NPup0VoZzA2Vft25A3gCY97KxSddLYIL29woqSWxdiWEEG9qLIMN6SBHGlZRFxqH+Q23Epbq590kNtXgSIzy3zoq6w1ytyl+3Au0al//qf/t5Fv+ud/+h+oz6nWC5MOehXxaKLzFIBGJSIT0XeSn1a7O3ghSihJHk2qt5Q1xuxb75SIRORHPTmVkYPcyWUSsTX80ebVt9Oia4wMsp7Lmu+YmWHkTSMex2/8e0uR9lJaoa7zk5v2PZd+B7DPriaECIFQSSgtOxqSJGUi1DW1gYznk+AURcscskLnHPcy+JQbNCLbEHgSUF5vWSVBeCxfN+AHjB7oj9jxgZK5uIbN12LIOvlmfQxrpaDQO7igjG440XQY/BncBFwbzOlKaXP0mHW35za4A5yBqH6MtrpgTCNsfdJIP9g+rd7SOglAetzNHvYHcQegcHpbqG6q+8WtmI89HOnHUmqk2UPoaVYXvrwYhQFyTQ7JM0QUkQmLBkfkyxpNQ3NEf0YZ1hiS81SOSxDMJJOmoeE08mHOlXY34DPZ5tb0iS8xDFROcTC740C9pPr7cHcaBkaO88Shm0ozci5NZv/GtP7mXBYcy2a7EEUE4BnBM00b5MKdWhpJ4ubxdcaW7J3juRBTYg9/Hkl36OBhINCpVw7ZfZUgAC95rN27jiihk6KcyWfLmZo2rnWzIGvuwXmK5QkbLYKcplXph8ew9zqfCY3fu5sbWB8V5S6QuKETdmiMJN1tdQZt+fyNY9yUMpa6pfej13tdrzsHw4nwc/l2Xz7IgbUbYVDDBrcLYtvOP+Uukx+LnUXYejaYFRYqOBiF0XBNa2dLnEnXezXzHXhXt2XfgD3mku2grv7MgbGWFdQ1K5BmZEmSQ0bxfUcowTXZ8COxCetVRqE7iTk5/Y8pgr0lUKvfJIv0c1mUYP1QS7cndrEig0VC7OGVh/uePAbtct9flH383zCh/SQyZggOCrlaXLifLHEUBiP+e4t00LJ9wnnag3XyHhqJGlaSzNos5DPoy6Wct1HOWfq6rDBmUkinjkuCUcNiLzsDfWCoYyjPbL8tUHCKsZ+8yUGJ1eXNwujMuK5OY9qDen/VzjG5RpHIXDSzLoIRF19pi2i8tkHlUDMoB46cfFPK57h/ZfxBFothD0q/qaJYWNWMxIuiqVTBNHPdGr+gfeQoOSYGxQncEuUn7Mexjs9dUQdj+qg9RQxLC2wqqg2kp7nLpE+zNN5iq6nb2VwNW9NcLmczOtZMhoASSMJzaPyjvUfV5cui0SXtLHCVwK4MUxUIMQmP5hyMbdZDNpSKQQXu+0JOWr0legWU1LjMasW5NV6O3saUUM9hOcxlwu2YZJRXanLXRWBrYTmGw2XCui5r6iaIuh8fA/6ZRV6hc8Yl+wS7kg88HsR7/zgtLg/Ddw9OrRHjB4HSs0EjujWsxJqHEDOu2RQDFemkKGtDHWj6MLayHyHDUFWSjEEnwRcncvQSH84brZpUY99UPYwChnwzZidF/CtVHJKQQplew9RnaVdsKyLDkc8FhvJsZ+iwVWMXTYbHrWqz2cU7KkwV4rlhKovfCk+9b41LMqDRNiTyEGZs8J0MGbDQDNEF4Y4QkZo0bzyppxmc2HLM0pP1ANqQQZwbhBa5b9uJ4pPOifUARMGJAymhoLwvl6Kpl/nUORM3y2LEWgknSa73fAtMXYT8O4BucxHLG8ZGk3mgFCKgvRl0gMb7EycK4bsWsh4LshHIREL10qNUlMua3p++5jqvUy9MQCH1nUaSWimTVmkqER7lrrTkE48Rq3tTVp7kSv8gEaAnqyfmAhrTECXJUS3u98wwPxUSl+kNTH+TAqHdBxYI7fYHe/3BcTIYnOwdnRweNPpA7a4rENo/PDo+3DcVQru/ZIVQrKjh18qgv83KoAGO8csWNPjiGbQD+2wbx4tnDnzxzKl8rV0+E4wSb3IEt+o9JpvFDlegpCH+4SiYWtfMUuoMWvVp4t2OdveP97RE5On2oAXx8ngUfgeqKcXHM8nWvW9wQxmORT76eE8hcgokMaiPkWeCpsev3Wb55JanzbobF70DBuPyQY5RBD3l+fJ6Cg6GeVWm9Z5SOPKWDS8HUNWWszBdeYswL24wbk+3n/38DYRczXlHJefZ+zcntNBY7WN6WlxiMIrLZP/5n/6/yYePy+VFVhK7XlcTVpsKb5yY6DrGgzoOHYhIQ1LSusap/y2mA1orHV3i7mYXZClhCnlS2UgwD5Woi8xgTf9g+5L5TTAnlAOdkzIvSXQTQVx9pJQOVM+coym4aGI2O46G9g+Ww0btgV5xsT6LlTcxsk8VFq5E8HGrX/pWpLflVvI6vcdM3BPnBmroyfWJkup7doyw1MyvGAeBsFqbEmLkGfgpiDehU3vu2jHFu/w0TCVkWmOItd0hx+FyaheW8NW4W0z8UVH4SRCGFCBjkF4XjfXTSLpbQ4c36QLkPneribRvt8cFLSXDsSE0bn7REeJ672VSJJGKUznqmkpor52qQF9Ox+hl4go7Ysf46Q5b8mF5PtzeW1Bcdj8e+EjdI7p79mEiJPp6f9pSottKTl1hCdY3UQFnEnamd+WqrZcUfDPfWCIL7AdkgV/wnoxr8o/T9iONt5XATPchC+OvXRMofJDJaKhIGzI3QMTH0DvHuA9PQqBNIBm78cgZBB0T7rwVAS7Cs4C6/aH30BiQrGeTLzGJSG+7pjZ/6042c4NwqZPt68VXvawCzIU7NZ+oBqXK0lCnaQ0crTMsEZ0cFGsEl1Ovk+8+nMF8YsAPjdl7Md3lw2/zd8gXcKOxANXnk3foGhfJG1PUmuKU2EBGIkWN3iLJ43w7224tyBq/j5j/JIwYvCtFhTpeNMRp2YPQ8QN7VJQZjAHgoWgKOtEE8F8bSmomJ7JFMHU+AxQ06JvQXBcBaYiYt6vUyT/aaA2fNM2AplT49lsW/hvKP9MnPugE76iOiBrQnDYRmFT0EvwYkXeIqqhZFCpQ3f90g+Vlyde4v7yLBG6QpWfeKoZGjsfNO7IBpWvGt7+jG1tonwbbt79WoNLyFMmy0JpWNM0w4JQ8Or8cXrx/9/L89YvhxemrF4+cHxcclxWKIuXbAMaHMKO1lQJH/JVpSBgHrVB6jfG8SM+BjR/LLtmqB8tzN9d8WumDINE+l4SzOHWVNHUjEL0msIT0PZJOwrPCcWQto05VpoTUli4ay7xMKtfLIm6q+IqvhgHd0ivCgZf8qUTuILgqs3w5W3XI9tp502+5XqbBn3ubWZVN2wwaDKwITvdQ0I9OYuVtDFK/8cdudLbz4gYtyoyoz5UGUOEfFMyff+rjrIishppvNrBysv7cDsXTaRkr3/SEx8VLA1/nh/I6wbRjqyDiQuqZUYnrZHqXWcSl6ikwVRA5UhjvJ6AkeaIjmVtFYkTxIzj2IH1ZakozNCmOV0dqraODc8J2sxykUBoqdgE4DoFGA4UQiMB/X76zwaP4dZoP61LAqQbaqeOctUvRYl5OU8FmTZfItx7kMrtGQu/cOzQi1APX3Ls10aM6CARxK+HAJ19xk404T+5uamsFGFTtm7wgxcYonQp2SdOwpp1CioBpCs0FLZ5XmiNeu9XO+HIHHvkmA18jCIvlWfwZbTw7X7Raqadd7Q9rTq72rFoNarQtZg+NV5SbnJfJuetxC8W4RqrPvfn5/2/vW5fbOLI0f09H9DvU4MdY7iZIggAvYtuekKhLq0eyGKJkd8fGBqMAFMmyABQGBYiiOxyxr7H//Cx+lH2SzXPNk1lZAOhbe2M9MTFjQlWZWXk5eS7f+U5gUaOAQm4GONf1DQcFhbCNudjqZjfxlUPMdJyOwJsJp4EIJWsub7u+Mt9Jirpsa2Oc8nvZBqxbTEQk94nnpA4fQUIoRsqhSlCZIrvRcNtQNT/TcNmPAB40uTXAImYgQUhuGo7sR48LDHdi4qOrAbeIlmJkiqE3FxdYATPVt/Qs7Eu35fuyCztpv6Ny6LN6ns9Y74xOi1ilmusVnhd/a7m+sZXNdk60V+uV+4W+sSadmgj34CYNTVnG34v+8cC8+SlPjyYReaPOvmJram43QhP7Nvw2sXmXaLJVx365cjL36/PsLWVZXgm9Y82KT8IW0BLwNCEcV0Z5UEuVLVyh4qMUza0ELUzrxElGeB/aCeGV2zAhDXMdVKyUyX9Np4fAH+FWbloUtFwFk+9gKlTOKJdIq8GryUl0hA6mjKV37xe5073cqFMVVRIvdEC36hCQkZxIncbGbLp9d7Mvq9sdo/XjkdimLZCzk0h20Xjewq1Cr11WtzP7KiXgEWRHUwHtZYRu//omeMfokbD0m01cxKPGsoiqzX8UIs3Q7kytQG5g26w3BuTgwlUvJ7LzClVOQNB3TCVZHjkocKFv21cvyTyv6ZaWfOT9HhfD1TVXKzATENvWYA+RnYAl/8IeSGJSP0EppVA8WuQct49QPKnz13CIWhFqpPhxGz0BXfQNtzxPI+cN8lOMvXxMKXv4nM8gK2M4kQV/VSOkP6TzGLikbGIDZAFA5QDsOj4Aa/y/wXduzPlIyiPo4c3Ti7c4u5AmWkYhsPN09hh5VqC6Le8tBcuV082VDCQPK3AQC9mZ9357my2JiP5b/iG/oAir0QNC/62TAlbvTcZCtWgIcbPHJUnR+L623qZg3jcVJzH3YNMsbWjm1q8y604ix4ovLGtMo41RqOSih7GnwOHf2hApvSxCJhR0s+G2jnVjdkTozDyqOOnLTO8SuAfZf8i6lQVkmbCYvWVS69NQxFt29Mavb1FuCA6lpr83kHI2VNA9ZK6xjR0Jpw7jHbyng3AE0WKSw1TKWRjn+iNgGBO9vNlnXDqn4fn1kEkVcan53RgPCk+leMkpT57IiOrQyCNVHdhHb1OXK+FNZ8USQKldBeqnPWqmTNbCX2SioNivTcwg7TJQFmngCJbpAlrGbZhVcrvFm+1V/o2UKl0w1lApOAz7YCIaTq4m62fomGy9hruhjjwuLUkIepCIZCi4dHezc4IhAr6Ib3jSdid3kj1OxFyV65JLJtF+j69vX5H0kYDnJSs+e0FO0h3AF6OnoObQtuT7+FYjnyvYkXATE38TAAQBY8nPskXehm+I/AkeDRAFANAF48zUxnEYqROU/gn3xmrG3rIC86ag7B9MIbp4Nx/2wBPufayuHbh3lFwILyGNwKMWoslZV3pC2RxIdGjVLBXF0UlfFOE8+OFwpNh15vTo0fsuAUUazqMIopvqpq7YCqN0n1tWL+88ywAlf6jbJhEwDrt51VDivLctPc3xisfLCs5b+mBU/dLdIlWESNb0I/F1K7UxNywPP9Z83axNQuwcbZerNdgfhEFSDbYlBJ+42Vqu6k4QFTLj7WQGjp4HYSOK8CFrgyo3E4xCpD+qGbL4KfT/3GoDeiVYNCOtmGqCXfVcVPn5O1H5IwgCXtjDfJHeCTeFxTmDms3MjdX78Kub6OMjQB8DKPEnwI8PDu4LP+71To5tfQIZwK8IP45Aob/Dj3+b8OODwWECfvz/A1YWUdBu597Ou5gylQZA9z0A+it6KsA/B+8noc+9k6OoCbHRPbRR66QJCZ3Jva2xbC7E+JyI+IB+wzvUmUZO/NTFLrLsFtMvGjjLz/bcr75YgZovsI7QPryE4wkfVL8q3RpJwPXJwz4Drvu7gzb/foThQKAGJiVKsptEU+kGA/2B9nKhjCHKyigUS3mdIub1/VnXKZMH1yH34JZvwk9/ZZIUJsvTpDgEZen90dLeBbAZAhMw7Md17I6ea1QH7LOUfQQiwpklgUSpMrvwOp/EmHjuKWQmo2Z1Gi8W5OVq5VKuGlZbKnepoz0vIakKa4/SDavePa6NnFguO6YXswmQFT/DPrYZhjNCKqSwWoB3AbMtQZdwunqS6IbcPzStU9epBi+BFPRDKRW2W2y3tvRz3BDIsTlbTX2Rpc2tyCFrbq81myrMMjSeD/U7hbXYEvs8LDYn+SKrOjeJdu5wA1QN+VS4KH0Itl9XL0uoLWRyH5P2Rd+GjnlI2YLGMcO9INxv2oEU1sUWhxsga+Z3FCMzqwDZraj64r8ivB/sykUJR2hCT8aIz3RnqINaMCO4eytliIWTmJfsFVpbDCFRcu7do+fi5CrHUk2C8ShcL6dNMFWrJSTBue82sPk3BJsX7fa/8jFmUGIcd+2IZH1eTPE656IhmfvscTUFukm3zYtxK4UFxqc8MecN8rK+l2v+jKZI2NOZxZy2NjjONy82zyyYFxj4wZDdQ435De8CUtstdk/g7qR0CNmWng0gifZsX1C5TwBDENQ9hXxIn8JkuejhEpgvyhk7oZRPuy0DIfwIyxfMMyQXIm8ArqskHoUtv8seXB6VlIZCN6inBJPahLYqYXSdJG6j1vLz2LGUO6zw+DKtSRCfFTwyfzMgy1q+QiE5/jZFkZYPC7o700TEbZNCxwG8fe643WiNFy+KjABCqvVaXuFyqm1Hxw+PPQZyHqOj0/a+rJaqBQgz9jWdLcZtgTq3ZMG0fakwfjJpPfk5qEijqTTpxVZymdeUK/rZ0s3WtAgjHFqXGhvzjElFpYE0y3FISJoqv9RPFatPwrY8AxFJmR9xFb/7u+4A1lHyupq52UleWZtS9OHnpzgD9X+VMV55vSrBKi/qUyFXt2Y68c9itPB9WBvGaG7EWUTTyZ+obE2nWdFk40AkDGnnFh67hk1IISsRV3qs3d5ZzeRFm+qPRLJbJbzpFK7mzGnUvhv9ARKZ4NPwG4aYVR0atdgTzcItBW5NziTABfCFvUijNff35lgUrAkXiwE8qnuzEzNVU7DuNkemEOH99KIXrwPLxVePihmw6m7sHDJnQjpIxRh0Lm7QgRywE9o61Fg9FTkAOsheUgeHxVYmYGxILhfhV9uUOLVzig1WM01PaAAk5K6k4DJxpggXBlbIW1vYW9eB1u1iUo5VS/KGpZlvuGDhJfzpbItN0+wq1sY8DB4SAP90W44BsoFsFEoSye7j6HT9xO4015Wr54qzGpZ0E2+jtD7K59Y0N9t/BxXoYixSAvd3hl+3sWXj1PVUGNMK4R4LKPWIa+OLczPBOHWwvJGbl0RUeKK2q77eWNlgN+D1yzuC9qKkAvJt3fns8w4fgLKWU0PFhzfWutcxtG9GDQgaEb+F8trogUI8TwqoXalVyRv6iq8spYVpbngh6buK+v2ymosMkDJ8psIQ6jNIUbS01Sm3GiMvpShYNphSebJg5U5F/4yXHE0o8T2lwE5k66k8Dmubscr2c40lqN1KFDd2X0vXgJlWA/8XGAa/EQlCt/LXxVKIrFj2CmeZciN6BXhjN3w/s9pO7al285UwL2Jxjs0i7zmWZbjTBNZ8AhHVaz9psYYiOYfi1wqcvz9Gdjg168OfgLdn3rHEgxIO1CqH8OzfLgi/oMlOwdB42w3vEidhq6EYrwbVRkMlDKidoMijyNHojjNCcDJBPhNS7XBltuoWmnFTj6Fc4zNtpFZowULjQU1xu6ZoOqsFccHK4NmkpFAGB8h1IsVPEWczt4Lolos7toGFzlgJLnMFKFHQAyu+irML2YuD1oUp1WI9Jetazyz7aM/D2GfSBGoEo9d6IaVHhlcmSsfc05DxDmQW9d4MOHMbeVpygnCzTFrQ9rQAE7Csp5xFimUZza0v/nzxa4Rej0qKAfo3WqwARsdTC0Y5qVvv8xZnhl65gXyxn/+KLcTQEFp3IZsBMkDai1v2923wZVM7GvnLIX5n6nY3nAMWGSF5gT5ZqpolGECSm3CrzBFyHBKETCaJTF/Nuq0m4xS3WqI+Z/rEmo44DSk4nLI8G/zCGFSNZJX3IfBCV4zX29IxaQ7PnfFS3gix8uMih5jh40SYI/o63kislMAJTm5BVK9rj/P1s9DyxcNqfMfcGRBUDKYrNpVFHYDoahayHRMD+xoxkvBm2JLL1n9XKssBE/CmNt99XFq4YkPw2sxUEzWdhywJEakmjt1bm+pVWaY/ta13Cofb9Bw1o5sOCSTwaRzBsJCNUQXIq9Y+9UFp62+BWWd45/lnVSWXzUK0xW5SqKQRAcZYMHirUaoNE1SUh5YeA9AqRhpUcCvxZwzveDuhHgus6VifErdycJzbezlL1fOQaCBjNAM3T+3LfWzdo5xtX3RAIy68beVkwjo2bYn7tmaWbw0/tlMJUhnoxjnN0yz55rdwsQIvz2qOm0pOYOEBxIF8Y6mAu5kxsy9ttkU4knsdT+PHO0w21/isKIc2jwhJA5t5eIfRLVbkwnwR+FIIZBG15kes1roe+KJXNQuxaXjfh+7idVcNF1MLIyJjKOSUwBe0RYdAYyncSR4tOT0Rsofcvjm7WWA8A/8JvjxYQp/6tEEdAa8p7RomrIX632N2FJhipe2vB2ERdS3KLA1NGLptmtizBFcBINSoam7jQlzXRLAbktqNsVn9JZraiW2l+sDnG9zH+InUzo7UWKSNonV/1aRKVjHEJoOR+xYTz8+wXE5rD16Rae3Ld/IP98ZqCEzV46Jq68zN39zJT7BXgjR4qZDl93VyDwcHuEKwyqRpGUYRPeP8aG8svmkY0C9bDq01Z5bfkCuuBX68bUPozFvfTJP8g2SDWqVUytlrmuvBVI2bvW4ET8WudIIP9rVreUSTOFzOunUBlx+EXlDpa2Q3Be0HNUoTx0brTeOSk0ePVLi2ikHQ6g1Wa0SKEKICZGdPCpyyISyuixFJnFagVlvkFsTLrbLON0nsLVwBzBXqQNDLHkmQ0MWS0IxWD/NauEE+hDtGS2dgVNN/Y5kUbdtg89q8HqCdfVssKrXBPdNBHOpuWagnBWaOBIUr3FFY3Mnc6eYlIgEIC1dhDHiQnJLWFPavz58CIRBm1oJOOME0B4FMNcKdRrr32hINk2VvS3FVarCfQ1GAgtHA9uYvCTn9pPSxpiy71pqBa6MZtisZrS1/BXJdcG1Y494HT2Aro9zfDjiVFGXCeElGPtVr8zH+Oto/OFUbh8+xqZyhiDjutxANV4zUpom2oa4EHoVl81pnyeYJYFctzQP4OtEaD7Bp25z45vWjki4A3/A99uLJZkVseTev0KnowQuiQsWhwS0n02LU/EUR1PqNo9jMDAv4UBQnW4B9AYeIBR/HALGqNEE3ZTajBBEPUMPbJi7YHK7kMbR1J3UmrqrZ0rvmuSJELKCgUA/tn6Ts2JQehM1zui3GMycoZaO7L3E34rexcr/2Ygw843RV2CuZPieXe4G26uZdbki1GigtZKTyDG2Rr3BtcKCcijHuPk2r/cm1SnyXhnlBwKXW9UDW6ha3Q/A9RKPFJbpzQoCPY1d1sLKtbs41vVh6CSl8dpJsfb+R3BsYFQ15ZJ/B2uCCEBS4foUw29C5smNcK7W7CsC17Ow3jfbWEsLlxGqj3bdybNIAo9AW+M1rKsdNgCXcIXUNcHv1XfmCYPUyzKUg/4tgS+DtsOcmULor9w5+2l29ZBQtTToepGhbrm0p0Hjzhp4rUcFy9g3GzpQ9GKMhZZrrI6H62VgnHq2mNCP/P/LYBoHdEEvKsULKWH0aKJ/JTHGTVT9aVPN5AMtQ2XoVoJES8ZX2LPSQWCcEEKxmSyl7MHUiNsMCbFjRjsClCHJRf6Rs6CArnIvQBmptyq+/RZo8SRaRwygbxx/A9hwrc5YYszYEsLYHqkkY4IEbTnQOMg8LcbfhS5juAKQRH/JxnhAU++3OB5PRxMSLcu9HsuDxBnPSFPu5vYHa7so4AvA+dG+Aia9aQisIaRvPv7QTa6czrMHt6yeXbZELo9wopsHG8RoTT0dmJwIVkH9blSHKiqamvRIgbbQMhQr7+oEQ6AKKGjz6e0b0lhrOw5ySO2JPC2N6mheHe5ocwsnbjWQJ7ZuEz0DjmktIuGPlE/conmadoJHofYlOoqtefYfCmmpRTdI5M9DRiOI6pH6ymk1bClRKQUtLAoZ7ScljN4tSqDygN6Jo8cZQIX0QjTbyQw+LSXXrVN/UJ2xgKPYA37q9Ykar6hCa38KRI9cQ3GtWseDc0o2hR9eD3TIRN3UgZNYkCypElRT/J3BVs2JvGKUlcww6gfzbEcezEmTVYtlQuAWKK6IEmGlRidQI2zLoinGMcsBvxahOpCKFlQHXQfutrSYonITynbBIgyeCVLLgXy5KTPqk4w8tvNFi3fTbXhTmDd5udZS1wY8fOQ2wSvxuPALh8Jb5IlvQeFKz4y50yI1O/Ns5E+J5wy3VfGsSZqJioVlo8bnZawRlJvtoIAXXCWN/qczG6krGd8Id8NypAws4aJRbS9A771aDt2sfOGx6u/T9wCxSmhMKUa042602YOpU/sFLJG8KY0yEfl45FWxa7Cjh1CRwPG83Ywn/oJm+dfOmQojmOCoeGnXlpmlUzNGpEme11lV8ToGqUIK1Pv7a8HVUC0Y1OPlAsB/RZUmOtI2HNArGuBuKaiNf+TMTAZJAkVmWy0kRVzhqKu6s1XlIUNI1bPgyHq5J0AldpDXSD7NWtgNzeVtk40rd/JzFlsFc0mZeyrDaP+v5uxf0eTdFXEHRlvBltcFkSGyRi0c7XlwyYeljuKGGnHbBGQh8ynKD1RvS02BfteEB1/Q4LcY5QMacfUjFhq7prDJSnI8ZKy5oXQE9OiAg2lz4sRu4EcHua9noMLclWPB2+AraIbZyBYqUN8+eE0ACw8ONmWh7u3mssCFqoc4CyNv9wnPg5Bajm3PNF3p1cSHX/BqgMku8Aj6UYy8Bqf7wMqY2Mqo9HHhFoudAvLW4Lnx0nWclcIMTHhkf8VWRH7h7KFlE3ugnsdYFrRiUoVZOD51UyWXdiiKIE+wQwu7zamYRFNmPh440bj6kDvGVsxjl1Vg98XTShoXOIGTFjgP0twDDXTIKAl/R0AHF0BTs9OOLgZRbAhfaIgetwmb+cmVQC2Ax6WlrwkVyPAkIiPtoWH1sInjVITOuVjB9BPvdAKwx9ZETKnzbkEKBgiRryFu1uAvpTtSvYr1bLW0GjrMaJbBSfXtoLQdQiCZVCfd5+7hXiBKxZjlzUxRt3YXHxeir9G1MrWiMIry1qEplV/M/qcoL+aBT+ya9a3yJ44Gy5LHT6iXI2q8Rkba19wC3tBC0zIoCuhCpsCZi1t5+y+WKELnZxELSKJgPVemJRifOiMeEM018KEMu6+0yyVOQy7wOy0BxBlz84P0s1bdgqVIk4iyfAP3RQqqiI2RIhEdAiLN1FzbUEZ4iLD5CGEQUXgA+xgzu+w0/smBbznvqkMeV0hMb+aSpjj39uEQOLPwUzDdY5h58EHlLAW3A+gy62CsCtDJQFm/oCeTNYOYUMosT/ZK/c4LvCvoGSYE4IJMb2XQwepd8Jenb8bXqy6VsjNbwxyDwr1FgjaBTwj+AmBH8mHdvXoZZJaTs2YKbALUWqqFFAtrYGguGnYn4xZ/5tCT0SJE08oFY8n5yx6KG/P+ps7SG/yLt+RG/WvqsSC0ilDVIK5w9it1L7eW1kQTqXvx2ve7+QXf/JNs/Oe0fn/YifrvB8cby2vsnym6nvf865HYR49fvvHY/jdfOfTbpjb8Iu13/pN/Cbvc7M9mvxUxG1cbd0t5WFe+DYg3j3oGhy7O+aHLmxOXH082ma5C7nx4RVzGyfsEEFl151ZZvst2CZOSwVooK7/joREuPr3Oz/Fo8KFOJdrDTZlw4OwrSZw1/dAKevd53QFvCqdtwWs6c6RAARfw+2c5k3US/4YY3nkInywabVQifZaxVBHD571U+WyJWnw1F/JUHP8LBb02hYN9C57nxVNea9LedjSvXM1a+A6kDSSs2VyDZTCMc4XEZNPlTNPAggRSrOuNkZLdmAycJgN7gdcYGEFmBiIlpP3jt66BoL+O4jLAFtDHbm2jfy5PyvVsGsO84aakav3KrkdSqtwbSmFVGsbZj81bvB99ZW9/ql+w1vS/EVcY7VM5E6Jm3VEmJpo93e+vTYcZlPUKXHX8YXUpQuEhd1MhMSFf3WtRWqBInEHdG1ZZgJG+uXElom1AosqIjCqXwExsHS8078tmxo0gQOJgEdF1QCF691+PCyYCRELHZcfSThEFrkeDWfjQNS2cMa8FKiW7v1EtQSwQPjA2z/y0JF9oEGISbm9fL4zA4cCqAJfKqTlLb8bjpnUnJVJXN6L7zVo/5XMktIISD8RGgOUfgxo2y+0O+KNHrqXuGmUJw/B4GsUUfwUc2Tl0rbBeqnebjcXdZdeH26E6BjsoindKnwfiiAr/NMF9iLZUKaK14LejDtiFCjKuxa/EDuoykvBpSf/ImYFSs5ftbrzYcN+tmeAZUezIi85QuHyKQBqfHTkblViAr72MYW8bmWpQGA4gXNOSRyYltSeB/h9DxZI0notAvqUqSMUKTn94gpDfoSe+FRaoSOEkU6E1+x4WTyQW6DLNzXoqLm1zrjST++dwEksfjuMxCs3pIvB4Xq6GqyTU1EbqHZhj4ay4kmzAEMDA4SN1D4ERbGlGO7P6o6XLtEY6Sb4l4pTrJF09fx5V7YPOAYMyJgAVKkoz9kdiydUR8s3el/LZ4sfz32P2xzcUOrbiFCVVX2ir3xfn8nS9V87EmgXBZy5nBWGblDN6W7H6bd8OCyamHXV6kLtNl5cyF4VlXK6ytlGSXaE+i8FWeMe3fOwq1Aw0Jyi4RxOsafaSFRTSU+Ymt6Y+e4dArxj7+LwLR8oBplhBmESXHY808GM6Xxe1pFp40zW7/2lunhOKrjQpo9xY2EirW3BamxlXLm9V0WFNVGRKa7ACrd7MH2cBZkfvZn7NPTYvvJG35NHuVvy9aDjAHNai4B8PdoEBVF9wiu+nmnlRAQibBXywEAs5ExUv46axEHiOB4N2SzYF86SHZUtiXdP01zCDrxrCm/5ecoZrAUkQjQUeQ0ORH0Le1vdtZJahmeihR23h29fOHBeZ3ubP9CekrcF6S+88b6HoYTrPXs9qJL7dzgaMRilAqoR1gM7/J3UNddt8YZS+cfDfauTnnrTXYdL++nrGUIltvB5mtAFo7Bq9yc7OJzR5aCuUUA57LSDlonA7XG299yU+sZjuQo1QB0RtUDwfQwGS0oovG6cd2BLzRQ4VJ274wV6RMDlCVs3Dmld7JKuJnq5dIPs7rHyn/CmVCFjuc2XKm357So1SIkH/KQ1BbhmN7U9CL6D6NG+s0+/rsq/D7lUhtxSpQNfO6X/6hSLSR0r+qhGrqRN+zF5x+86PH0k5/Y1o6m1To9QS91S2LaVgQMtIB5I3cAtChWrDTl6sGem6HRPvs7qmXCzctoCEDPqHAULmkNtPNwnq7h/sGRcV9g888bRHfSaassr8S8YJIM/e0lu37t3Q32XhFtlt8biyv7wI6Um4x5GwfQXwNIiIzTPZqLUzmrhYhCdQED5UpJuD/9Znuf5MBQ4hXyiFLtf2YslgIQV/MICuF4NXxhT8CteXs4iLVyLOYK0qYxxf5rY+JwqYT6lDXuBrBqRbpJHCtqhTfyBmi0/t4WX+j+X7KyLe28fNJPipuKlh0yY4hhm4QMtjwn5NjclKwi2zekklxJ0tS+SVJvak+v/NH2Wu8vB7fCRfB8E4y5kBtmSGWDnOvAgsC5OaQD9C/ceMNoSqykrQZfRkmZTxaON3GaIt2E6VLncD2X5/Y9pk7ttXs+ovbETppL91Wv5RrB0Ngn+3xEwIXi6ocr2eU8V+AQ/lz8OrmHPL4fcm8p0kicSDLmFi/NeXbWYbo1jHgeSIhqYmVcfum0JtCBAC0oXiUhRIFp1Ji29vFIH5+K8cf4lVTKCYsrenKpUwVv2q6Xf7P//rfcbZpe+f+Y4x7RuIZ/GHJVsQx6JPwU2Mdru661aJLGIjEWNeNU3qgrfD12SWb+KenML+XHr6xw1CasraeMydnR4XS3OGuShyboxZPXaRA/u2COYYBCpI4AHJVSxyzrcCKu6RbnZyPqwrF4X9kj+boasHQwTndVJsDVUct7rhTiawxMQBzIufe+0hMEzbtX4I8UXp3+EHBl5waS2Xg5jo1vuNYQJHe8Hln3utQEXX5oXY/iPznU3fmxn5diRTPbgODI+JTbbzDGuhua031I/FZ8V+HKe1e3GNrTWuDZGVmW7Zy/ntVqYI/gzB5nZalYp9FDgAZRLh/eFu8JWO4mz26LdB/KzOAiSmLuzbfzbrtaKOnOZaXhW0gowNc5jUjR7fFxWBI+P51H/ex7uNgcLo/iHAxDzfWfTzqHx6auo/c/69U9rEtGP87RuanYWRc5xSOmvwiIJneySAAyfw/BVogdIn7LCdhxormSCNLDjyyhOKtZ/p4iCppNpdElAzc3ANEYJ7fMRHZRKCPJpZOldNS4JGDw+M+gUd6uydtsIsIrAQCViOrwEwwKQtKyBGrgZg08PvsHm3q0NDpvdAZ9178oKuGtfrGicRqgZoP0xVAZV0oqsv6zTWR5iFSwc1pz1+z4fuSNpDT3FOZL4KEMrbH8gI2K9lFybtew1OaGa9loaUpMdnUd8ZfKZfoosC6YCMJp529efrEbpNnjWFtiE6tC0r1mtG4e9kkyETYGkwgBdVuLiS6JOPxnA8DRxDPAvolM8BEaejWCB/OVY8AVo12tgkOuJ05LVfT7B3UD39TXJeAKCG2vUmVqhQDsasvUR0/zbJ37oq6KmeobY2Lj6fk4710VyWcuMTbW9p9hza4c10s6e5xc3vpLnw83fijU0zxh4auf5BgM+21RUu2XPdelJO+Nuqq69LDyZLdkL3CAHvTadULQidq/DB54OqaWu68dXvmPbBEZq8obN6ha0GN49B8elXOILK6usaq8Sk510/1ao88EKTAt0ByPW0SOI7M5CA2ntvKi+tCpb0w1aNUmaqt98P37DsBizvym5khNQTvWeu2PwFyHQJ0gGt7YbmeZREefZN/xHEkuqJz9ljmR3OT19nnCMZPIE/PeIdSNjqsUxdqETO8ITX35KXBQeo2c79SJLwO7nie2dp8PQlrd2TzcfZZnt04Kfp5Z+8/55/3j46PBgedL3iO8CiBWpOhWvPZXu4WAtrEgKYpBkNOhpICaYx82IUaym/hR7Ge80ld+ZQKtDDYYUwDpARSO5lBXtxnYBp98c9/ghJxCdHU926SLnmSvvvusz38d+MVwPB5IlQP58DML9Vno/21Rj77/SHrQi867SUHQp57xaJtABMEzkG8xPvu/JMQTmxhmGW54aKwZcRCPiDABzdIQuyC4Ww41Uw723ZK+vruQPa7e022hf4jiQJxFKKyQTURhk5HcOceDXInyYpxjXEHee/AvBepZBRXjmJNGHegSmFeLhbTvIQsQm6TDuYzLC/F40zakagd3je/otc9OMycddA/PN3fv68defLw8OGR2pHa/69kR6bU799tyJ9mQ/LTP7/xuL9FhsXPZLSgree+gDmnk0YerIEInJfyXGDd2QaSZt0xJApk40VOmKvxoportRN5hyhKC/nvxD47Qnb3pUUvUMwyZfX1Tg6O+pIz0ObEjGfQXQ5EM1FMJgwlYA43AO4tM1gVKAajVZQ0YZTr04ZJ/a1uyl/IAeBvmRUQKDllwtMc2FIE02qcT4xMzfWjkyM/auh1YjOplZSE2AfRohEGrgEtSvwboUOT7v4rCFGkolBH24GTKM7KdAPMe0w2LhG48hlh8kJf2icujLDD2Z5Aewdx6KuJ26TT3MPsuT1Jo2xZBFNfNEtw1hl+ZqzfDYHzD8hfgwNrSReCmkDV1RV8WjRyk6KdnML17CQt7BA+cI5HhZKrfQrCBhSkm+1FTOu1qOJKsf7jmiTrglszdRkC2sk2kungy+9TZysirMdOcB60KpM/XQwFb8+rbn4Tp7OhfHGn8a/FcAHw1QB6EQ69Yb8n9L80M2DY0NpmMNpOhBBB64z95AIGVkKk2nAmHc0nRnoFpHH+Quk4lCfD03o2kWg7yBsIpoibL4T3xYnlUbcXVG+sldZFqRp8EfaA6SVo+9zvBcCfsacU/BIRnIp3JF8V7p4jwCX/nCCQD1o+Q2ppieriZuIQiOGVQtKUOkr7wrac2t11evdp5vZZK+07vevHRGy8uKGx2g+IHYWKEgAV5NBILFGt0msYMII91ThViU90Y8JGLIsCgdlKH+4pxjuGnBXUOkwbRM6TZRljfLn3FC6HTlwxW+G6WGjM44t+fEATt8frt88v9oCTw2NhvJ/SM6dclZPkmBojIssXEaeUIBaIx4TETu1v8rxwZUkAu5F1DBM2AlW00gDs29sSiIGrOSFZcGFFNoDVFIHOo37arlTIjEcCY4E3cRr71+cZc/Qzh9B9G3+8Go/v6BgqTsqXMb42hUhThamewf4nXwDkcs0LSrjlQx6h11VC6pQzGMF9z9tydvfq7Cl+lyl7SEc4JU0PEwxBstKILSzGhpdM4LAS40S/9HOn+cyGxeJaGICRJRtGMATPwrhcEH2HayYduQ17fTqrkboi7Evahrud4LuQrtboGuc+erNkILnrn/JYYHhyGaMfxX8n6XbROIlvDyoJ0/mL2vdavW25VcaZpDtORNWKHrDvtWuhVHKrmpzh5GImMhofL9ZkjzAkDZ13bLyn2220is4S+SBbvr10nwbXHxQzOEcbQiYKqEKEftkXewW0JJ9xuSmFVwRXc1ZlZzflZCytnIE0LDGZtibia6LuaWqa4Ri5DpWw6dSFu49BS6I6nxAXyt7mQ7rJHzlhtBhj0jreiVxcueAjzPC8fHaHzkLBYRvGnyXw3OLzN4xZwWxZJAPhItGe4u2OPXnluuETKPGpByX6y9FJzfLqTnfOExY7Xj6cVZPV1GkKyO+ShN6EfX2dI/EpbcjO7fyymLnJWhWXZPV2cI7gdzfuy3LmhJT/JwBV41UMhdzcAiez/xomUCRhz8lGrOOjltjF/mzUFZkmyoomZ1wZkIKcUZkuIHscFpri57YIKFhKZl41Rz9IRF5Fen0JvJgB732FLNALEhMjuMZEj5ARnF1cwIzuuf7+diGsw8NCthlo1N1beLFiGmK9YVDg0OfVATamXVvT6xsijO5Rt3Y1J8s69WVqcPGTIp91V3Ouby2jhCGCzsDCnyq1ZCTkq6iybiT2jJRA9UzKN2usFeYKh0IE1eDJWow9eXl05be3juLsPB+9J9MWtNJ8cgsJGyJ+EEVdrWoZA8K9xJfvtDhS8/KZQZgXW3beCTKaOiFItZFP4nU7Wcac7f6CqDHsW3oKZuCdT5yrwbrbXDMMOdwE1AzoKVySFQc/TIol5y5jWT3crkr4N5KnZ/mH8pod1CXOFQszuCuvlqIiy+OTuPS0SYth/7hTaZxJjRvR3QuTnIW6k2scA1c6IlU9gA2eqyu4VyomXCB9XLcj/1OtAfjcIvfTg7kqijG4pX0s5CoCgpMyRwkL6CZArQH5FNoP3hshcA+LDYCjApgdSH7WrGugXiD1TTIkg1uoQPAyAK5OELFuWWEEwAD4AHSdT9N7FZK6aupWdMzRTb5wZhJuPFFQq9kHSnuD1QAAZCkJcSoz8VbEEJHJyMBoEdg/RNVPCecwRC7qALmW+Hk5FK7EvJ+FXjN14UyqeG1SI5cx6Frs/u3i9Ze0H5xaQCDUMaHzaVtAaI7Mp7nc3HjMhOl6UkiSTp7tflvO6afolN1jPN/UqdHQTtz7BQf1DINeWrPYIBWf4TZ6yqzCspvVjzVx8nZC1ZZRYXM6C224SMfSrM7TrNi93hV59AgwKujs5h9I7dM/SX0rlqMtJaiWRnN30pIlIVaBhsNEj3hrW+BgPklEPwvM7fAmfGJ9ux6uelNM5uwAtlWAA4BNIGXXO7Jge4z9xnjN6gB4j0SBmfCnvoKxwgUfSQW+czmbHjK4i/rG3/tDYMd024s578ZrTW47FiGdxvO7CJndZQ5htf/H7Xw66fL5/J+SDdtyESGZDHI92/g/3OFZeeW1L61UU/PnrRs1iWLKiGUDQTkaOv/p5NPnXz19c/Hi9Zcd9DoT/yUfJJxZYLyCk2j0CZlUvubFEqC0KlsarqEby6GX5UH1RUzRsiaXqmu6CzcgMUeUqGFR7Mf9Xl6xvaWetj1wf48RpL1wtkiJfAqYd+n+7vJS2BrIVD1AL4SEDR0dqVtR46kOPW8BVIc4Lw1iPl++e/mSK2wDrgX/o/RZGvgoeSTVn6FkyYgzeWkaFhy8LazwOAfKXKc/vXJKEGoZTsrUkDHgxPY6K6Hh5Vdd5k3RZZM72Tn7t1XJ46gml/FagpUXhPDx7JH+cu3uJnAWcPOYKWhc6gbTsdlDZESaDFM57oXoG5fGIibcbqI6a4kJ2eRB984TuVkNz5oop3quWTVlb9yi4BuKNRTVbtggQiGlq0U3w0IYdcqFr4qXXdBxfWsL2OO5zYNqVu7JZ1JE6V5z6c7NHuqrqLQ/ALViR+sj4dDHn6p7E3LgwBqIrT5b835d7zKhWjTAacRDt32Vxo8MDmL6fuR+E49BtXivnUNnkzoXq7DapsOEqftVuVgiC0/McrZ9Ez5YAFpune1lb5xZl6bkb7+gKeSBftTYfoE5lwQtLCMEVzYtvPIgBqoEnckDVfUlDJFytW2rP6CygEeXDMCqqv1G9dzvbhsrXZQaZHy1yVVbLhLuRIw2oOy7yj+A8s4+q7gRS0ZFbYFIXBSepnvLLxKN5FU+r+2pDYuE+9MZZft8RaEkKgRES2bvFD2GW9wpZlDxa9Fo+Hqsg5odygFpC1Xca3HLmVOKyjHdHs1WuHhXNQcX2x6oksqBjkQYMQN61BFFieopXNqjqnab2O1h1qTLmfUHx1+fkNYNYLe3NJ9+BJ9GuURuqcV7wB/Q8N+XIMBGgCisAfUyeq+ik4ATpa8a1GLFajxYM9XptoN833p1fQ0O38AcJqU3d6eNhSivkr3c6VASL3tLv4JlVM+1JkbBxvAWqmdCRSFOVO/gxgTKEbyG63k+Ktba6DMsJYdOA+szEOezqSgLc3mf2wUFNl9h3nUjmi6sA2bxG8wHfB2Ie75ueMbW9FmgfDb+WfQ7XyGqz92UIGGgTd4Ybh90SCPo0OYmL/XCnn4K6vnMM7fb5Q99YI65sPc5YvGdiUduNUcq1/kdnS1/tCDMJJ5XOGZbSjb014iJpIoafJpbVDfdoWADnAgE8LsAYNuuBwBC+YXkUNlN6bYRmjSo2c6r+Wpeb9eeAYJ0OCztg9Js43R8WZ+SdwgeY0DtzLoazJZrAl1mFMiWK2Pbi9gKdoyMeAHc17INYnmFXE1rfAiWax2W2gmi+bAChhbcgbgR4DzkoPDwdnV33/IubTdRkWMI2zwDxUDLRAz25x8pMWVu7TThis0XXO2HV2tY3JRU0RqckyQ3iMgq2e2bHDX54mMxWlEWpmHzII8mcTcpogolEJUABdEl+97Hb9WswxUDHPF2k/jJhY8eIpacmv6E6PTktl5gLAK9e7xF6LFtT60Q8I/qWvVhFUgkwbcISkqJPBN1FleShyBQuYOPa5oLuNLoc3x2rKnNvAPoNIiL0aeQb1MrKvw5vAWKRRfLnVzl5FGlTK9kYlQytiycp1wboOJ0iz1gP8LzeVNcymERxhkMcb97kWwPoj32hgSrSaMrXRrYG1P0Q7ITXqAaKax2bspvDYSx+RZ8EPu0gekfRiS90nII5jE5SA6mBQnJagzM0RigEqOR2w2TnZjpETSEF97nhlK7bqo8jehD+3Z9XxToFYY6QJReD/l3tcbc4S/IziA2S6r8Qq6WioCHHHKAxmBHsY9sSy1ygRldJCeBuIYBJlSl8HH1kWQpplR4Q3Fd28F2F+OzmHGo2VmO1QTuNWgRq3VY/3vW+Q9Ife9sOXYxZyaRkWFZbp8snIha1x7nT6ZLaHDlqrYwCWXogWTNp1jdFGs1oihzq/aPavV2NSyaCLtQRdVj0rnIQbVhoe8J9GGDYCpL5f2OASJN2Fjpiw18QDAn8DMGcpCZs04i2WKZ8gwUyzv3NKaQhI4J0lWSCmSw/K9HRT77+pxjUT7adgNbrCYHQlvl1DZoBzt+bnrAcSDHshZPjeYZuad68OJNn0erejCDtdwDfXiAc89EjLgOdkxCmrtaZ5xbJ08KSeqhPib/shPw4lDcPuIP1Wlgo4Bj7mxnQICEMlybdM5JWOVuhrlXUzfNNW4RaE+MlrC7uVtecW/Qo+i3QVi0cTlwx1Q5C1oTweef4RZ3IfXrH9znVcn4saQaC95f9NZXS3C6s5MgxBbthCjdiYr1gqtIBVOhzNgwiGc4t6VYDpxosSOEG7QCVN7nqgIcQqnE6pqERrh2zZNL5fgEePg9kz7Tpbb28Arq8itd2DROw3Vik/59r/MFsrFDgl0Apv9pnUp3bla6ORQop3/o8gTi7xTs0OWvu3xXdJ0l4IYMI4M3f+aRJaZDqMHxstVpkSKtuLd/oekBV1fXXPyuWyjr/jP3JpMO31mXddThc/r1l5/mCuTu7VwnmOWw6ZhL0rrTWhdhMP99IfXTQEjQRShVCLIXVyQ68N6D/1y4K342Zr8mlInWeyYXZntkp+AkcBFGFHRoYEDw14aU20GLCTtTu1qHjDckwiuFEQGGQ8iyXZD/BwzpAi3N/fHHP3QFthpp5tm3XcxRzxYrxmlDITFNPRFINeNKBYU5i8QYXVvQi3iByD8GIWe2TvmCb5IYco5I+xAj4wFNNSx75VGcqAlwaJ2ClahhF97NBoZBh0hYAatHnXZsryE58PlXtrBsgxoYBbRncAbt4nFIYFzN2u/3tl45+o9FuzmmEHCxCXxJNhR9NwYt3c8/fO83EWOFGd8p0M62fl8Bmyr3SInOAL9kVz0PBPW5rRcJBjDYH7DtfIXe4Vm3EbZvbw89XhSrm+eQVsho0IIcuvC7p9ksNR7jR+i0mKjJH74P9rpgv1Efv8AyKGbb62ZrOs+DWQhzoybV9XXRSIqSqs6xxxvawYyPxueD8cvh2wAV7HMoEjUy3UvHwdi8A5hpOPiLeYkkBllCOKx0A18ouEjxwcUVuI3gxKAzWhj3tE4x1d5s21e3c/A6z3/4/oHfmeiH1pJT9Q/fs6r1KUds5B+MJwtmoLkM0KkPyUcIB0ArCgiH0avoSqVDUXMRINniNwZ4LDcPrQ6jkqySDRY1OaoYdgACM1WWmuFv5DhGTxnV0zSYu5/aSeKrxchBs2E/ZLJw/0O4NA9Lew3rDe/ZCOqLJ4QGWg0nZQ2YCVWhNbVnQW6LegRJv0w2jwXPpu4CAC69mitPTpAD47YYBplhMpTILLV7C8vtUpEGBMY08Cd1gGYiIPWi8B5vnO44JhchqBrjkPsrp4Iu5SjrALCvi8UxO36mTJJPNbtyZ0NYKPpRtdza4xK3+npn3INwRzFPO7LDSJVO9vzFMwvmoFAEgOdcTxTCx7Bewn7cX4MMxUOkziifEBYVrhToAty+EN3OTS135m7AJJRIxKezPJp9NrMqhuFaxisJwITtO7QuB24AVCfYpGLriQdYvt7cL6hZ7DHO1JlZZVQ8IekUSDBk1A0cK2wOXx08JAr8OZrO67vsSXldQvbAE876jwt1tns37Kw5lbec5mJghyMQ4UGZW+75u+yi/Lh0o9nRv6lsEPzCFsFOhvbXTgYmyQ4V7aJ/hOXM+LpwH4GKvFPJ1npuzrUukfiBGaVaCYJHwFXuNqiXlFSNRNYcegCZQ1F6ETsPsLwMHPlyvGOF5KchNncLv9JLLuIG55n8ZUt3Dy7cHV5zvhSlgMwIjMgehPWuKt4JHKUO9Id3s3G196YYV1oeqYxV9miTUSu1NHsFkJ9kGLyh+7c1JG5CcQjSZ4vTkMln8TdRb32+KKfWhDnZ5y9cowy7kTybSgwwyT3FxG8EgKDmvMJ4drFY5joB7Y1yfi+BdgNUljEFsDCNzIB3oadKcrYHhGzbUgUHAwhWy4eaLWLhaJlh3ZwYj6KNTRvnPtDAFjYvGQrzGgTWBUN9UDPEf/amDqmfVz8ORmJ1D/x68edSh1MEDaKfc0aB4HpekV/9yetXAjZg2AfvuB8/GSNnbrvr7E75kzTMqFnSAXJvDmG9eaaFUdkSpwKxs8Lq62jE+wp7kkCcYEhoH971DRiaitH00HNvtqijG8UKuxc5gmlv81ilayIAl2hHCwxQ4iwGxeqT3JwggzohAaJqWYGbtnKWA42asnkkjqqV2Cvc7tKhaHCkg2nZHUD6AeEPHL26InAM/E4tuy6hQi1ROQLDNYPGIc95pPXPgnnZdjswTtojrEB2fURoxmq+A9dobROj6lk5nxcEwfDQ7lRSqrKtCI5wSGVx6hV4rCkuDSHhtfhe9IHgfUFy1NffbWK1fZAZeS9uymuIgjnTdbU+tFVj4qQTrC8TOqHCWS0GRlHqWFSRINzjcgyeF6wUEZDPwLLsKFbcbC64Ewy5PQGzNubW1p4U0BA9oM1JtQSCAvPriJiPdk9+AoHWwWFIoHW4v4lA62D/6OFR3zAx8wB+HQatkOLod+qs33KJ8h5M67+C/gnZtA7ckDT4Mv4AToRxl+6NNIdy39NrPeLnAf6Izwc0W2tbTvJuHTx02xhDgULnQDxb4KwnVKCkBbJrGFJIKCFAXP+zsVGxqETCH/9AWXAfBFeHTlD1X/391UtueRfpJOarxRxuOSci6eeSSy9SnwL5+eMfEK5eRmnB6GdBJz9PUneItPccRQD1dlSASgbMox9zLH83v9OXYFX/+Ae5m2+4xC3RRYmTeJrPEOyc5BY77AmfdKO+7QvI0ssnnhBxrdDsA+vg/n1EZu8YReYgO9iHtY9F5uBkk8gcHB/2ez1DOrj/6wnMDZv1dwn62yQf7D8MievXbnGQdq6DaYnuJY0jd/WmTIm7Jptg9qrkYGgs8NY2npR4/UGPJd5qGdTDxYAgqoIg/LhV9WShKezRDwQtkNqCYBvsiG8C7ozIy7qLxMpo0swKFE1e4JYUGR2unPWL2avLKOEJmEA/IjY4X0xKzrWCV7BghpiW3LnYRJKhZ7ROdlgB7sGy+rqxX5bjzw/3ewfHvc4XfyUgnXw+2g8SnAa9Gj0tmPcefCFEiFPCcbB/0iodPaYwqCZI7jWpcSuqcgIT3vI+LZC3+2UF8VMkoc2aWZMGRLSlZfJqhJlPPFMxObA0cDZBhA0YuOyagA0A2XF4lSHbsWD4xIoM+mQADJjh7Mvmsn/J7gA+M4W8cLIZKDUOAGqaE/dLXEH9bu8o238I4vAgLp+y8QpyG++of/QvuoI2iI/f76Df6h0U6e+/S5J/tST5jgQCkraCG6MLaFIsppe+5I9NXRh5J3vh3wmLw7S1nOYSdifiJXho4JKUXer5grUoCBt05BjFJtFmAFOPLAOMbSR1/sHB4IDuNaje0GspI6ORfEWg3FD2GU7kLWNFIIDO2wAd90GQlyICbuk9pwJnfgmf4kwGT1CgN/p1F66dRhgTh9uIY7YWnIjnSgIC9AXI8yUGqU1Wftp0GAttIaYhcSa5jFwA9d9CegyElpuUETjudO1ViFAHyZ1E/dKcGvCJLulLwqjQ38NCA/Gbs5iTtRlUDHiaP/TTw28qQIlApK8GYYIKxOSaqoEmjacLDHq+gWIxzYmew4dkQYONFjicl7mTRwDpl/PWAokK32mtnxM85zECkkeZSV1wrN7BYCC4vgN3gKfpxbCJ+L6ZY9ZW+Q321hXVAEhMnKUCt3TW2If/clikaoYAZ/QMoR/D8sNcFUwIrTQYOHnpTtvZvwNXffAJLKy1DEZLBNsn/uz28VlwUyfKwsAwGgylOgw0g/AkWm4PijxIagZHYdBgYfqNmgghcmt+4EXTjJW/CDAHZqbnvhYloQTCaYBPYooRaD2scxHusSYDNfJyzd28qRAe7Pb/3ErbCHN02ExClkIWPgHFKV5sD6qQFEISGSBOSzTKqKMmvCSC9SVowxN3J+2LBU+sXJYCtcVRjRt1QW4xzCTLNltNh+BYw2DDIJOSLoDJqbMH5W6xy53APPZ2B8DnE70M8QqNafj0009bvr1NtAdkxmwd32BcQ5IK9BN4ryEtQrgtHiDagOdhN/uyut1hEYKj0pGLasPh8UWC6BhHq1U84a8GxYqIQOiBuH1g2Upbwpe/hhdBq4BjfFPwgY1rSrbdtFjeVGNLuomzolJjg/RraYvYh8w94ZRuUJuYmMLeEsTVLNOZaHYmtNE4RmxmGabS8ARs0W4w8216Vojc8CcBW31f3LXoE+0VqnTTyew251N5Z842886ErDNp2RTPhe2wYqYxLI7DbbeckXJWXmIFRLlWaePRpeo2G1wNALcrr1eBsdHankSvhXfJ2UI5XAVUl7r5pfvhvDJMr4FtxQ1Ahw2ESEqOoZn1FvOJ9McY/kfzOM7rG0qu9pAgEHe3xWQEgA+BInC62xZ7raGredKE5+9e+FHItakQL8rkV03FHzAxMVo0rixrKgK3VLpdKdAj6U3fKPIvlfGmRZ2CVCUsTxx0xLEdHmxCLwyJKCQL29CJQ8Kt3vIt9Y97WifKDkcHotwLdjAtX9UsLrfhe3zwic2uloYPEns3Bct+9+ZlrTBlQg6TiDu7uIj3KDMCG0khNQeZvmcMnDZXZUJLo0GlDlRTgNiyX2gIiSbFmY7pYbHaaFPVveUT7wInTi4uXhpSQfx5hwWlYMTo/lCqZ2KpFyzKX9++PYde4P83ZkrwbrCGkTz0QEPhicYWcCzVqJpIffarlq8SP4Ds3JZjGM58+y7LUa9Bul0stq2BSpTTMHkAtW9v+iS5qMmElnk5m7kvhxxrI5GhL/QEbAgqHp12yEVxfyjGPkEx3P8e3xeK0TvqD07Up+sH8CsVM2t3F/3u0f1tenR7R/sJRMZvzIHW+ct339EhQpQ8OjbBrwkLJpFs15JTl9Dp2TtZUyftQp5LwzIOD6AeGhKIM7fi2O18T3gUYByIaZHknAYwIeGMBjMOGdDECIU78T3N3i0y4Bf1qXc/hVFDYxc9mKEDRLL6LCWG6CeUhySQCo2BqskPl4MCYSNXFysRSafrkSItlKWb/+rZv8x/90WG818nwV/HwV9HwV+HwV9hm/3gr4PgLzsS+xw9hTVVIRkmw8qyE6nrvLxhUsLTqOLqfm//4Unni2jXnGaPkDjlmROUnDjvlv65PhCt+DmtOGbx3vjqtJsH88P3v+xomtcVVt50qyvngqHClxJO7NHN4y6eK8AZ33V7g6P+fne47B7sOjMHIUfu3sDrSASRO1lFMXPieCnNHB2lL7DbeZdjPXvEKV3vHez3Dvf2B3vR+d6dz675Rs1Xbp8vpLPXMzcDz6sLpFWv5VeczkuMtUdfEg/hxtkrcWd7rZHXQwD/7J+c9genBw/ve0kfPHz4sH9sIq8y77/OJZ0SmmHUJ/3Ez3734PgAqdJr9NcdAfF2UpjD09FBIJruFpHe66NIn5TXN8vbAv5vho2zDLes2dIc0ZRmF8JDgUT3kggvwG5CGXIrIHcnxfIT5vRAw6T8toBcbgxpOYsyKVr7EAm3sgHS358US3R0kFTgpAnOhSdNHS4Pqrss7oZ6d34z51rLf/wDlDUBogx3it1f5VX2gPIULgnJ/yD75OvzJ08u+Yb4JPs0+498Ov8L/p/s36VN/zRXh74cjyeXE//Sp9k/4YamXiSDYVlFPTOVwAPs8+Ulk19/8tP7tPCWFkhF75eVZ8ctKIvt5RltdOJ9+C3ItqPu/qDb62VOz3YnfHBwX1RJvz84OLHAxt6viCpplyMheG3dcz8/ZmIfNOzfj/XWfYK2z2jttPVkZhz2Dz4S20zeSHALSiQ2E7xO4Cf3UjHTB8zd4p59Ls+iTlQtxmwrnHb21QwRhPym5o4ADyFg+qi1Pn+mu9869WqoV1Z9OXX7RNuGDVh8nJcLNTLdz+Xp8eHBkVpBB3CK7+pLZl2ILFr+9ZJ9sf7ixd7du5NqlE/KbyMrFv4FaVzB43E5mof/wmei7k5z/BcnPf5JZ0MB85i+JOJM/4CXrfOSjyaUQBdBEXAhxv9ueqDgPPcw1D9aimKHaSIHZhiv3Kc4HfwVVtFSmTvFX7tT86sZG3rHeVWX8t/2AbR1+YEP8t/248FTyB835f/cApvjhHTgVjl4yP13aYr37B8gOxoLAP4GLDiPaYbuaHUpy3CPDUB+x8x53+DFaZr3wj9NP34ZYIZ5wvfGYwH3yaPhatgewnmPRtWyMAOeBHfvjq7kWbsyIPHdnYwLsSf/YR7UFeofmZG4Zdmzf3SxSMfCTJGs3KCfdHztJX6Tt8OV/A6bu8nrG9IK3V18VIwfXg17B8PB1eAozx/mB4P+8OBoeHx1MD4YDURKDmAm6+UlXI01LGRZ1JcUqIcrtHfUG/QOHw56D/9C3rFLJ2Ive5f7l+jjuASP3PC095fv/i+re9hX', 'yes');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(231, 'toolset_executed_upgrade_commands', 'a:3:{i:0;s:55:\"Toolset_Upgrade_Command_Delete_Obsolete_Upgrade_Options\";i:1;s:57:\"Toolset_Upgrade_Command_M2M_V1_Database_Structure_Upgrade\";i:2;s:57:\"Toolset_Upgrade_Command_M2M_V2_Database_Structure_Upgrade\";}', 'no'),
(232, 'toolset_data_structure_version', '3', 'yes'),
(234, 'wpcf_users_options', '1', 'yes'),
(235, 'wpcf-custom-taxonomies', 'a:2:{s:8:\"category\";a:28:{s:4:\"name\";s:8:\"category\";s:5:\"label\";s:13:\"Chuyên mục\";s:6:\"labels\";a:23:{s:4:\"name\";s:13:\"Chuyên mục\";s:13:\"singular_name\";s:13:\"Chuyên mục\";s:12:\"search_items\";s:25:\"Tìm kiếm chuyên mục\";s:13:\"popular_items\";N;s:9:\"all_items\";s:24:\"Tất cả chuyên mục\";s:11:\"parent_item\";s:26:\"Chuyên mục hiện tại\";s:17:\"parent_item_colon\";s:27:\"Chuyên mục hiện tại:\";s:9:\"edit_item\";s:27:\"Chỉnh sửa chuyên mục\";s:9:\"view_item\";s:17:\"Xem chuyên mục\";s:11:\"update_item\";s:21:\"Lưu các thay đổi\";s:12:\"add_new_item\";s:19:\"Thêm chuyên mục\";s:13:\"new_item_name\";s:21:\"Tên danh mục mới\";s:26:\"separate_items_with_commas\";N;s:19:\"add_or_remove_items\";N;s:21:\"choose_from_most_used\";N;s:9:\"not_found\";s:30:\"Không tìm thấy mục nào.\";s:8:\"no_terms\";s:24:\"Không có chuyên mục\";s:21:\"items_list_navigation\";s:41:\"Điều hướng danh sách chuyên mục\";s:10:\"items_list\";s:24:\"Danh sách chuyên mục\";s:9:\"most_used\";s:20:\"Dùng nhiều nhất\";s:13:\"back_to_items\";s:31:\"&larr; Quay lại Chuyên mục\";s:9:\"menu_name\";s:13:\"Chuyên mục\";s:14:\"name_admin_bar\";s:8:\"category\";}s:11:\"description\";s:0:\"\";s:6:\"public\";b:1;s:18:\"publicly_queryable\";b:1;s:12:\"hierarchical\";b:1;s:7:\"show_ui\";b:1;s:12:\"show_in_menu\";b:1;s:17:\"show_in_nav_menus\";b:1;s:13:\"show_tagcloud\";b:1;s:18:\"show_in_quick_edit\";b:1;s:17:\"show_admin_column\";b:1;s:11:\"meta_box_cb\";s:24:\"post_categories_meta_box\";s:20:\"meta_box_sanitize_cb\";s:40:\"taxonomy_meta_box_sanitize_cb_checkboxes\";s:11:\"object_type\";a:1:{i:0;s:4:\"post\";}s:3:\"cap\";a:4:{s:12:\"manage_terms\";s:17:\"manage_categories\";s:10:\"edit_terms\";s:15:\"edit_categories\";s:12:\"delete_terms\";s:17:\"delete_categories\";s:12:\"assign_terms\";s:17:\"assign_categories\";}s:7:\"rewrite\";a:4:{s:10:\"with_front\";b:1;s:12:\"hierarchical\";b:1;s:7:\"ep_mask\";i:512;s:4:\"slug\";s:8:\"category\";}s:9:\"query_var\";s:13:\"category_name\";s:21:\"update_count_callback\";s:0:\"\";s:12:\"show_in_rest\";b:1;s:9:\"rest_base\";s:10:\"categories\";s:21:\"rest_controller_class\";s:24:\"WP_REST_Terms_Controller\";s:12:\"default_term\";N;s:15:\"rest_controller\";N;s:8:\"_builtin\";b:1;s:4:\"slug\";s:8:\"category\";s:8:\"supports\";a:1:{s:4:\"post\";i:1;}}s:8:\"post_tag\";a:28:{s:4:\"name\";s:8:\"post_tag\";s:5:\"label\";s:5:\"Thẻ\";s:6:\"labels\";a:23:{s:4:\"name\";s:5:\"Thẻ\";s:13:\"singular_name\";s:5:\"Thẻ\";s:12:\"search_items\";s:10:\"Tìm Thẻ\";s:13:\"popular_items\";s:18:\"Thẻ phổ biến\";s:9:\"all_items\";s:16:\"Tất cả thẻ\";s:11:\"parent_item\";N;s:17:\"parent_item_colon\";N;s:9:\"edit_item\";s:11:\"Sửa thẻ\";s:9:\"view_item\";s:9:\"Xem thẻ\";s:11:\"update_item\";s:18:\"Cập nhật thẻ\";s:12:\"add_new_item\";s:11:\"Thêm thẻ\";s:13:\"new_item_name\";s:15:\"Thêm tag mới\";s:26:\"separate_items_with_commas\";s:47:\"Phân cách các thẻ bằng dấu phẩy (,).\";s:19:\"add_or_remove_items\";s:16:\"Thêm/Xóa thẻ\";s:21:\"choose_from_most_used\";s:55:\"Chọn từ những thẻ được dùng nhiều nhất\";s:9:\"not_found\";s:43:\"Không tìm thấy thẻ đánh dấu nào.\";s:8:\"no_terms\";s:21:\"Không có thẻ nào\";s:21:\"items_list_navigation\";s:37:\"Điều hướng danh sách thẻ tag\";s:10:\"items_list\";s:14:\"Danh sách tag\";s:9:\"most_used\";s:20:\"Dùng nhiều nhất\";s:13:\"back_to_items\";s:23:\"&larr; Quay lại Thẻ\";s:9:\"menu_name\";s:5:\"Thẻ\";s:14:\"name_admin_bar\";s:8:\"post_tag\";}s:11:\"description\";s:0:\"\";s:6:\"public\";b:1;s:18:\"publicly_queryable\";b:1;s:12:\"hierarchical\";b:0;s:7:\"show_ui\";b:1;s:12:\"show_in_menu\";b:1;s:17:\"show_in_nav_menus\";b:1;s:13:\"show_tagcloud\";b:1;s:18:\"show_in_quick_edit\";b:1;s:17:\"show_admin_column\";b:1;s:11:\"meta_box_cb\";s:18:\"post_tags_meta_box\";s:20:\"meta_box_sanitize_cb\";s:35:\"taxonomy_meta_box_sanitize_cb_input\";s:11:\"object_type\";a:1:{i:0;s:4:\"post\";}s:3:\"cap\";a:4:{s:12:\"manage_terms\";s:16:\"manage_post_tags\";s:10:\"edit_terms\";s:14:\"edit_post_tags\";s:12:\"delete_terms\";s:16:\"delete_post_tags\";s:12:\"assign_terms\";s:16:\"assign_post_tags\";}s:7:\"rewrite\";a:4:{s:10:\"with_front\";b:1;s:12:\"hierarchical\";b:0;s:7:\"ep_mask\";i:1024;s:4:\"slug\";s:3:\"tag\";}s:9:\"query_var\";s:3:\"tag\";s:21:\"update_count_callback\";s:0:\"\";s:12:\"show_in_rest\";b:1;s:9:\"rest_base\";s:4:\"tags\";s:21:\"rest_controller_class\";s:24:\"WP_REST_Terms_Controller\";s:12:\"default_term\";N;s:15:\"rest_controller\";N;s:8:\"_builtin\";b:1;s:4:\"slug\";s:8:\"post_tag\";s:8:\"supports\";a:1:{s:4:\"post\";i:1;}}}', 'yes'),
(240, 'installer_repositories_with_theme', 'a:1:{i:0;s:7:\"toolset\";}', 'yes'),
(245, '_transient_health-check-site-status-result', '{\"good\":12,\"recommended\":7,\"critical\":0}', 'yes'),
(329, 'otgs_active_components', 'a:2:{s:6:\"plugin\";a:5:{i:0;a:3:{s:4:\"File\";s:34:\"advanced-custom-fields-pro/acf.php\";s:4:\"Name\";s:26:\"Advanced Custom Fields PRO\";s:7:\"Version\";s:5:\"5.8.7\";}i:1;a:3:{s:4:\"File\";s:37:\"breadcrumb-navxt/breadcrumb-navxt.php\";s:4:\"Name\";s:16:\"Breadcrumb NavXT\";s:7:\"Version\";s:5:\"6.6.0\";}i:2;a:3:{s:4:\"File\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:4:\"Name\";s:14:\"Contact Form 7\";s:7:\"Version\";s:5:\"5.3.1\";}i:3;a:3:{s:4:\"File\";s:14:\"types/wpcf.php\";s:4:\"Name\";s:13:\"Toolset Types\";s:7:\"Version\";s:5:\"2.3.5\";}i:4;a:3:{s:4:\"File\";s:27:\"wp-pagenavi/wp-pagenavi.php\";s:4:\"Name\";s:11:\"WP-PageNavi\";s:7:\"Version\";s:6:\"2.93.3\";}}s:5:\"theme\";a:1:{i:0;a:3:{s:8:\"Template\";s:13:\"corewordpress\";s:4:\"Name\";s:4:\"Core\";s:7:\"Version\";s:3:\"1.0\";}}}', 'yes'),
(356, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:\"auto_add\";a:0:{}}', 'yes'),
(396, 'recovery_mode_email_last_sent', '1618029993', 'yes'),
(397, 'services-cat_children', 'a:0:{}', 'yes'),
(398, 'bcn_version', '6.6.0', 'no'),
(399, 'bcn_options_bk', 'a:94:{s:17:\"bmainsite_display\";b:1;s:18:\"Hmainsite_template\";s:251:\"<span property=\"itemListElement\" typeof=\"ListItem\"><a property=\"item\" typeof=\"WebPage\" title=\"Go to %title%.\" href=\"%link%\" class=\"%type%\" bcn-aria-current><span property=\"name\">%htitle%</span></a><meta property=\"position\" content=\"%position%\"></span>\";s:28:\"Hmainsite_template_no_anchor\";s:195:\"<span property=\"itemListElement\" typeof=\"ListItem\"><span property=\"name\" class=\"%type%\">%htitle%</span><meta property=\"url\" content=\"%link%\"><meta property=\"position\" content=\"%position%\"></span>\";s:13:\"bhome_display\";b:0;s:14:\"Hhome_template\";s:251:\"<span property=\"itemListElement\" typeof=\"ListItem\"><a property=\"item\" typeof=\"WebPage\" title=\"Go to %title%.\" href=\"%link%\" class=\"%type%\" bcn-aria-current><span property=\"name\">%htitle%</span></a><meta property=\"position\" content=\"%position%\"></span>\";s:24:\"Hhome_template_no_anchor\";s:195:\"<span property=\"itemListElement\" typeof=\"ListItem\"><span property=\"name\" class=\"%type%\">%htitle%</span><meta property=\"url\" content=\"%link%\"><meta property=\"position\" content=\"%position%\"></span>\";s:13:\"bblog_display\";b:1;s:10:\"hseparator\";s:6:\" &gt; \";s:12:\"blimit_title\";b:0;s:17:\"amax_title_length\";i:20;s:20:\"bcurrent_item_linked\";b:0;s:28:\"bpost_page_hierarchy_display\";b:1;s:33:\"bpost_page_hierarchy_parent_first\";b:1;s:25:\"Spost_page_hierarchy_type\";s:15:\"BCN_POST_PARENT\";s:19:\"Hpost_page_template\";s:251:\"<span property=\"itemListElement\" typeof=\"ListItem\"><a property=\"item\" typeof=\"WebPage\" title=\"Go to %title%.\" href=\"%link%\" class=\"%type%\" bcn-aria-current><span property=\"name\">%htitle%</span></a><meta property=\"position\" content=\"%position%\"></span>\";s:29:\"Hpost_page_template_no_anchor\";s:195:\"<span property=\"itemListElement\" typeof=\"ListItem\"><span property=\"name\" class=\"%type%\">%htitle%</span><meta property=\"url\" content=\"%link%\"><meta property=\"position\" content=\"%position%\"></span>\";s:15:\"apost_page_root\";s:2:\"12\";s:15:\"Hpaged_template\";s:41:\"<span class=\"%type%\">Page %htitle%</span>\";s:14:\"bpaged_display\";b:0;s:19:\"Hpost_post_template\";s:251:\"<span property=\"itemListElement\" typeof=\"ListItem\"><a property=\"item\" typeof=\"WebPage\" title=\"Go to %title%.\" href=\"%link%\" class=\"%type%\" bcn-aria-current><span property=\"name\">%htitle%</span></a><meta property=\"position\" content=\"%position%\"></span>\";s:29:\"Hpost_post_template_no_anchor\";s:195:\"<span property=\"itemListElement\" typeof=\"ListItem\"><span property=\"name\" class=\"%type%\">%htitle%</span><meta property=\"url\" content=\"%link%\"><meta property=\"position\" content=\"%position%\"></span>\";s:15:\"apost_post_root\";s:1:\"0\";s:28:\"bpost_post_hierarchy_display\";b:1;s:33:\"bpost_post_hierarchy_parent_first\";b:1;s:27:\"bpost_post_taxonomy_referer\";b:0;s:25:\"Spost_post_hierarchy_type\";s:8:\"category\";s:32:\"bpost_attachment_archive_display\";b:1;s:34:\"bpost_attachment_hierarchy_display\";b:1;s:39:\"bpost_attachment_hierarchy_parent_first\";b:1;s:33:\"bpost_attachment_taxonomy_referer\";b:0;s:31:\"Spost_attachment_hierarchy_type\";s:15:\"BCN_POST_PARENT\";s:21:\"apost_attachment_root\";i:0;s:25:\"Hpost_attachment_template\";s:251:\"<span property=\"itemListElement\" typeof=\"ListItem\"><a property=\"item\" typeof=\"WebPage\" title=\"Go to %title%.\" href=\"%link%\" class=\"%type%\" bcn-aria-current><span property=\"name\">%htitle%</span></a><meta property=\"position\" content=\"%position%\"></span>\";s:35:\"Hpost_attachment_template_no_anchor\";s:195:\"<span property=\"itemListElement\" typeof=\"ListItem\"><span property=\"name\" class=\"%type%\">%htitle%</span><meta property=\"url\" content=\"%link%\"><meta property=\"position\" content=\"%position%\"></span>\";s:13:\"H404_template\";s:195:\"<span property=\"itemListElement\" typeof=\"ListItem\"><span property=\"name\" class=\"%type%\">%htitle%</span><meta property=\"url\" content=\"%link%\"><meta property=\"position\" content=\"%position%\"></span>\";s:10:\"S404_title\";s:3:\"404\";s:16:\"Hsearch_template\";s:316:\"<span property=\"itemListElement\" typeof=\"ListItem\"><span property=\"name\">Tìm kiếm cho &#039;<a property=\"item\" typeof=\"WebPage\" title=\"Go to the first page of search results for %title%.\" href=\"%link%\" class=\"%type%\" bcn-aria-current>%htitle%</a>&#039;</span><meta property=\"position\" content=\"%position%\"></span>\";s:26:\"Hsearch_template_no_anchor\";s:64:\"<span class=\"%type%\">Tìm kiếm cho &#039;%htitle%&#039;</span>\";s:22:\"Htax_post_tag_template\";s:268:\"<span property=\"itemListElement\" typeof=\"ListItem\"><a property=\"item\" typeof=\"WebPage\" title=\"Go to the %title% tag archives.\" href=\"%link%\" class=\"%type%\" bcn-aria-current><span property=\"name\">%htitle%</span></a><meta property=\"position\" content=\"%position%\"></span>\";s:32:\"Htax_post_tag_template_no_anchor\";s:195:\"<span property=\"itemListElement\" typeof=\"ListItem\"><span property=\"name\" class=\"%type%\">%htitle%</span><meta property=\"url\" content=\"%link%\"><meta property=\"position\" content=\"%position%\"></span>\";s:25:\"Htax_post_format_template\";s:264:\"<span property=\"itemListElement\" typeof=\"ListItem\"><a property=\"item\" typeof=\"WebPage\" title=\"Go to the %title% archives.\" href=\"%link%\" class=\"%type%\" bcn-aria-current><span property=\"name\">%htitle%</span></a><meta property=\"position\" content=\"%position%\"></span>\";s:35:\"Htax_post_format_template_no_anchor\";s:195:\"<span property=\"itemListElement\" typeof=\"ListItem\"><span property=\"name\" class=\"%type%\">%htitle%</span><meta property=\"url\" content=\"%link%\"><meta property=\"position\" content=\"%position%\"></span>\";s:16:\"Hauthor_template\";s:258:\"<span property=\"itemListElement\" typeof=\"ListItem\"><span property=\"name\">Articles by: <a title=\"Go to the first page of posts by %title%.\" href=\"%link%\" class=\"%type%\" bcn-aria-current>%htitle%</a></span><meta property=\"position\" content=\"%position%\"></span>\";s:26:\"Hauthor_template_no_anchor\";s:49:\"<span class=\"%type%\">Articles by: %htitle%</span>\";s:12:\"Sauthor_name\";s:12:\"display_name\";s:12:\"aauthor_root\";i:0;s:22:\"Htax_category_template\";s:273:\"<span property=\"itemListElement\" typeof=\"ListItem\"><a property=\"item\" typeof=\"WebPage\" title=\"Go to the %title% category archives.\" href=\"%link%\" class=\"%type%\" bcn-aria-current><span property=\"name\">%htitle%</span></a><meta property=\"position\" content=\"%position%\"></span>\";s:32:\"Htax_category_template_no_anchor\";s:195:\"<span property=\"itemListElement\" typeof=\"ListItem\"><span property=\"name\" class=\"%type%\">%htitle%</span><meta property=\"url\" content=\"%link%\"><meta property=\"position\" content=\"%position%\"></span>\";s:14:\"Hdate_template\";s:264:\"<span property=\"itemListElement\" typeof=\"ListItem\"><a property=\"item\" typeof=\"WebPage\" title=\"Go to the %title% archives.\" href=\"%link%\" class=\"%type%\" bcn-aria-current><span property=\"name\">%htitle%</span></a><meta property=\"position\" content=\"%position%\"></span>\";s:24:\"Hdate_template_no_anchor\";s:195:\"<span property=\"itemListElement\" typeof=\"ListItem\"><span property=\"name\" class=\"%type%\">%htitle%</span><meta property=\"url\" content=\"%link%\"><meta property=\"position\" content=\"%position%\"></span>\";s:38:\"bpost_acf-field-group_taxonomy_referer\";b:0;s:30:\"Hpost_acf-field-group_template\";s:251:\"<span property=\"itemListElement\" typeof=\"ListItem\"><a property=\"item\" typeof=\"WebPage\" title=\"Go to %title%.\" href=\"%link%\" class=\"%type%\" bcn-aria-current><span property=\"name\">%htitle%</span></a><meta property=\"position\" content=\"%position%\"></span>\";s:40:\"Hpost_acf-field-group_template_no_anchor\";s:195:\"<span property=\"itemListElement\" typeof=\"ListItem\"><span property=\"name\" class=\"%type%\">%htitle%</span><meta property=\"url\" content=\"%link%\"><meta property=\"position\" content=\"%position%\"></span>\";s:26:\"apost_acf-field-group_root\";i:0;s:39:\"bpost_acf-field-group_hierarchy_display\";b:0;s:37:\"bpost_acf-field-group_archive_display\";b:0;s:36:\"Spost_acf-field-group_hierarchy_type\";s:10:\"BCN_PARENT\";s:44:\"bpost_acf-field-group_hierarchy_parent_first\";b:0;s:32:\"bpost_acf-field_taxonomy_referer\";b:0;s:24:\"Hpost_acf-field_template\";s:251:\"<span property=\"itemListElement\" typeof=\"ListItem\"><a property=\"item\" typeof=\"WebPage\" title=\"Go to %title%.\" href=\"%link%\" class=\"%type%\" bcn-aria-current><span property=\"name\">%htitle%</span></a><meta property=\"position\" content=\"%position%\"></span>\";s:34:\"Hpost_acf-field_template_no_anchor\";s:195:\"<span property=\"itemListElement\" typeof=\"ListItem\"><span property=\"name\" class=\"%type%\">%htitle%</span><meta property=\"url\" content=\"%link%\"><meta property=\"position\" content=\"%position%\"></span>\";s:20:\"apost_acf-field_root\";i:0;s:33:\"bpost_acf-field_hierarchy_display\";b:0;s:31:\"bpost_acf-field_archive_display\";b:0;s:30:\"Spost_acf-field_hierarchy_type\";s:10:\"BCN_PARENT\";s:38:\"bpost_acf-field_hierarchy_parent_first\";b:0;s:41:\"bpost_wpcf7_contact_form_taxonomy_referer\";b:0;s:33:\"Hpost_wpcf7_contact_form_template\";s:251:\"<span property=\"itemListElement\" typeof=\"ListItem\"><a property=\"item\" typeof=\"WebPage\" title=\"Go to %title%.\" href=\"%link%\" class=\"%type%\" bcn-aria-current><span property=\"name\">%htitle%</span></a><meta property=\"position\" content=\"%position%\"></span>\";s:43:\"Hpost_wpcf7_contact_form_template_no_anchor\";s:195:\"<span property=\"itemListElement\" typeof=\"ListItem\"><span property=\"name\" class=\"%type%\">%htitle%</span><meta property=\"url\" content=\"%link%\"><meta property=\"position\" content=\"%position%\"></span>\";s:29:\"apost_wpcf7_contact_form_root\";i:0;s:42:\"bpost_wpcf7_contact_form_hierarchy_display\";b:0;s:40:\"bpost_wpcf7_contact_form_archive_display\";b:0;s:39:\"Spost_wpcf7_contact_form_hierarchy_type\";s:8:\"BCN_DATE\";s:47:\"bpost_wpcf7_contact_form_hierarchy_parent_first\";b:0;s:30:\"bpost_service_taxonomy_referer\";b:0;s:22:\"Hpost_service_template\";s:251:\"<span property=\"itemListElement\" typeof=\"ListItem\"><a property=\"item\" typeof=\"WebPage\" title=\"Go to %title%.\" href=\"%link%\" class=\"%type%\" bcn-aria-current><span property=\"name\">%htitle%</span></a><meta property=\"position\" content=\"%position%\"></span>\";s:32:\"Hpost_service_template_no_anchor\";s:195:\"<span property=\"itemListElement\" typeof=\"ListItem\"><span property=\"name\" class=\"%type%\">%htitle%</span><meta property=\"url\" content=\"%link%\"><meta property=\"position\" content=\"%position%\"></span>\";s:18:\"apost_service_root\";i:0;s:31:\"bpost_service_hierarchy_display\";b:1;s:29:\"bpost_service_archive_display\";b:1;s:28:\"Spost_service_hierarchy_type\";s:11:\"service-cat\";s:36:\"bpost_service_hierarchy_parent_first\";b:0;s:25:\"Htax_service-cat_template\";s:287:\"<span property=\"itemListElement\" typeof=\"ListItem\"><a property=\"item\" typeof=\"WebPage\" title=\"Go to the %title% Danh mục Dịch vụ archives.\" href=\"%link%\" class=\"%type%\" bcn-aria-current><span property=\"name\">%htitle%</span></a><meta property=\"position\" content=\"%position%\"></span>\";s:35:\"Htax_service-cat_template_no_anchor\";s:142:\"<span property=\"itemListElement\" typeof=\"ListItem\"><span property=\"name\">%htitle%</span><meta property=\"position\" content=\"%position%\"></span>\";s:31:\"bpost_services_taxonomy_referer\";b:0;s:23:\"Hpost_services_template\";s:251:\"<span property=\"itemListElement\" typeof=\"ListItem\"><a property=\"item\" typeof=\"WebPage\" title=\"Go to %title%.\" href=\"%link%\" class=\"%type%\" bcn-aria-current><span property=\"name\">%htitle%</span></a><meta property=\"position\" content=\"%position%\"></span>\";s:33:\"Hpost_services_template_no_anchor\";s:195:\"<span property=\"itemListElement\" typeof=\"ListItem\"><span property=\"name\" class=\"%type%\">%htitle%</span><meta property=\"url\" content=\"%link%\"><meta property=\"position\" content=\"%position%\"></span>\";s:19:\"apost_services_root\";i:0;s:32:\"bpost_services_hierarchy_display\";b:0;s:30:\"bpost_services_archive_display\";b:0;s:29:\"Spost_services_hierarchy_type\";s:12:\"services-cat\";s:37:\"bpost_services_hierarchy_parent_first\";b:0;s:26:\"Htax_services-cat_template\";s:287:\"<span property=\"itemListElement\" typeof=\"ListItem\"><a property=\"item\" typeof=\"WebPage\" title=\"Go to the %title% Danh mục Dịch vụ archives.\" href=\"%link%\" class=\"%type%\" bcn-aria-current><span property=\"name\">%htitle%</span></a><meta property=\"position\" content=\"%position%\"></span>\";s:36:\"Htax_services-cat_template_no_anchor\";s:142:\"<span property=\"itemListElement\" typeof=\"ListItem\"><span property=\"name\">%htitle%</span><meta property=\"position\" content=\"%position%\"></span>\";}', 'yes'),
(400, 'bcn_options', 'a:94:{s:17:\"bmainsite_display\";b:1;s:18:\"Hmainsite_template\";s:251:\"<span property=\"itemListElement\" typeof=\"ListItem\"><a property=\"item\" typeof=\"WebPage\" title=\"Go to %title%.\" href=\"%link%\" class=\"%type%\" bcn-aria-current><span property=\"name\">%htitle%</span></a><meta property=\"position\" content=\"%position%\"></span>\";s:28:\"Hmainsite_template_no_anchor\";s:195:\"<span property=\"itemListElement\" typeof=\"ListItem\"><span property=\"name\" class=\"%type%\">%htitle%</span><meta property=\"url\" content=\"%link%\"><meta property=\"position\" content=\"%position%\"></span>\";s:13:\"bhome_display\";b:0;s:14:\"Hhome_template\";s:251:\"<span property=\"itemListElement\" typeof=\"ListItem\"><a property=\"item\" typeof=\"WebPage\" title=\"Go to %title%.\" href=\"%link%\" class=\"%type%\" bcn-aria-current><span property=\"name\">%htitle%</span></a><meta property=\"position\" content=\"%position%\"></span>\";s:24:\"Hhome_template_no_anchor\";s:195:\"<span property=\"itemListElement\" typeof=\"ListItem\"><span property=\"name\" class=\"%type%\">%htitle%</span><meta property=\"url\" content=\"%link%\"><meta property=\"position\" content=\"%position%\"></span>\";s:13:\"bblog_display\";b:1;s:10:\"hseparator\";s:3:\" / \";s:12:\"blimit_title\";b:0;s:17:\"amax_title_length\";i:20;s:20:\"bcurrent_item_linked\";b:0;s:28:\"bpost_page_hierarchy_display\";b:1;s:33:\"bpost_page_hierarchy_parent_first\";b:1;s:25:\"Spost_page_hierarchy_type\";s:15:\"BCN_POST_PARENT\";s:19:\"Hpost_page_template\";s:251:\"<span property=\"itemListElement\" typeof=\"ListItem\"><a property=\"item\" typeof=\"WebPage\" title=\"Go to %title%.\" href=\"%link%\" class=\"%type%\" bcn-aria-current><span property=\"name\">%htitle%</span></a><meta property=\"position\" content=\"%position%\"></span>\";s:29:\"Hpost_page_template_no_anchor\";s:195:\"<span property=\"itemListElement\" typeof=\"ListItem\"><span property=\"name\" class=\"%type%\">%htitle%</span><meta property=\"url\" content=\"%link%\"><meta property=\"position\" content=\"%position%\"></span>\";s:15:\"apost_page_root\";s:2:\"12\";s:15:\"Hpaged_template\";s:41:\"<span class=\"%type%\">Page %htitle%</span>\";s:14:\"bpaged_display\";b:0;s:19:\"Hpost_post_template\";s:251:\"<span property=\"itemListElement\" typeof=\"ListItem\"><a property=\"item\" typeof=\"WebPage\" title=\"Go to %title%.\" href=\"%link%\" class=\"%type%\" bcn-aria-current><span property=\"name\">%htitle%</span></a><meta property=\"position\" content=\"%position%\"></span>\";s:29:\"Hpost_post_template_no_anchor\";s:195:\"<span property=\"itemListElement\" typeof=\"ListItem\"><span property=\"name\" class=\"%type%\">%htitle%</span><meta property=\"url\" content=\"%link%\"><meta property=\"position\" content=\"%position%\"></span>\";s:15:\"apost_post_root\";s:1:\"0\";s:28:\"bpost_post_hierarchy_display\";b:1;s:33:\"bpost_post_hierarchy_parent_first\";b:1;s:27:\"bpost_post_taxonomy_referer\";b:0;s:25:\"Spost_post_hierarchy_type\";s:8:\"category\";s:32:\"bpost_attachment_archive_display\";b:1;s:34:\"bpost_attachment_hierarchy_display\";b:1;s:39:\"bpost_attachment_hierarchy_parent_first\";b:1;s:33:\"bpost_attachment_taxonomy_referer\";b:0;s:31:\"Spost_attachment_hierarchy_type\";s:15:\"BCN_POST_PARENT\";s:21:\"apost_attachment_root\";i:0;s:25:\"Hpost_attachment_template\";s:251:\"<span property=\"itemListElement\" typeof=\"ListItem\"><a property=\"item\" typeof=\"WebPage\" title=\"Go to %title%.\" href=\"%link%\" class=\"%type%\" bcn-aria-current><span property=\"name\">%htitle%</span></a><meta property=\"position\" content=\"%position%\"></span>\";s:35:\"Hpost_attachment_template_no_anchor\";s:195:\"<span property=\"itemListElement\" typeof=\"ListItem\"><span property=\"name\" class=\"%type%\">%htitle%</span><meta property=\"url\" content=\"%link%\"><meta property=\"position\" content=\"%position%\"></span>\";s:13:\"H404_template\";s:195:\"<span property=\"itemListElement\" typeof=\"ListItem\"><span property=\"name\" class=\"%type%\">%htitle%</span><meta property=\"url\" content=\"%link%\"><meta property=\"position\" content=\"%position%\"></span>\";s:10:\"S404_title\";s:3:\"404\";s:16:\"Hsearch_template\";s:316:\"<span property=\"itemListElement\" typeof=\"ListItem\"><span property=\"name\">Tìm kiếm cho &#039;<a property=\"item\" typeof=\"WebPage\" title=\"Go to the first page of search results for %title%.\" href=\"%link%\" class=\"%type%\" bcn-aria-current>%htitle%</a>&#039;</span><meta property=\"position\" content=\"%position%\"></span>\";s:26:\"Hsearch_template_no_anchor\";s:64:\"<span class=\"%type%\">Tìm kiếm cho &#039;%htitle%&#039;</span>\";s:22:\"Htax_post_tag_template\";s:268:\"<span property=\"itemListElement\" typeof=\"ListItem\"><a property=\"item\" typeof=\"WebPage\" title=\"Go to the %title% tag archives.\" href=\"%link%\" class=\"%type%\" bcn-aria-current><span property=\"name\">%htitle%</span></a><meta property=\"position\" content=\"%position%\"></span>\";s:32:\"Htax_post_tag_template_no_anchor\";s:195:\"<span property=\"itemListElement\" typeof=\"ListItem\"><span property=\"name\" class=\"%type%\">%htitle%</span><meta property=\"url\" content=\"%link%\"><meta property=\"position\" content=\"%position%\"></span>\";s:25:\"Htax_post_format_template\";s:264:\"<span property=\"itemListElement\" typeof=\"ListItem\"><a property=\"item\" typeof=\"WebPage\" title=\"Go to the %title% archives.\" href=\"%link%\" class=\"%type%\" bcn-aria-current><span property=\"name\">%htitle%</span></a><meta property=\"position\" content=\"%position%\"></span>\";s:35:\"Htax_post_format_template_no_anchor\";s:195:\"<span property=\"itemListElement\" typeof=\"ListItem\"><span property=\"name\" class=\"%type%\">%htitle%</span><meta property=\"url\" content=\"%link%\"><meta property=\"position\" content=\"%position%\"></span>\";s:16:\"Hauthor_template\";s:258:\"<span property=\"itemListElement\" typeof=\"ListItem\"><span property=\"name\">Articles by: <a title=\"Go to the first page of posts by %title%.\" href=\"%link%\" class=\"%type%\" bcn-aria-current>%htitle%</a></span><meta property=\"position\" content=\"%position%\"></span>\";s:26:\"Hauthor_template_no_anchor\";s:49:\"<span class=\"%type%\">Articles by: %htitle%</span>\";s:12:\"Sauthor_name\";s:12:\"display_name\";s:12:\"aauthor_root\";i:0;s:22:\"Htax_category_template\";s:273:\"<span property=\"itemListElement\" typeof=\"ListItem\"><a property=\"item\" typeof=\"WebPage\" title=\"Go to the %title% category archives.\" href=\"%link%\" class=\"%type%\" bcn-aria-current><span property=\"name\">%htitle%</span></a><meta property=\"position\" content=\"%position%\"></span>\";s:32:\"Htax_category_template_no_anchor\";s:195:\"<span property=\"itemListElement\" typeof=\"ListItem\"><span property=\"name\" class=\"%type%\">%htitle%</span><meta property=\"url\" content=\"%link%\"><meta property=\"position\" content=\"%position%\"></span>\";s:14:\"Hdate_template\";s:264:\"<span property=\"itemListElement\" typeof=\"ListItem\"><a property=\"item\" typeof=\"WebPage\" title=\"Go to the %title% archives.\" href=\"%link%\" class=\"%type%\" bcn-aria-current><span property=\"name\">%htitle%</span></a><meta property=\"position\" content=\"%position%\"></span>\";s:24:\"Hdate_template_no_anchor\";s:195:\"<span property=\"itemListElement\" typeof=\"ListItem\"><span property=\"name\" class=\"%type%\">%htitle%</span><meta property=\"url\" content=\"%link%\"><meta property=\"position\" content=\"%position%\"></span>\";s:38:\"bpost_acf-field-group_taxonomy_referer\";b:0;s:30:\"Hpost_acf-field-group_template\";s:251:\"<span property=\"itemListElement\" typeof=\"ListItem\"><a property=\"item\" typeof=\"WebPage\" title=\"Go to %title%.\" href=\"%link%\" class=\"%type%\" bcn-aria-current><span property=\"name\">%htitle%</span></a><meta property=\"position\" content=\"%position%\"></span>\";s:40:\"Hpost_acf-field-group_template_no_anchor\";s:195:\"<span property=\"itemListElement\" typeof=\"ListItem\"><span property=\"name\" class=\"%type%\">%htitle%</span><meta property=\"url\" content=\"%link%\"><meta property=\"position\" content=\"%position%\"></span>\";s:26:\"apost_acf-field-group_root\";i:0;s:39:\"bpost_acf-field-group_hierarchy_display\";b:0;s:37:\"bpost_acf-field-group_archive_display\";b:0;s:36:\"Spost_acf-field-group_hierarchy_type\";s:10:\"BCN_PARENT\";s:44:\"bpost_acf-field-group_hierarchy_parent_first\";b:0;s:32:\"bpost_acf-field_taxonomy_referer\";b:0;s:24:\"Hpost_acf-field_template\";s:251:\"<span property=\"itemListElement\" typeof=\"ListItem\"><a property=\"item\" typeof=\"WebPage\" title=\"Go to %title%.\" href=\"%link%\" class=\"%type%\" bcn-aria-current><span property=\"name\">%htitle%</span></a><meta property=\"position\" content=\"%position%\"></span>\";s:34:\"Hpost_acf-field_template_no_anchor\";s:195:\"<span property=\"itemListElement\" typeof=\"ListItem\"><span property=\"name\" class=\"%type%\">%htitle%</span><meta property=\"url\" content=\"%link%\"><meta property=\"position\" content=\"%position%\"></span>\";s:20:\"apost_acf-field_root\";i:0;s:33:\"bpost_acf-field_hierarchy_display\";b:0;s:31:\"bpost_acf-field_archive_display\";b:0;s:30:\"Spost_acf-field_hierarchy_type\";s:10:\"BCN_PARENT\";s:38:\"bpost_acf-field_hierarchy_parent_first\";b:0;s:41:\"bpost_wpcf7_contact_form_taxonomy_referer\";b:0;s:33:\"Hpost_wpcf7_contact_form_template\";s:251:\"<span property=\"itemListElement\" typeof=\"ListItem\"><a property=\"item\" typeof=\"WebPage\" title=\"Go to %title%.\" href=\"%link%\" class=\"%type%\" bcn-aria-current><span property=\"name\">%htitle%</span></a><meta property=\"position\" content=\"%position%\"></span>\";s:43:\"Hpost_wpcf7_contact_form_template_no_anchor\";s:195:\"<span property=\"itemListElement\" typeof=\"ListItem\"><span property=\"name\" class=\"%type%\">%htitle%</span><meta property=\"url\" content=\"%link%\"><meta property=\"position\" content=\"%position%\"></span>\";s:29:\"apost_wpcf7_contact_form_root\";i:0;s:42:\"bpost_wpcf7_contact_form_hierarchy_display\";b:0;s:40:\"bpost_wpcf7_contact_form_archive_display\";b:0;s:39:\"Spost_wpcf7_contact_form_hierarchy_type\";s:8:\"BCN_DATE\";s:47:\"bpost_wpcf7_contact_form_hierarchy_parent_first\";b:0;s:30:\"bpost_service_taxonomy_referer\";b:0;s:22:\"Hpost_service_template\";s:251:\"<span property=\"itemListElement\" typeof=\"ListItem\"><a property=\"item\" typeof=\"WebPage\" title=\"Go to %title%.\" href=\"%link%\" class=\"%type%\" bcn-aria-current><span property=\"name\">%htitle%</span></a><meta property=\"position\" content=\"%position%\"></span>\";s:32:\"Hpost_service_template_no_anchor\";s:195:\"<span property=\"itemListElement\" typeof=\"ListItem\"><span property=\"name\" class=\"%type%\">%htitle%</span><meta property=\"url\" content=\"%link%\"><meta property=\"position\" content=\"%position%\"></span>\";s:18:\"apost_service_root\";i:0;s:31:\"bpost_service_hierarchy_display\";b:0;s:29:\"bpost_service_archive_display\";b:0;s:28:\"Spost_service_hierarchy_type\";s:11:\"service-cat\";s:36:\"bpost_service_hierarchy_parent_first\";b:0;s:25:\"Htax_service-cat_template\";s:287:\"<span property=\"itemListElement\" typeof=\"ListItem\"><a property=\"item\" typeof=\"WebPage\" title=\"Go to the %title% Danh mục Dịch vụ archives.\" href=\"%link%\" class=\"%type%\" bcn-aria-current><span property=\"name\">%htitle%</span></a><meta property=\"position\" content=\"%position%\"></span>\";s:35:\"Htax_service-cat_template_no_anchor\";s:142:\"<span property=\"itemListElement\" typeof=\"ListItem\"><span property=\"name\">%htitle%</span><meta property=\"position\" content=\"%position%\"></span>\";s:31:\"bpost_services_taxonomy_referer\";b:0;s:23:\"Hpost_services_template\";s:251:\"<span property=\"itemListElement\" typeof=\"ListItem\"><a property=\"item\" typeof=\"WebPage\" title=\"Go to %title%.\" href=\"%link%\" class=\"%type%\" bcn-aria-current><span property=\"name\">%htitle%</span></a><meta property=\"position\" content=\"%position%\"></span>\";s:33:\"Hpost_services_template_no_anchor\";s:195:\"<span property=\"itemListElement\" typeof=\"ListItem\"><span property=\"name\" class=\"%type%\">%htitle%</span><meta property=\"url\" content=\"%link%\"><meta property=\"position\" content=\"%position%\"></span>\";s:19:\"apost_services_root\";i:0;s:32:\"bpost_services_hierarchy_display\";b:0;s:30:\"bpost_services_archive_display\";b:0;s:29:\"Spost_services_hierarchy_type\";s:12:\"services-cat\";s:37:\"bpost_services_hierarchy_parent_first\";b:0;s:26:\"Htax_services-cat_template\";s:287:\"<span property=\"itemListElement\" typeof=\"ListItem\"><a property=\"item\" typeof=\"WebPage\" title=\"Go to the %title% Danh mục Dịch vụ archives.\" href=\"%link%\" class=\"%type%\" bcn-aria-current><span property=\"name\">%htitle%</span></a><meta property=\"position\" content=\"%position%\"></span>\";s:36:\"Htax_services-cat_template_no_anchor\";s:142:\"<span property=\"itemListElement\" typeof=\"ListItem\"><span property=\"name\">%htitle%</span><meta property=\"position\" content=\"%position%\"></span>\";}', 'yes'),
(489, 'secret_key', 'waj^zLk5#g]jPe##o_2fa601ko~W6#.Y.+.aws</rkcgOt,XaHI17gb/sx!y!FX>', 'no'),
(578, 'options_f_one_title', '', 'no'),
(579, '_options_f_one_title', 'field_608a223db8e5a', 'no'),
(580, 'options_f_one_address', '', 'no'),
(581, '_options_f_one_address', 'field_60669d2993e70', 'no'),
(582, 'options_f_one_phone', '', 'no'),
(583, '_options_f_one_phone', 'field_60669da393e71', 'no'),
(584, 'options_f_one_email', '', 'no'),
(585, '_options_f_one_email', 'field_603c6d8294513', 'no'),
(586, 'options_f_two_title', '', 'no'),
(587, '_options_f_two_title', 'field_608a2255b8e5b', 'no'),
(588, 'options_f_two_address', '', 'no'),
(589, '_options_f_two_address', 'field_608a21ecb6a48', 'no'),
(590, 'options_f_two_phone', '', 'no'),
(591, '_options_f_two_phone', 'field_608a21f1b6a49', 'no'),
(592, 'options_f_two_email', '', 'no'),
(593, '_options_f_two_email', 'field_608a21f6b6a4a', 'no'),
(594, 'options_f_support_title', '', 'no'),
(595, '_options_f_support_title', 'field_60d937d521038', 'no'),
(596, 'options_f_support_select', '', 'no'),
(597, '_options_f_support_select', 'field_60d937f721039', 'no'),
(598, 'options_f_fanpage_title', '', 'no'),
(599, '_options_f_fanpage_title', 'field_6075682411da3', 'no'),
(600, 'options_f_fanpage_iframe', '', 'no'),
(601, '_options_f_fanpage_iframe', 'field_603c6ea89451d', 'no'),
(602, 'options_f_socical_facebook', 'https://facebook', 'no'),
(603, '_options_f_socical_facebook', 'field_60d938652103b', 'no'),
(604, 'options_f_socical_youtube', 'https://youtube', 'no'),
(605, '_options_f_socical_youtube', 'field_60d938822103d', 'no'),
(606, 'options_f_socical_insta', 'https://insta', 'no'),
(607, '_options_f_socical_insta', 'field_60d9388d2103e', 'no'),
(608, 'options_f_socical_twiter', 'https://twitter', 'no'),
(609, '_options_f_socical_twiter', 'field_60d938972103f', 'no'),
(610, 'options_f_bottom_copyright', '© 2021 Ecommerce software by <a href=\"#\" title=\"\">GCO Software</a>', 'no'),
(611, '_options_f_bottom_copyright', 'field_60d938d721040', 'no'),
(612, 'options_f_bottom_menu', '', 'no'),
(613, '_options_f_bottom_menu', 'field_60d938fc21041', 'no'),
(614, 'options_h_top_menu', '', 'no'),
(615, '_options_h_top_menu', 'field_60dae8d17e34d', 'no'),
(616, 'options_h_top_phone', '', 'no'),
(617, '_options_h_top_phone', 'field_608a1c235308c', 'no'),
(618, 'options_h_top_fb_like', '', 'no'),
(619, '_options_h_top_fb_like', 'field_608a1c8f5308f', 'no'),
(620, 'options_h_logo', '311', 'no'),
(621, '_options_h_logo', 'field_607565820f90c', 'no'),
(622, 'options_h_phone_support', '', 'no'),
(623, '_options_h_phone_support', 'field_608a1cdf53091', 'no'),
(624, 'options_smtp_host', 'smtp.gmail.com', 'no'),
(625, '_options_smtp_host', 'field_60a72615f47c3', 'no'),
(626, 'options_smtp_encryption', 'ssl', 'no'),
(627, '_options_smtp_encryption', 'field_60d93285153c6', 'no'),
(628, 'options_smtp_port', '465', 'no'),
(629, '_options_smtp_port', 'field_60d93294153c7', 'no'),
(630, 'options_smtp_auth', 'true', 'no'),
(631, '_options_smtp_auth', 'field_60a72638f47c6', 'no'),
(632, 'options_smtp_user', 'tiepnguyen220194@gmail.com', 'no'),
(633, '_options_smtp_user', 'field_60d932b0153c9', 'no'),
(634, 'options_smtp_pass', 'iigfkglfqmbibqos', 'no'),
(635, '_options_smtp_pass', 'field_60d932bb153ca', 'no'),
(655, 'options_f_title_section_1', '', 'no'),
(656, '_options_f_title_section_1', 'field_608a223db8e5a', 'no'),
(657, 'options_f_address_1', '', 'no'),
(658, '_options_f_address_1', 'field_60669d2993e70', 'no'),
(659, 'options_f_phone_1', '', 'no'),
(660, '_options_f_phone_1', 'field_60669da393e71', 'no'),
(661, 'options_f_email_1', '', 'no'),
(662, '_options_f_email_1', 'field_603c6d8294513', 'no'),
(663, 'options_f_title_section_2', '', 'no'),
(664, '_options_f_title_section_2', 'field_608a2255b8e5b', 'no'),
(665, 'options_f_address_2', '', 'no'),
(666, '_options_f_address_2', 'field_608a21ecb6a48', 'no'),
(667, 'options_f_phone_2', '', 'no'),
(668, '_options_f_phone_2', 'field_608a21f1b6a49', 'no'),
(669, 'options_f_email_2', '', 'no'),
(670, '_options_f_email_2', 'field_608a21f6b6a4a', 'no'),
(671, 'options_f_support_title_section', '', 'no'),
(672, '_options_f_support_title_section', 'field_60d937d521038', 'no'),
(673, 'options_f_fanpage_title_section', '', 'no'),
(674, '_options_f_fanpage_title_section', 'field_6075682411da3', 'no'),
(675, 'options_favicon', '311', 'no'),
(676, '_options_favicon', 'field_60d93090334e7', 'no'),
(687, 'options_socical_phone', '0912339513', 'no'),
(688, '_options_socical_phone', 'field_60d930e4334e8', 'no'),
(689, 'options_socical_zalo', '0912339513', 'no'),
(690, '_options_socical_zalo', 'field_60d9310f334e9', 'no'),
(691, 'options_socical_messenger', '', 'no'),
(692, '_options_socical_messenger', 'field_60d93125334ea', 'no'),
(693, 'options_socical_chat_fb', '', 'no'),
(694, '_options_socical_chat_fb', 'field_60d931d0334eb', 'no'),
(726, 'auto_update_core_dev', 'enabled', 'yes'),
(727, 'auto_update_core_minor', 'enabled', 'yes'),
(728, 'auto_update_core_major', 'unset', 'yes'),
(729, 'db_upgraded', '', 'yes'),
(732, 'can_compress_scripts', '1', 'no'),
(745, 'https_detection_errors', 'a:1:{s:20:\"https_request_failed\";a:1:{i:0;s:37:\"Yêu cầu HTTPS không thành công.\";}}', 'yes'),
(789, 'options_h_phone', '', 'no'),
(790, '_options_h_phone', 'field_608a1c235308c', 'no'),
(791, 'options_f_logo', '269', 'no'),
(792, '_options_f_logo', 'field_60d937bb21037', 'no'),
(793, 'options_h_slogan', '', 'no'),
(794, '_options_h_slogan', 'field_60d93758144a5', 'no'),
(795, 'options_customer_address', 'Số nhà B20 khu nhà ở cán bộ Quốc Hội, ngõ 252 Lương Thế Vinh, quận Nam Từ Liêm, TP. Hà Nội', 'no'),
(796, '_options_customer_address', 'field_60d92fd7334e4', 'no'),
(797, 'options_customer_phone', '091 233 9513', 'no'),
(798, '_options_customer_phone', 'field_60d92feb334e6', 'no'),
(799, 'options_customer_email', 'saoviettracdia@gmail.com', 'no'),
(800, '_options_customer_email', 'field_60d92fde334e5', 'no'),
(807, 'options_f_subscribe_title', 'Đăng ký nhận tin khuyến mãi', 'no'),
(808, '_options_f_subscribe_title', 'field_60dae5e430dd0', 'no'),
(809, 'options_f_subscribe_form', '70', 'no'),
(810, '_options_f_subscribe_form', 'field_60dae60930dd1', 'no'),
(900, 'options_customer_slogan', 'Trắc địa Sao Việt - Chính xác đến từng Milimet', 'no'),
(901, '_options_customer_slogan', 'field_6135f44287cf2', 'no'),
(902, 'options_socical_back_to_top', '1', 'no'),
(903, '_options_socical_back_to_top', 'field_60f6516b68cf3', 'no'),
(929, 'remove_taxonomy_base_slug_settings_what_taxonomies', 'a:2:{i:0;s:8:\"category\";i:1;s:11:\"product_cat\";}', 'yes'),
(940, 'options_f_socical_twitter', 'https://twitter', 'no'),
(941, '_options_f_socical_twitter', 'field_60d938972103f', 'no'),
(957, 'options_f_form_title', 'Đăng ký để nhận khuyến mại và báo giá', 'no'),
(958, '_options_f_form_title', 'field_60dae5e430dd0', 'no'),
(959, 'options_f_form', '70', 'no'),
(960, '_options_f_form', 'field_60dae60930dd1', 'no'),
(985, 'widget_block', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(994, 'wp_force_deactivated_plugins', 'a:0:{}', 'yes'),
(1007, 'theme_mods_dogiadung', 'a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:4:{s:7:\"primary\";i:7;s:16:\"category_product\";i:9;s:5:\"intro\";i:10;s:6:\"policy\";i:11;}s:18:\"custom_css_post_id\";i:-1;}', 'yes'),
(1013, 'options_customer_company_name', 'Đồ gia dụng', 'no'),
(1014, '_options_customer_company_name', 'field_60d93758144a5', 'no'),
(1015, 'options_customer_address_children_0_title', 'VPGD Hà Nội', 'no'),
(1016, '_options_customer_address_children_0_title', 'field_61237e30b9827', 'no'),
(1017, 'options_customer_address_children_0_desc', 'Địa chỉ : Số nhà B20 khu nhà ở cán bộ Quốc Hội, ngõ 252 Lương Thế Vinh, quận Nam Từ Liêm, TP. Hà Nội.\r\nTel : 024 85888390\r\nHotline : 0912 339 513 / 0936 050 513', 'no'),
(1018, '_options_customer_address_children_0_desc', 'field_61237e68b9828', 'no'),
(1019, 'options_customer_address_children_1_title', 'Chi nhánh Thanh Hóa', 'no'),
(1020, '_options_customer_address_children_1_title', 'field_61237e30b9827', 'no'),
(1021, 'options_customer_address_children_1_desc', 'Địa chỉ : Số 04/26 Tô Hiến Thành, P. Điện Biên, TP Thanh Hóa.\r\nHoline : 0976.949.163', 'no'),
(1022, '_options_customer_address_children_1_desc', 'field_61237e68b9828', 'no'),
(1023, 'options_customer_address_children', '2', 'no'),
(1024, '_options_customer_address_children', 'field_61237e30b9826', 'no'),
(1025, 'options_f_service_0_image', '276', 'no'),
(1026, '_options_f_service_0_image', 'field_60d938102103a', 'no'),
(1027, 'options_f_service_0_title', 'Cam kết hàng chính hãng', 'no'),
(1028, '_options_f_service_0_title', 'field_612381be3fc8f', 'no'),
(1029, 'options_f_service_0_desc', 'Cho đơn hàng từ 2.000.000 đ', 'no'),
(1030, '_options_f_service_0_desc', 'field_612381c73fc90', 'no'),
(1031, 'options_f_service_1_image', '275', 'no'),
(1032, '_options_f_service_1_image', 'field_60d938102103a', 'no'),
(1033, 'options_f_service_1_title', 'Cam kết giá tốt nhất', 'no'),
(1034, '_options_f_service_1_title', 'field_612381be3fc8f', 'no'),
(1035, 'options_f_service_1_desc', 'Cho đơn hàng từ 2.000.000 đ', 'no'),
(1036, '_options_f_service_1_desc', 'field_612381c73fc90', 'no'),
(1037, 'options_f_service_2_image', '277', 'no'),
(1038, '_options_f_service_2_image', 'field_60d938102103a', 'no'),
(1039, 'options_f_service_2_title', 'Đổi trả hàng trong vòng 30 ngày', 'no'),
(1040, '_options_f_service_2_title', 'field_612381be3fc8f', 'no'),
(1041, 'options_f_service_2_desc', 'Cho đơn hàng từ 2.000.000 đ', 'no'),
(1042, '_options_f_service_2_desc', 'field_612381c73fc90', 'no'),
(1043, 'options_f_service', '3', 'no'),
(1044, '_options_f_service', 'field_60d937f721039', 'no'),
(1046, 'options_f_socical_title', 'Kết nối với chúng tôi', 'no'),
(1047, '_options_f_socical_title', 'field_61238f27cf4dc', 'no'),
(1076, 'category_children', 'a:0:{}', 'yes'),
(1083, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:1:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:6:\"latest\";s:8:\"download\";s:57:\"https://downloads.wordpress.org/release/wordpress-5.8.zip\";s:6:\"locale\";s:2:\"vi\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:57:\"https://downloads.wordpress.org/release/wordpress-5.8.zip\";s:10:\"no_content\";s:68:\"https://downloads.wordpress.org/release/wordpress-5.8-no-content.zip\";s:11:\"new_bundled\";s:69:\"https://downloads.wordpress.org/release/wordpress-5.8-new-bundled.zip\";s:7:\"partial\";s:0:\"\";s:8:\"rollback\";s:0:\"\";}s:7:\"current\";s:3:\"5.8\";s:7:\"version\";s:3:\"5.8\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.6\";s:15:\"partial_version\";s:0:\"\";}}s:12:\"last_checked\";i:1630026048;s:15:\"version_checked\";s:3:\"5.8\";s:12:\"translations\";a:0:{}}', 'no'),
(1087, '_site_transient_update_themes', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1640834001;s:7:\"checked\";a:1:{s:9:\"dogiadung\";s:3:\"1.0\";}s:8:\"response\";a:0:{}s:9:\"no_update\";a:0:{}s:12:\"translations\";a:0:{}}', 'no'),
(1088, 'action_scheduler_hybrid_store_demarkation', '304', 'yes'),
(1089, 'schema-ActionScheduler_StoreSchema', '4.0.1629800498', 'yes'),
(1090, 'schema-ActionScheduler_LoggerSchema', '2.0.1629800498', 'yes'),
(1093, 'woocommerce_schema_version', '430', 'yes'),
(1094, 'woocommerce_store_address', 'Trường Chinh', 'yes'),
(1095, 'woocommerce_store_address_2', 'Thanh Xuân', 'yes'),
(1096, 'woocommerce_store_city', 'Hà Nội', 'yes'),
(1097, 'woocommerce_default_country', 'VN', 'yes'),
(1098, 'woocommerce_store_postcode', '10000', 'yes'),
(1099, 'woocommerce_allowed_countries', 'all', 'yes'),
(1100, 'woocommerce_all_except_countries', 'a:0:{}', 'yes'),
(1101, 'woocommerce_specific_allowed_countries', 'a:0:{}', 'yes'),
(1102, 'woocommerce_ship_to_countries', '', 'yes'),
(1103, 'woocommerce_specific_ship_to_countries', 'a:0:{}', 'yes'),
(1104, 'woocommerce_default_customer_address', 'base', 'yes'),
(1105, 'woocommerce_calc_taxes', 'no', 'yes'),
(1106, 'woocommerce_enable_coupons', 'no', 'yes'),
(1107, 'woocommerce_calc_discounts_sequentially', 'no', 'no'),
(1108, 'woocommerce_currency', 'VND', 'yes'),
(1109, 'woocommerce_currency_pos', 'right', 'yes'),
(1110, 'woocommerce_price_thousand_sep', '.', 'yes'),
(1111, 'woocommerce_price_decimal_sep', '.', 'yes'),
(1112, 'woocommerce_price_num_decimals', '0', 'yes'),
(1113, 'woocommerce_shop_page_id', '305', 'yes'),
(1114, 'woocommerce_cart_redirect_after_add', 'yes', 'yes'),
(1115, 'woocommerce_enable_ajax_add_to_cart', 'yes', 'yes'),
(1116, 'woocommerce_placeholder_image', '304', 'yes'),
(1117, 'woocommerce_weight_unit', 'kg', 'yes'),
(1118, 'woocommerce_dimension_unit', 'cm', 'yes'),
(1119, 'woocommerce_enable_reviews', 'yes', 'yes'),
(1120, 'woocommerce_review_rating_verification_label', 'yes', 'no'),
(1121, 'woocommerce_review_rating_verification_required', 'no', 'no'),
(1122, 'woocommerce_enable_review_rating', 'yes', 'yes'),
(1123, 'woocommerce_review_rating_required', 'yes', 'no'),
(1124, 'woocommerce_manage_stock', 'yes', 'yes'),
(1125, 'woocommerce_hold_stock_minutes', '60', 'no'),
(1126, 'woocommerce_notify_low_stock', 'yes', 'no'),
(1127, 'woocommerce_notify_no_stock', 'yes', 'no'),
(1128, 'woocommerce_stock_email_recipient', 'tiepnguyen220194@gmail.com', 'no'),
(1129, 'woocommerce_notify_low_stock_amount', '2', 'no'),
(1130, 'woocommerce_notify_no_stock_amount', '0', 'yes'),
(1131, 'woocommerce_hide_out_of_stock_items', 'no', 'yes'),
(1132, 'woocommerce_stock_format', '', 'yes'),
(1133, 'woocommerce_file_download_method', 'force', 'no'),
(1134, 'woocommerce_downloads_redirect_fallback_allowed', 'no', 'no'),
(1135, 'woocommerce_downloads_require_login', 'no', 'no'),
(1136, 'woocommerce_downloads_grant_access_after_payment', 'yes', 'no'),
(1137, 'woocommerce_downloads_add_hash_to_filename', 'yes', 'yes'),
(1138, 'woocommerce_prices_include_tax', 'no', 'yes'),
(1139, 'woocommerce_tax_based_on', 'shipping', 'yes'),
(1140, 'woocommerce_shipping_tax_class', 'inherit', 'yes'),
(1141, 'woocommerce_tax_round_at_subtotal', 'no', 'yes'),
(1142, 'woocommerce_tax_classes', '', 'yes'),
(1143, 'woocommerce_tax_display_shop', 'excl', 'yes'),
(1144, 'woocommerce_tax_display_cart', 'excl', 'yes'),
(1145, 'woocommerce_price_display_suffix', '', 'yes'),
(1146, 'woocommerce_tax_total_display', 'itemized', 'no'),
(1147, 'woocommerce_enable_shipping_calc', 'yes', 'no'),
(1148, 'woocommerce_shipping_cost_requires_address', 'no', 'yes'),
(1149, 'woocommerce_ship_to_destination', 'billing', 'no'),
(1150, 'woocommerce_shipping_debug_mode', 'no', 'yes'),
(1151, 'woocommerce_enable_guest_checkout', 'yes', 'no'),
(1152, 'woocommerce_enable_checkout_login_reminder', 'no', 'no'),
(1153, 'woocommerce_enable_signup_and_login_from_checkout', 'no', 'no'),
(1154, 'woocommerce_enable_myaccount_registration', 'no', 'no'),
(1155, 'woocommerce_registration_generate_username', 'yes', 'no'),
(1156, 'woocommerce_registration_generate_password', 'yes', 'no'),
(1157, 'woocommerce_erasure_request_removes_order_data', 'no', 'no'),
(1158, 'woocommerce_erasure_request_removes_download_data', 'no', 'no'),
(1159, 'woocommerce_allow_bulk_remove_personal_data', 'no', 'no'),
(1160, 'woocommerce_registration_privacy_policy_text', 'Thông tin cá nhân của bạn sẽ được sử dụng để tăng trải nghiệm sử dụng website, quản lý truy cập vào tài khoản của bạn, và cho các mục đích cụ thể khác được mô tả trong [privacy_policy].', 'yes'),
(1161, 'woocommerce_checkout_privacy_policy_text', 'Thông tin cá nhân của bạn sẽ được sử dụng để xử lý đơn hàng, tăng trải nghiệm sử dụng website, và cho các mục đích cụ thể khác đã được mô tả trong [privacy_policy].', 'yes'),
(1162, 'woocommerce_delete_inactive_accounts', 'a:2:{s:6:\"number\";s:0:\"\";s:4:\"unit\";s:6:\"months\";}', 'no'),
(1163, 'woocommerce_trash_pending_orders', '', 'no'),
(1164, 'woocommerce_trash_failed_orders', '', 'no'),
(1165, 'woocommerce_trash_cancelled_orders', '', 'no'),
(1166, 'woocommerce_anonymize_completed_orders', 'a:2:{s:6:\"number\";s:0:\"\";s:4:\"unit\";s:6:\"months\";}', 'no'),
(1167, 'woocommerce_email_from_name', 'Đồ gia dụng', 'no'),
(1168, 'woocommerce_email_from_address', 'tiepnguyen220194@gmail.com', 'no'),
(1169, 'woocommerce_email_header_image', '', 'no'),
(1170, 'woocommerce_email_footer_text', '{site_title} &mdash; Built with {WooCommerce}', 'no'),
(1171, 'woocommerce_email_base_color', '#96588a', 'no'),
(1172, 'woocommerce_email_background_color', '#f7f7f7', 'no'),
(1173, 'woocommerce_email_body_background_color', '#ffffff', 'no'),
(1174, 'woocommerce_email_text_color', '#3c3c3c', 'no'),
(1175, 'woocommerce_merchant_email_notifications', 'no', 'no'),
(1176, 'woocommerce_cart_page_id', '306', 'no'),
(1177, 'woocommerce_checkout_page_id', '307', 'no'),
(1178, 'woocommerce_myaccount_page_id', '308', 'no'),
(1179, 'woocommerce_terms_page_id', '', 'no'),
(1180, 'woocommerce_force_ssl_checkout', 'no', 'yes'),
(1181, 'woocommerce_unforce_ssl_checkout', 'no', 'yes'),
(1182, 'woocommerce_checkout_pay_endpoint', 'order-pay', 'yes'),
(1183, 'woocommerce_checkout_order_received_endpoint', 'order-received', 'yes'),
(1184, 'woocommerce_myaccount_add_payment_method_endpoint', 'add-payment-method', 'yes'),
(1185, 'woocommerce_myaccount_delete_payment_method_endpoint', 'delete-payment-method', 'yes'),
(1186, 'woocommerce_myaccount_set_default_payment_method_endpoint', 'set-default-payment-method', 'yes'),
(1187, 'woocommerce_myaccount_orders_endpoint', 'orders', 'yes'),
(1188, 'woocommerce_myaccount_view_order_endpoint', 'view-order', 'yes'),
(1189, 'woocommerce_myaccount_downloads_endpoint', 'downloads', 'yes'),
(1190, 'woocommerce_myaccount_edit_account_endpoint', 'edit-account', 'yes'),
(1191, 'woocommerce_myaccount_edit_address_endpoint', 'edit-address', 'yes'),
(1192, 'woocommerce_myaccount_payment_methods_endpoint', 'payment-methods', 'yes'),
(1193, 'woocommerce_myaccount_lost_password_endpoint', 'lost-password', 'yes'),
(1194, 'woocommerce_logout_endpoint', 'customer-logout', 'yes'),
(1195, 'woocommerce_api_enabled', 'no', 'yes'),
(1196, 'woocommerce_allow_tracking', 'no', 'no');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1197, 'woocommerce_show_marketplace_suggestions', 'yes', 'no'),
(1198, 'woocommerce_single_image_width', '600', 'yes'),
(1199, 'woocommerce_thumbnail_image_width', '300', 'yes'),
(1200, 'woocommerce_checkout_highlight_required_fields', 'yes', 'yes'),
(1201, 'woocommerce_demo_store', 'no', 'no'),
(1202, 'woocommerce_permalinks', 'a:5:{s:12:\"product_base\";s:9:\"/san-pham\";s:13:\"category_base\";s:17:\"danh-muc-san-pham\";s:8:\"tag_base\";s:16:\"tu-khoa-san-pham\";s:14:\"attribute_base\";s:0:\"\";s:22:\"use_verbose_page_rules\";b:0;}', 'yes'),
(1203, 'current_theme_supports_woocommerce', 'yes', 'yes'),
(1204, 'woocommerce_queue_flush_rewrite_rules', 'no', 'yes'),
(1205, '_transient_wc_attribute_taxonomies', 'a:0:{}', 'yes'),
(1207, 'default_product_cat', '25', 'yes'),
(1209, 'woocommerce_refund_returns_page_id', '309', 'yes'),
(1212, 'woocommerce_paypal_settings', 'a:23:{s:7:\"enabled\";s:2:\"no\";s:5:\"title\";s:6:\"PayPal\";s:11:\"description\";s:120:\"Trả thông qua Paypal; bạn có thể thanh toán với thẻ tín dụng nếu bạn không có tài khoản PayPal.\";s:5:\"email\";s:26:\"tiepnguyen220194@gmail.com\";s:8:\"advanced\";s:0:\"\";s:8:\"testmode\";s:2:\"no\";s:5:\"debug\";s:2:\"no\";s:16:\"ipn_notification\";s:3:\"yes\";s:14:\"receiver_email\";s:26:\"tiepnguyen220194@gmail.com\";s:14:\"identity_token\";s:0:\"\";s:14:\"invoice_prefix\";s:3:\"WC-\";s:13:\"send_shipping\";s:3:\"yes\";s:16:\"address_override\";s:2:\"no\";s:13:\"paymentaction\";s:4:\"sale\";s:9:\"image_url\";s:0:\"\";s:11:\"api_details\";s:0:\"\";s:12:\"api_username\";s:0:\"\";s:12:\"api_password\";s:0:\"\";s:13:\"api_signature\";s:0:\"\";s:20:\"sandbox_api_username\";s:0:\"\";s:20:\"sandbox_api_password\";s:0:\"\";s:21:\"sandbox_api_signature\";s:0:\"\";s:12:\"_should_load\";s:2:\"no\";}', 'yes'),
(1213, 'woocommerce_version', '5.6.0', 'yes'),
(1214, 'woocommerce_db_version', '5.6.0', 'yes'),
(1215, 'woocommerce_inbox_variant_assignment', '3', 'yes'),
(1219, '_transient_jetpack_autoloader_plugin_paths', 'a:1:{i:0;s:29:\"{{WP_PLUGIN_DIR}}/woocommerce\";}', 'yes'),
(1220, 'action_scheduler_lock_async-request-runner', '1640834348', 'yes'),
(1221, 'woocommerce_admin_notices', 'a:1:{i:0;s:20:\"no_secure_connection\";}', 'yes'),
(1222, 'woocommerce_maxmind_geolocation_settings', 'a:1:{s:15:\"database_prefix\";s:32:\"7A98BXoEyUt4vkolocEGCFAGJzTwlG7M\";}', 'yes'),
(1223, '_transient_woocommerce_webhook_ids_status_active', 'a:0:{}', 'yes'),
(1224, 'widget_woocommerce_widget_cart', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(1225, 'widget_woocommerce_layered_nav_filters', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(1226, 'widget_woocommerce_layered_nav', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(1227, 'widget_woocommerce_price_filter', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(1228, 'widget_woocommerce_product_categories', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(1229, 'widget_woocommerce_product_search', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(1230, 'widget_woocommerce_product_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(1231, 'widget_woocommerce_products', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(1232, 'widget_woocommerce_recently_viewed_products', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(1233, 'widget_woocommerce_top_rated_products', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(1234, 'widget_woocommerce_recent_reviews', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(1235, 'widget_woocommerce_rating_filter', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(1238, 'woocommerce_admin_version', '2.5.1', 'yes'),
(1239, 'woocommerce_admin_install_timestamp', '1629800503', 'yes'),
(1240, 'wc_remote_inbox_notifications_wca_updated', '', 'no'),
(1241, 'wc_remote_inbox_notifications_specs', 'a:0:{}', 'no'),
(1242, 'wc_remote_inbox_notifications_stored_state', 'O:8:\"stdClass\":2:{s:22:\"there_were_no_products\";b:1;s:22:\"there_are_now_products\";b:1;}', 'no'),
(1247, 'wc_blocks_db_schema_version', '260', 'yes'),
(1248, 'woocommerce_meta_box_errors', 'a:0:{}', 'yes'),
(1251, '_transient_woocommerce_reports-transient-version', '1640834221', 'yes'),
(1262, 'action_scheduler_migration_status', 'complete', 'yes'),
(1264, 'woocommerce_onboarding_profile', 'a:8:{s:12:\"setup_client\";b:1;s:8:\"industry\";a:1:{i:0;a:1:{s:4:\"slug\";s:21:\"electronics-computers\";}}s:13:\"product_types\";a:1:{i:0;s:8:\"physical\";}s:13:\"product_count\";s:8:\"101-1000\";s:14:\"selling_venues\";s:2:\"no\";s:19:\"business_extensions\";a:0:{}s:5:\"theme\";s:9:\"dogiadung\";s:9:\"completed\";b:1;}', 'yes'),
(1272, 'woocommerce_task_list_tracked_completed_tasks', 'a:1:{i:0;s:13:\"store_details\";}', 'yes'),
(1278, 'woocommerce_task_list_welcome_modal_dismissed', 'yes', 'yes'),
(1284, '_transient_shipping-transient-version', '1629800992', 'yes'),
(1287, 'woocommerce_cod_settings', 'a:6:{s:7:\"enabled\";s:3:\"yes\";s:5:\"title\";s:35:\"Trả tiền mặt khi nhận hàng\";s:11:\"description\";s:33:\"Trả tiền mặt khi giao hàng\";s:12:\"instructions\";s:33:\"Trả tiền mặt khi giao hàng\";s:18:\"enable_for_methods\";a:0:{}s:18:\"enable_for_virtual\";s:3:\"yes\";}', 'yes'),
(1288, 'woocommerce_gateway_order', 'a:3:{s:4:\"bacs\";i:0;s:6:\"cheque\";i:1;s:3:\"cod\";i:2;}', 'yes'),
(1294, '_transient_product_query-transient-version', '1640834254', 'yes'),
(1295, '_transient_product-transient-version', '1640834254', 'yes'),
(1311, '_transient_orders-transient-version', '1629802005', 'yes'),
(1324, 'woocommerce_admin_last_orders_milestone', '1', 'yes'),
(1374, '_transient_wc_count_comments', 'O:8:\"stdClass\":7:{s:14:\"total_comments\";i:1;s:3:\"all\";i:1;s:8:\"approved\";s:1:\"1\";s:9:\"moderated\";i:0;s:4:\"spam\";i:0;s:5:\"trash\";i:0;s:12:\"post-trashed\";i:0;}', 'yes'),
(1389, 'woocommerce_catalog_rows', '1', 'yes'),
(1390, 'woocommerce_maybe_regenerate_images_hash', '991b1ca641921cf0f5baf7a2fe85861b', 'yes'),
(1415, 'woocommerce_default_catalog_orderby', 'date', 'yes'),
(1436, 'woocommerce_sales_record_date', '2021-08-24', 'yes'),
(1437, 'woocommerce_sales_record_amount', '950000', 'yes'),
(1497, 'widget_show_post_taxonomy_product', 'a:2:{i:2;a:3:{s:5:\"title\";s:24:\"Sản phẩm nổi bật\";s:11:\"id_taxonomy\";s:2:\"26\";s:11:\"number_post\";s:1:\"3\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(1510, 'widget_show_post_product_featured', 'a:2:{i:2;a:2:{s:5:\"title\";s:24:\"Sản phẩm nổi bật\";s:11:\"number_post\";s:1:\"4\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(1535, 'widget_hotline', 'a:2:{i:2;a:4:{s:5:\"title\";s:18:\"Hotline hỗ trợ\";s:7:\"hotline\";s:10:\"0912339513\";s:12:\"policy_title\";s:27:\"Đổi trả trong 30 ngày\";s:12:\"policy_value\";s:32:\"cho đơn hàng từ 2.000.000đ\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(1543, 'widget_policy', 'a:2:{i:2;a:2:{s:5:\"title\";s:27:\"Đổi trả trong 30 ngày\";s:6:\"policy\";s:33:\"cho đơn hàng từ 2.000.000đ2\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(1611, '_site_transient_update_plugins', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1640833997;s:8:\"response\";a:4:{s:37:\"breadcrumb-navxt/breadcrumb-navxt.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:30:\"w.org/plugins/breadcrumb-navxt\";s:4:\"slug\";s:16:\"breadcrumb-navxt\";s:6:\"plugin\";s:37:\"breadcrumb-navxt/breadcrumb-navxt.php\";s:11:\"new_version\";s:5:\"7.0.1\";s:3:\"url\";s:47:\"https://wordpress.org/plugins/breadcrumb-navxt/\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/plugin/breadcrumb-navxt.7.0.1.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:69:\"https://ps.w.org/breadcrumb-navxt/assets/icon-256x256.png?rev=2410525\";s:2:\"1x\";s:61:\"https://ps.w.org/breadcrumb-navxt/assets/icon.svg?rev=1927103\";s:3:\"svg\";s:61:\"https://ps.w.org/breadcrumb-navxt/assets/icon.svg?rev=1927103\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:72:\"https://ps.w.org/breadcrumb-navxt/assets/banner-1544x500.png?rev=1927103\";s:2:\"1x\";s:71:\"https://ps.w.org/breadcrumb-navxt/assets/banner-772x250.png?rev=1927103\";}s:11:\"banners_rtl\";a:0:{}s:8:\"requires\";s:3:\"4.9\";s:6:\"tested\";s:5:\"5.8.2\";s:12:\"requires_php\";s:3:\"7.0\";}s:36:\"contact-form-7/wp-contact-form-7.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:28:\"w.org/plugins/contact-form-7\";s:4:\"slug\";s:14:\"contact-form-7\";s:6:\"plugin\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:11:\"new_version\";s:5:\"5.5.3\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/contact-form-7/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/contact-form-7.5.5.3.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:67:\"https://ps.w.org/contact-form-7/assets/icon-256x256.png?rev=2279696\";s:2:\"1x\";s:59:\"https://ps.w.org/contact-form-7/assets/icon.svg?rev=2339255\";s:3:\"svg\";s:59:\"https://ps.w.org/contact-form-7/assets/icon.svg?rev=2339255\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/contact-form-7/assets/banner-1544x500.png?rev=860901\";s:2:\"1x\";s:68:\"https://ps.w.org/contact-form-7/assets/banner-772x250.png?rev=880427\";}s:11:\"banners_rtl\";a:0:{}s:8:\"requires\";s:3:\"5.7\";s:6:\"tested\";s:5:\"5.8.2\";s:12:\"requires_php\";b:0;}s:27:\"woocommerce/woocommerce.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:25:\"w.org/plugins/woocommerce\";s:4:\"slug\";s:11:\"woocommerce\";s:6:\"plugin\";s:27:\"woocommerce/woocommerce.php\";s:11:\"new_version\";s:5:\"6.0.0\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/woocommerce/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/woocommerce.6.0.0.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:64:\"https://ps.w.org/woocommerce/assets/icon-256x256.png?rev=2366418\";s:2:\"1x\";s:64:\"https://ps.w.org/woocommerce/assets/icon-128x128.png?rev=2366418\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/woocommerce/assets/banner-1544x500.png?rev=2366418\";s:2:\"1x\";s:66:\"https://ps.w.org/woocommerce/assets/banner-772x250.png?rev=2366418\";}s:11:\"banners_rtl\";a:0:{}s:8:\"requires\";s:3:\"5.6\";s:6:\"tested\";s:5:\"5.8.2\";s:12:\"requires_php\";s:3:\"7.0\";}s:34:\"advanced-custom-fields-pro/acf.php\";O:8:\"stdClass\":8:{s:4:\"slug\";s:26:\"advanced-custom-fields-pro\";s:6:\"plugin\";s:34:\"advanced-custom-fields-pro/acf.php\";s:11:\"new_version\";s:6:\"5.11.4\";s:3:\"url\";s:36:\"https://www.advancedcustomfields.com\";s:6:\"tested\";s:5:\"5.8.1\";s:7:\"package\";s:0:\"\";s:5:\"icons\";a:1:{s:7:\"default\";s:63:\"https://ps.w.org/advanced-custom-fields/assets/icon-256x256.png\";}s:7:\"banners\";a:2:{s:3:\"low\";s:77:\"https://ps.w.org/advanced-custom-fields/assets/banner-772x250.jpg?rev=1729102\";s:4:\"high\";s:78:\"https://ps.w.org/advanced-custom-fields/assets/banner-1544x500.jpg?rev=1729099\";}}}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:1:{s:55:\"remove-taxonomy-base-slug/remove-taxonomy-base-slug.php\";O:8:\"stdClass\":10:{s:2:\"id\";s:39:\"w.org/plugins/remove-taxonomy-base-slug\";s:4:\"slug\";s:25:\"remove-taxonomy-base-slug\";s:6:\"plugin\";s:55:\"remove-taxonomy-base-slug/remove-taxonomy-base-slug.php\";s:11:\"new_version\";s:3:\"2.1\";s:3:\"url\";s:56:\"https://wordpress.org/plugins/remove-taxonomy-base-slug/\";s:7:\"package\";s:68:\"https://downloads.wordpress.org/plugin/remove-taxonomy-base-slug.zip\";s:5:\"icons\";a:1:{s:7:\"default\";s:69:\"https://s.w.org/plugins/geopattern-icon/remove-taxonomy-base-slug.svg\";}s:7:\"banners\";a:0:{}s:11:\"banners_rtl\";a:0:{}s:8:\"requires\";s:3:\"3.4\";}}s:7:\"checked\";a:6:{s:35:\"advanced-cf7-db/advanced-cf7-db.php\";s:5:\"1.8.2\";s:34:\"advanced-custom-fields-pro/acf.php\";s:5:\"5.8.7\";s:37:\"breadcrumb-navxt/breadcrumb-navxt.php\";s:5:\"6.6.0\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:5:\"5.4.1\";s:55:\"remove-taxonomy-base-slug/remove-taxonomy-base-slug.php\";s:3:\"2.1\";s:27:\"woocommerce/woocommerce.php\";s:5:\"5.6.0\";}}', 'no'),
(1801, 'options_single_product_form_quote', '379', 'no'),
(1802, '_options_single_product_form_quote', 'field_61286003dfcab', 'no'),
(1935, 'options_h_service', 'Giao hàng miễn phí <br> Hỗ trợ kỹ thuật 24/7', 'no'),
(1936, '_options_h_service', 'field_6135f55e3fc6e', 'no'),
(2085, 'product_cat_children', 'a:2:{i:26;a:2:{i:0;i:28;i:1;i:29;}i:27;a:1:{i:0;i:30;}}', 'yes'),
(2142, '_site_transient_timeout_theme_roots', '1640835790', 'no'),
(2143, '_site_transient_theme_roots', 'a:1:{s:9:\"dogiadung\";s:7:\"/themes\";}', 'no'),
(2144, '_site_transient_timeout_php_check_e26e33de4a278e301580d402dcb3d659', '1641438791', 'no'),
(2145, '_site_transient_php_check_e26e33de4a278e301580d402dcb3d659', 'a:5:{s:19:\"recommended_version\";s:3:\"7.4\";s:15:\"minimum_version\";s:6:\"5.6.20\";s:12:\"is_supported\";b:1;s:9:\"is_secure\";b:1;s:13:\"is_acceptable\";b:1;}', 'no'),
(2146, '_transient_timeout__woocommerce_helper_subscriptions', '1640834897', 'no'),
(2147, '_transient__woocommerce_helper_subscriptions', 'a:0:{}', 'no'),
(2148, '_transient_timeout__woocommerce_helper_updates', '1640877197', 'no'),
(2149, '_transient__woocommerce_helper_updates', 'a:4:{s:4:\"hash\";s:32:\"d751713988987e9331980363e24189ce\";s:7:\"updated\";i:1640833997;s:8:\"products\";a:0:{}s:6:\"errors\";a:1:{i:0;s:10:\"http-error\";}}', 'no'),
(2150, '_transient_timeout_acf_plugin_updates', '1641006800', 'no'),
(2151, '_transient_acf_plugin_updates', 'a:4:{s:7:\"plugins\";a:1:{s:34:\"advanced-custom-fields-pro/acf.php\";a:8:{s:4:\"slug\";s:26:\"advanced-custom-fields-pro\";s:6:\"plugin\";s:34:\"advanced-custom-fields-pro/acf.php\";s:11:\"new_version\";s:6:\"5.11.4\";s:3:\"url\";s:36:\"https://www.advancedcustomfields.com\";s:6:\"tested\";s:5:\"5.8.1\";s:7:\"package\";s:0:\"\";s:5:\"icons\";a:1:{s:7:\"default\";s:63:\"https://ps.w.org/advanced-custom-fields/assets/icon-256x256.png\";}s:7:\"banners\";a:2:{s:3:\"low\";s:77:\"https://ps.w.org/advanced-custom-fields/assets/banner-772x250.jpg?rev=1729102\";s:4:\"high\";s:78:\"https://ps.w.org/advanced-custom-fields/assets/banner-1544x500.jpg?rev=1729099\";}}}s:10:\"expiration\";i:172800;s:6:\"status\";i:1;s:7:\"checked\";a:1:{s:34:\"advanced-custom-fields-pro/acf.php\";s:5:\"5.8.7\";}}', 'no'),
(2157, '_transient_timeout_wc_shipping_method_count_legacy', '1643426165', 'no'),
(2158, '_transient_wc_shipping_method_count_legacy', 'a:2:{s:7:\"version\";s:10:\"1629800992\";s:5:\"value\";i:0;}', 'no'),
(2159, '_site_transient_timeout_browser_8a72c97a4056bf9b22e4883892ea4b09', '1641438975', 'no'),
(2160, '_site_transient_browser_8a72c97a4056bf9b22e4883892ea4b09', 'a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:13:\"96.0.4664.110\";s:8:\"platform\";s:9:\"Macintosh\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(2161, '_transient_timeout_orders-all-statuses', '1641439032', 'no'),
(2162, '_transient_orders-all-statuses', 'a:2:{s:7:\"version\";s:10:\"1640834221\";s:5:\"value\";a:1:{i:0;s:13:\"wc-processing\";}}', 'no'),
(2164, '_transient_timeout_wc_term_counts', '1643426260', 'no'),
(2165, '_transient_wc_term_counts', 'a:6:{i:25;s:1:\"0\";i:31;s:1:\"5\";i:26;s:1:\"4\";i:27;s:1:\"3\";i:29;s:1:\"1\";i:28;s:1:\"2\";}', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `wp_postmeta`
--

CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_postmeta`
--

INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(29, 12, '_edit_lock', '1630396202:1'),
(57, 29, '_edit_lock', '1629799041:1'),
(61, 31, '_edit_lock', '1629797904:1'),
(65, 29, '_edit_last', '1'),
(68, 36, '_edit_lock', '1629793308:1'),
(69, 36, '_wp_page_template', 'template-contact.php'),
(70, 41, '_form', '[text* your-name class:form-control placeholder \"Họ tên\"]\n    [email* your-email class:form-control placeholder \"Email\"]\n    [textarea* your-message class:form-control placeholder \"Nội dung\"]\n[submit class:btn class:w-100 class:bold class:buy-btn \"Gửi\"]'),
(71, 41, '_mail', 'a:9:{s:6:\"active\";b:1;s:7:\"subject\";s:13:\"[_site_title]\";s:6:\"sender\";s:41:\"[_site_title] <wordpress@wordpress.local>\";s:9:\"recipient\";s:19:\"[_site_admin_email]\";s:4:\"body\";s:167:\"Gửi đến từ: [your-name] <[your-email]>\n\nNội dung thông điệp:\n[your-message]\n\n-- \nEmail này được gửi đến từ website [_site_title] ([_site_url])\";s:18:\"additional_headers\";s:0:\"\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(72, 41, '_mail_2', 'a:9:{s:6:\"active\";b:0;s:7:\"subject\";s:30:\"[_site_title] \"[your-subject]\"\";s:6:\"sender\";s:45:\"[_site_title] <wordpress@corewordpress.local>\";s:9:\"recipient\";s:12:\"[your-email]\";s:4:\"body\";s:142:\"Nội dung thông điệp:\n[your-message]\n\n-- \nEmail này được gửi đến từ form liên hệ của website [_site_title] ([_site_url])\";s:18:\"additional_headers\";s:29:\"Reply-To: [_site_admin_email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(73, 41, '_messages', 'a:22:{s:12:\"mail_sent_ok\";s:53:\"Xin cảm ơn, form đã được gửi thành công.\";s:12:\"mail_sent_ng\";s:118:\"Có lỗi xảy ra trong quá trình gửi. Xin vui lòng thử lại hoặc liên hệ người quản trị website.\";s:16:\"validation_error\";s:86:\"Có một hoặc nhiều mục nhập có lỗi. Vui lòng kiểm tra và thử lại.\";s:4:\"spam\";s:118:\"Có lỗi xảy ra trong quá trình gửi. Xin vui lòng thử lại hoặc liên hệ người quản trị website.\";s:12:\"accept_terms\";s:67:\"Bạn phải chấp nhận điều khoản trước khi gửi form.\";s:16:\"invalid_required\";s:28:\"Mục này là bắt buộc.\";s:16:\"invalid_too_long\";s:36:\"Nhập quá số kí tự cho phép.\";s:17:\"invalid_too_short\";s:44:\"Nhập ít hơn số kí tự tối thiểu.\";s:13:\"upload_failed\";s:36:\"Tải file lên không thành công.\";s:24:\"upload_file_type_invalid\";s:69:\"Bạn không được phép tải lên file theo định dạng này.\";s:21:\"upload_file_too_large\";s:31:\"File kích thước quá lớn.\";s:23:\"upload_failed_php_error\";s:36:\"Tải file lên không thành công.\";s:12:\"invalid_date\";s:46:\"Định dạng ngày tháng không hợp lệ.\";s:14:\"date_too_early\";s:58:\"Ngày này trước ngày sớm nhất được cho phép.\";s:13:\"date_too_late\";s:54:\"Ngày này quá ngày gần nhất được cho phép.\";s:14:\"invalid_number\";s:38:\"Định dạng số không hợp lệ.\";s:16:\"number_too_small\";s:48:\"Con số nhỏ hơn số nhỏ nhất cho phép.\";s:16:\"number_too_large\";s:48:\"Con số lớn hơn số lớn nhất cho phép.\";s:23:\"quiz_answer_not_correct\";s:30:\"Câu trả lời chưa đúng.\";s:13:\"invalid_email\";s:38:\"Địa chỉ e-mail không hợp lệ.\";s:11:\"invalid_url\";s:22:\"URL không hợp lệ.\";s:11:\"invalid_tel\";s:39:\"Số điện thoại không hợp lệ.\";}'),
(74, 41, '_additional_settings', ''),
(75, 41, '_locale', 'vi'),
(80, 31, '_edit_last', '1'),
(83, 31, 'test2_0_hihi', 'mot'),
(84, 31, '_test2_0_hihi', 'field_5fdac3bb642a7'),
(85, 31, 'test2_1_hihi', 'hai'),
(86, 31, '_test2_1_hihi', 'field_5fdac3bb642a7'),
(87, 31, 'test2', '2'),
(88, 31, '_test2', 'field_5fdac066178b1'),
(89, 48, 'test2_0_hihi', 'mot'),
(90, 48, '_test2_0_hihi', 'field_5fdac3bb642a7'),
(91, 48, 'test2_1_hihi', 'hai'),
(92, 48, '_test2_1_hihi', 'field_5fdac3bb642a7'),
(93, 48, 'test2', '2'),
(94, 48, '_test2', 'field_5fdac066178b1'),
(122, 54, '_edit_last', '1'),
(123, 54, '_edit_lock', '1611311765:1'),
(124, 55, '_menu_item_type', 'post_type'),
(125, 55, '_menu_item_menu_item_parent', '0'),
(126, 55, '_menu_item_object_id', '12'),
(127, 55, '_menu_item_object', 'page'),
(128, 55, '_menu_item_target', ''),
(129, 55, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(130, 55, '_menu_item_xfn', ''),
(131, 55, '_menu_item_url', ''),
(142, 57, '_menu_item_type', 'post_type'),
(143, 57, '_menu_item_menu_item_parent', '0'),
(144, 57, '_menu_item_object_id', '36'),
(145, 57, '_menu_item_object', 'page'),
(146, 57, '_menu_item_target', ''),
(147, 57, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(148, 57, '_menu_item_xfn', ''),
(149, 57, '_menu_item_url', ''),
(159, 62, '_edit_last', '1'),
(160, 62, '_edit_lock', '1614243002:1'),
(161, 64, '_edit_last', '1'),
(164, 64, 'test2', ''),
(165, 64, '_test2', 'field_5fdac066178b1'),
(166, 65, 'test2', ''),
(167, 65, '_test2', 'field_5fdac066178b1'),
(168, 64, '_edit_lock', '1629797913:1'),
(171, 64, '_wp_old_slug', 'bai-viet-2-2'),
(174, 29, 'test2', ''),
(175, 29, '_test2', 'field_5fdac066178b1'),
(176, 33, 'test2', ''),
(177, 33, '_test2', 'field_5fdac066178b1'),
(178, 36, '_edit_last', '1'),
(182, 70, '_form', '[email* your-email class:form-control placeholder \"Email\"]\n[submit class:btn class:bold class:regis-btn \"Gửi\"]'),
(183, 70, '_mail', 'a:9:{s:6:\"active\";b:1;s:7:\"subject\";s:13:\"[_site_title]\";s:6:\"sender\";s:41:\"[_site_title] <wordpress@wordpress.local>\";s:9:\"recipient\";s:19:\"[_site_admin_email]\";s:4:\"body\";s:123:\"<[your-email]> Muốn đăng ký nhận tin\n\n\n-- \nEmail này được gửi đến từ website [_site_title] ([_site_url])\";s:18:\"additional_headers\";s:0:\"\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(184, 70, '_mail_2', 'a:9:{s:6:\"active\";b:0;s:7:\"subject\";s:30:\"[_site_title] \"[your-subject]\"\";s:6:\"sender\";s:45:\"[_site_title] <wordpress@corewordpress.local>\";s:9:\"recipient\";s:12:\"[your-email]\";s:4:\"body\";s:142:\"Nội dung thông điệp:\n[your-message]\n\n-- \nEmail này được gửi đến từ form liên hệ của website [_site_title] ([_site_url])\";s:18:\"additional_headers\";s:29:\"Reply-To: [_site_admin_email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(185, 70, '_messages', 'a:22:{s:12:\"mail_sent_ok\";s:53:\"Xin cảm ơn, form đã được gửi thành công.\";s:12:\"mail_sent_ng\";s:118:\"Có lỗi xảy ra trong quá trình gửi. Xin vui lòng thử lại hoặc liên hệ người quản trị website.\";s:16:\"validation_error\";s:86:\"Có một hoặc nhiều mục nhập có lỗi. Vui lòng kiểm tra và thử lại.\";s:4:\"spam\";s:118:\"Có lỗi xảy ra trong quá trình gửi. Xin vui lòng thử lại hoặc liên hệ người quản trị website.\";s:12:\"accept_terms\";s:67:\"Bạn phải chấp nhận điều khoản trước khi gửi form.\";s:16:\"invalid_required\";s:28:\"Mục này là bắt buộc.\";s:16:\"invalid_too_long\";s:36:\"Nhập quá số kí tự cho phép.\";s:17:\"invalid_too_short\";s:44:\"Nhập ít hơn số kí tự tối thiểu.\";s:13:\"upload_failed\";s:36:\"Tải file lên không thành công.\";s:24:\"upload_file_type_invalid\";s:69:\"Bạn không được phép tải lên file theo định dạng này.\";s:21:\"upload_file_too_large\";s:31:\"File kích thước quá lớn.\";s:23:\"upload_failed_php_error\";s:36:\"Tải file lên không thành công.\";s:12:\"invalid_date\";s:46:\"Định dạng ngày tháng không hợp lệ.\";s:14:\"date_too_early\";s:58:\"Ngày này trước ngày sớm nhất được cho phép.\";s:13:\"date_too_late\";s:54:\"Ngày này quá ngày gần nhất được cho phép.\";s:14:\"invalid_number\";s:38:\"Định dạng số không hợp lệ.\";s:16:\"number_too_small\";s:48:\"Con số nhỏ hơn số nhỏ nhất cho phép.\";s:16:\"number_too_large\";s:48:\"Con số lớn hơn số lớn nhất cho phép.\";s:23:\"quiz_answer_not_correct\";s:30:\"Câu trả lời chưa đúng.\";s:13:\"invalid_email\";s:38:\"Địa chỉ e-mail không hợp lệ.\";s:11:\"invalid_url\";s:22:\"URL không hợp lệ.\";s:11:\"invalid_tel\";s:39:\"Số điện thoại không hợp lệ.\";}'),
(186, 70, '_additional_settings', ''),
(187, 70, '_locale', 'vi'),
(195, 138, '_edit_lock', '1629980703:1'),
(196, 138, '_edit_last', '1'),
(199, 101, '_edit_lock', '1630926693:1'),
(200, 101, '_edit_last', '1'),
(206, 125, '_edit_lock', '1629793713:1'),
(207, 125, '_edit_last', '1'),
(208, 36, 'contact_info_title', 'Liên hệ với chúng tôi'),
(209, 36, '_contact_info_title', 'field_6066c748823cd'),
(210, 36, 'contact_info_address', ''),
(211, 36, '_contact_info_address', 'field_6066c7dd823d2'),
(212, 36, 'contact_contact_title', 'Gửi liên hệ'),
(213, 36, '_contact_contact_title', 'field_6066c6a4823c7'),
(214, 36, 'contact_contact_form', '41'),
(215, 36, '_contact_contact_form', 'field_60769726c0e5d'),
(216, 36, 'contact_map', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.7749747466105!2d105.82069491493218!3d21.00165548601306!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ac8429ac32cb%3A0x592668996cef591f!2zMzE1IFRyxrDhu51uZyBDaGluaCwgS2jGsMahbmcgVGjGsOG7o25nLCDEkOG7kW5nIMSQYSwgSMOgIE7hu5lp!5e0!3m2!1svi!2s!4v1533628589156\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>'),
(217, 36, '_contact_map', 'field_6066c834823d5'),
(218, 186, 'contact_info_title', ''),
(219, 186, '_contact_info_title', 'field_6066c748823cd'),
(220, 186, 'contact_info_address', ''),
(221, 186, '_contact_info_address', 'field_6066c7dd823d2'),
(222, 186, 'contact_contact_title', ''),
(223, 186, '_contact_contact_title', 'field_6066c6a4823c7'),
(224, 186, 'contact_contact_form', '41'),
(225, 186, '_contact_contact_form', 'field_60769726c0e5d'),
(226, 186, 'contact_map', ''),
(227, 186, '_contact_map', 'field_6066c834823d5'),
(352, 234, '_edit_last', '1'),
(353, 234, '_wp_page_template', 'default'),
(354, 234, '_edit_lock', '1629796685:1'),
(355, 234, 'intro_intro_image', ''),
(356, 234, '_intro_intro_image', 'field_60e274960b608'),
(357, 234, 'intro_intro_title', ''),
(358, 234, '_intro_intro_title', 'field_60e274b70b609'),
(359, 234, 'intro_intro_meta', ''),
(360, 234, '_intro_intro_meta', 'field_60e274d90b60a'),
(361, 234, 'intro_intro_desc', ''),
(362, 234, '_intro_intro_desc', 'field_60e275000b60b'),
(363, 234, 'intro_history_title', ''),
(364, 234, '_intro_history_title', 'field_60e275800b60c'),
(365, 234, 'intro_history_content', ''),
(366, 234, '_intro_history_content', 'field_60e275e00b60e'),
(367, 234, 'intro_target', ''),
(368, 234, '_intro_target', 'field_60e27661d63ca'),
(369, 234, 'intro_value_title', ''),
(370, 234, '_intro_value_title', 'field_60e27728d63cf'),
(371, 234, 'intro_value_content', ''),
(372, 234, '_intro_value_content', 'field_60e27751d63d0'),
(373, 234, 'intro_project_select', ''),
(374, 234, '_intro_project_select', 'field_60e2777ed63d2'),
(375, 235, 'intro_intro_image', ''),
(376, 235, '_intro_intro_image', 'field_60e274960b608'),
(377, 235, 'intro_intro_title', ''),
(378, 235, '_intro_intro_title', 'field_60e274b70b609'),
(379, 235, 'intro_intro_meta', ''),
(380, 235, '_intro_intro_meta', 'field_60e274d90b60a'),
(381, 235, 'intro_intro_desc', ''),
(382, 235, '_intro_intro_desc', 'field_60e275000b60b'),
(383, 235, 'intro_history_title', ''),
(384, 235, '_intro_history_title', 'field_60e275800b60c'),
(385, 235, 'intro_history_content', ''),
(386, 235, '_intro_history_content', 'field_60e275e00b60e'),
(387, 235, 'intro_target', ''),
(388, 235, '_intro_target', 'field_60e27661d63ca'),
(389, 235, 'intro_value_title', ''),
(390, 235, '_intro_value_title', 'field_60e27728d63cf'),
(391, 235, 'intro_value_content', ''),
(392, 235, '_intro_value_content', 'field_60e27751d63d0'),
(393, 235, 'intro_project_select', ''),
(394, 235, '_intro_project_select', 'field_60e2777ed63d2'),
(395, 256, '_menu_item_type', 'post_type'),
(396, 256, '_menu_item_menu_item_parent', '0'),
(397, 256, '_menu_item_object_id', '234'),
(398, 256, '_menu_item_object', 'page'),
(399, 256, '_menu_item_target', ''),
(400, 256, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(401, 256, '_menu_item_xfn', ''),
(402, 256, '_menu_item_url', ''),
(422, 55, '_wp_old_date', '2021-01-22'),
(423, 57, '_wp_old_date', '2021-01-22'),
(425, 263, 'contact_info_title', 'Thông tin liên hệ'),
(426, 263, '_contact_info_title', 'field_6066c748823cd'),
(427, 263, 'contact_info_address', ''),
(428, 263, '_contact_info_address', 'field_6066c7dd823d2'),
(429, 263, 'contact_contact_title', 'Liên hệ với chúng tôi'),
(430, 263, '_contact_contact_title', 'field_6066c6a4823c7'),
(431, 263, 'contact_contact_form', '41'),
(432, 263, '_contact_contact_form', 'field_60769726c0e5d'),
(433, 263, 'contact_map', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.3082428731996!2d105.82533131438663!3d21.02034898600291!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ab783d7a1163%3A0x4ebc7dcdca42f7ed!2zMTcwIMSQw6ogTGEgVGjDoG5oLCDDlCBDaOG7oyBE4burYSwgxJDhu5FuZyDEkGEsIEjDoCBO4buZaSwgVmnhu4d0IE5hbQ!5e0!3m2!1svi!2s!4v1515998843160\"\r\n                                width=\"100%\" height=\"600\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>'),
(434, 263, '_contact_map', 'field_6066c834823d5'),
(435, 264, '_edit_last', '1'),
(436, 264, '_edit_lock', '1628494718:1'),
(437, 264, '_wp_page_template', 'template-news.php'),
(438, 266, '_menu_item_type', 'post_type'),
(439, 266, '_menu_item_menu_item_parent', '0'),
(440, 266, '_menu_item_object_id', '264'),
(441, 266, '_menu_item_object', 'page'),
(442, 266, '_menu_item_target', ''),
(443, 266, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(444, 266, '_menu_item_xfn', ''),
(445, 266, '_menu_item_url', ''),
(447, 55, '_wp_old_date', '2021-07-16'),
(448, 256, '_wp_old_date', '2021-07-16'),
(451, 57, '_wp_old_date', '2021-07-16'),
(455, 275, '_wp_attached_file', '2021/08/36.png'),
(456, 275, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:28;s:6:\"height\";i:23;s:4:\"file\";s:14:\"2021/08/36.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(457, 276, '_wp_attached_file', '2021/08/37.png'),
(458, 276, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:35;s:6:\"height\";i:31;s:4:\"file\";s:14:\"2021/08/37.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(459, 277, '_wp_attached_file', '2021/08/38.png'),
(460, 277, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:35;s:6:\"height\";i:35;s:4:\"file\";s:14:\"2021/08/38.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(488, 55, '_wp_old_date', '2021-08-09'),
(489, 256, '_wp_old_date', '2021-08-09'),
(490, 266, '_wp_old_date', '2021-08-09'),
(493, 57, '_wp_old_date', '2021-08-09'),
(494, 281, '_menu_item_type', 'post_type'),
(495, 281, '_menu_item_menu_item_parent', '0'),
(496, 281, '_menu_item_object_id', '234'),
(497, 281, '_menu_item_object', 'page'),
(498, 281, '_menu_item_target', ''),
(499, 281, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(500, 281, '_menu_item_xfn', ''),
(501, 281, '_menu_item_url', ''),
(503, 282, '_menu_item_type', 'post_type'),
(504, 282, '_menu_item_menu_item_parent', '0'),
(505, 282, '_menu_item_object_id', '36'),
(506, 282, '_menu_item_object', 'page'),
(507, 282, '_menu_item_target', ''),
(508, 282, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(509, 282, '_menu_item_xfn', ''),
(510, 282, '_menu_item_url', ''),
(512, 283, '_menu_item_type', 'post_type'),
(513, 283, '_menu_item_menu_item_parent', '0'),
(514, 283, '_menu_item_object_id', '264'),
(515, 283, '_menu_item_object', 'page'),
(516, 283, '_menu_item_target', ''),
(517, 283, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(518, 283, '_menu_item_xfn', ''),
(519, 283, '_menu_item_url', ''),
(521, 284, '_menu_item_type', 'post_type'),
(522, 284, '_menu_item_menu_item_parent', '0'),
(523, 284, '_menu_item_object_id', '234'),
(524, 284, '_menu_item_object', 'page'),
(525, 284, '_menu_item_target', ''),
(526, 284, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(527, 284, '_menu_item_xfn', ''),
(528, 284, '_menu_item_url', ''),
(529, 286, 'contact_info_title', 'Liên hệ với chúng tôi'),
(530, 286, '_contact_info_title', 'field_6066c748823cd'),
(531, 286, 'contact_info_address', ''),
(532, 286, '_contact_info_address', 'field_6066c7dd823d2'),
(533, 286, 'contact_contact_title', 'Gửi liên hệ'),
(534, 286, '_contact_contact_title', 'field_6066c6a4823c7'),
(535, 286, 'contact_contact_form', '41'),
(536, 286, '_contact_contact_form', 'field_60769726c0e5d'),
(537, 286, 'contact_map', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.3082428731996!2d105.82533131438663!3d21.02034898600291!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ab783d7a1163%3A0x4ebc7dcdca42f7ed!2zMTcwIMSQw6ogTGEgVGjDoG5oLCDDlCBDaOG7oyBE4burYSwgxJDhu5FuZyDEkGEsIEjDoCBO4buZaSwgVmnhu4d0IE5hbQ!5e0!3m2!1svi!2s!4v1515998843160\"\r\n                                width=\"100%\" height=\"600\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>'),
(538, 286, '_contact_map', 'field_6066c834823d5'),
(539, 287, 'contact_info_title', 'Liên hệ với chúng tôi'),
(540, 287, '_contact_info_title', 'field_6066c748823cd'),
(541, 287, 'contact_info_address', ''),
(542, 287, '_contact_info_address', 'field_6066c7dd823d2'),
(543, 287, 'contact_contact_title', 'Gửi liên hệ'),
(544, 287, '_contact_contact_title', 'field_6066c6a4823c7'),
(545, 287, 'contact_contact_form', '41'),
(546, 287, '_contact_contact_form', 'field_60769726c0e5d'),
(547, 287, 'contact_map', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.7749747466105!2d105.82069491493218!3d21.00165548601306!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ac8429ac32cb%3A0x592668996cef591f!2zMzE1IFRyxrDhu51uZyBDaGluaCwgS2jGsMahbmcgVGjGsOG7o25nLCDEkOG7kW5nIMSQYSwgSMOgIE7hu5lp!5e0!3m2!1svi!2s!4v1533628589156\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>'),
(548, 287, '_contact_map', 'field_6066c834823d5'),
(549, 288, 'intro_intro_image', ''),
(550, 288, '_intro_intro_image', 'field_60e274960b608'),
(551, 288, 'intro_intro_title', ''),
(552, 288, '_intro_intro_title', 'field_60e274b70b609'),
(553, 288, 'intro_intro_meta', ''),
(554, 288, '_intro_intro_meta', 'field_60e274d90b60a'),
(555, 288, 'intro_intro_desc', ''),
(556, 288, '_intro_intro_desc', 'field_60e275000b60b'),
(557, 288, 'intro_history_title', ''),
(558, 288, '_intro_history_title', 'field_60e275800b60c'),
(559, 288, 'intro_history_content', ''),
(560, 288, '_intro_history_content', 'field_60e275e00b60e'),
(561, 288, 'intro_target', ''),
(562, 288, '_intro_target', 'field_60e27661d63ca'),
(563, 288, 'intro_value_title', ''),
(564, 288, '_intro_value_title', 'field_60e27728d63cf'),
(565, 288, 'intro_value_content', ''),
(566, 288, '_intro_value_content', 'field_60e27751d63d0'),
(567, 288, 'intro_project_select', ''),
(568, 288, '_intro_project_select', 'field_60e2777ed63d2'),
(569, 289, 'intro_intro_image', ''),
(570, 289, '_intro_intro_image', 'field_60e274960b608'),
(571, 289, 'intro_intro_title', ''),
(572, 289, '_intro_intro_title', 'field_60e274b70b609'),
(573, 289, 'intro_intro_meta', ''),
(574, 289, '_intro_intro_meta', 'field_60e274d90b60a'),
(575, 289, 'intro_intro_desc', ''),
(576, 289, '_intro_intro_desc', 'field_60e275000b60b'),
(577, 289, 'intro_history_title', ''),
(578, 289, '_intro_history_title', 'field_60e275800b60c'),
(579, 289, 'intro_history_content', ''),
(580, 289, '_intro_history_content', 'field_60e275e00b60e'),
(581, 289, 'intro_target', ''),
(582, 289, '_intro_target', 'field_60e27661d63ca'),
(583, 289, 'intro_value_title', ''),
(584, 289, '_intro_value_title', 'field_60e27728d63cf'),
(585, 289, 'intro_value_content', ''),
(586, 289, '_intro_value_content', 'field_60e27751d63d0'),
(587, 289, 'intro_project_select', ''),
(588, 289, '_intro_project_select', 'field_60e2777ed63d2'),
(589, 290, 'intro_intro_image', ''),
(590, 290, '_intro_intro_image', 'field_60e274960b608'),
(591, 290, 'intro_intro_title', ''),
(592, 290, '_intro_intro_title', 'field_60e274b70b609'),
(593, 290, 'intro_intro_meta', ''),
(594, 290, '_intro_intro_meta', 'field_60e274d90b60a'),
(595, 290, 'intro_intro_desc', ''),
(596, 290, '_intro_intro_desc', 'field_60e275000b60b'),
(597, 290, 'intro_history_title', ''),
(598, 290, '_intro_history_title', 'field_60e275800b60c'),
(599, 290, 'intro_history_content', ''),
(600, 290, '_intro_history_content', 'field_60e275e00b60e'),
(601, 290, 'intro_target', ''),
(602, 290, '_intro_target', 'field_60e27661d63ca'),
(603, 290, 'intro_value_title', ''),
(604, 290, '_intro_value_title', 'field_60e27728d63cf'),
(605, 290, 'intro_value_content', ''),
(606, 290, '_intro_value_content', 'field_60e27751d63d0'),
(607, 290, 'intro_project_select', ''),
(608, 290, '_intro_project_select', 'field_60e2777ed63d2'),
(609, 291, '_wp_attached_file', '2020/12/45.jpg'),
(610, 291, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:280;s:6:\"height\";i:200;s:4:\"file\";s:14:\"2020/12/45.jpg\";s:5:\"sizes\";a:2:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:14:\"45-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"p-product\";a:4:{s:4:\"file\";s:14:\"45-200x200.jpg\";s:5:\"width\";i:200;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(611, 292, '_wp_attached_file', '2020/12/46.jpg'),
(612, 292, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:280;s:6:\"height\";i:200;s:4:\"file\";s:14:\"2020/12/46.jpg\";s:5:\"sizes\";a:2:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:14:\"46-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"p-product\";a:4:{s:4:\"file\";s:14:\"46-200x200.jpg\";s:5:\"width\";i:200;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(613, 293, '_wp_attached_file', '2020/12/47.jpg'),
(614, 293, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:280;s:6:\"height\";i:200;s:4:\"file\";s:14:\"2020/12/47.jpg\";s:5:\"sizes\";a:2:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:14:\"47-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"p-product\";a:4:{s:4:\"file\";s:14:\"47-200x200.jpg\";s:5:\"width\";i:200;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(615, 294, '_wp_attached_file', '2020/12/48.jpg'),
(616, 294, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:280;s:6:\"height\";i:200;s:4:\"file\";s:14:\"2020/12/48.jpg\";s:5:\"sizes\";a:2:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:14:\"48-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"p-product\";a:4:{s:4:\"file\";s:14:\"48-200x200.jpg\";s:5:\"width\";i:200;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(617, 29, '_thumbnail_id', '291'),
(620, 29, '_wp_old_slug', 'bai-viet-1'),
(621, 296, 'test2', ''),
(622, 296, '_test2', 'field_5fdac066178b1'),
(625, 31, '_thumbnail_id', '292'),
(628, 64, '_thumbnail_id', '293'),
(631, 66, 'test2', ''),
(632, 66, '_test2', 'field_5fdac066178b1'),
(633, 297, '_edit_last', '1'),
(634, 297, '_edit_lock', '1629798210:1'),
(635, 297, '_thumbnail_id', '294'),
(642, 301, '_wp_attached_file', '2021/08/banner13.jpg'),
(643, 301, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:281;s:6:\"height\";i:562;s:4:\"file\";s:20:\"2021/08/banner13.jpg\";s:5:\"sizes\";a:4:{s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:20:\"banner13-281x300.jpg\";s:5:\"width\";i:281;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:20:\"banner13-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"p-post\";a:4:{s:4:\"file\";s:20:\"banner13-280x200.jpg\";s:5:\"width\";i:280;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"p-product\";a:4:{s:4:\"file\";s:20:\"banner13-200x200.jpg\";s:5:\"width\";i:200;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(646, 303, '_wp_attached_file', '2021/08/logo.png'),
(647, 303, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:234;s:6:\"height\";i:47;s:4:\"file\";s:16:\"2021/08/logo.png\";s:5:\"sizes\";a:2:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:15:\"logo-100x47.png\";s:5:\"width\";i:100;s:6:\"height\";i:47;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"p-product\";a:4:{s:4:\"file\";s:15:\"logo-200x47.png\";s:5:\"width\";i:200;s:6:\"height\";i:47;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(648, 304, '_wp_attached_file', 'woocommerce-placeholder.png'),
(649, 304, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1200;s:6:\"height\";i:1200;s:4:\"file\";s:27:\"woocommerce-placeholder.png\";s:5:\"sizes\";a:6:{s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:35:\"woocommerce-placeholder-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-600x600.png\";s:5:\"width\";i:600;s:6:\"height\";i:600;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-768x768.png\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"p-post\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-280x200.png\";s:5:\"width\";i:280;s:6:\"height\";i:200;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"p-product\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-200x200.png\";s:5:\"width\";i:200;s:6:\"height\";i:200;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(650, 310, '_edit_last', '1'),
(651, 310, '_edit_lock', '1629972789:1'),
(652, 310, '_regular_price', '1000000'),
(653, 310, '_sale_price', '950000'),
(654, 310, 'total_sales', '1'),
(655, 310, '_tax_status', 'taxable'),
(656, 310, '_tax_class', ''),
(657, 310, '_manage_stock', 'no'),
(658, 310, '_backorders', 'no'),
(659, 310, '_sold_individually', 'no'),
(660, 310, '_virtual', 'no'),
(661, 310, '_downloadable', 'no'),
(662, 310, '_download_limit', '-1'),
(663, 310, '_download_expiry', '-1'),
(664, 310, '_stock', NULL),
(665, 310, '_stock_status', 'instock'),
(666, 310, '_wc_average_rating', '4.00'),
(667, 310, '_wc_review_count', '1'),
(668, 310, '_product_version', '5.6.0'),
(669, 310, '_price', '950000'),
(670, 310, 'product_price_regular', ''),
(671, 310, '_product_price_regular', 'field_608f83c4c0ec8'),
(672, 310, 'product_price_sale', ''),
(673, 310, '_product_price_sale', 'field_608f83d1c0ec9'),
(674, 310, 's_p_gallery', ''),
(675, 310, '_s_p_gallery', 'field_608fc22539813'),
(676, 310, 's_p_detail', ''),
(677, 310, '_s_p_detail', 'field_608fc28f39816'),
(756, 311, '_wp_attached_file', '2021/08/logo-1.png'),
(757, 311, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:159;s:6:\"height\";i:87;s:4:\"file\";s:18:\"2021/08/logo-1.png\";s:5:\"sizes\";a:2:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:17:\"logo-1-100x87.png\";s:5:\"width\";i:100;s:6:\"height\";i:87;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:17:\"logo-1-100x87.png\";s:5:\"width\";i:100;s:6:\"height\";i:87;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(758, 312, '_order_key', 'wc_order_77Yh4FXTtm6hw'),
(759, 312, '_customer_user', '1'),
(760, 312, '_payment_method', 'cod'),
(761, 312, '_payment_method_title', 'Trả tiền mặt khi nhận hàng'),
(762, 312, '_customer_ip_address', '127.0.0.1'),
(763, 312, '_customer_user_agent', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36'),
(764, 312, '_created_via', 'checkout'),
(765, 312, '_cart_hash', '374cf2f5a9ea02df661cf346f2f8e3dc'),
(766, 312, '_billing_first_name', 'Tiệp'),
(767, 312, '_billing_address_1', 'Hà Nội'),
(768, 312, '_billing_email', 'tiepnguyen220194@gmail.com'),
(769, 312, '_billing_phone', '0359117322'),
(770, 312, '_order_currency', 'VND'),
(771, 312, '_cart_discount', '0'),
(772, 312, '_cart_discount_tax', '0'),
(773, 312, '_order_shipping', '0'),
(774, 312, '_order_shipping_tax', '0'),
(775, 312, '_order_tax', '0'),
(776, 312, '_order_total', '950000'),
(777, 312, '_order_version', '5.6.0'),
(778, 312, '_prices_include_tax', 'no'),
(779, 312, '_billing_address_index', 'Tiệp   Hà Nội      tiepnguyen220194@gmail.com 0359117322'),
(780, 312, '_shipping_address_index', '         '),
(781, 312, 'is_vat_exempt', 'no'),
(782, 312, '_download_permissions_granted', 'yes'),
(783, 312, '_recorded_sales', 'yes'),
(784, 312, '_recorded_coupon_usage_counts', 'yes'),
(785, 312, '_order_stock_reduced', 'yes'),
(786, 312, '_new_order_email_sent', 'true'),
(796, 314, '_menu_item_type', 'taxonomy'),
(797, 314, '_menu_item_menu_item_parent', '321'),
(798, 314, '_menu_item_object_id', '26'),
(799, 314, '_menu_item_object', 'product_cat'),
(800, 314, '_menu_item_target', ''),
(801, 314, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(802, 314, '_menu_item_xfn', ''),
(803, 314, '_menu_item_url', ''),
(823, 317, '_menu_item_type', 'taxonomy'),
(824, 317, '_menu_item_menu_item_parent', '321'),
(825, 317, '_menu_item_object_id', '27'),
(826, 317, '_menu_item_object', 'product_cat'),
(827, 317, '_menu_item_target', ''),
(828, 317, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(829, 317, '_menu_item_xfn', ''),
(830, 317, '_menu_item_url', ''),
(841, 55, '_wp_old_date', '2021-08-23'),
(842, 256, '_wp_old_date', '2021-08-23'),
(843, 266, '_wp_old_date', '2021-08-23'),
(846, 57, '_wp_old_date', '2021-08-23'),
(847, 319, '_edit_last', '1'),
(848, 319, '_edit_lock', '1640834149:1'),
(849, 319, '_regular_price', '2000000'),
(850, 319, 'total_sales', '0'),
(851, 319, '_tax_status', 'taxable'),
(852, 319, '_tax_class', ''),
(853, 319, '_manage_stock', 'no'),
(854, 319, '_backorders', 'no'),
(855, 319, '_sold_individually', 'no'),
(856, 319, '_virtual', 'no'),
(857, 319, '_downloadable', 'no'),
(858, 319, '_download_limit', '-1'),
(859, 319, '_download_expiry', '-1'),
(860, 319, '_stock', NULL),
(861, 319, '_stock_status', 'instock'),
(862, 319, '_wc_average_rating', '0'),
(863, 319, '_wc_review_count', '0'),
(864, 319, '_product_version', '5.6.0'),
(865, 319, '_price', '2000000'),
(866, 320, '_edit_last', '1'),
(867, 320, '_edit_lock', '1640834150:1'),
(868, 320, 'total_sales', '0'),
(869, 320, '_tax_status', 'taxable'),
(870, 320, '_tax_class', ''),
(871, 320, '_manage_stock', 'no'),
(872, 320, '_backorders', 'no'),
(873, 320, '_sold_individually', 'no'),
(874, 320, '_virtual', 'no'),
(875, 320, '_downloadable', 'no'),
(876, 320, '_download_limit', '-1'),
(877, 320, '_download_expiry', '-1'),
(878, 320, '_stock', NULL),
(879, 320, '_stock_status', 'instock'),
(880, 320, '_wc_average_rating', '0'),
(881, 320, '_wc_review_count', '0'),
(882, 320, '_product_version', '5.6.0'),
(883, 310, '_wc_rating_count', 'a:1:{i:4;i:1;}'),
(884, 321, '_menu_item_type', 'custom'),
(885, 321, '_menu_item_menu_item_parent', '0'),
(886, 321, '_menu_item_object_id', '321'),
(887, 321, '_menu_item_object', 'custom'),
(888, 321, '_menu_item_target', ''),
(889, 321, '_menu_item_classes', 'a:2:{i:0;s:9:\"d-lg-none\";i:1;s:7:\"d-block\";}'),
(890, 321, '_menu_item_xfn', ''),
(891, 321, '_menu_item_url', 'https://#'),
(893, 322, '_menu_item_type', 'taxonomy'),
(894, 322, '_menu_item_menu_item_parent', '0'),
(895, 322, '_menu_item_object_id', '26'),
(896, 322, '_menu_item_object', 'product_cat'),
(897, 322, '_menu_item_target', ''),
(898, 322, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(899, 322, '_menu_item_xfn', ''),
(900, 322, '_menu_item_url', ''),
(902, 323, '_menu_item_type', 'taxonomy'),
(903, 323, '_menu_item_menu_item_parent', '0'),
(904, 323, '_menu_item_object_id', '27'),
(905, 323, '_menu_item_object', 'product_cat'),
(906, 323, '_menu_item_target', ''),
(907, 323, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(908, 323, '_menu_item_xfn', ''),
(909, 323, '_menu_item_url', ''),
(915, 326, '_edit_last', '1'),
(916, 326, '_edit_lock', '1640834150:1'),
(917, 326, '_regular_price', '2000000'),
(918, 326, '_sale_price', '950000'),
(919, 326, 'total_sales', '0'),
(920, 326, '_tax_status', 'taxable'),
(921, 326, '_tax_class', ''),
(922, 326, '_manage_stock', 'no'),
(923, 326, '_backorders', 'no'),
(924, 326, '_sold_individually', 'no'),
(925, 326, '_virtual', 'no'),
(926, 326, '_downloadable', 'no'),
(927, 326, '_download_limit', '-1'),
(928, 326, '_download_expiry', '-1'),
(929, 326, '_stock', NULL),
(930, 326, '_stock_status', 'instock'),
(931, 326, '_wc_average_rating', '0'),
(932, 326, '_wc_review_count', '0'),
(933, 326, '_product_version', '5.6.0'),
(934, 326, '_price', '950000'),
(935, 327, '_edit_last', '1'),
(936, 327, '_edit_lock', '1640834150:1'),
(937, 327, '_regular_price', '1000000'),
(938, 327, 'total_sales', '0'),
(939, 327, '_tax_status', 'taxable'),
(940, 327, '_tax_class', ''),
(941, 327, '_manage_stock', 'no'),
(942, 327, '_backorders', 'no'),
(943, 327, '_sold_individually', 'no'),
(944, 327, '_virtual', 'no'),
(945, 327, '_downloadable', 'no'),
(946, 327, '_download_limit', '-1'),
(947, 327, '_download_expiry', '-1'),
(948, 327, '_stock', NULL),
(949, 327, '_stock_status', 'instock'),
(950, 327, '_wc_average_rating', '0'),
(951, 327, '_wc_review_count', '0'),
(952, 327, '_product_version', '5.6.0'),
(953, 327, '_price', '1000000'),
(954, 329, '_wp_attached_file', '2021/08/43.jpg'),
(955, 329, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1180;s:6:\"height\";i:300;s:4:\"file\";s:14:\"2021/08/43.jpg\";s:5:\"sizes\";a:9:{s:12:\"medium_large\";a:4:{s:4:\"file\";s:14:\"43-768x195.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:195;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"p-post\";a:4:{s:4:\"file\";s:14:\"43-280x200.jpg\";s:5:\"width\";i:280;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"p-product\";a:4:{s:4:\"file\";s:14:\"43-200x200.jpg\";s:5:\"width\";i:200;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:14:\"43-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:14:\"43-600x153.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:153;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:14:\"43-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:14:\"43-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:14:\"43-600x153.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:153;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:14:\"43-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(956, 330, '_wp_attached_file', '2021/08/39.jpg'),
(957, 330, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:500;s:6:\"height\";i:500;s:4:\"file\";s:14:\"2021/08/39.jpg\";s:5:\"sizes\";a:6:{s:6:\"p-post\";a:4:{s:4:\"file\";s:14:\"39-280x200.jpg\";s:5:\"width\";i:280;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"p-product\";a:4:{s:4:\"file\";s:14:\"39-200x200.jpg\";s:5:\"width\";i:200;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:14:\"39-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:14:\"39-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:14:\"39-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:14:\"39-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(958, 331, '_wp_attached_file', '2021/08/40.jpg'),
(959, 331, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:102;s:6:\"height\";i:102;s:4:\"file\";s:14:\"2021/08/40.jpg\";s:5:\"sizes\";a:2:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:14:\"40-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:14:\"40-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(960, 332, '_wp_attached_file', '2021/08/41.jpg'),
(961, 332, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:102;s:6:\"height\";i:102;s:4:\"file\";s:14:\"2021/08/41.jpg\";s:5:\"sizes\";a:2:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:14:\"41-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:14:\"41-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(962, 333, '_wp_attached_file', '2021/08/42.jpg'),
(963, 333, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:102;s:6:\"height\";i:102;s:4:\"file\";s:14:\"2021/08/42.jpg\";s:5:\"sizes\";a:2:{s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:14:\"42-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:14:\"42-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(964, 310, '_thumbnail_id', '330'),
(965, 310, '_product_image_gallery', '339,340,341'),
(966, 334, '_edit_last', '1'),
(967, 334, '_edit_lock', '1629890335:1'),
(968, 310, 'thong_số_co_bản_0_title', 'Hãng sản xuất'),
(969, 310, '_thong_số_co_bản_0_title', 'field_612621f4dc7a7'),
(970, 310, 'thong_số_co_bản_0_desc', 'Midea'),
(971, 310, '_thong_số_co_bản_0_desc', 'field_612621fbdc7a8'),
(972, 310, 'thong_số_co_bản_1_title', 'Model'),
(973, 310, '_thong_số_co_bản_1_title', 'field_612621f4dc7a7'),
(974, 310, 'thong_số_co_bản_1_desc', 'MI-K1917EF(R)'),
(975, 310, '_thong_số_co_bản_1_desc', 'field_612621fbdc7a8'),
(976, 310, 'thong_số_co_bản_2_title', 'Màu sắc'),
(977, 310, '_thong_số_co_bản_2_title', 'field_612621f4dc7a7'),
(978, 310, 'thong_số_co_bản_2_desc', 'Đỏ, Kem'),
(979, 310, '_thong_số_co_bản_2_desc', 'field_612621fbdc7a8'),
(980, 310, 'thong_số_co_bản_3_title', 'Loại bếp'),
(981, 310, '_thong_số_co_bản_3_title', 'field_612621f4dc7a7'),
(982, 310, 'thong_số_co_bản_3_desc', 'Bếp từ'),
(983, 310, '_thong_số_co_bản_3_desc', 'field_612621fbdc7a8'),
(984, 310, 'thong_số_co_bản_4_title', 'Xuất xứ'),
(985, 310, '_thong_số_co_bản_4_title', 'field_612621f4dc7a7'),
(986, 310, 'thong_số_co_bản_4_desc', 'Việt Nam'),
(987, 310, '_thong_số_co_bản_4_desc', 'field_612621fbdc7a8'),
(988, 310, 'thong_số_co_bản_5_title', 'Bảo hành chính hãng'),
(989, 310, '_thong_số_co_bản_5_title', 'field_612621f4dc7a7'),
(990, 310, 'thong_số_co_bản_5_desc', '12 Tháng'),
(991, 310, '_thong_số_co_bản_5_desc', 'field_612621fbdc7a8'),
(992, 310, 'thong_số_co_bản_6_title', 'Công suất'),
(993, 310, '_thong_số_co_bản_6_title', 'field_612621f4dc7a7'),
(994, 310, 'thong_số_co_bản_6_desc', '1900W'),
(995, 310, '_thong_số_co_bản_6_desc', 'field_612621fbdc7a8'),
(996, 310, 'thong_số_co_bản_7_title', 'Điều khiển nhiêt độ'),
(997, 310, '_thong_số_co_bản_7_title', 'field_612621f4dc7a7'),
(998, 310, 'thong_số_co_bản_7_desc', 'Núm vặn'),
(999, 310, '_thong_số_co_bản_7_desc', 'field_612621fbdc7a8'),
(1000, 310, 'thong_số_co_bản_8_title', 'Chất liệu mặt bếp'),
(1001, 310, '_thong_số_co_bản_8_title', 'field_612621f4dc7a7'),
(1002, 310, 'thong_số_co_bản_8_desc', 'Kính chịu nhiệt'),
(1003, 310, '_thong_số_co_bản_8_desc', 'field_612621fbdc7a8'),
(1004, 310, 'thong_số_co_bản', '9'),
(1005, 310, '_thong_số_co_bản', 'field_612621cedc7a6'),
(1006, 310, 'single_product_parameter_0_title', 'Hãng sản xuất'),
(1007, 310, '_single_product_parameter_0_title', 'field_612621f4dc7a7'),
(1008, 310, 'single_product_parameter_0_desc', 'Midea'),
(1009, 310, '_single_product_parameter_0_desc', 'field_612621fbdc7a8'),
(1010, 310, 'single_product_parameter_1_title', 'Model'),
(1011, 310, '_single_product_parameter_1_title', 'field_612621f4dc7a7'),
(1012, 310, 'single_product_parameter_1_desc', 'MI-K1917EF(R)'),
(1013, 310, '_single_product_parameter_1_desc', 'field_612621fbdc7a8'),
(1014, 310, 'single_product_parameter_2_title', 'Màu sắc'),
(1015, 310, '_single_product_parameter_2_title', 'field_612621f4dc7a7'),
(1016, 310, 'single_product_parameter_2_desc', 'Đỏ, Kem'),
(1017, 310, '_single_product_parameter_2_desc', 'field_612621fbdc7a8'),
(1018, 310, 'single_product_parameter_3_title', 'Loại bếp'),
(1019, 310, '_single_product_parameter_3_title', 'field_612621f4dc7a7'),
(1020, 310, 'single_product_parameter_3_desc', 'Bếp từ'),
(1021, 310, '_single_product_parameter_3_desc', 'field_612621fbdc7a8'),
(1022, 310, 'single_product_parameter_4_title', 'Xuất xứ'),
(1023, 310, '_single_product_parameter_4_title', 'field_612621f4dc7a7'),
(1024, 310, 'single_product_parameter_4_desc', 'Việt Nam'),
(1025, 310, '_single_product_parameter_4_desc', 'field_612621fbdc7a8'),
(1026, 310, 'single_product_parameter_5_title', 'Bảo hành chính hãng'),
(1027, 310, '_single_product_parameter_5_title', 'field_612621f4dc7a7'),
(1028, 310, 'single_product_parameter_5_desc', '12 Tháng'),
(1029, 310, '_single_product_parameter_5_desc', 'field_612621fbdc7a8'),
(1030, 310, 'single_product_parameter_6_title', 'Công suất'),
(1031, 310, '_single_product_parameter_6_title', 'field_612621f4dc7a7'),
(1032, 310, 'single_product_parameter_6_desc', '1900W'),
(1033, 310, '_single_product_parameter_6_desc', 'field_612621fbdc7a8'),
(1034, 310, 'single_product_parameter_7_title', 'Điều khiển nhiêt độ'),
(1035, 310, '_single_product_parameter_7_title', 'field_612621f4dc7a7'),
(1036, 310, 'single_product_parameter_7_desc', 'Núm vặn'),
(1037, 310, '_single_product_parameter_7_desc', 'field_612621fbdc7a8'),
(1038, 310, 'single_product_parameter_8_title', 'Chất liệu mặt bếp'),
(1039, 310, '_single_product_parameter_8_title', 'field_612621f4dc7a7'),
(1040, 310, 'single_product_parameter_8_desc', 'Kính chịu nhiệt'),
(1041, 310, '_single_product_parameter_8_desc', 'field_612621fbdc7a8'),
(1042, 310, 'single_product_parameter', '9'),
(1043, 310, '_single_product_parameter', 'field_612621cedc7a6'),
(1044, 339, '_wp_attached_file', '2021/08/slider2.jpg'),
(1045, 339, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1920;s:6:\"height\";i:1158;s:4:\"file\";s:19:\"2021/08/slider2.jpg\";s:5:\"sizes\";a:10:{s:12:\"medium_large\";a:4:{s:4:\"file\";s:19:\"slider2-768x463.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:463;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:20:\"slider2-1536x926.jpg\";s:5:\"width\";i:1536;s:6:\"height\";i:926;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"p-post\";a:4:{s:4:\"file\";s:19:\"slider2-280x200.jpg\";s:5:\"width\";i:280;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"p-product\";a:4:{s:4:\"file\";s:19:\"slider2-200x200.jpg\";s:5:\"width\";i:200;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:19:\"slider2-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:19:\"slider2-600x362.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:362;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:19:\"slider2-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:19:\"slider2-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:19:\"slider2-600x362.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:362;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:19:\"slider2-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1046, 340, '_wp_attached_file', '2021/08/46.jpg'),
(1047, 340, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:280;s:6:\"height\";i:200;s:4:\"file\";s:14:\"2021/08/46.jpg\";s:5:\"sizes\";a:3:{s:9:\"p-product\";a:4:{s:4:\"file\";s:14:\"46-200x200.jpg\";s:5:\"width\";i:200;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:14:\"46-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:14:\"46-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1048, 341, '_wp_attached_file', '2021/08/48.jpg');
INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1049, 341, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:280;s:6:\"height\";i:200;s:4:\"file\";s:14:\"2021/08/48.jpg\";s:5:\"sizes\";a:3:{s:9:\"p-product\";a:4:{s:4:\"file\";s:14:\"48-200x200.jpg\";s:5:\"width\";i:200;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:14:\"48-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:14:\"48-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1050, 353, '_wp_attached_file', '2021/08/banner.jpg'),
(1051, 353, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:220;s:6:\"height\";i:426;s:4:\"file\";s:18:\"2021/08/banner.jpg\";s:5:\"sizes\";a:6:{s:6:\"p-post\";a:4:{s:4:\"file\";s:18:\"banner-220x200.jpg\";s:5:\"width\";i:220;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"p-product\";a:4:{s:4:\"file\";s:18:\"banner-200x200.jpg\";s:5:\"width\";i:200;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:18:\"banner-220x300.jpg\";s:5:\"width\";i:220;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:18:\"banner-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:18:\"banner-220x300.jpg\";s:5:\"width\";i:220;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:18:\"banner-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1052, 354, '_wp_attached_file', '2021/08/slider.jpg'),
(1053, 354, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:706;s:6:\"height\";i:426;s:4:\"file\";s:18:\"2021/08/slider.jpg\";s:5:\"sizes\";a:8:{s:6:\"p-post\";a:4:{s:4:\"file\";s:18:\"slider-280x200.jpg\";s:5:\"width\";i:280;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"p-product\";a:4:{s:4:\"file\";s:18:\"slider-200x200.jpg\";s:5:\"width\";i:200;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:18:\"slider-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:18:\"slider-600x362.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:362;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:18:\"slider-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:18:\"slider-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:18:\"slider-600x362.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:362;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:18:\"slider-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1054, 12, '_edit_last', '1'),
(1055, 12, '_wp_page_template', 'default'),
(1056, 12, 'home_slide_0_image', '354'),
(1057, 12, '_home_slide_0_image', 'field_60f54961f67f9'),
(1058, 12, 'home_slide_0_url', 'https://facebook.com'),
(1059, 12, '_home_slide_0_url', 'field_606be15c2caee'),
(1060, 12, 'home_slide_1_image', '339'),
(1061, 12, '_home_slide_1_image', 'field_60f54961f67f9'),
(1062, 12, 'home_slide_1_url', 'https://facebook.com'),
(1063, 12, '_home_slide_1_url', 'field_606be15c2caee'),
(1064, 12, 'home_slide', '2'),
(1065, 12, '_home_slide', 'field_606be121e5e58'),
(1066, 12, 'home_slide_image_ads', '353'),
(1067, 12, '_home_slide_image_ads', 'field_61275bba23124'),
(1068, 12, 'home_slide_image_ads_url', 'https://facebook.com'),
(1069, 12, '_home_slide_image_ads_url', 'field_612760ba91d40'),
(1070, 12, 'image_ads_one', '356'),
(1071, 12, '_image_ads_one', 'field_606be121e5e71'),
(1072, 12, 'image_ads_one_url', 'https://facebook.com'),
(1073, 12, '_image_ads_one_url', 'field_61275cdf23126'),
(1074, 12, 'image_ads_two', '357'),
(1075, 12, '_image_ads_two', 'field_61275d0423127'),
(1076, 12, 'image_ads_two_url', 'https://facebook.com'),
(1077, 12, '_image_ads_two_url', 'field_61275d1123128'),
(1078, 12, 'home_product_select_cat', '3'),
(1079, 12, '_home_product_select_cat', 'field_60e28c5994cd3'),
(1080, 12, 'image_ads_three', '358'),
(1081, 12, '_image_ads_three', 'field_61275e812312b'),
(1082, 12, 'image_ads_three_url', 'https://facebook.com'),
(1083, 12, '_image_ads_three_url', 'field_61275e852312c'),
(1084, 12, 'image_ads_four', '359'),
(1085, 12, '_image_ads_four', 'field_61275e8a2312d'),
(1086, 12, 'image_ads_four_url', 'https://facebook.com'),
(1087, 12, '_image_ads_four_url', 'field_61275e902312e'),
(1088, 355, 'home_slide_0_image', '354'),
(1089, 355, '_home_slide_0_image', 'field_60f54961f67f9'),
(1090, 355, 'home_slide_0_url', 'https://facebook.com'),
(1091, 355, '_home_slide_0_url', 'field_606be15c2caee'),
(1092, 355, 'home_slide_1_image', '339'),
(1093, 355, '_home_slide_1_image', 'field_60f54961f67f9'),
(1094, 355, 'home_slide_1_url', 'https://facebook.com'),
(1095, 355, '_home_slide_1_url', 'field_606be15c2caee'),
(1096, 355, 'home_slide', '2'),
(1097, 355, '_home_slide', 'field_606be121e5e58'),
(1098, 355, 'home_slide_image_ads', '353'),
(1099, 355, '_home_slide_image_ads', 'field_61275bba23124'),
(1100, 355, 'home_slide_image_ads_url', 'https://facebook.com'),
(1101, 355, '_home_slide_image_ads_url', 'field_612760ba91d40'),
(1102, 355, 'image_ads_one', ''),
(1103, 355, '_image_ads_one', 'field_606be121e5e71'),
(1104, 355, 'image_ads_one_url', ''),
(1105, 355, '_image_ads_one_url', 'field_61275cdf23126'),
(1106, 355, 'image_ads_two', ''),
(1107, 355, '_image_ads_two', 'field_61275d0423127'),
(1108, 355, 'image_ads_two_url', ''),
(1109, 355, '_image_ads_two_url', 'field_61275d1123128'),
(1110, 355, 'home_product_select_cat', ''),
(1111, 355, '_home_product_select_cat', 'field_60e28c5994cd3'),
(1112, 355, 'image_ads_three', ''),
(1113, 355, '_image_ads_three', 'field_61275e812312b'),
(1114, 355, 'image_ads_three_url', ''),
(1115, 355, '_image_ads_three_url', 'field_61275e852312c'),
(1116, 355, 'image_ads_four', ''),
(1117, 355, '_image_ads_four', 'field_61275e8a2312d'),
(1118, 355, 'image_ads_four_url', ''),
(1119, 355, '_image_ads_four_url', 'field_61275e902312e'),
(1120, 356, '_wp_attached_file', '2021/08/banner2.jpg'),
(1121, 356, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:580;s:6:\"height\";i:250;s:4:\"file\";s:19:\"2021/08/banner2.jpg\";s:5:\"sizes\";a:6:{s:6:\"p-post\";a:4:{s:4:\"file\";s:19:\"banner2-280x200.jpg\";s:5:\"width\";i:280;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"p-product\";a:4:{s:4:\"file\";s:19:\"banner2-200x200.jpg\";s:5:\"width\";i:200;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:19:\"banner2-300x250.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:250;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:19:\"banner2-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:19:\"banner2-300x250.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:250;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:19:\"banner2-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1122, 357, '_wp_attached_file', '2021/08/banner3.jpg'),
(1123, 357, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:581;s:6:\"height\";i:250;s:4:\"file\";s:19:\"2021/08/banner3.jpg\";s:5:\"sizes\";a:6:{s:6:\"p-post\";a:4:{s:4:\"file\";s:19:\"banner3-280x200.jpg\";s:5:\"width\";i:280;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"p-product\";a:4:{s:4:\"file\";s:19:\"banner3-200x200.jpg\";s:5:\"width\";i:200;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:19:\"banner3-300x250.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:250;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:19:\"banner3-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:19:\"banner3-300x250.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:250;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:19:\"banner3-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1124, 358, '_wp_attached_file', '2021/08/banner11.jpg'),
(1125, 358, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:580;s:6:\"height\";i:250;s:4:\"file\";s:20:\"2021/08/banner11.jpg\";s:5:\"sizes\";a:6:{s:6:\"p-post\";a:4:{s:4:\"file\";s:20:\"banner11-280x200.jpg\";s:5:\"width\";i:280;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"p-product\";a:4:{s:4:\"file\";s:20:\"banner11-200x200.jpg\";s:5:\"width\";i:200;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:20:\"banner11-300x250.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:250;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:20:\"banner11-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:20:\"banner11-300x250.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:250;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:20:\"banner11-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1126, 359, '_wp_attached_file', '2021/08/banner12.jpg'),
(1127, 359, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:580;s:6:\"height\";i:250;s:4:\"file\";s:20:\"2021/08/banner12.jpg\";s:5:\"sizes\";a:6:{s:6:\"p-post\";a:4:{s:4:\"file\";s:20:\"banner12-280x200.jpg\";s:5:\"width\";i:280;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"p-product\";a:4:{s:4:\"file\";s:20:\"banner12-200x200.jpg\";s:5:\"width\";i:200;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:20:\"banner12-300x250.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:250;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:20:\"banner12-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:20:\"banner12-300x250.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:250;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:20:\"banner12-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1128, 12, 'home_product_select_cat_0_taxonomy_product', '26'),
(1129, 12, '_home_product_select_cat_0_taxonomy_product', 'field_61275dd723129'),
(1130, 12, 'home_product_select_cat_1_taxonomy_product', '27'),
(1131, 12, '_home_product_select_cat_1_taxonomy_product', 'field_61275dd723129'),
(1132, 360, 'home_slide_0_image', '354'),
(1133, 360, '_home_slide_0_image', 'field_60f54961f67f9'),
(1134, 360, 'home_slide_0_url', 'https://facebook.com'),
(1135, 360, '_home_slide_0_url', 'field_606be15c2caee'),
(1136, 360, 'home_slide_1_image', '339'),
(1137, 360, '_home_slide_1_image', 'field_60f54961f67f9'),
(1138, 360, 'home_slide_1_url', 'https://facebook.com'),
(1139, 360, '_home_slide_1_url', 'field_606be15c2caee'),
(1140, 360, 'home_slide', '2'),
(1141, 360, '_home_slide', 'field_606be121e5e58'),
(1142, 360, 'home_slide_image_ads', '353'),
(1143, 360, '_home_slide_image_ads', 'field_61275bba23124'),
(1144, 360, 'home_slide_image_ads_url', 'https://facebook.com'),
(1145, 360, '_home_slide_image_ads_url', 'field_612760ba91d40'),
(1146, 360, 'image_ads_one', '356'),
(1147, 360, '_image_ads_one', 'field_606be121e5e71'),
(1148, 360, 'image_ads_one_url', 'https://facebook.com'),
(1149, 360, '_image_ads_one_url', 'field_61275cdf23126'),
(1150, 360, 'image_ads_two', '357'),
(1151, 360, '_image_ads_two', 'field_61275d0423127'),
(1152, 360, 'image_ads_two_url', 'https://facebook.com'),
(1153, 360, '_image_ads_two_url', 'field_61275d1123128'),
(1154, 360, 'home_product_select_cat', '2'),
(1155, 360, '_home_product_select_cat', 'field_60e28c5994cd3'),
(1156, 360, 'image_ads_three', '358'),
(1157, 360, '_image_ads_three', 'field_61275e812312b'),
(1158, 360, 'image_ads_three_url', 'https://facebook.com'),
(1159, 360, '_image_ads_three_url', 'field_61275e852312c'),
(1160, 360, 'image_ads_four', '359'),
(1161, 360, '_image_ads_four', 'field_61275e8a2312d'),
(1162, 360, 'image_ads_four_url', 'https://facebook.com'),
(1163, 360, '_image_ads_four_url', 'field_61275e902312e'),
(1164, 360, 'home_product_select_cat_0_taxonomy_product', '26'),
(1165, 360, '_home_product_select_cat_0_taxonomy_product', 'field_61275dd723129'),
(1166, 360, 'home_product_select_cat_1_taxonomy_product', '27'),
(1167, 360, '_home_product_select_cat_1_taxonomy_product', 'field_61275dd723129'),
(1170, 361, 'home_slide_0_image', '354'),
(1171, 361, '_home_slide_0_image', 'field_60f54961f67f9'),
(1172, 361, 'home_slide_0_url', 'https://facebook.com'),
(1173, 361, '_home_slide_0_url', 'field_606be15c2caee'),
(1174, 361, 'home_slide_1_image', '339'),
(1175, 361, '_home_slide_1_image', 'field_60f54961f67f9'),
(1176, 361, 'home_slide_1_url', 'https://facebook.com'),
(1177, 361, '_home_slide_1_url', 'field_606be15c2caee'),
(1178, 361, 'home_slide', '2'),
(1179, 361, '_home_slide', 'field_606be121e5e58'),
(1180, 361, 'home_slide_image_ads', '353'),
(1181, 361, '_home_slide_image_ads', 'field_61275bba23124'),
(1182, 361, 'home_slide_image_ads_url', 'https://facebook.com'),
(1183, 361, '_home_slide_image_ads_url', 'field_612760ba91d40'),
(1184, 361, 'image_ads_one', '356'),
(1185, 361, '_image_ads_one', 'field_606be121e5e71'),
(1186, 361, 'image_ads_one_url', 'https://facebook.com'),
(1187, 361, '_image_ads_one_url', 'field_61275cdf23126'),
(1188, 361, 'image_ads_two', '357'),
(1189, 361, '_image_ads_two', 'field_61275d0423127'),
(1190, 361, 'image_ads_two_url', 'https://facebook.com'),
(1191, 361, '_image_ads_two_url', 'field_61275d1123128'),
(1192, 361, 'home_product_select_cat', '3'),
(1193, 361, '_home_product_select_cat', 'field_60e28c5994cd3'),
(1194, 361, 'image_ads_three', '358'),
(1195, 361, '_image_ads_three', 'field_61275e812312b'),
(1196, 361, 'image_ads_three_url', 'https://facebook.com'),
(1197, 361, '_image_ads_three_url', 'field_61275e852312c'),
(1198, 361, 'image_ads_four', '359'),
(1199, 361, '_image_ads_four', 'field_61275e8a2312d'),
(1200, 361, 'image_ads_four_url', 'https://facebook.com'),
(1201, 361, '_image_ads_four_url', 'field_61275e902312e'),
(1202, 361, 'home_product_select_cat_0_taxonomy_product', '26'),
(1203, 361, '_home_product_select_cat_0_taxonomy_product', 'field_61275dd723129'),
(1204, 361, 'home_product_select_cat_1_taxonomy_product', '27'),
(1205, 361, '_home_product_select_cat_1_taxonomy_product', 'field_61275dd723129'),
(1206, 361, 'home_product_select_cat_2_taxonomy_product', '25'),
(1207, 361, '_home_product_select_cat_2_taxonomy_product', 'field_61275dd723129'),
(1208, 362, 'home_slide_0_image', '354'),
(1209, 362, '_home_slide_0_image', 'field_60f54961f67f9'),
(1210, 362, 'home_slide_0_url', 'https://facebook.com'),
(1211, 362, '_home_slide_0_url', 'field_606be15c2caee'),
(1212, 362, 'home_slide_1_image', '339'),
(1213, 362, '_home_slide_1_image', 'field_60f54961f67f9'),
(1214, 362, 'home_slide_1_url', 'https://facebook.com'),
(1215, 362, '_home_slide_1_url', 'field_606be15c2caee'),
(1216, 362, 'home_slide', '2'),
(1217, 362, '_home_slide', 'field_606be121e5e58'),
(1218, 362, 'home_slide_image_ads', '353'),
(1219, 362, '_home_slide_image_ads', 'field_61275bba23124'),
(1220, 362, 'home_slide_image_ads_url', 'https://facebook.com'),
(1221, 362, '_home_slide_image_ads_url', 'field_612760ba91d40'),
(1222, 362, 'image_ads_one', '356'),
(1223, 362, '_image_ads_one', 'field_606be121e5e71'),
(1224, 362, 'image_ads_one_url', 'https://facebook.com'),
(1225, 362, '_image_ads_one_url', 'field_61275cdf23126'),
(1226, 362, 'image_ads_two', '357'),
(1227, 362, '_image_ads_two', 'field_61275d0423127'),
(1228, 362, 'image_ads_two_url', 'https://facebook.com'),
(1229, 362, '_image_ads_two_url', 'field_61275d1123128'),
(1230, 362, 'home_product_select_cat', '2'),
(1231, 362, '_home_product_select_cat', 'field_60e28c5994cd3'),
(1232, 362, 'image_ads_three', '358'),
(1233, 362, '_image_ads_three', 'field_61275e812312b'),
(1234, 362, 'image_ads_three_url', 'https://facebook.com'),
(1235, 362, '_image_ads_three_url', 'field_61275e852312c'),
(1236, 362, 'image_ads_four', '359'),
(1237, 362, '_image_ads_four', 'field_61275e8a2312d'),
(1238, 362, 'image_ads_four_url', 'https://facebook.com'),
(1239, 362, '_image_ads_four_url', 'field_61275e902312e'),
(1240, 362, 'home_product_select_cat_0_taxonomy_product', '26'),
(1241, 362, '_home_product_select_cat_0_taxonomy_product', 'field_61275dd723129'),
(1242, 362, 'home_product_select_cat_1_taxonomy_product', '27'),
(1243, 362, '_home_product_select_cat_1_taxonomy_product', 'field_61275dd723129'),
(1244, 12, 'home_product_buy_hot_title', 'Sản phẩm bán chạy'),
(1245, 12, '_home_product_buy_hot_title', 'field_61276583f0892'),
(1246, 12, 'home_product_buy_hot_select', 'a:5:{i:0;s:3:\"327\";i:1;s:3:\"320\";i:2;s:3:\"326\";i:3;s:3:\"319\";i:4;s:3:\"310\";}'),
(1247, 12, '_home_product_buy_hot_select', 'field_6127658df0893'),
(1248, 366, 'home_slide_0_image', '354'),
(1249, 366, '_home_slide_0_image', 'field_60f54961f67f9'),
(1250, 366, 'home_slide_0_url', 'https://facebook.com'),
(1251, 366, '_home_slide_0_url', 'field_606be15c2caee'),
(1252, 366, 'home_slide_1_image', '339'),
(1253, 366, '_home_slide_1_image', 'field_60f54961f67f9'),
(1254, 366, 'home_slide_1_url', 'https://facebook.com'),
(1255, 366, '_home_slide_1_url', 'field_606be15c2caee'),
(1256, 366, 'home_slide', '2'),
(1257, 366, '_home_slide', 'field_606be121e5e58'),
(1258, 366, 'home_slide_image_ads', '353'),
(1259, 366, '_home_slide_image_ads', 'field_61275bba23124'),
(1260, 366, 'home_slide_image_ads_url', 'https://facebook.com'),
(1261, 366, '_home_slide_image_ads_url', 'field_612760ba91d40'),
(1262, 366, 'image_ads_one', '356'),
(1263, 366, '_image_ads_one', 'field_606be121e5e71'),
(1264, 366, 'image_ads_one_url', 'https://facebook.com'),
(1265, 366, '_image_ads_one_url', 'field_61275cdf23126'),
(1266, 366, 'image_ads_two', '357'),
(1267, 366, '_image_ads_two', 'field_61275d0423127'),
(1268, 366, 'image_ads_two_url', 'https://facebook.com'),
(1269, 366, '_image_ads_two_url', 'field_61275d1123128'),
(1270, 366, 'home_product_select_cat', '2'),
(1271, 366, '_home_product_select_cat', 'field_60e28c5994cd3'),
(1272, 366, 'image_ads_three', '358'),
(1273, 366, '_image_ads_three', 'field_61275e812312b'),
(1274, 366, 'image_ads_three_url', 'https://facebook.com'),
(1275, 366, '_image_ads_three_url', 'field_61275e852312c'),
(1276, 366, 'image_ads_four', '359'),
(1277, 366, '_image_ads_four', 'field_61275e8a2312d'),
(1278, 366, 'image_ads_four_url', 'https://facebook.com'),
(1279, 366, '_image_ads_four_url', 'field_61275e902312e'),
(1280, 366, 'home_product_select_cat_0_taxonomy_product', '26'),
(1281, 366, '_home_product_select_cat_0_taxonomy_product', 'field_61275dd723129'),
(1282, 366, 'home_product_select_cat_1_taxonomy_product', '27'),
(1283, 366, '_home_product_select_cat_1_taxonomy_product', 'field_61275dd723129'),
(1284, 366, 'home_product_buy_hot_title', 'Sản phẩm bán chạy'),
(1285, 366, '_home_product_buy_hot_title', 'field_61276583f0892'),
(1286, 366, 'home_product_buy_hot_select', 'a:5:{i:0;s:3:\"327\";i:1;s:3:\"320\";i:2;s:3:\"326\";i:3;s:3:\"319\";i:4;s:3:\"310\";}'),
(1287, 366, '_home_product_buy_hot_select', 'field_6127658df0893'),
(1288, 371, '_wp_attached_file', '2021/08/banner4.jpg'),
(1289, 371, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:220;s:6:\"height\";i:347;s:4:\"file\";s:19:\"2021/08/banner4.jpg\";s:5:\"sizes\";a:6:{s:6:\"p-post\";a:4:{s:4:\"file\";s:19:\"banner4-220x200.jpg\";s:5:\"width\";i:220;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"p-product\";a:4:{s:4:\"file\";s:19:\"banner4-200x200.jpg\";s:5:\"width\";i:200;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:19:\"banner4-220x300.jpg\";s:5:\"width\";i:220;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:19:\"banner4-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:19:\"banner4-220x300.jpg\";s:5:\"width\";i:220;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:19:\"banner4-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1290, 372, '_wp_attached_file', '2021/08/banner5.jpg'),
(1291, 372, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:220;s:6:\"height\";i:346;s:4:\"file\";s:19:\"2021/08/banner5.jpg\";s:5:\"sizes\";a:6:{s:6:\"p-post\";a:4:{s:4:\"file\";s:19:\"banner5-220x200.jpg\";s:5:\"width\";i:220;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"p-product\";a:4:{s:4:\"file\";s:19:\"banner5-200x200.jpg\";s:5:\"width\";i:200;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:19:\"banner5-220x300.jpg\";s:5:\"width\";i:220;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:19:\"banner5-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:19:\"banner5-220x300.jpg\";s:5:\"width\";i:220;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:19:\"banner5-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1292, 373, '_wp_attached_file', '2021/08/banner6.jpg'),
(1293, 373, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:220;s:6:\"height\";i:347;s:4:\"file\";s:19:\"2021/08/banner6.jpg\";s:5:\"sizes\";a:6:{s:6:\"p-post\";a:4:{s:4:\"file\";s:19:\"banner6-220x200.jpg\";s:5:\"width\";i:220;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"p-product\";a:4:{s:4:\"file\";s:19:\"banner6-200x200.jpg\";s:5:\"width\";i:200;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:19:\"banner6-220x300.jpg\";s:5:\"width\";i:220;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:19:\"banner6-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:19:\"banner6-220x300.jpg\";s:5:\"width\";i:220;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:19:\"banner6-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1294, 374, '_wp_attached_file', '2021/08/banner7.jpg'),
(1295, 374, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:220;s:6:\"height\";i:346;s:4:\"file\";s:19:\"2021/08/banner7.jpg\";s:5:\"sizes\";a:6:{s:6:\"p-post\";a:4:{s:4:\"file\";s:19:\"banner7-220x200.jpg\";s:5:\"width\";i:220;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"p-product\";a:4:{s:4:\"file\";s:19:\"banner7-200x200.jpg\";s:5:\"width\";i:200;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:19:\"banner7-220x300.jpg\";s:5:\"width\";i:220;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:19:\"banner7-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:19:\"banner7-220x300.jpg\";s:5:\"width\";i:220;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:19:\"banner7-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1296, 375, '_wp_attached_file', '2021/08/banner9.jpg'),
(1297, 375, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:220;s:6:\"height\";i:347;s:4:\"file\";s:19:\"2021/08/banner9.jpg\";s:5:\"sizes\";a:6:{s:6:\"p-post\";a:4:{s:4:\"file\";s:19:\"banner9-220x200.jpg\";s:5:\"width\";i:220;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"p-product\";a:4:{s:4:\"file\";s:19:\"banner9-200x200.jpg\";s:5:\"width\";i:200;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:19:\"banner9-220x300.jpg\";s:5:\"width\";i:220;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:19:\"banner9-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:19:\"banner9-220x300.jpg\";s:5:\"width\";i:220;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:19:\"banner9-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1298, 376, '_wp_attached_file', '2021/08/banner10.jpg'),
(1299, 376, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:220;s:6:\"height\";i:346;s:4:\"file\";s:20:\"2021/08/banner10.jpg\";s:5:\"sizes\";a:6:{s:6:\"p-post\";a:4:{s:4:\"file\";s:20:\"banner10-220x200.jpg\";s:5:\"width\";i:220;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"p-product\";a:4:{s:4:\"file\";s:20:\"banner10-200x200.jpg\";s:5:\"width\";i:200;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:20:\"banner10-220x300.jpg\";s:5:\"width\";i:220;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:20:\"banner10-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:20:\"banner10-220x300.jpg\";s:5:\"width\";i:220;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:20:\"banner10-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1300, 12, 'home_product_select_cat_0_taxonomy_product_image_ads_one', '371'),
(1301, 12, '_home_product_select_cat_0_taxonomy_product_image_ads_one', 'field_6127674015cdf'),
(1302, 12, 'home_product_select_cat_0_taxonomy_product_image_ads_one_url', 'https://facebook.com'),
(1303, 12, '_home_product_select_cat_0_taxonomy_product_image_ads_one_url', 'field_6127679415ce1'),
(1304, 12, 'home_product_select_cat_0_taxonomy_product_image_ads_two', '372'),
(1305, 12, '_home_product_select_cat_0_taxonomy_product_image_ads_two', 'field_6127676515ce0'),
(1306, 12, 'home_product_select_cat_0_taxonomy_product_image_ads_two_url', 'https://facebook.com'),
(1307, 12, '_home_product_select_cat_0_taxonomy_product_image_ads_two_url', 'field_612767c615ce2'),
(1308, 12, 'home_product_select_cat_1_taxonomy_product_image_ads_one', '373'),
(1309, 12, '_home_product_select_cat_1_taxonomy_product_image_ads_one', 'field_6127674015cdf'),
(1310, 12, 'home_product_select_cat_1_taxonomy_product_image_ads_one_url', 'https://facebook.com'),
(1311, 12, '_home_product_select_cat_1_taxonomy_product_image_ads_one_url', 'field_6127679415ce1'),
(1312, 12, 'home_product_select_cat_1_taxonomy_product_image_ads_two', '374'),
(1313, 12, '_home_product_select_cat_1_taxonomy_product_image_ads_two', 'field_6127676515ce0'),
(1314, 12, 'home_product_select_cat_1_taxonomy_product_image_ads_two_url', 'https://facebook.com'),
(1315, 12, '_home_product_select_cat_1_taxonomy_product_image_ads_two_url', 'field_612767c615ce2'),
(1316, 12, 'home_product_select_cat_2_taxonomy_product', '31'),
(1317, 12, '_home_product_select_cat_2_taxonomy_product', 'field_61275dd723129'),
(1318, 12, 'home_product_select_cat_2_taxonomy_product_image_ads_one', '375'),
(1319, 12, '_home_product_select_cat_2_taxonomy_product_image_ads_one', 'field_6127674015cdf'),
(1320, 12, 'home_product_select_cat_2_taxonomy_product_image_ads_one_url', 'https://facebook.com'),
(1321, 12, '_home_product_select_cat_2_taxonomy_product_image_ads_one_url', 'field_6127679415ce1'),
(1322, 12, 'home_product_select_cat_2_taxonomy_product_image_ads_two', '376'),
(1323, 12, '_home_product_select_cat_2_taxonomy_product_image_ads_two', 'field_6127676515ce0'),
(1324, 12, 'home_product_select_cat_2_taxonomy_product_image_ads_two_url', 'https://facebook.com'),
(1325, 12, '_home_product_select_cat_2_taxonomy_product_image_ads_two_url', 'field_612767c615ce2'),
(1326, 377, 'home_slide_0_image', '354'),
(1327, 377, '_home_slide_0_image', 'field_60f54961f67f9'),
(1328, 377, 'home_slide_0_url', 'https://facebook.com'),
(1329, 377, '_home_slide_0_url', 'field_606be15c2caee'),
(1330, 377, 'home_slide_1_image', '339'),
(1331, 377, '_home_slide_1_image', 'field_60f54961f67f9'),
(1332, 377, 'home_slide_1_url', 'https://facebook.com'),
(1333, 377, '_home_slide_1_url', 'field_606be15c2caee'),
(1334, 377, 'home_slide', '2'),
(1335, 377, '_home_slide', 'field_606be121e5e58'),
(1336, 377, 'home_slide_image_ads', '353'),
(1337, 377, '_home_slide_image_ads', 'field_61275bba23124'),
(1338, 377, 'home_slide_image_ads_url', 'https://facebook.com'),
(1339, 377, '_home_slide_image_ads_url', 'field_612760ba91d40'),
(1340, 377, 'image_ads_one', '356'),
(1341, 377, '_image_ads_one', 'field_606be121e5e71'),
(1342, 377, 'image_ads_one_url', 'https://facebook.com'),
(1343, 377, '_image_ads_one_url', 'field_61275cdf23126'),
(1344, 377, 'image_ads_two', '357'),
(1345, 377, '_image_ads_two', 'field_61275d0423127'),
(1346, 377, 'image_ads_two_url', 'https://facebook.com'),
(1347, 377, '_image_ads_two_url', 'field_61275d1123128'),
(1348, 377, 'home_product_select_cat', '3'),
(1349, 377, '_home_product_select_cat', 'field_60e28c5994cd3'),
(1350, 377, 'image_ads_three', '358'),
(1351, 377, '_image_ads_three', 'field_61275e812312b'),
(1352, 377, 'image_ads_three_url', 'https://facebook.com'),
(1353, 377, '_image_ads_three_url', 'field_61275e852312c'),
(1354, 377, 'image_ads_four', '359'),
(1355, 377, '_image_ads_four', 'field_61275e8a2312d'),
(1356, 377, 'image_ads_four_url', 'https://facebook.com'),
(1357, 377, '_image_ads_four_url', 'field_61275e902312e'),
(1358, 377, 'home_product_select_cat_0_taxonomy_product', '26'),
(1359, 377, '_home_product_select_cat_0_taxonomy_product', 'field_61275dd723129'),
(1360, 377, 'home_product_select_cat_1_taxonomy_product', '27'),
(1361, 377, '_home_product_select_cat_1_taxonomy_product', 'field_61275dd723129'),
(1362, 377, 'home_product_buy_hot_title', 'Sản phẩm bán chạy'),
(1363, 377, '_home_product_buy_hot_title', 'field_61276583f0892'),
(1364, 377, 'home_product_buy_hot_select', 'a:5:{i:0;s:3:\"327\";i:1;s:3:\"320\";i:2;s:3:\"326\";i:3;s:3:\"319\";i:4;s:3:\"310\";}'),
(1365, 377, '_home_product_buy_hot_select', 'field_6127658df0893'),
(1366, 377, 'home_product_select_cat_0_taxonomy_product_image_ads_one', '371'),
(1367, 377, '_home_product_select_cat_0_taxonomy_product_image_ads_one', 'field_6127674015cdf'),
(1368, 377, 'home_product_select_cat_0_taxonomy_product_image_ads_one_url', 'https://facebook.com'),
(1369, 377, '_home_product_select_cat_0_taxonomy_product_image_ads_one_url', 'field_6127679415ce1'),
(1370, 377, 'home_product_select_cat_0_taxonomy_product_image_ads_two', '372'),
(1371, 377, '_home_product_select_cat_0_taxonomy_product_image_ads_two', 'field_6127676515ce0'),
(1372, 377, 'home_product_select_cat_0_taxonomy_product_image_ads_two_url', 'https://facebook.com'),
(1373, 377, '_home_product_select_cat_0_taxonomy_product_image_ads_two_url', 'field_612767c615ce2'),
(1374, 377, 'home_product_select_cat_1_taxonomy_product_image_ads_one', '373'),
(1375, 377, '_home_product_select_cat_1_taxonomy_product_image_ads_one', 'field_6127674015cdf'),
(1376, 377, 'home_product_select_cat_1_taxonomy_product_image_ads_one_url', 'https://facebook.com'),
(1377, 377, '_home_product_select_cat_1_taxonomy_product_image_ads_one_url', 'field_6127679415ce1'),
(1378, 377, 'home_product_select_cat_1_taxonomy_product_image_ads_two', '374'),
(1379, 377, '_home_product_select_cat_1_taxonomy_product_image_ads_two', 'field_6127676515ce0'),
(1380, 377, 'home_product_select_cat_1_taxonomy_product_image_ads_two_url', 'https://facebook.com'),
(1381, 377, '_home_product_select_cat_1_taxonomy_product_image_ads_two_url', 'field_612767c615ce2'),
(1382, 377, 'home_product_select_cat_2_taxonomy_product', '25'),
(1383, 377, '_home_product_select_cat_2_taxonomy_product', 'field_61275dd723129'),
(1384, 377, 'home_product_select_cat_2_taxonomy_product_image_ads_one', '375'),
(1385, 377, '_home_product_select_cat_2_taxonomy_product_image_ads_one', 'field_6127674015cdf'),
(1386, 377, 'home_product_select_cat_2_taxonomy_product_image_ads_one_url', 'https://facebook.com'),
(1387, 377, '_home_product_select_cat_2_taxonomy_product_image_ads_one_url', 'field_6127679415ce1'),
(1388, 377, 'home_product_select_cat_2_taxonomy_product_image_ads_two', '376'),
(1389, 377, '_home_product_select_cat_2_taxonomy_product_image_ads_two', 'field_6127676515ce0'),
(1390, 377, 'home_product_select_cat_2_taxonomy_product_image_ads_two_url', 'https://facebook.com'),
(1391, 377, '_home_product_select_cat_2_taxonomy_product_image_ads_two_url', 'field_612767c615ce2'),
(1392, 378, 'home_slide_0_image', '354'),
(1393, 378, '_home_slide_0_image', 'field_60f54961f67f9'),
(1394, 378, 'home_slide_0_url', 'https://facebook.com'),
(1395, 378, '_home_slide_0_url', 'field_606be15c2caee'),
(1396, 378, 'home_slide_1_image', '339'),
(1397, 378, '_home_slide_1_image', 'field_60f54961f67f9'),
(1398, 378, 'home_slide_1_url', 'https://facebook.com'),
(1399, 378, '_home_slide_1_url', 'field_606be15c2caee'),
(1400, 378, 'home_slide', '2'),
(1401, 378, '_home_slide', 'field_606be121e5e58'),
(1402, 378, 'home_slide_image_ads', '353'),
(1403, 378, '_home_slide_image_ads', 'field_61275bba23124'),
(1404, 378, 'home_slide_image_ads_url', 'https://facebook.com'),
(1405, 378, '_home_slide_image_ads_url', 'field_612760ba91d40'),
(1406, 378, 'image_ads_one', '356'),
(1407, 378, '_image_ads_one', 'field_606be121e5e71'),
(1408, 378, 'image_ads_one_url', 'https://facebook.com'),
(1409, 378, '_image_ads_one_url', 'field_61275cdf23126'),
(1410, 378, 'image_ads_two', '357'),
(1411, 378, '_image_ads_two', 'field_61275d0423127'),
(1412, 378, 'image_ads_two_url', 'https://facebook.com'),
(1413, 378, '_image_ads_two_url', 'field_61275d1123128'),
(1414, 378, 'home_product_select_cat', '3'),
(1415, 378, '_home_product_select_cat', 'field_60e28c5994cd3'),
(1416, 378, 'image_ads_three', '358'),
(1417, 378, '_image_ads_three', 'field_61275e812312b'),
(1418, 378, 'image_ads_three_url', 'https://facebook.com'),
(1419, 378, '_image_ads_three_url', 'field_61275e852312c'),
(1420, 378, 'image_ads_four', '359'),
(1421, 378, '_image_ads_four', 'field_61275e8a2312d'),
(1422, 378, 'image_ads_four_url', 'https://facebook.com'),
(1423, 378, '_image_ads_four_url', 'field_61275e902312e'),
(1424, 378, 'home_product_select_cat_0_taxonomy_product', '26'),
(1425, 378, '_home_product_select_cat_0_taxonomy_product', 'field_61275dd723129'),
(1426, 378, 'home_product_select_cat_1_taxonomy_product', '27'),
(1427, 378, '_home_product_select_cat_1_taxonomy_product', 'field_61275dd723129'),
(1428, 378, 'home_product_buy_hot_title', 'Sản phẩm bán chạy'),
(1429, 378, '_home_product_buy_hot_title', 'field_61276583f0892'),
(1430, 378, 'home_product_buy_hot_select', 'a:5:{i:0;s:3:\"327\";i:1;s:3:\"320\";i:2;s:3:\"326\";i:3;s:3:\"319\";i:4;s:3:\"310\";}'),
(1431, 378, '_home_product_buy_hot_select', 'field_6127658df0893'),
(1432, 378, 'home_product_select_cat_0_taxonomy_product_image_ads_one', '371'),
(1433, 378, '_home_product_select_cat_0_taxonomy_product_image_ads_one', 'field_6127674015cdf'),
(1434, 378, 'home_product_select_cat_0_taxonomy_product_image_ads_one_url', 'https://facebook.com'),
(1435, 378, '_home_product_select_cat_0_taxonomy_product_image_ads_one_url', 'field_6127679415ce1'),
(1436, 378, 'home_product_select_cat_0_taxonomy_product_image_ads_two', '372'),
(1437, 378, '_home_product_select_cat_0_taxonomy_product_image_ads_two', 'field_6127676515ce0'),
(1438, 378, 'home_product_select_cat_0_taxonomy_product_image_ads_two_url', 'https://facebook.com'),
(1439, 378, '_home_product_select_cat_0_taxonomy_product_image_ads_two_url', 'field_612767c615ce2'),
(1440, 378, 'home_product_select_cat_1_taxonomy_product_image_ads_one', '373'),
(1441, 378, '_home_product_select_cat_1_taxonomy_product_image_ads_one', 'field_6127674015cdf'),
(1442, 378, 'home_product_select_cat_1_taxonomy_product_image_ads_one_url', 'https://facebook.com'),
(1443, 378, '_home_product_select_cat_1_taxonomy_product_image_ads_one_url', 'field_6127679415ce1'),
(1444, 378, 'home_product_select_cat_1_taxonomy_product_image_ads_two', '374'),
(1445, 378, '_home_product_select_cat_1_taxonomy_product_image_ads_two', 'field_6127676515ce0'),
(1446, 378, 'home_product_select_cat_1_taxonomy_product_image_ads_two_url', 'https://facebook.com'),
(1447, 378, '_home_product_select_cat_1_taxonomy_product_image_ads_two_url', 'field_612767c615ce2'),
(1448, 378, 'home_product_select_cat_2_taxonomy_product', '31'),
(1449, 378, '_home_product_select_cat_2_taxonomy_product', 'field_61275dd723129'),
(1450, 378, 'home_product_select_cat_2_taxonomy_product_image_ads_one', '375'),
(1451, 378, '_home_product_select_cat_2_taxonomy_product_image_ads_one', 'field_6127674015cdf'),
(1452, 378, 'home_product_select_cat_2_taxonomy_product_image_ads_one_url', 'https://facebook.com'),
(1453, 378, '_home_product_select_cat_2_taxonomy_product_image_ads_one_url', 'field_6127679415ce1'),
(1454, 378, 'home_product_select_cat_2_taxonomy_product_image_ads_two', '376'),
(1455, 378, '_home_product_select_cat_2_taxonomy_product_image_ads_two', 'field_6127676515ce0'),
(1456, 378, 'home_product_select_cat_2_taxonomy_product_image_ads_two_url', 'https://facebook.com'),
(1457, 378, '_home_product_select_cat_2_taxonomy_product_image_ads_two_url', 'field_612767c615ce2'),
(1458, 319, 'single_product_parameter_0_title', 'Hãng sản xuất'),
(1459, 319, '_single_product_parameter_0_title', 'field_612621f4dc7a7'),
(1460, 319, 'single_product_parameter_0_desc', 'Asean'),
(1461, 319, '_single_product_parameter_0_desc', 'field_612621fbdc7a8'),
(1462, 319, 'single_product_parameter', '1'),
(1463, 319, '_single_product_parameter', 'field_612621cedc7a6'),
(1464, 379, '_form', '<div class=\"single-product-form-quote-title\">Báo Giá Máy</div>\n<div class=\"single-product-form-quote-desc\">Quý khách vui lòng cung cấp thông tin dưới đây để nhận báo giá!</div>\n\n<div class=\"form-group\">\n<label for=\"your-name\">Họ và tên (*)</label>\n    [text* your-name id:your-name class:form-control]\n</div>\n<div class=\"form-group\">\n<label for=\"your-phone\">Số điện thoại (*)</label>\n    [tel* your-phone id:your-phone class:form-control]\n</div>\n<div class=\"form-group\">\n<label for=\"your-email\">Email (*)</label>\n    [email* your-email id:your-email class:form-control]\n</div>\n<div class=\"form-group\">\n<label for=\"your-type\">Loại máy cần nhận báo giá (*)</label>\n    [text* your-type id:your-type class:form-control]\n</div>\n\n[submit class:btn class:w-100 class:bold class:buy-btn \"Yêu cầu gửi báo giá\"]'),
(1465, 379, '_mail', 'a:9:{s:6:\"active\";b:1;s:7:\"subject\";s:30:\"[_site_title] \"[your-subject]\"\";s:6:\"sender\";s:41:\"[_site_title] <wordpress@wordpress.local>\";s:9:\"recipient\";s:19:\"[_site_admin_email]\";s:4:\"body\";s:229:\"Gửi đến từ: [your-name] <[your-email]>\nSố điện thoại: [your-phone]\nLoại máy cần nhận báo giá: [your-type]\n\n-- \nEmail này được gửi đến từ form liên hệ của website [_site_title] ([_site_url])\";s:18:\"additional_headers\";s:22:\"Reply-To: [your-email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(1466, 379, '_mail_2', 'a:9:{s:6:\"active\";b:0;s:7:\"subject\";s:30:\"[_site_title] \"[your-subject]\"\";s:6:\"sender\";s:41:\"[_site_title] <wordpress@wordpress.local>\";s:9:\"recipient\";s:12:\"[your-email]\";s:4:\"body\";s:142:\"Nội dung thông điệp:\n[your-message]\n\n-- \nEmail này được gửi đến từ form liên hệ của website [_site_title] ([_site_url])\";s:18:\"additional_headers\";s:29:\"Reply-To: [_site_admin_email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(1467, 379, '_messages', 'a:22:{s:12:\"mail_sent_ok\";s:53:\"Xin cảm ơn, form đã được gửi thành công.\";s:12:\"mail_sent_ng\";s:118:\"Có lỗi xảy ra trong quá trình gửi. Xin vui lòng thử lại hoặc liên hệ người quản trị website.\";s:16:\"validation_error\";s:86:\"Có một hoặc nhiều mục nhập có lỗi. Vui lòng kiểm tra và thử lại.\";s:4:\"spam\";s:118:\"Có lỗi xảy ra trong quá trình gửi. Xin vui lòng thử lại hoặc liên hệ người quản trị website.\";s:12:\"accept_terms\";s:67:\"Bạn phải chấp nhận điều khoản trước khi gửi form.\";s:16:\"invalid_required\";s:28:\"Mục này là bắt buộc.\";s:16:\"invalid_too_long\";s:36:\"Nhập quá số kí tự cho phép.\";s:17:\"invalid_too_short\";s:44:\"Nhập ít hơn số kí tự tối thiểu.\";s:13:\"upload_failed\";s:36:\"Tải file lên không thành công.\";s:24:\"upload_file_type_invalid\";s:69:\"Bạn không được phép tải lên file theo định dạng này.\";s:21:\"upload_file_too_large\";s:31:\"File kích thước quá lớn.\";s:23:\"upload_failed_php_error\";s:36:\"Tải file lên không thành công.\";s:12:\"invalid_date\";s:46:\"Định dạng ngày tháng không hợp lệ.\";s:14:\"date_too_early\";s:58:\"Ngày này trước ngày sớm nhất được cho phép.\";s:13:\"date_too_late\";s:54:\"Ngày này quá ngày gần nhất được cho phép.\";s:14:\"invalid_number\";s:38:\"Định dạng số không hợp lệ.\";s:16:\"number_too_small\";s:48:\"Con số nhỏ hơn số nhỏ nhất cho phép.\";s:16:\"number_too_large\";s:48:\"Con số lớn hơn số lớn nhất cho phép.\";s:23:\"quiz_answer_not_correct\";s:30:\"Câu trả lời chưa đúng.\";s:13:\"invalid_email\";s:38:\"Địa chỉ e-mail không hợp lệ.\";s:11:\"invalid_url\";s:22:\"URL không hợp lệ.\";s:11:\"invalid_tel\";s:39:\"Số điện thoại không hợp lệ.\";}'),
(1468, 379, '_additional_settings', ''),
(1469, 379, '_locale', 'vi'),
(1470, 383, '_menu_item_type', 'taxonomy'),
(1471, 383, '_menu_item_menu_item_parent', '322'),
(1472, 383, '_menu_item_object_id', '28'),
(1473, 383, '_menu_item_object', 'product_cat'),
(1474, 383, '_menu_item_target', ''),
(1475, 383, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1476, 383, '_menu_item_xfn', ''),
(1477, 383, '_menu_item_url', ''),
(1479, 384, '_menu_item_type', 'taxonomy'),
(1480, 384, '_menu_item_menu_item_parent', '322'),
(1481, 384, '_menu_item_object_id', '29'),
(1482, 384, '_menu_item_object', 'product_cat'),
(1483, 384, '_menu_item_target', ''),
(1484, 384, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1485, 384, '_menu_item_xfn', ''),
(1486, 384, '_menu_item_url', ''),
(1488, 322, '_wp_old_date', '2021-08-25'),
(1489, 323, '_wp_old_date', '2021-08-25'),
(1490, 319, '_thumbnail_id', '330'),
(1491, 320, '_thumbnail_id', '330'),
(1492, 320, 'single_product_parameter', ''),
(1493, 320, '_single_product_parameter', 'field_612621cedc7a6'),
(1494, 326, '_thumbnail_id', '330'),
(1495, 326, 'single_product_parameter', ''),
(1496, 326, '_single_product_parameter', 'field_612621cedc7a6'),
(1497, 327, '_thumbnail_id', '330'),
(1498, 327, 'single_product_parameter', ''),
(1499, 327, '_single_product_parameter', 'field_612621cedc7a6');

-- --------------------------------------------------------

--
-- Table structure for table `wp_posts`
--

CREATE TABLE `wp_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_posts`
--

INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(12, 1, '2020-12-07 18:23:52', '2020-12-07 11:23:52', '<!-- wp:paragraph -->\r\n<p>&nbsp;</p>\r\n<!-- /wp:paragraph -->\r\n\r\n<!-- wp:paragraph -->\r\n<p>&nbsp;</p>\r\n<!-- /wp:paragraph -->\r\n\r\n<!-- wp:paragraph -->\r\n<p>&nbsp;</p>\r\n<!-- /wp:paragraph -->', 'Trang chủ', '', 'publish', 'closed', 'closed', '', 'trang-chu', '', '', '2021-08-26 17:13:24', '2021-08-26 10:13:24', '', 0, 'http://wordpress.local/GCO/maytracdiasaoviet/?page_id=12', 0, 'page', '', 0),
(13, 1, '2020-12-07 18:23:52', '2020-12-07 11:23:52', '<!-- wp:paragraph -->\n<p>ad</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->', 'Trang chủ', '', 'inherit', 'closed', 'closed', '', '12-revision-v1', '', '', '2020-12-07 18:23:52', '2020-12-07 11:23:52', '', 12, 'http://wordpress.local/GCO/maytracdiasaoviet/uncategorized/12-revision-v1.html', 0, 'revision', '', 0),
(14, 1, '2020-12-07 18:24:43', '2020-12-07 11:24:43', '<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->', 'Trang chủ', '', 'inherit', 'closed', 'closed', '', '12-revision-v1', '', '', '2020-12-07 18:24:43', '2020-12-07 11:24:43', '', 12, 'http://wordpress.local/GCO/maytracdiasaoviet/uncategorized/12-revision-v1.html', 0, 'revision', '', 0),
(29, 1, '2020-12-08 18:07:57', '2020-12-08 11:07:57', '<!-- wp:paragraph -->\r\n<p>Một Studio chuyên thiết kế những đồ dùng gia đình tại Na Uy KAOS được thành lập bởi Mette Hansen Bordal và Gineline Kalleberg đã chinh phục rất nhiều gia đình bởi những sản phẩm hiện đại, có tính ứng dụng cao và đặc biệt hướng tới các em nhỏ. Gần đây trên Kickstarter họ đã đưa ra một dự án gây quỹ cho ý tưởng kinh doanh mới nhất và gần như đã chạm được đến bước cho sản phẩm ra đời.</p>\r\n<p>Tên gọi KAOS Endeløs được các nhà thiết kế lớn của KAOS lấy cảm hứng từ một hơi thở hoài niệm xa xưa về ấu thơ của họ, với những thanh treo bằng gỗ trong nhà và rồi sự sáng tạo đã cho họ nhìn ra được tính ứng dụng cao hơn của món đồ nội thất tưởng đơn giản mà đặc biệt đa công dụng này.</p>\r\n<p>Endeløs với ý nghĩa là \"sự vô cùng\", là định nghĩa của chính sản phẩm, bởi tác dụng \"vô hạn\" mà chiếc thang gỗ này đem lại. Nó nhiều hơn một thiết bị tập thể dục, giúp các bé phát triển cơ thể cùng các chức năng vận động như leo trèo, bám, với, đu... và còn có thể làm nơi lưu trữ bài tập về nhà, treo quần áo, những món đồ thủ công do chính các bé làm ra.</p>\r\n<p>Endeløs với ý nghĩa là \"sự vô cùng\", là định nghĩa của chính sản phẩm, bởi tác dụng \"vô hạn\" mà chiếc thang gỗ này đem lại. Nó nhiều hơn một thiết bị tập thể dục, giúp các bé phát triển cơ thể cùng các chức năng vận động như leo trèo, bám, với, đu... và còn có thể làm nơi lưu trữ bài tập về nhà, treo quần áo, những món đồ thủ công do chính các bé làm ra.</p>\r\n<!-- /wp:paragraph -->', 'Tác dụng từ chiếc thang gỗ trong phòng trẻ', 'Một Studio chuyên thiết kế những đồ dùng gia đình tại Na Uy KAOS được thành lập bởi Mette Hansen Bordal và Gineline Kalleberg đã chinh phục rất nhiều gia đình bởi những sản phẩm hiện đại, có tính ứng dụng cao và đặc biệt hướng tới các em nhỏ. Gần đây trên Kickstarter họ đã đưa ra một dự án gây quỹ cho ý tưởng kinh doanh mới nhất và gần như đã chạm được đến bước cho sản phẩm ra đời.', 'publish', 'open', 'open', '', 'thang-go-trong-phong-tre', '', '', '2021-08-24 16:57:21', '2021-08-24 09:57:21', '', 0, 'http://wordpress.local/GCO/maytracdiasaoviet/?p=29', 0, 'post', '', 0),
(30, 1, '2020-12-08 18:07:57', '2020-12-08 11:07:57', '<!-- wp:paragraph -->\n<p>Nội dung bài viết 1</p>\n<!-- /wp:paragraph -->', 'Bài viết 1', '', 'inherit', 'closed', 'closed', '', '29-revision-v1', '', '', '2020-12-08 18:07:57', '2020-12-08 11:07:57', '', 29, 'http://wordpress.local/GCO/maytracdiasaoviet/uncategorized/29-revision-v1.html', 0, 'revision', '', 0),
(31, 1, '2020-12-08 18:10:46', '2020-12-08 11:10:46', '<!-- wp:paragraph -->\r\n<p>Nội dung Bài viết 2</p>\r\n<!-- /wp:paragraph -->', 'Bài viết 2', 'Tom tắt của Bài viết 2', 'publish', 'open', 'open', '', 'bai-viet-2', '', '', '2021-08-24 16:40:35', '2021-08-24 09:40:35', '', 0, 'http://wordpress.local/GCO/maytracdiasaoviet/?p=31', 0, 'post', '', 0),
(32, 1, '2020-12-08 18:10:46', '2020-12-08 11:10:46', '<!-- wp:paragraph -->\n<p>Nội dung Bài viết 2</p>\n<!-- /wp:paragraph -->', 'Bài viết 2', 'Tom tắt của Bài viết 2', 'inherit', 'closed', 'closed', '', '31-revision-v1', '', '', '2020-12-08 18:10:46', '2020-12-08 11:10:46', '', 31, 'http://wordpress.local/GCO/maytracdiasaoviet/uncategorized/31-revision-v1.html', 0, 'revision', '', 0),
(33, 1, '2020-12-08 18:12:04', '2020-12-08 11:12:04', '<!-- wp:paragraph -->\n<p>Nội dung Bài viết 1</p>\n<!-- /wp:paragraph -->', 'Bài viết 1', 'Tóm tắt của Bài viết 1', 'inherit', 'closed', 'closed', '', '29-revision-v1', '', '', '2020-12-08 18:12:04', '2020-12-08 11:12:04', '', 29, 'http://wordpress.local/GCO/maytracdiasaoviet/uncategorized/29-revision-v1.html', 0, 'revision', '', 0),
(36, 1, '2020-12-08 19:05:11', '2020-12-08 12:05:11', '', 'Liên hệ', '', 'publish', 'closed', 'closed', '', 'lien-he', '', '', '2021-08-24 14:42:09', '2021-08-24 07:42:09', '', 0, 'http://wordpress.local/GCO/maytracdiasaoviet/?page_id=36', 0, 'page', '', 0),
(37, 1, '2020-12-08 19:05:11', '2020-12-08 12:05:11', '', 'Trang liên hệ', '', 'inherit', 'closed', 'closed', '', '36-revision-v1', '', '', '2020-12-08 19:05:11', '2020-12-08 12:05:11', '', 36, 'http://wordpress.local/GCO/maytracdiasaoviet/chuyen-muc-3/36-revision-v1.html', 0, 'revision', '', 0),
(41, 1, '2020-12-08 20:25:12', '2020-12-08 13:25:12', '[text* your-name class:form-control placeholder \"Họ tên\"]\r\n    [email* your-email class:form-control placeholder \"Email\"]\r\n    [textarea* your-message class:form-control placeholder \"Nội dung\"]\r\n[submit class:btn class:w-100 class:bold class:buy-btn \"Gửi\"]\n1\n[_site_title]\n[_site_title] <wordpress@wordpress.local>\n[_site_admin_email]\nGửi đến từ: [your-name] <[your-email]>\r\n\r\nNội dung thông điệp:\r\n[your-message]\r\n\r\n-- \r\nEmail này được gửi đến từ website [_site_title] ([_site_url])\n\n\n\n\n\n[_site_title] \"[your-subject]\"\n[_site_title] <wordpress@corewordpress.local>\n[your-email]\nNội dung thông điệp:\r\n[your-message]\r\n\r\n-- \r\nEmail này được gửi đến từ form liên hệ của website [_site_title] ([_site_url])\nReply-To: [_site_admin_email]\n\n\n\nXin cảm ơn, form đã được gửi thành công.\nCó lỗi xảy ra trong quá trình gửi. Xin vui lòng thử lại hoặc liên hệ người quản trị website.\nCó một hoặc nhiều mục nhập có lỗi. Vui lòng kiểm tra và thử lại.\nCó lỗi xảy ra trong quá trình gửi. Xin vui lòng thử lại hoặc liên hệ người quản trị website.\nBạn phải chấp nhận điều khoản trước khi gửi form.\nMục này là bắt buộc.\nNhập quá số kí tự cho phép.\nNhập ít hơn số kí tự tối thiểu.\nTải file lên không thành công.\nBạn không được phép tải lên file theo định dạng này.\nFile kích thước quá lớn.\nTải file lên không thành công.\nĐịnh dạng ngày tháng không hợp lệ.\nNgày này trước ngày sớm nhất được cho phép.\nNgày này quá ngày gần nhất được cho phép.\nĐịnh dạng số không hợp lệ.\nCon số nhỏ hơn số nhỏ nhất cho phép.\nCon số lớn hơn số lớn nhất cho phép.\nCâu trả lời chưa đúng.\nĐịa chỉ e-mail không hợp lệ.\nURL không hợp lệ.\nSố điện thoại không hợp lệ.', 'Liên hệ', '', 'publish', 'closed', 'closed', '', 'form-lien-he-1', '', '', '2021-08-24 14:39:44', '2021-08-24 07:39:44', '', 0, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=wpcf7_contact_form&#038;p=41', 0, 'wpcf7_contact_form', '', 0),
(48, 1, '2020-12-20 01:57:10', '2020-12-19 18:57:10', '<!-- wp:paragraph -->\n<p>Nội dung Bài viết 2</p>\n<!-- /wp:paragraph -->', 'Bài viết 2', 'Tom tắt của Bài viết 2', 'inherit', 'closed', 'closed', '', '31-revision-v1', '', '', '2020-12-20 01:57:10', '2020-12-19 18:57:10', '', 31, 'http://wordpress.local/GCO/maytracdiasaoviet/chuyen-muc-3/31-revision-v1.html', 0, 'revision', '', 0),
(54, 1, '2021-01-22 17:38:25', '2021-01-22 10:38:25', '', 'Dự án một', '', 'publish', 'open', 'closed', '', 'du-an-mot', '', '', '2021-01-22 17:38:25', '2021-01-22 10:38:25', '', 0, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=duan&#038;p=54', 0, 'duan', '', 0),
(55, 1, '2021-08-25 16:31:22', '2021-01-22 10:38:42', ' ', '', '', 'publish', 'closed', 'closed', '', '55', '', '', '2021-08-25 16:31:22', '2021-08-25 09:31:22', '', 0, 'http://wordpress.local/GCO/maytracdiasaoviet/?p=55', 1, 'nav_menu_item', '', 0),
(57, 1, '2021-08-25 16:31:22', '2021-01-22 10:38:42', ' ', '', '', 'publish', 'closed', 'closed', '', '57', '', '', '2021-08-25 16:31:22', '2021-08-25 09:31:22', '', 0, 'http://wordpress.local/GCO/maytracdiasaoviet/?p=57', 7, 'nav_menu_item', '', 0),
(62, 1, '2021-02-25 15:51:56', '2021-02-25 08:51:56', '', 'Dịch vụ 1', '', 'publish', 'open', 'closed', '', 'dich-vu-1', '', '', '2021-02-25 15:51:56', '2021-02-25 08:51:56', '', 0, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=services&#038;p=62', 0, 'services', '', 0),
(64, 1, '2021-03-18 16:30:23', '2021-03-18 09:30:23', '', 'Bài viết 3', '', 'publish', 'open', 'open', '', 'bai-viet-3', '', '', '2021-08-24 16:40:43', '2021-08-24 09:40:43', '', 0, 'http://wordpress.local/GCO/maytracdiasaoviet/?p=64', 0, 'post', '', 0),
(65, 1, '2021-03-18 16:30:23', '2021-03-18 09:30:23', '', 'Bài viết 2', '', 'inherit', 'closed', 'closed', '', '64-revision-v1', '', '', '2021-03-18 16:30:23', '2021-03-18 09:30:23', '', 64, 'http://wordpress.local/GCO/maytracdiasaoviet/chuyen-muc-3/64-revision-v1.html', 0, 'revision', '', 0),
(66, 1, '2021-03-18 16:32:31', '2021-03-18 09:32:31', '', 'Bài viết 3', '', 'inherit', 'closed', 'closed', '', '64-revision-v1', '', '', '2021-03-18 16:32:31', '2021-03-18 09:32:31', '', 64, 'http://wordpress.local/GCO/maytracdiasaoviet/chuyen-muc-3/64-revision-v1.html', 0, 'revision', '', 0),
(69, 1, '2021-05-06 11:55:20', '2021-05-06 04:55:20', '', 'Liên hệ', '', 'inherit', 'closed', 'closed', '', '36-revision-v1', '', '', '2021-05-06 11:55:20', '2021-05-06 04:55:20', '', 36, 'http://wordpress.local/GCO/maytracdiasaoviet/chuyen-muc-3/36-revision-v1.html', 0, 'revision', '', 0),
(70, 1, '2021-05-06 12:00:05', '2021-05-06 05:00:05', '[email* your-email class:form-control placeholder \"Email\"]\r\n[submit class:btn class:bold class:regis-btn \"Gửi\"]\n1\n[_site_title]\n[_site_title] <wordpress@wordpress.local>\n[_site_admin_email]\n<[your-email]> Muốn đăng ký nhận tin\r\n\r\n\r\n-- \r\nEmail này được gửi đến từ website [_site_title] ([_site_url])\n\n\n\n\n\n[_site_title] \"[your-subject]\"\n[_site_title] <wordpress@corewordpress.local>\n[your-email]\nNội dung thông điệp:\r\n[your-message]\r\n\r\n-- \r\nEmail này được gửi đến từ form liên hệ của website [_site_title] ([_site_url])\nReply-To: [_site_admin_email]\n\n\n\nXin cảm ơn, form đã được gửi thành công.\nCó lỗi xảy ra trong quá trình gửi. Xin vui lòng thử lại hoặc liên hệ người quản trị website.\nCó một hoặc nhiều mục nhập có lỗi. Vui lòng kiểm tra và thử lại.\nCó lỗi xảy ra trong quá trình gửi. Xin vui lòng thử lại hoặc liên hệ người quản trị website.\nBạn phải chấp nhận điều khoản trước khi gửi form.\nMục này là bắt buộc.\nNhập quá số kí tự cho phép.\nNhập ít hơn số kí tự tối thiểu.\nTải file lên không thành công.\nBạn không được phép tải lên file theo định dạng này.\nFile kích thước quá lớn.\nTải file lên không thành công.\nĐịnh dạng ngày tháng không hợp lệ.\nNgày này trước ngày sớm nhất được cho phép.\nNgày này quá ngày gần nhất được cho phép.\nĐịnh dạng số không hợp lệ.\nCon số nhỏ hơn số nhỏ nhất cho phép.\nCon số lớn hơn số lớn nhất cho phép.\nCâu trả lời chưa đúng.\nĐịa chỉ e-mail không hợp lệ.\nURL không hợp lệ.\nSố điện thoại không hợp lệ.', 'Đăng ký nhận tin', '', 'publish', 'closed', 'closed', '', 'ho-tro-truc-tuyen', '', '', '2021-08-23 18:46:45', '2021-08-23 11:46:45', '', 0, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=wpcf7_contact_form&#038;p=70', 0, 'wpcf7_contact_form', '', 0),
(87, 1, '2021-05-06 12:03:47', '2021-05-06 05:03:47', 'a:6:{s:4:\"type\";s:4:\"link\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";}', 'Link', 'link', 'publish', 'closed', 'closed', '', 'field_6066a2acf1756', '', '', '2021-05-06 12:03:47', '2021-05-06 05:03:47', '', 73, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&p=87', 0, 'acf-field', '', 0),
(100, 1, '2021-05-06 12:03:47', '2021-05-06 05:03:47', 'a:6:{s:4:\"type\";s:4:\"link\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";}', 'Link', 'link', 'publish', 'closed', 'closed', '', 'field_6066a2cff1757', '', '', '2021-05-06 12:03:47', '2021-05-06 05:03:47', '', 73, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&p=100', 0, 'acf-field', '', 0),
(101, 1, '2021-05-06 12:03:47', '2021-05-06 05:03:47', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:12:\"options_page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:14:\"theme-settings\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Thông tin khách hàng - Header - Footer - Socical,SMTP', 'thong-tin-khach-hang-header-footer-socicalsmtp', 'publish', 'closed', 'closed', '', 'group_607565705e6d1', '', '', '2021-09-06 18:03:59', '2021-09-06 11:03:59', '', 0, 'http://wordpress.local/GCO/maytracdiasaoviet/?p=101', 0, 'acf-field-group', '', 0),
(110, 1, '2021-05-06 12:03:47', '2021-05-06 05:03:47', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"30\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:4:\"full\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Logo Header', 'h_logo', 'publish', 'closed', 'closed', '', 'field_607565820f90c', '', '', '2021-08-23 17:49:11', '2021-08-23 10:49:11', '', 101, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&#038;p=110', 1, 'acf-field', '', 0),
(125, 1, '2021-05-06 12:03:47', '2021-05-06 05:03:47', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:13:\"page_template\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:20:\"template-contact.php\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Nhóm trường Trang Liên hệ', 'nhom-truong-trang-lien-he', 'publish', 'closed', 'closed', '', 'group_6066c67bd4dac', '', '', '2021-08-24 14:31:45', '2021-08-24 07:31:45', '', 0, 'http://wordpress.local/GCO/maytracdiasaoviet/?p=125', 0, 'acf-field-group', '', 0),
(126, 1, '2021-05-06 12:03:47', '2021-05-06 05:03:47', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'Thông tin liên hệ', '', 'publish', 'closed', 'closed', '', 'field_6066c727823cc', '', '', '2021-05-06 12:03:47', '2021-05-06 05:03:47', '', 125, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&p=126', 0, 'acf-field', '', 0),
(127, 1, '2021-05-06 12:03:47', '2021-05-06 05:03:47', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Tiêu đề', 'contact_info_title', 'publish', 'closed', 'closed', '', 'field_6066c748823cd', '', '', '2021-05-06 12:03:47', '2021-05-06 05:03:47', '', 125, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&p=127', 1, 'acf-field', '', 0),
(133, 1, '2021-05-06 12:03:47', '2021-05-06 05:03:47', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'Form liên hệ', '', 'publish', 'closed', 'closed', '', 'field_6066c698823c6', '', '', '2021-08-24 14:31:45', '2021-08-24 07:31:45', '', 125, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&#038;p=133', 2, 'acf-field', '', 0),
(134, 1, '2021-05-06 12:03:47', '2021-05-06 05:03:47', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Tiêu đề', 'contact_contact_title', 'publish', 'closed', 'closed', '', 'field_6066c6a4823c7', '', '', '2021-08-24 14:31:45', '2021-08-24 07:31:45', '', 125, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&#038;p=134', 3, 'acf-field', '', 0),
(135, 1, '2021-05-06 12:03:47', '2021-05-06 05:03:47', 'a:11:{s:4:\"type\";s:11:\"post_object\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"post_type\";a:1:{i:0;s:18:\"wpcf7_contact_form\";}s:8:\"taxonomy\";s:0:\"\";s:10:\"allow_null\";i:0;s:8:\"multiple\";i:0;s:13:\"return_format\";s:2:\"id\";s:2:\"ui\";i:1;}', 'Form', 'contact_contact_form', 'publish', 'closed', 'closed', '', 'field_60769726c0e5d', '', '', '2021-08-24 14:31:45', '2021-08-24 07:31:45', '', 125, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&#038;p=135', 4, 'acf-field', '', 0),
(136, 1, '2021-05-06 12:03:47', '2021-05-06 05:03:47', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'Bản đồ', '', 'publish', 'closed', 'closed', '', 'field_6066c82f823d4', '', '', '2021-08-24 14:31:45', '2021-08-24 07:31:45', '', 125, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&#038;p=136', 5, 'acf-field', '', 0),
(137, 1, '2021-05-06 12:03:47', '2021-05-06 05:03:47', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";i:6;s:9:\"new_lines\";s:0:\"\";}', 'Bản đồ', 'contact_map', 'publish', 'closed', 'closed', '', 'field_6066c834823d5', '', '', '2021-08-24 14:31:45', '2021-08-24 07:31:45', '', 125, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&#038;p=137', 6, 'acf-field', '', 0),
(138, 1, '2021-05-06 12:03:56', '2021-05-06 05:03:56', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"page_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:10:\"front_page\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Nhóm trường Trang Trang chủ', 'nhom-truong-trang-trang-chu', 'publish', 'closed', 'closed', '', 'group_606be121de9cc', '', '', '2021-08-26 17:36:03', '2021-08-26 10:36:03', '', 0, 'http://wordpress.local/GCO/maytracdiasaoviet/?p=138', 0, 'acf-field-group', '', 0),
(139, 1, '2021-05-06 12:03:56', '2021-05-06 05:03:56', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'Slide', '', 'publish', 'closed', 'closed', '', 'field_606be121e5e46', '', '', '2021-05-06 12:03:56', '2021-05-06 05:03:56', '', 138, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&p=139', 0, 'acf-field', '', 0),
(140, 1, '2021-05-06 12:03:56', '2021-05-06 05:03:56', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:0:\"\";}', 'Nội dung', 'home_slide', 'publish', 'closed', 'closed', '', 'field_606be121e5e58', '', '', '2021-08-26 16:29:18', '2021-08-26 09:29:18', '', 138, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&#038;p=140', 1, 'acf-field', '', 0),
(142, 1, '2021-05-06 12:03:56', '2021-05-06 05:03:56', 'a:7:{s:4:\"type\";s:3:\"url\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:8:\"https://\";}', 'Đường dẫn', 'url', 'publish', 'closed', 'closed', '', 'field_606be15c2caee', '', '', '2021-07-28 12:02:33', '2021-07-28 05:02:33', '', 140, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&#038;p=142', 1, 'acf-field', '', 0),
(143, 1, '2021-05-06 12:03:56', '2021-05-06 05:03:56', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'Box quảng cáo một', 'về_chung_toi', 'publish', 'closed', 'closed', '', 'field_606be121e5e65', '', '', '2021-08-26 16:37:51', '2021-08-26 09:37:51', '', 138, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&#038;p=143', 4, 'acf-field', '', 0),
(144, 1, '2021-05-06 12:03:56', '2021-05-06 05:03:56', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"20\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:4:\"full\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Ảnh 1', 'image_ads_one', 'publish', 'closed', 'closed', '', 'field_606be121e5e71', '', '', '2021-08-26 16:37:51', '2021-08-26 09:37:51', '', 138, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&#038;p=144', 5, 'acf-field', '', 0),
(186, 1, '2021-06-17 10:03:26', '2021-06-17 03:03:26', '', 'Liên hệ', '', 'inherit', 'closed', 'closed', '', '36-revision-v1', '', '', '2021-06-17 10:03:26', '2021-06-17 03:03:26', '', 36, 'http://wordpress.local/GCO/maytracdiasaoviet/chuyen-muc-3/36-revision-v1.html', 0, 'revision', '', 0),
(192, 1, '2021-06-28 09:20:53', '2021-06-28 02:20:53', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'Footer', 'footer', 'publish', 'closed', 'closed', '', 'field_60d92f48334e1', '', '', '2021-09-06 18:03:36', '2021-09-06 11:03:36', '', 101, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&#038;p=192', 11, 'acf-field', '', 0),
(193, 1, '2021-06-28 09:20:53', '2021-06-28 02:20:53', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'Socical,SMTP', 'socical-smtp', 'publish', 'closed', 'closed', '', 'field_60d92fb5334e2', '', '', '2021-09-06 18:03:36', '2021-09-06 11:03:36', '', 101, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&#038;p=193', 21, 'acf-field', '', 0),
(194, 1, '2021-06-28 09:20:53', '2021-06-28 02:20:53', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"30\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:4:\"full\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Favicon', 'favicon', 'publish', 'closed', 'closed', '', 'field_60d93090334e7', '', '', '2021-08-23 17:49:11', '2021-08-23 10:49:11', '', 101, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&#038;p=194', 2, 'acf-field', '', 0),
(195, 1, '2021-06-28 09:20:53', '2021-06-28 02:20:53', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:20:\"Số điện thoại\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Socical Phone', 'socical_phone', 'publish', 'closed', 'closed', '', 'field_60d930e4334e8', '', '', '2021-09-06 18:03:36', '2021-09-06 11:03:36', '', 101, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&#038;p=195', 22, 'acf-field', '', 0),
(196, 1, '2021-06-28 09:20:53', '2021-06-28 02:20:53', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:20:\"Số điện thoại\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Socical Zalo', 'socical_zalo', 'publish', 'closed', 'closed', '', 'field_60d9310f334e9', '', '', '2021-09-06 18:03:36', '2021-09-06 11:03:36', '', 101, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&#038;p=196', 23, 'acf-field', '', 0),
(197, 1, '2021-06-28 09:20:53', '2021-06-28 02:20:53', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:40:\"Link chat fanpage : m.me/103616215275569\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Socical Messenger', 'socical_messenger', 'publish', 'closed', 'closed', '', 'field_60d93125334ea', '', '', '2021-09-06 18:03:37', '2021-09-06 11:03:37', '', 101, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&#038;p=197', 24, 'acf-field', '', 0),
(198, 1, '2021-06-28 09:20:53', '2021-06-28 02:20:53', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:17:\"SDK chat facebook\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";i:6;s:9:\"new_lines\";s:0:\"\";}', 'Socical Chat Facebook', 'socical_chat_fb', 'publish', 'closed', 'closed', '', 'field_60d931d0334eb', '', '', '2021-09-06 18:03:37', '2021-09-06 11:03:37', '', 101, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&#038;p=198', 25, 'acf-field', '', 0),
(199, 1, '2021-06-28 09:20:53', '2021-06-28 02:20:53', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'Thông tin khách hàng', 'thong_tin_khach_hang', 'publish', 'closed', 'closed', '', 'field_60d92fbc334e3', '', '', '2021-06-28 10:10:57', '2021-06-28 03:10:57', '', 101, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&#038;p=199', 0, 'acf-field', '', 0),
(200, 1, '2021-06-28 09:20:53', '2021-06-28 02:20:53', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Địa chỉ', 'customer_address', 'publish', 'closed', 'closed', '', 'field_60d92fd7334e4', '', '', '2021-09-06 17:58:33', '2021-09-06 10:58:33', '', 101, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&#038;p=200', 5, 'acf-field', '', 0),
(201, 1, '2021-06-28 09:20:53', '2021-06-28 02:20:53', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Số điện thoại', 'customer_phone', 'publish', 'closed', 'closed', '', 'field_60d92feb334e6', '', '', '2021-09-06 17:58:33', '2021-09-06 10:58:33', '', 101, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&#038;p=201', 6, 'acf-field', '', 0),
(202, 1, '2021-06-28 09:20:53', '2021-06-28 02:20:53', 'a:9:{s:4:\"type\";s:5:\"email\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";}', 'Email', 'customer_email', 'publish', 'closed', 'closed', '', 'field_60d92fde334e5', '', '', '2021-09-06 17:58:33', '2021-09-06 10:58:33', '', 101, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&#038;p=202', 7, 'acf-field', '', 0),
(204, 1, '2021-06-28 09:26:31', '2021-06-28 02:26:31', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'SMTP Encryption', 'smtp_encryption', 'publish', 'closed', 'closed', '', 'field_60d93285153c6', '', '', '2021-09-06 18:03:37', '2021-09-06 11:03:37', '', 101, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&#038;p=204', 27, 'acf-field', '', 0),
(205, 1, '2021-06-28 09:26:31', '2021-06-28 02:26:31', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'SMTP Port', 'smtp_port', 'publish', 'closed', 'closed', '', 'field_60d93294153c7', '', '', '2021-09-06 18:03:37', '2021-09-06 11:03:37', '', 101, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&#038;p=205', 28, 'acf-field', '', 0),
(207, 1, '2021-06-28 09:26:31', '2021-06-28 02:26:31', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'SMTP Username', 'smtp_user', 'publish', 'closed', 'closed', '', 'field_60d932b0153c9', '', '', '2021-09-06 18:03:37', '2021-09-06 11:03:37', '', 101, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&#038;p=207', 29, 'acf-field', '', 0),
(208, 1, '2021-06-28 09:26:31', '2021-06-28 02:26:31', 'a:8:{s:4:\"type\";s:8:\"password\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";}', 'SMTP Password', 'smtp_pass', 'publish', 'closed', 'closed', '', 'field_60d932bb153ca', '', '', '2021-09-06 18:03:37', '2021-09-06 11:03:37', '', 101, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&#038;p=208', 30, 'acf-field', '', 0),
(209, 1, '2021-06-28 09:44:05', '2021-06-28 02:44:05', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Tên công ty', 'customer_company_name', 'publish', 'closed', 'closed', '', 'field_60d93758144a5', '', '', '2021-08-23 17:48:34', '2021-08-23 10:48:34', '', 101, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&#038;p=209', 3, 'acf-field', '', 0),
(212, 1, '2021-06-28 09:51:38', '2021-06-28 02:51:38', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:0:\"\";}', 'Dịch vụ', 'f_service', 'publish', 'closed', 'closed', '', 'field_60d937f721039', '', '', '2021-09-06 18:03:36', '2021-09-06 11:03:36', '', 101, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&#038;p=212', 12, 'acf-field', '', 0),
(213, 1, '2021-06-28 09:51:39', '2021-06-28 02:51:39', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:4:\"full\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Ảnh', 'image', 'publish', 'closed', 'closed', '', 'field_60d938102103a', '', '', '2021-08-23 18:10:07', '2021-08-23 11:10:07', '', 212, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&#038;p=213', 0, 'acf-field', '', 0),
(214, 1, '2021-06-28 09:51:39', '2021-06-28 02:51:39', 'a:7:{s:4:\"type\";s:3:\"url\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:8:\"https://\";}', 'MXH Facebook', 'f_socical_facebook', 'publish', 'closed', 'closed', '', 'field_60d938652103b', '', '', '2021-09-06 18:03:36', '2021-09-06 11:03:36', '', 101, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&#038;p=214', 16, 'acf-field', '', 0),
(215, 1, '2021-06-28 09:51:39', '2021-06-28 02:51:39', 'a:7:{s:4:\"type\";s:3:\"url\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:8:\"https://\";}', 'MXH Youtube', 'f_socical_youtube', 'publish', 'closed', 'closed', '', 'field_60d938822103d', '', '', '2021-09-06 18:03:36', '2021-09-06 11:03:36', '', 101, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&#038;p=215', 19, 'acf-field', '', 0),
(216, 1, '2021-06-28 09:51:39', '2021-06-28 02:51:39', 'a:7:{s:4:\"type\";s:3:\"url\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:8:\"https://\";}', 'MXH Instagram', 'f_socical_insta', 'publish', 'closed', 'closed', '', 'field_60d9388d2103e', '', '', '2021-09-06 18:03:36', '2021-09-06 11:03:36', '', 101, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&#038;p=216', 17, 'acf-field', '', 0),
(217, 1, '2021-06-28 09:51:39', '2021-06-28 02:51:39', 'a:7:{s:4:\"type\";s:3:\"url\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:8:\"https://\";}', 'MXH Twitter', 'f_socical_twitter', 'publish', 'closed', 'closed', '', 'field_60d938972103f', '', '', '2021-09-06 18:03:36', '2021-09-06 11:03:36', '', 101, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&#038;p=217', 18, 'acf-field', '', 0),
(218, 1, '2021-06-28 09:51:39', '2021-06-28 02:51:39', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Copyright', 'f_bottom_copyright', 'publish', 'closed', 'closed', '', 'field_60d938d721040', '', '', '2021-09-06 18:03:36', '2021-09-06 11:03:36', '', 101, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&#038;p=218', 20, 'acf-field', '', 0),
(221, 1, '2021-06-29 16:23:45', '2021-06-29 09:23:45', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Tiêu đề form', 'f_form_title', 'publish', 'closed', 'closed', '', 'field_60dae5e430dd0', '', '', '2021-09-06 18:03:36', '2021-09-06 11:03:36', '', 101, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&#038;p=221', 13, 'acf-field', '', 0),
(222, 1, '2021-06-29 16:23:45', '2021-06-29 09:23:45', 'a:11:{s:4:\"type\";s:11:\"post_object\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"post_type\";a:1:{i:0;s:18:\"wpcf7_contact_form\";}s:8:\"taxonomy\";s:0:\"\";s:10:\"allow_null\";i:0;s:8:\"multiple\";i:0;s:13:\"return_format\";s:2:\"id\";s:2:\"ui\";i:1;}', 'Chọn Form', 'f_form', 'publish', 'closed', 'closed', '', 'field_60dae60930dd1', '', '', '2021-09-06 18:03:36', '2021-09-06 11:03:36', '', 101, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&#038;p=222', 14, 'acf-field', '', 0),
(229, 1, '2021-07-05 11:37:36', '2021-07-05 04:37:36', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'Danh mục sản phẩm', '_copy', 'publish', 'closed', 'closed', '', 'field_60e28c3994cd1', '', '', '2021-08-26 16:37:51', '2021-08-26 09:37:51', '', 138, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&#038;p=229', 9, 'acf-field', '', 0),
(231, 1, '2021-07-05 11:37:36', '2021-07-05 04:37:36', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:0:\"\";}', 'Chọn danh mục', 'home_product_select_cat', 'publish', 'closed', 'closed', '', 'field_60e28c5994cd3', '', '', '2021-08-26 16:37:51', '2021-08-26 09:37:51', '', 138, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&#038;p=231', 10, 'acf-field', '', 0),
(234, 1, '2021-07-13 11:25:37', '2021-07-13 04:25:37', 'Công ty Cổ phần Công nghệ và Truyền thông GCO – GCO Group (tiền thân là Công ty Cổ phần Công nghệ Helios Việt Nam) hoạt động trong các lĩnh vực: Software, Digital Marketing, E-Learning, Online Media và Brand Creative. GCO Group đã trải qua 6 năm trên hành trình xây dựng và phát triển, đối mặt và vượt qua rất nhiều khó khăn từ những ngày đầu thành lập. Khó khăn cũng là cơ hội để GCO Group tích lũy cho mình kinh nghiệm và bản lĩnh. Hơn 150 thành viên GCO Group luôn nỗ lực không ngừng hướng đến mục tiêu trở thành công ty Công nghệ và Truyền thông hàng đầu cả nước, vươn ra tầm khu vực\r\n\r\n<span style=\"color: #ff0000;\">Chúng tôi mang đến điều gì cho bạn?</span>\r\n\r\nHành trình 6 năm phát triển không phải là quá dài nhưng đủ để cho GCO Group tạo dựng cho mình nền tảng nhất định trong lĩnh vực hoạt động. Với đội ngũ hơn 150 nhân sự trẻ, năng động, nhiệt huyết, chuyên nghiệp, GCO Group tự hào trở thành người bạn đồng hành tin cậy của hơn 1000 đối tác với hơn 500 khách hàng thường xuyên. Dịch vụ chuyên nghiệp, nhân sự tận tâm, quy trình làm việc bài bản, hỗ trợ thường xuyên, GCO Group tự tin mang đến những giải pháp hiệu quả phục vụ lợi ích khách hàng.', 'Giới thiệu', '', 'publish', 'closed', 'closed', '', 'gioi-thieu', '', '', '2021-08-24 15:37:14', '2021-08-24 08:37:14', '', 0, 'http://wordpress.local/GCO/maytracdiasaoviet/?page_id=234', 0, 'page', '', 0),
(235, 1, '2021-07-13 11:25:37', '2021-07-13 04:25:37', '', 'Giới thiệu', '', 'inherit', 'closed', 'closed', '', '234-revision-v1', '', '', '2021-07-13 11:25:37', '2021-07-13 04:25:37', '', 234, 'http://wordpress.local/GCO/maytracdiasaoviet/?p=235', 0, 'revision', '', 0),
(245, 1, '2021-07-13 11:26:02', '2021-07-13 04:26:02', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"20\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Năm', 'year', 'publish', 'closed', 'closed', '', 'field_60e275f90b60f', '', '', '2021-07-13 11:26:02', '2021-07-13 04:26:02', '', 236, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&p=245', 0, 'acf-field', '', 0),
(246, 1, '2021-07-13 11:26:02', '2021-07-13 04:26:02', 'a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:6:\"visual\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'Nội dung phát triển', 'year_content', 'publish', 'closed', 'closed', '', 'field_60e276170b610', '', '', '2021-07-13 11:26:02', '2021-07-13 04:26:02', '', 236, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&p=246', 1, 'acf-field', '', 0),
(249, 1, '2021-07-13 11:26:02', '2021-07-13 04:26:02', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"20\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Tiêu đề', 'title', 'publish', 'closed', 'closed', '', 'field_60e276cfd63cb', '', '', '2021-07-13 11:26:02', '2021-07-13 04:26:02', '', 236, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&p=249', 0, 'acf-field', '', 0),
(250, 1, '2021-07-13 11:26:02', '2021-07-13 04:26:02', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";i:6;s:9:\"new_lines\";s:0:\"\";}', 'Mô tả', 'desc', 'publish', 'closed', 'closed', '', 'field_60e276dad63cc', '', '', '2021-07-13 11:26:02', '2021-07-13 04:26:02', '', 236, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&p=250', 1, 'acf-field', '', 0),
(256, 1, '2021-08-25 16:31:22', '2021-07-16 07:44:01', ' ', '', '', 'publish', 'closed', 'closed', '', '256', '', '', '2021-08-25 16:31:22', '2021-08-25 09:31:22', '', 0, 'http://wordpress.local/GCO/maytracdiasaoviet/?p=256', 2, 'nav_menu_item', '', 0),
(259, 1, '2021-07-19 16:44:20', '2021-07-19 09:44:20', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"20\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:4:\"full\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Ảnh', 'image', 'publish', 'closed', 'closed', '', 'field_60f54961f67f9', '', '', '2021-08-26 16:41:01', '2021-08-26 09:41:01', '', 140, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&#038;p=259', 0, 'acf-field', '', 0),
(261, 1, '2021-07-20 11:30:55', '2021-07-20 04:30:55', 'a:10:{s:4:\"type\";s:10:\"true_false\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:7:\"message\";s:20:\"Có hiện nút ko ?\";s:13:\"default_value\";i:0;s:2:\"ui\";i:0;s:10:\"ui_on_text\";s:0:\"\";s:11:\"ui_off_text\";s:0:\"\";}', 'Socical Back to top', 'socical_back_to_top', 'publish', 'closed', 'closed', '', 'field_60f6516b68cf3', '', '', '2021-09-06 18:03:37', '2021-09-06 11:03:37', '', 101, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&#038;p=261', 26, 'acf-field', '', 0),
(263, 1, '2021-08-03 10:52:46', '2021-08-03 03:52:46', '', 'Liên hệ', '', 'inherit', 'closed', 'closed', '', '36-revision-v1', '', '', '2021-08-03 10:52:46', '2021-08-03 03:52:46', '', 36, 'http://wordpress.local/GCO/maytracdiasaoviet/?p=263', 0, 'revision', '', 0),
(264, 1, '2021-08-09 14:40:58', '2021-08-09 07:40:58', '', 'Tin tức', '', 'publish', 'closed', 'closed', '', 'tin-tuc', '', '', '2021-08-09 14:40:58', '2021-08-09 07:40:58', '', 0, 'http://wordpress.local/GCO/maytracdiasaoviet/?page_id=264', 0, 'page', '', 0),
(265, 1, '2021-08-09 14:40:58', '2021-08-09 07:40:58', '', 'Tin tức', '', 'inherit', 'closed', 'closed', '', '264-revision-v1', '', '', '2021-08-09 14:40:58', '2021-08-09 07:40:58', '', 264, 'http://wordpress.local/GCO/maytracdiasaoviet/?p=265', 0, 'revision', '', 0),
(266, 1, '2021-08-25 16:31:22', '2021-08-09 07:41:17', ' ', '', '', 'publish', 'closed', 'closed', '', '266', '', '', '2021-08-25 16:31:22', '2021-08-25 09:31:22', '', 0, 'http://wordpress.local/GCO/maytracdiasaoviet/?p=266', 3, 'nav_menu_item', '', 0),
(270, 1, '2021-08-23 17:54:50', '2021-08-23 10:54:50', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:0:\"\";}', 'Chi nhánh', 'customer_address_children', 'publish', 'closed', 'closed', '', 'field_61237e30b9826', '', '', '2021-09-06 17:58:33', '2021-09-06 10:58:33', '', 101, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&#038;p=270', 8, 'acf-field', '', 0),
(271, 1, '2021-08-23 17:54:50', '2021-08-23 10:54:50', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Tiêu đề', 'title', 'publish', 'closed', 'closed', '', 'field_61237e30b9827', '', '', '2021-08-23 17:54:50', '2021-08-23 10:54:50', '', 270, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&p=271', 0, 'acf-field', '', 0);
INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(272, 1, '2021-08-23 17:54:50', '2021-08-23 10:54:50', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";i:6;s:9:\"new_lines\";s:0:\"\";}', 'Mô tả', 'desc', 'publish', 'closed', 'closed', '', 'field_61237e68b9828', '', '', '2021-08-23 17:54:50', '2021-08-23 10:54:50', '', 270, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&p=272', 1, 'acf-field', '', 0),
(273, 1, '2021-08-23 18:10:07', '2021-08-23 11:10:07', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Tiêu đề', 'title', 'publish', 'closed', 'closed', '', 'field_612381be3fc8f', '', '', '2021-08-23 18:10:07', '2021-08-23 11:10:07', '', 212, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&p=273', 1, 'acf-field', '', 0),
(274, 1, '2021-08-23 18:10:07', '2021-08-23 11:10:07', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Mô tả', 'desc', 'publish', 'closed', 'closed', '', 'field_612381c73fc90', '', '', '2021-08-23 18:10:07', '2021-08-23 11:10:07', '', 212, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&p=274', 2, 'acf-field', '', 0),
(275, 1, '2021-08-23 18:11:24', '2021-08-23 11:11:24', '', '36', '', 'inherit', 'open', 'closed', '', '36', '', '', '2021-08-23 18:11:24', '2021-08-23 11:11:24', '', 0, 'http://wordpress.local/GCO/maytracdiasaoviet/wp-content/uploads/2021/08/36.png', 0, 'attachment', 'image/png', 0),
(276, 1, '2021-08-23 18:11:24', '2021-08-23 11:11:24', '', '37', '', 'inherit', 'open', 'closed', '', '37', '', '', '2021-08-23 18:11:24', '2021-08-23 11:11:24', '', 0, 'http://wordpress.local/GCO/maytracdiasaoviet/wp-content/uploads/2021/08/37.png', 0, 'attachment', 'image/png', 0),
(277, 1, '2021-08-23 18:11:25', '2021-08-23 11:11:25', '', '38', '', 'inherit', 'open', 'closed', '', '38', '', '', '2021-08-23 18:11:25', '2021-08-23 11:11:25', '', 0, 'http://wordpress.local/GCO/maytracdiasaoviet/wp-content/uploads/2021/08/38.png', 0, 'attachment', 'image/png', 0),
(281, 1, '2021-08-23 19:03:14', '2021-08-23 12:03:14', ' ', '', '', 'publish', 'closed', 'closed', '', '281', '', '', '2021-08-23 19:03:14', '2021-08-23 12:03:14', '', 0, 'http://wordpress.local/GCO/maytracdiasaoviet/?p=281', 1, 'nav_menu_item', '', 0),
(282, 1, '2021-08-23 19:03:14', '2021-08-23 12:03:14', ' ', '', '', 'publish', 'closed', 'closed', '', '282', '', '', '2021-08-23 19:03:14', '2021-08-23 12:03:14', '', 0, 'http://wordpress.local/GCO/maytracdiasaoviet/?p=282', 2, 'nav_menu_item', '', 0),
(283, 1, '2021-08-23 19:03:47', '2021-08-23 12:03:47', ' ', '', '', 'publish', 'closed', 'closed', '', '283', '', '', '2021-08-23 19:03:47', '2021-08-23 12:03:47', '', 0, 'http://wordpress.local/GCO/maytracdiasaoviet/?p=283', 1, 'nav_menu_item', '', 0),
(284, 1, '2021-08-23 19:03:47', '2021-08-23 12:03:47', ' ', '', '', 'publish', 'closed', 'closed', '', '284', '', '', '2021-08-23 19:03:47', '2021-08-23 12:03:47', '', 0, 'http://wordpress.local/GCO/maytracdiasaoviet/?p=284', 2, 'nav_menu_item', '', 0),
(285, 1, '2021-08-23 19:06:22', '2021-08-23 12:06:22', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Tiêu đề MXH', 'f_socical_title', 'publish', 'closed', 'closed', '', 'field_61238f27cf4dc', '', '', '2021-09-06 18:03:36', '2021-09-06 11:03:36', '', 101, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&#038;p=285', 15, 'acf-field', '', 0),
(286, 1, '2021-08-24 14:40:20', '2021-08-24 07:40:20', '', 'Liên hệ', '', 'inherit', 'closed', 'closed', '', '36-revision-v1', '', '', '2021-08-24 14:40:20', '2021-08-24 07:40:20', '', 36, 'http://wordpress.local/GCO/maytracdiasaoviet/?p=286', 0, 'revision', '', 0),
(287, 1, '2021-08-24 14:42:09', '2021-08-24 07:42:09', '', 'Liên hệ', '', 'inherit', 'closed', 'closed', '', '36-revision-v1', '', '', '2021-08-24 14:42:09', '2021-08-24 07:42:09', '', 36, 'http://wordpress.local/GCO/maytracdiasaoviet/?p=287', 0, 'revision', '', 0),
(288, 1, '2021-08-24 15:31:57', '2021-08-24 08:31:57', 'Công ty Cổ phần Công nghệ và Truyền thông GCO – GCO Group (tiền thân là Công ty Cổ phần Công nghệ Helios Việt Nam) hoạt động trong các lĩnh vực: Software, Digital Marketing, E-Learning, Online Media và Brand Creative. GCO Group đã trải qua 6 năm trên hành trình xây dựng và phát triển, đối mặt và vượt qua rất nhiều khó khăn từ những ngày đầu thành lập. Khó khăn cũng là cơ hội để GCO Group tích lũy cho mình kinh nghiệm và bản lĩnh. Hơn 150 thành viên GCO Group luôn nỗ lực không ngừng hướng đến mục tiêu trở thành công ty Công nghệ và Truyền thông hàng đầu cả nước, vươn ra tầm khu vực\r\n<h2>Chúng tôi mang đến điều gì cho bạn?</h2>\r\nHành trình 6 năm phát triển không phải là quá dài nhưng đủ để cho GCO Group tạo dựng cho mình nền tảng nhất định trong lĩnh vực hoạt động. Với đội ngũ hơn 150 nhân sự trẻ, năng động, nhiệt huyết, chuyên nghiệp, GCO Group tự hào trở thành người bạn đồng hành tin cậy của hơn 1000 đối tác với hơn 500 khách hàng thường xuyên. Dịch vụ chuyên nghiệp, nhân sự tận tâm, quy trình làm việc bài bản, hỗ trợ thường xuyên, GCO Group tự tin mang đến những giải pháp hiệu quả phục vụ lợi ích khách hàng.', 'Giới thiệu', '', 'inherit', 'closed', 'closed', '', '234-revision-v1', '', '', '2021-08-24 15:31:57', '2021-08-24 08:31:57', '', 234, 'http://wordpress.local/GCO/maytracdiasaoviet/?p=288', 0, 'revision', '', 0),
(289, 1, '2021-08-24 15:32:20', '2021-08-24 08:32:20', 'Công ty Cổ phần Công nghệ và Truyền thông GCO – GCO Group (tiền thân là Công ty Cổ phần Công nghệ Helios Việt Nam) hoạt động trong các lĩnh vực: Software, Digital Marketing, E-Learning, Online Media và Brand Creative. GCO Group đã trải qua 6 năm trên hành trình xây dựng và phát triển, đối mặt và vượt qua rất nhiều khó khăn từ những ngày đầu thành lập. Khó khăn cũng là cơ hội để GCO Group tích lũy cho mình kinh nghiệm và bản lĩnh. Hơn 150 thành viên GCO Group luôn nỗ lực không ngừng hướng đến mục tiêu trở thành công ty Công nghệ và Truyền thông hàng đầu cả nước, vươn ra tầm khu vực\r\n<h2><span style=\"color: #ff0000;\">Chúng tôi mang đến điều gì cho bạn?</span></h2>\r\nHành trình 6 năm phát triển không phải là quá dài nhưng đủ để cho GCO Group tạo dựng cho mình nền tảng nhất định trong lĩnh vực hoạt động. Với đội ngũ hơn 150 nhân sự trẻ, năng động, nhiệt huyết, chuyên nghiệp, GCO Group tự hào trở thành người bạn đồng hành tin cậy của hơn 1000 đối tác với hơn 500 khách hàng thường xuyên. Dịch vụ chuyên nghiệp, nhân sự tận tâm, quy trình làm việc bài bản, hỗ trợ thường xuyên, GCO Group tự tin mang đến những giải pháp hiệu quả phục vụ lợi ích khách hàng.', 'Giới thiệu', '', 'inherit', 'closed', 'closed', '', '234-revision-v1', '', '', '2021-08-24 15:32:20', '2021-08-24 08:32:20', '', 234, 'http://wordpress.local/GCO/maytracdiasaoviet/?p=289', 0, 'revision', '', 0),
(290, 1, '2021-08-24 15:37:14', '2021-08-24 08:37:14', 'Công ty Cổ phần Công nghệ và Truyền thông GCO – GCO Group (tiền thân là Công ty Cổ phần Công nghệ Helios Việt Nam) hoạt động trong các lĩnh vực: Software, Digital Marketing, E-Learning, Online Media và Brand Creative. GCO Group đã trải qua 6 năm trên hành trình xây dựng và phát triển, đối mặt và vượt qua rất nhiều khó khăn từ những ngày đầu thành lập. Khó khăn cũng là cơ hội để GCO Group tích lũy cho mình kinh nghiệm và bản lĩnh. Hơn 150 thành viên GCO Group luôn nỗ lực không ngừng hướng đến mục tiêu trở thành công ty Công nghệ và Truyền thông hàng đầu cả nước, vươn ra tầm khu vực\r\n\r\n<span style=\"color: #ff0000;\">Chúng tôi mang đến điều gì cho bạn?</span>\r\n\r\nHành trình 6 năm phát triển không phải là quá dài nhưng đủ để cho GCO Group tạo dựng cho mình nền tảng nhất định trong lĩnh vực hoạt động. Với đội ngũ hơn 150 nhân sự trẻ, năng động, nhiệt huyết, chuyên nghiệp, GCO Group tự hào trở thành người bạn đồng hành tin cậy của hơn 1000 đối tác với hơn 500 khách hàng thường xuyên. Dịch vụ chuyên nghiệp, nhân sự tận tâm, quy trình làm việc bài bản, hỗ trợ thường xuyên, GCO Group tự tin mang đến những giải pháp hiệu quả phục vụ lợi ích khách hàng.', 'Giới thiệu', '', 'inherit', 'closed', 'closed', '', '234-revision-v1', '', '', '2021-08-24 15:37:14', '2021-08-24 08:37:14', '', 234, 'http://wordpress.local/GCO/maytracdiasaoviet/?p=290', 0, 'revision', '', 0),
(291, 1, '2021-08-24 16:21:13', '2021-08-24 09:21:13', '', '45', '', 'inherit', 'open', 'closed', '', '45', '', '', '2021-08-24 16:21:13', '2021-08-24 09:21:13', '', 29, 'http://wordpress.local/GCO/maytracdiasaoviet/wp-content/uploads/2020/12/45.jpg', 0, 'attachment', 'image/jpeg', 0),
(292, 1, '2021-08-24 16:21:14', '2021-08-24 09:21:14', '', '46', '', 'inherit', 'open', 'closed', '', '46', '', '', '2021-08-24 16:21:14', '2021-08-24 09:21:14', '', 29, 'http://wordpress.local/GCO/maytracdiasaoviet/wp-content/uploads/2020/12/46.jpg', 0, 'attachment', 'image/jpeg', 0),
(293, 1, '2021-08-24 16:21:15', '2021-08-24 09:21:15', '', '47', '', 'inherit', 'open', 'closed', '', '47', '', '', '2021-08-24 16:21:15', '2021-08-24 09:21:15', '', 29, 'http://wordpress.local/GCO/maytracdiasaoviet/wp-content/uploads/2020/12/47.jpg', 0, 'attachment', 'image/jpeg', 0),
(294, 1, '2021-08-24 16:21:17', '2021-08-24 09:21:17', '', '48', '', 'inherit', 'open', 'closed', '', '48', '', '', '2021-08-24 16:21:17', '2021-08-24 09:21:17', '', 29, 'http://wordpress.local/GCO/maytracdiasaoviet/wp-content/uploads/2020/12/48.jpg', 0, 'attachment', 'image/jpeg', 0),
(295, 1, '2021-08-24 16:21:35', '2021-08-24 09:21:35', '<!-- wp:paragraph -->\n<p>Nội dung Bài viết 1</p>\n<!-- /wp:paragraph -->', 'Khám phá những tác dụng bất ngờ từ chiếc thang gỗ trong phòng trẻ', 'Tóm tắt của Bài viết 1', 'inherit', 'closed', 'closed', '', '29-autosave-v1', '', '', '2021-08-24 16:21:35', '2021-08-24 09:21:35', '', 29, 'http://wordpress.local/GCO/maytracdiasaoviet/?p=295', 0, 'revision', '', 0),
(296, 1, '2021-08-24 16:22:25', '2021-08-24 09:22:25', '<!-- wp:paragraph -->\r\n<p>Một Studio chuyên thiết kế những đồ dùng gia đình tại Na Uy KAOS được thành lập bởi Mette Hansen Bordal và Gineline Kalleberg đã chinh phục rất nhiều gia đình bởi những sản phẩm hiện đại, có tính ứng dụng cao và đặc biệt hướng tới các em nhỏ. Gần đây trên Kickstarter họ đã đưa ra một dự án gây quỹ cho ý tưởng kinh doanh mới nhất và gần như đã chạm được đến bước cho sản phẩm ra đời.</p>\r\n<p>Tên gọi KAOS Endeløs được các nhà thiết kế lớn của KAOS lấy cảm hứng từ một hơi thở hoài niệm xa xưa về ấu thơ của họ, với những thanh treo bằng gỗ trong nhà và rồi sự sáng tạo đã cho họ nhìn ra được tính ứng dụng cao hơn của món đồ nội thất tưởng đơn giản mà đặc biệt đa công dụng này.</p>\r\n<p>Endeløs với ý nghĩa là \"sự vô cùng\", là định nghĩa của chính sản phẩm, bởi tác dụng \"vô hạn\" mà chiếc thang gỗ này đem lại. Nó nhiều hơn một thiết bị tập thể dục, giúp các bé phát triển cơ thể cùng các chức năng vận động như leo trèo, bám, với, đu... và còn có thể làm nơi lưu trữ bài tập về nhà, treo quần áo, những món đồ thủ công do chính các bé làm ra.</p>\r\n<p>Endeløs với ý nghĩa là \"sự vô cùng\", là định nghĩa của chính sản phẩm, bởi tác dụng \"vô hạn\" mà chiếc thang gỗ này đem lại. Nó nhiều hơn một thiết bị tập thể dục, giúp các bé phát triển cơ thể cùng các chức năng vận động như leo trèo, bám, với, đu... và còn có thể làm nơi lưu trữ bài tập về nhà, treo quần áo, những món đồ thủ công do chính các bé làm ra.</p>\r\n<!-- /wp:paragraph -->', 'Khám phá những tác dụng bất ngờ từ chiếc thang gỗ trong phòng trẻ', 'Một Studio chuyên thiết kế những đồ dùng gia đình tại Na Uy KAOS được thành lập bởi Mette Hansen Bordal và Gineline Kalleberg đã chinh phục rất nhiều gia đình bởi những sản phẩm hiện đại, có tính ứng dụng cao và đặc biệt hướng tới các em nhỏ. Gần đây trên Kickstarter họ đã đưa ra một dự án gây quỹ cho ý tưởng kinh doanh mới nhất và gần như đã chạm được đến bước cho sản phẩm ra đời.', 'inherit', 'closed', 'closed', '', '29-revision-v1', '', '', '2021-08-24 16:22:25', '2021-08-24 09:22:25', '', 29, 'http://wordpress.local/GCO/maytracdiasaoviet/?p=296', 0, 'revision', '', 0),
(297, 1, '2021-08-24 16:40:56', '2021-08-24 09:40:56', '', 'Bài viết 4', '', 'publish', 'open', 'open', '', 'bai-viet-4', '', '', '2021-08-24 16:40:56', '2021-08-24 09:40:56', '', 0, 'http://wordpress.local/GCO/maytracdiasaoviet/?p=297', 0, 'post', '', 0),
(298, 1, '2021-08-24 16:40:56', '2021-08-24 09:40:56', '', 'Bài viết 4', '', 'inherit', 'closed', 'closed', '', '297-revision-v1', '', '', '2021-08-24 16:40:56', '2021-08-24 09:40:56', '', 297, 'http://wordpress.local/GCO/maytracdiasaoviet/?p=298', 0, 'revision', '', 0),
(299, 1, '2021-08-24 16:55:03', '2021-08-24 09:55:03', '<!-- wp:paragraph -->\r\n<p>Một Studio chuyên thiết kế những đồ dùng gia đình tại Na Uy KAOS được thành lập bởi Mette Hansen Bordal và Gineline Kalleberg đã chinh phục rất nhiều gia đình bởi những sản phẩm hiện đại, có tính ứng dụng cao và đặc biệt hướng tới các em nhỏ. Gần đây trên Kickstarter họ đã đưa ra một dự án gây quỹ cho ý tưởng kinh doanh mới nhất và gần như đã chạm được đến bước cho sản phẩm ra đời.</p>\r\n<p>Tên gọi KAOS Endeløs được các nhà thiết kế lớn của KAOS lấy cảm hứng từ một hơi thở hoài niệm xa xưa về ấu thơ của họ, với những thanh treo bằng gỗ trong nhà và rồi sự sáng tạo đã cho họ nhìn ra được tính ứng dụng cao hơn của món đồ nội thất tưởng đơn giản mà đặc biệt đa công dụng này.</p>\r\n<p>Endeløs với ý nghĩa là \"sự vô cùng\", là định nghĩa của chính sản phẩm, bởi tác dụng \"vô hạn\" mà chiếc thang gỗ này đem lại. Nó nhiều hơn một thiết bị tập thể dục, giúp các bé phát triển cơ thể cùng các chức năng vận động như leo trèo, bám, với, đu... và còn có thể làm nơi lưu trữ bài tập về nhà, treo quần áo, những món đồ thủ công do chính các bé làm ra.</p>\r\n<p>Endeløs với ý nghĩa là \"sự vô cùng\", là định nghĩa của chính sản phẩm, bởi tác dụng \"vô hạn\" mà chiếc thang gỗ này đem lại. Nó nhiều hơn một thiết bị tập thể dục, giúp các bé phát triển cơ thể cùng các chức năng vận động như leo trèo, bám, với, đu... và còn có thể làm nơi lưu trữ bài tập về nhà, treo quần áo, những món đồ thủ công do chính các bé làm ra.</p>\r\n<!-- /wp:paragraph -->', 'Tác dụng bất ngờ từ chiếc thang gỗ trong phòng trẻ', 'Một Studio chuyên thiết kế những đồ dùng gia đình tại Na Uy KAOS được thành lập bởi Mette Hansen Bordal và Gineline Kalleberg đã chinh phục rất nhiều gia đình bởi những sản phẩm hiện đại, có tính ứng dụng cao và đặc biệt hướng tới các em nhỏ. Gần đây trên Kickstarter họ đã đưa ra một dự án gây quỹ cho ý tưởng kinh doanh mới nhất và gần như đã chạm được đến bước cho sản phẩm ra đời.', 'inherit', 'closed', 'closed', '', '29-revision-v1', '', '', '2021-08-24 16:55:03', '2021-08-24 09:55:03', '', 29, 'http://wordpress.local/GCO/maytracdiasaoviet/?p=299', 0, 'revision', '', 0),
(300, 1, '2021-08-24 16:57:21', '2021-08-24 09:57:21', '<!-- wp:paragraph -->\r\n<p>Một Studio chuyên thiết kế những đồ dùng gia đình tại Na Uy KAOS được thành lập bởi Mette Hansen Bordal và Gineline Kalleberg đã chinh phục rất nhiều gia đình bởi những sản phẩm hiện đại, có tính ứng dụng cao và đặc biệt hướng tới các em nhỏ. Gần đây trên Kickstarter họ đã đưa ra một dự án gây quỹ cho ý tưởng kinh doanh mới nhất và gần như đã chạm được đến bước cho sản phẩm ra đời.</p>\r\n<p>Tên gọi KAOS Endeløs được các nhà thiết kế lớn của KAOS lấy cảm hứng từ một hơi thở hoài niệm xa xưa về ấu thơ của họ, với những thanh treo bằng gỗ trong nhà và rồi sự sáng tạo đã cho họ nhìn ra được tính ứng dụng cao hơn của món đồ nội thất tưởng đơn giản mà đặc biệt đa công dụng này.</p>\r\n<p>Endeløs với ý nghĩa là \"sự vô cùng\", là định nghĩa của chính sản phẩm, bởi tác dụng \"vô hạn\" mà chiếc thang gỗ này đem lại. Nó nhiều hơn một thiết bị tập thể dục, giúp các bé phát triển cơ thể cùng các chức năng vận động như leo trèo, bám, với, đu... và còn có thể làm nơi lưu trữ bài tập về nhà, treo quần áo, những món đồ thủ công do chính các bé làm ra.</p>\r\n<p>Endeløs với ý nghĩa là \"sự vô cùng\", là định nghĩa của chính sản phẩm, bởi tác dụng \"vô hạn\" mà chiếc thang gỗ này đem lại. Nó nhiều hơn một thiết bị tập thể dục, giúp các bé phát triển cơ thể cùng các chức năng vận động như leo trèo, bám, với, đu... và còn có thể làm nơi lưu trữ bài tập về nhà, treo quần áo, những món đồ thủ công do chính các bé làm ra.</p>\r\n<!-- /wp:paragraph -->', 'Tác dụng từ chiếc thang gỗ trong phòng trẻ', 'Một Studio chuyên thiết kế những đồ dùng gia đình tại Na Uy KAOS được thành lập bởi Mette Hansen Bordal và Gineline Kalleberg đã chinh phục rất nhiều gia đình bởi những sản phẩm hiện đại, có tính ứng dụng cao và đặc biệt hướng tới các em nhỏ. Gần đây trên Kickstarter họ đã đưa ra một dự án gây quỹ cho ý tưởng kinh doanh mới nhất và gần như đã chạm được đến bước cho sản phẩm ra đời.', 'inherit', 'closed', 'closed', '', '29-revision-v1', '', '', '2021-08-24 16:57:21', '2021-08-24 09:57:21', '', 29, 'http://wordpress.local/GCO/maytracdiasaoviet/?p=300', 0, 'revision', '', 0),
(301, 1, '2021-08-24 17:00:56', '2021-08-24 10:00:56', '', 'banner13', '', 'inherit', 'open', 'closed', '', 'banner13', '', '', '2021-08-24 17:00:56', '2021-08-24 10:00:56', '', 0, 'http://wordpress.local/GCO/maytracdiasaoviet/wp-content/uploads/2021/08/banner13.jpg', 0, 'attachment', 'image/jpeg', 0),
(303, 1, '2021-08-24 17:12:21', '2021-08-24 10:12:21', '', 'logo', '', 'inherit', 'open', 'closed', '', 'logo-2', '', '', '2021-08-24 17:12:21', '2021-08-24 10:12:21', '', 0, 'http://wordpress.local/GCO/maytracdiasaoviet/wp-content/uploads/2021/08/logo.png', 0, 'attachment', 'image/png', 0),
(304, 1, '2021-08-24 17:21:40', '2021-08-24 10:21:40', '', 'woocommerce-placeholder', '', 'inherit', 'open', 'closed', '', 'woocommerce-placeholder', '', '', '2021-08-24 17:21:40', '2021-08-24 10:21:40', '', 0, 'http://wordpress.local/GCO/maytracdiasaoviet/wp-content/uploads/2021/08/woocommerce-placeholder.png', 0, 'attachment', 'image/png', 0),
(305, 1, '2021-08-24 17:21:40', '2021-08-24 10:21:40', '', 'Cửa hàng', '', 'publish', 'closed', 'closed', '', 'cua-hang', '', '', '2021-08-24 17:21:40', '2021-08-24 10:21:40', '', 0, 'http://wordpress.local/GCO/maytracdiasaoviet/cua-hang', 0, 'page', '', 0),
(306, 1, '2021-08-24 17:21:40', '2021-08-24 10:21:40', '<!-- wp:shortcode -->[woocommerce_cart]<!-- /wp:shortcode -->', 'Giỏ hàng', '', 'publish', 'closed', 'closed', '', 'gio-hang', '', '', '2021-08-24 17:21:40', '2021-08-24 10:21:40', '', 0, 'http://wordpress.local/GCO/maytracdiasaoviet/gio-hang', 0, 'page', '', 0),
(307, 1, '2021-08-24 17:21:40', '2021-08-24 10:21:40', '<!-- wp:shortcode -->[woocommerce_checkout]<!-- /wp:shortcode -->', 'Thanh toán', '', 'publish', 'closed', 'closed', '', 'thanh-toan', '', '', '2021-08-24 17:21:40', '2021-08-24 10:21:40', '', 0, 'http://wordpress.local/GCO/maytracdiasaoviet/thanh-toan', 0, 'page', '', 0),
(308, 1, '2021-08-24 17:21:40', '2021-08-24 10:21:40', '<!-- wp:shortcode -->[woocommerce_my_account]<!-- /wp:shortcode -->', 'Tài khoản', '', 'publish', 'closed', 'closed', '', 'tai-khoan', '', '', '2021-08-24 17:21:40', '2021-08-24 10:21:40', '', 0, 'http://wordpress.local/GCO/maytracdiasaoviet/tai-khoan', 0, 'page', '', 0),
(309, 1, '2021-08-24 17:21:40', '0000-00-00 00:00:00', '<!-- wp:paragraph -->\n<p><b>This is a sample page.</b></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<h3>Overview</h3>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Our refund and returns policy lasts 30 days. If 30 days have passed since your purchase, we can’t offer you a full refund or exchange.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>To be eligible for a return, your item must be unused and in the same condition that you received it. It must also be in the original packaging.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Several types of goods are exempt from being returned. Perishable goods such as food, flowers, newspapers or magazines cannot be returned. We also do not accept products that are intimate or sanitary goods, hazardous materials, or flammable liquids or gases.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Additional non-returnable items:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:list -->\n<ul>\n<li>Gift cards</li>\n<li>Downloadable software products</li>\n<li>Some health and personal care items</li>\n</ul>\n<!-- /wp:list -->\n\n<!-- wp:paragraph -->\n<p>To complete your return, we require a receipt or proof of purchase.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Please do not send your purchase back to the manufacturer.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>There are certain situations where only partial refunds are granted:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:list -->\n<ul>\n<li>Book with obvious signs of use</li>\n<li>CD, DVD, VHS tape, software, video game, cassette tape, or vinyl record that has been opened.</li>\n<li>Any item not in its original condition, is damaged or missing parts for reasons not due to our error.</li>\n<li>Any item that is returned more than 30 days after delivery</li>\n</ul>\n<!-- /wp:list -->\n\n<!-- wp:paragraph -->\n<h2>Refunds</h2>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Once your return is received and inspected, we will send you an email to notify you that we have received your returned item. We will also notify you of the approval or rejection of your refund.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>If you are approved, then your refund will be processed, and a credit will automatically be applied to your credit card or original method of payment, within a certain amount of days.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<b>Late or missing refunds</b>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>If you haven’t received a refund yet, first check your bank account again.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Then contact your credit card company, it may take some time before your refund is officially posted.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Next contact your bank. There is often some processing time before a refund is posted.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>If you’ve done all of this and you still have not received your refund yet, please contact us at {email address}.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<b>Sale items</b>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Only regular priced items may be refunded. Sale items cannot be refunded.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<h2>Exchanges</h2>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>We only replace items if they are defective or damaged. If you need to exchange it for the same item, send us an email at {email address} and send your item to: {physical address}.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<h2>Gifts</h2>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>If the item was marked as a gift when purchased and shipped directly to you, you’ll receive a gift credit for the value of your return. Once the returned item is received, a gift certificate will be mailed to you.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>If the item wasn’t marked as a gift when purchased, or the gift giver had the order shipped to themselves to give to you later, we will send a refund to the gift giver and they will find out about your return.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<h2>Shipping returns</h2>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>To return your product, you should mail your product to: {physical address}.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>You will be responsible for paying for your own shipping costs for returning your item. Shipping costs are non-refundable. If you receive a refund, the cost of return shipping will be deducted from your refund.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Depending on where you live, the time it may take for your exchanged product to reach you may vary.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>If you are returning more expensive items, you may consider using a trackable shipping service or purchasing shipping insurance. We don’t guarantee that we will receive your returned item.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<h2>Need help?</h2>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Contact us at {email} for questions related to refunds and returns.</p>\n<!-- /wp:paragraph -->', 'Refund and Returns Policy', '', 'draft', 'closed', 'closed', '', 'refund_returns', '', '', '2021-08-24 17:21:40', '0000-00-00 00:00:00', '', 0, 'http://wordpress.local/GCO/maytracdiasaoviet/?page_id=309', 0, 'page', '', 0),
(310, 1, '2021-08-24 17:31:39', '2021-08-24 10:31:39', 'Áo thun nam thời trang với thiết kế cổ tròn, tay ngắn mang đến vẻ ngoài năng động, phong cách cho phái mạnh Form áo ôm vừa vặn thoải mái khi mặc mang lại sự tự tin và năng động cho phái mạnh Nhiều màu cho phái mạnh tha hồ lựa chọn, dễ dàng kết hợp với nhiều trang phục khác, thêm phong cá tính Đường chỉ may đẹp, tinh tế mang đến sự an tâm tuyệt đối cho nam giới khi sử dụng sản phẩm Chất liệu polyester chống nhăn, co và rất bền màu, đem lại sự thoải mái cho người mặc, tự tin vận động Kết hợp hài hòa được với các trang phục từ bụi bặm cá tính như quần short, quần jean đến những phong cách đơn giản cổ điển như quần tây quần kaki,..\r\n\r\nÁo thun nam là item số 1 nằm trong những mẫu trang phục của các anh chàng phái mạnh. Sở dĩ nhờ sự thoải mái cùng kiểu dáng đơn giản dễ dàng kết hợp với hầu hết những mẫu trang phục và phụ kiện khác mà trong tủ đồ của mỗi anh chàng đều có ít nhất một chiếc áo thun. Cùng với xu hướng phái triển của thời trang ngày nay áo thun nam đã được cách điệu và thiết kế những gam màu đầy thời trang và phong cách hơn, cùng tham khảo với Zanado nha!', 'Sản phẩm 1', 'Mô tả Sản phẩm 1', 'publish', 'open', 'closed', '', 'san-pham-1', '', '', '2021-08-26 17:13:09', '2021-08-26 10:13:09', '', 0, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=product&#038;p=310', 0, 'product', '', 1),
(311, 1, '2021-08-24 17:33:24', '2021-08-24 10:33:24', '', 'logo', '', 'inherit', 'open', 'closed', '', 'logo', '', '', '2021-08-24 17:33:24', '2021-08-24 10:33:24', '', 0, 'http://wordpress.local/GCO/maytracdiasaoviet/wp-content/uploads/2021/08/logo-1.png', 0, 'attachment', 'image/png', 0),
(312, 1, '2021-08-24 17:46:40', '2021-08-24 10:46:40', '', 'Order &ndash; Tháng Tám 24, 2021 @ 05:46 Chiều', 'ghi chú', 'wc-processing', 'open', 'closed', 'wc_order_77Yh4FXTtm6hw', 'don-hang-aug-24-2021-1046-am', '', '', '2021-08-24 17:46:40', '2021-08-24 10:46:40', '', 0, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=shop_order&#038;p=312', 0, 'shop_order', '', 1),
(314, 1, '2021-08-25 16:31:22', '2021-08-25 09:05:47', ' ', '', '', 'publish', 'closed', 'closed', '', '314', '', '', '2021-08-25 16:31:22', '2021-08-25 09:31:22', '', 0, 'http://wordpress.local/GCO/maytracdiasaoviet/?p=314', 5, 'nav_menu_item', '', 0),
(317, 1, '2021-08-25 16:31:22', '2021-08-25 09:05:47', ' ', '', '', 'publish', 'closed', 'closed', '', '317', '', '', '2021-08-25 16:31:22', '2021-08-25 09:31:22', '', 0, 'http://wordpress.local/GCO/maytracdiasaoviet/?p=317', 6, 'nav_menu_item', '', 0),
(319, 1, '2021-08-25 16:06:53', '2021-08-25 09:06:53', 'Sản phẩm 2', 'Sản phẩm 2', 'Sản phẩm 2', 'publish', 'open', 'closed', '', 'san-pham-2', '', '', '2021-12-30 10:17:09', '2021-12-30 03:17:09', '', 0, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=product&#038;p=319', 0, 'product', '', 0),
(320, 1, '2021-08-25 16:07:22', '2021-08-25 09:07:22', '', 'Sản phẩm 3', '', 'publish', 'open', 'closed', '', 'san-pham-3', '', '', '2021-12-30 10:17:19', '2021-12-30 03:17:19', '', 0, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=product&#038;p=320', 0, 'product', '', 0),
(321, 1, '2021-08-25 16:31:22', '2021-08-25 09:31:22', '', 'Sản phẩm', '', 'publish', 'closed', 'closed', '', 'san-pham', '', '', '2021-08-25 16:31:22', '2021-08-25 09:31:22', '', 0, 'http://wordpress.local/GCO/maytracdiasaoviet/?p=321', 4, 'nav_menu_item', '', 0),
(322, 1, '2021-09-03 14:15:59', '2021-08-25 09:32:36', ' ', '', '', 'publish', 'closed', 'closed', '', '322', '', '', '2021-09-03 14:15:59', '2021-09-03 07:15:59', '', 0, 'http://wordpress.local/GCO/maytracdiasaoviet/?p=322', 1, 'nav_menu_item', '', 0),
(323, 1, '2021-09-03 14:15:59', '2021-08-25 09:32:36', ' ', '', '', 'publish', 'closed', 'closed', '', '323', '', '', '2021-09-03 14:15:59', '2021-09-03 07:15:59', '', 0, 'http://wordpress.local/GCO/maytracdiasaoviet/?p=323', 4, 'nav_menu_item', '', 0),
(326, 1, '2021-08-25 16:49:41', '2021-08-25 09:49:41', '', 'Sản phẩm 4', '', 'publish', 'open', 'closed', '', 'san-pham-4', '', '', '2021-12-30 10:17:25', '2021-12-30 03:17:25', '', 0, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=product&#038;p=326', 0, 'product', '', 0),
(327, 1, '2021-08-25 16:50:08', '2021-08-25 09:50:08', '', 'Sản phẩm 5', '', 'publish', 'open', 'closed', '', 'san-pham-5', '', '', '2021-12-30 10:17:34', '2021-12-30 03:17:34', '', 0, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=product&#038;p=327', 0, 'product', '', 0),
(328, 1, '2021-08-25 17:05:25', '2021-08-25 10:05:25', '', 'Sản phẩm 5', '<p><br data-mce-bogus=\"1\"></p>', 'inherit', 'closed', 'closed', '', '327-autosave-v1', '', '', '2021-08-25 17:05:25', '2021-08-25 10:05:25', '', 327, 'http://wordpress.local/GCO/maytracdiasaoviet/?p=328', 0, 'revision', '', 0),
(329, 1, '2021-08-25 17:06:30', '2021-08-25 10:06:30', '', '43', '', 'inherit', 'open', 'closed', '', '43', '', '', '2021-08-25 17:06:30', '2021-08-25 10:06:30', '', 0, 'http://wordpress.local/GCO/maytracdiasaoviet/wp-content/uploads/2021/08/43.jpg', 0, 'attachment', 'image/jpeg', 0),
(330, 1, '2021-08-25 17:31:52', '2021-08-25 10:31:52', '', '39', '', 'inherit', 'open', 'closed', '', '39', '', '', '2021-08-25 17:31:52', '2021-08-25 10:31:52', '', 310, 'http://wordpress.local/GCO/maytracdiasaoviet/wp-content/uploads/2021/08/39.jpg', 0, 'attachment', 'image/jpeg', 0),
(331, 1, '2021-08-25 17:31:54', '2021-08-25 10:31:54', '', '40', '', 'inherit', 'open', 'closed', '', '40', '', '', '2021-08-25 17:31:54', '2021-08-25 10:31:54', '', 310, 'http://wordpress.local/GCO/maytracdiasaoviet/wp-content/uploads/2021/08/40.jpg', 0, 'attachment', 'image/jpeg', 0),
(332, 1, '2021-08-25 17:31:56', '2021-08-25 10:31:56', '', '41', '', 'inherit', 'open', 'closed', '', '41', '', '', '2021-08-25 17:31:56', '2021-08-25 10:31:56', '', 310, 'http://wordpress.local/GCO/maytracdiasaoviet/wp-content/uploads/2021/08/41.jpg', 0, 'attachment', 'image/jpeg', 0),
(333, 1, '2021-08-25 17:31:57', '2021-08-25 10:31:57', '', '42', '', 'inherit', 'open', 'closed', '', '42', '', '', '2021-08-25 17:31:57', '2021-08-25 10:31:57', '', 310, 'http://wordpress.local/GCO/maytracdiasaoviet/wp-content/uploads/2021/08/42.jpg', 0, 'attachment', 'image/jpeg', 0),
(334, 1, '2021-08-25 17:57:17', '2021-08-25 10:57:17', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:7:\"product\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Nhóm trường Trang Sản phẩm', 'nhom-truong-trang-san-pham', 'publish', 'closed', 'closed', '', 'group_612621c53aab5', '', '', '2021-08-25 18:00:28', '2021-08-25 11:00:28', '', 0, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field-group&#038;p=334', 0, 'acf-field-group', '', 0),
(335, 1, '2021-08-25 17:57:17', '2021-08-25 10:57:17', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:0:\"\";}', 'Thông số cơ bản', 'single_product_parameter', 'publish', 'closed', 'closed', '', 'field_612621cedc7a6', '', '', '2021-08-25 18:00:28', '2021-08-25 11:00:28', '', 334, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&#038;p=335', 0, 'acf-field', '', 0),
(336, 1, '2021-08-25 17:57:17', '2021-08-25 10:57:17', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Tiêu đề', 'title', 'publish', 'closed', 'closed', '', 'field_612621f4dc7a7', '', '', '2021-08-25 17:57:17', '2021-08-25 10:57:17', '', 335, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&p=336', 0, 'acf-field', '', 0),
(337, 1, '2021-08-25 17:57:17', '2021-08-25 10:57:17', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Nội dung', 'desc', 'publish', 'closed', 'closed', '', 'field_612621fbdc7a8', '', '', '2021-08-25 17:57:17', '2021-08-25 10:57:17', '', 335, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&p=337', 1, 'acf-field', '', 0),
(338, 1, '2021-08-25 18:01:05', '2021-08-25 11:01:05', 'Áo thun nam thời trang với thiết kế cổ tròn, tay ngắn mang đến vẻ ngoài năng động, phong cách cho phái mạnh Form áo ôm vừa vặn thoải mái khi mặc mang lại sự tự tin và năng động cho phái mạnh Nhiều màu cho phái mạnh tha hồ lựa chọn, dễ dàng kết hợp với nhiều trang phục khác, thêm phong cá tính Đường chỉ may đẹp, tinh tế mang đến sự an tâm tuyệt đối cho nam giới khi sử dụng sản phẩm Chất liệu polyester chống nhăn, co và rất bền màu, đem lại sự thoải mái cho người mặc, tự tin vận động Kết hợp hài hòa được với các trang phục từ bụi bặm cá tính như quần short, quần jean đến những phong cách đơn giản cổ điển như quần tây quần kaki,..\n\nÁo thun nam là item số 1 nằm trong những mẫu trang phục của các anh chàng phái mạnh. Sở dĩ nhờ sự thoải mái cùng kiểu dáng đơn giản dễ dàng kết hợp với hầu hết những mẫu trang phục và phụ kiện khác mà trong tủ đồ của mỗi anh chàng đều có ít nhất một chiếc áo thun. Cùng với xu hướng phái triển của thời trang ngày nay áo thun nam đã được cách điệu và thiết kế những gam màu đầy thời trang và phong cách hơn, cùng tham khảo với Zanado nha!', 'Sản phẩm 1', 'Mô tả Sản phẩm 1', 'inherit', 'closed', 'closed', '', '310-autosave-v1', '', '', '2021-08-25 18:01:05', '2021-08-25 11:01:05', '', 310, 'http://wordpress.local/GCO/maytracdiasaoviet/?p=338', 0, 'revision', '', 0),
(339, 1, '2021-08-25 18:39:54', '2021-08-25 11:39:54', '', 'slider2', '', 'inherit', 'open', 'closed', '', 'slider2', '', '', '2021-08-25 18:39:54', '2021-08-25 11:39:54', '', 310, 'http://wordpress.local/GCO/maytracdiasaoviet/wp-content/uploads/2021/08/slider2.jpg', 0, 'attachment', 'image/jpeg', 0),
(340, 1, '2021-08-25 18:40:09', '2021-08-25 11:40:09', '', '46', '', 'inherit', 'open', 'closed', '', '46-2', '', '', '2021-08-25 18:40:09', '2021-08-25 11:40:09', '', 310, 'http://wordpress.local/GCO/maytracdiasaoviet/wp-content/uploads/2021/08/46.jpg', 0, 'attachment', 'image/jpeg', 0),
(341, 1, '2021-08-25 18:40:10', '2021-08-25 11:40:10', '', '48', '', 'inherit', 'open', 'closed', '', '48-2', '', '', '2021-08-25 18:40:10', '2021-08-25 11:40:10', '', 310, 'http://wordpress.local/GCO/maytracdiasaoviet/wp-content/uploads/2021/08/48.jpg', 0, 'attachment', 'image/jpeg', 0),
(342, 1, '2021-08-26 16:29:18', '2021-08-26 09:29:18', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"20\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:4:\"full\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Ảnh quảng cáo', 'home_slide_image_ads', 'publish', 'closed', 'closed', '', 'field_61275bba23124', '', '', '2021-08-26 16:29:18', '2021-08-26 09:29:18', '', 138, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&p=342', 2, 'acf-field', '', 0),
(343, 1, '2021-08-26 16:29:18', '2021-08-26 09:29:18', 'a:7:{s:4:\"type\";s:3:\"url\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"80\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:8:\"https://\";}', 'Đường dẫn ảnh 1', 'image_ads_one_url', 'publish', 'closed', 'closed', '', 'field_61275cdf23126', '', '', '2021-08-26 16:41:54', '2021-08-26 09:41:54', '', 138, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&#038;p=343', 6, 'acf-field', '', 0),
(344, 1, '2021-08-26 16:29:18', '2021-08-26 09:29:18', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"20\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:4:\"full\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Ảnh 2', 'image_ads_two', 'publish', 'closed', 'closed', '', 'field_61275d0423127', '', '', '2021-08-26 16:37:51', '2021-08-26 09:37:51', '', 138, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&#038;p=344', 7, 'acf-field', '', 0),
(345, 1, '2021-08-26 16:29:18', '2021-08-26 09:29:18', 'a:7:{s:4:\"type\";s:3:\"url\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"80\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:8:\"https://\";}', 'Đường dẫn ảnh 2', 'image_ads_two_url', 'publish', 'closed', 'closed', '', 'field_61275d1123128', '', '', '2021-08-26 16:41:54', '2021-08-26 09:41:54', '', 138, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&#038;p=345', 8, 'acf-field', '', 0),
(346, 1, '2021-08-26 16:29:19', '2021-08-26 09:29:19', 'a:13:{s:4:\"type\";s:8:\"taxonomy\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:8:\"taxonomy\";s:11:\"product_cat\";s:10:\"field_type\";s:6:\"select\";s:10:\"allow_null\";i:0;s:8:\"add_term\";i:1;s:10:\"save_terms\";i:0;s:10:\"load_terms\";i:0;s:13:\"return_format\";s:6:\"object\";s:8:\"multiple\";i:0;}', 'Chọn danh mục', 'taxonomy_product', 'publish', 'closed', 'closed', '', 'field_61275dd723129', '', '', '2021-08-26 16:29:19', '2021-08-26 09:29:19', '', 231, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&p=346', 0, 'acf-field', '', 0),
(347, 1, '2021-08-26 16:29:19', '2021-08-26 09:29:19', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'Box quảng cáo hai', 'về_chung_toi_copy', 'publish', 'closed', 'closed', '', 'field_61275e7b2312a', '', '', '2021-08-26 16:37:51', '2021-08-26 09:37:51', '', 138, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&#038;p=347', 11, 'acf-field', '', 0),
(348, 1, '2021-08-26 16:29:19', '2021-08-26 09:29:19', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"20\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:4:\"full\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Ảnh 1', 'image_ads_three', 'publish', 'closed', 'closed', '', 'field_61275e812312b', '', '', '2021-08-26 16:37:51', '2021-08-26 09:37:51', '', 138, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&#038;p=348', 12, 'acf-field', '', 0),
(349, 1, '2021-08-26 16:29:19', '2021-08-26 09:29:19', 'a:7:{s:4:\"type\";s:3:\"url\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"80\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:8:\"https://\";}', 'Đường dẫn ảnh 1', 'image_ads_three_url', 'publish', 'closed', 'closed', '', 'field_61275e852312c', '', '', '2021-08-26 16:42:18', '2021-08-26 09:42:18', '', 138, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&#038;p=349', 13, 'acf-field', '', 0),
(350, 1, '2021-08-26 16:29:19', '2021-08-26 09:29:19', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"20\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:4:\"full\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Ảnh 2', 'image_ads_four', 'publish', 'closed', 'closed', '', 'field_61275e8a2312d', '', '', '2021-08-26 16:37:52', '2021-08-26 09:37:52', '', 138, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&#038;p=350', 14, 'acf-field', '', 0),
(351, 1, '2021-08-26 16:29:19', '2021-08-26 09:29:19', 'a:7:{s:4:\"type\";s:3:\"url\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"80\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:8:\"https://\";}', 'Đường dẫn ảnh 2', 'image_ads_four_url', 'publish', 'closed', 'closed', '', 'field_61275e902312e', '', '', '2021-08-26 16:42:18', '2021-08-26 09:42:18', '', 138, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&#038;p=351', 15, 'acf-field', '', 0),
(352, 1, '2021-08-26 16:37:51', '2021-08-26 09:37:51', 'a:7:{s:4:\"type\";s:3:\"url\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"80\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:8:\"https://\";}', 'Đường dẫn ảnh quảng cáo', 'home_slide_image_ads_url', 'publish', 'closed', 'closed', '', 'field_612760ba91d40', '', '', '2021-08-26 16:41:33', '2021-08-26 09:41:33', '', 138, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&#038;p=352', 3, 'acf-field', '', 0),
(353, 1, '2021-08-26 16:39:22', '2021-08-26 09:39:22', '', 'banner', '', 'inherit', 'open', 'closed', '', 'banner', '', '', '2021-08-26 16:39:22', '2021-08-26 09:39:22', '', 12, 'http://wordpress.local/GCO/maytracdiasaoviet/wp-content/uploads/2021/08/banner.jpg', 0, 'attachment', 'image/jpeg', 0),
(354, 1, '2021-08-26 16:40:11', '2021-08-26 09:40:11', '', 'slider', '', 'inherit', 'open', 'closed', '', 'slider', '', '', '2021-08-26 16:40:11', '2021-08-26 09:40:11', '', 12, 'http://wordpress.local/GCO/maytracdiasaoviet/wp-content/uploads/2021/08/slider.jpg', 0, 'attachment', 'image/jpeg', 0),
(355, 1, '2021-08-26 16:40:53', '2021-08-26 09:40:53', '<!-- wp:paragraph -->\r\n<p>&nbsp;</p>\r\n<!-- /wp:paragraph -->\r\n\r\n<!-- wp:paragraph -->\r\n<p>&nbsp;</p>\r\n<!-- /wp:paragraph -->\r\n\r\n<!-- wp:paragraph -->\r\n<p>&nbsp;</p>\r\n<!-- /wp:paragraph -->', 'Trang chủ', '', 'inherit', 'closed', 'closed', '', '12-revision-v1', '', '', '2021-08-26 16:40:53', '2021-08-26 09:40:53', '', 12, 'http://wordpress.local/GCO/maytracdiasaoviet/?p=355', 0, 'revision', '', 0),
(356, 1, '2021-08-26 16:43:20', '2021-08-26 09:43:20', '', 'banner2', '', 'inherit', 'open', 'closed', '', 'banner2', '', '', '2021-08-26 16:43:20', '2021-08-26 09:43:20', '', 12, 'http://wordpress.local/GCO/maytracdiasaoviet/wp-content/uploads/2021/08/banner2.jpg', 0, 'attachment', 'image/jpeg', 0),
(357, 1, '2021-08-26 16:43:22', '2021-08-26 09:43:22', '', 'banner3', '', 'inherit', 'open', 'closed', '', 'banner3', '', '', '2021-08-26 16:43:22', '2021-08-26 09:43:22', '', 12, 'http://wordpress.local/GCO/maytracdiasaoviet/wp-content/uploads/2021/08/banner3.jpg', 0, 'attachment', 'image/jpeg', 0),
(358, 1, '2021-08-26 16:43:23', '2021-08-26 09:43:23', '', 'banner11', '', 'inherit', 'open', 'closed', '', 'banner11', '', '', '2021-08-26 16:43:23', '2021-08-26 09:43:23', '', 12, 'http://wordpress.local/GCO/maytracdiasaoviet/wp-content/uploads/2021/08/banner11.jpg', 0, 'attachment', 'image/jpeg', 0),
(359, 1, '2021-08-26 16:43:26', '2021-08-26 09:43:26', '', 'banner12', '', 'inherit', 'open', 'closed', '', 'banner12', '', '', '2021-08-26 16:43:26', '2021-08-26 09:43:26', '', 12, 'http://wordpress.local/GCO/maytracdiasaoviet/wp-content/uploads/2021/08/banner12.jpg', 0, 'attachment', 'image/jpeg', 0),
(360, 1, '2021-08-26 16:44:12', '2021-08-26 09:44:12', '<!-- wp:paragraph -->\r\n<p>&nbsp;</p>\r\n<!-- /wp:paragraph -->\r\n\r\n<!-- wp:paragraph -->\r\n<p>&nbsp;</p>\r\n<!-- /wp:paragraph -->\r\n\r\n<!-- wp:paragraph -->\r\n<p>&nbsp;</p>\r\n<!-- /wp:paragraph -->', 'Trang chủ', '', 'inherit', 'closed', 'closed', '', '12-revision-v1', '', '', '2021-08-26 16:44:12', '2021-08-26 09:44:12', '', 12, 'http://wordpress.local/GCO/maytracdiasaoviet/?p=360', 0, 'revision', '', 0),
(361, 1, '2021-08-26 16:44:26', '2021-08-26 09:44:26', '<!-- wp:paragraph -->\r\n<p>&nbsp;</p>\r\n<!-- /wp:paragraph -->\r\n\r\n<!-- wp:paragraph -->\r\n<p>&nbsp;</p>\r\n<!-- /wp:paragraph -->\r\n\r\n<!-- wp:paragraph -->\r\n<p>&nbsp;</p>\r\n<!-- /wp:paragraph -->', 'Trang chủ', '', 'inherit', 'closed', 'closed', '', '12-revision-v1', '', '', '2021-08-26 16:44:26', '2021-08-26 09:44:26', '', 12, 'http://wordpress.local/GCO/maytracdiasaoviet/?p=361', 0, 'revision', '', 0),
(362, 1, '2021-08-26 16:44:40', '2021-08-26 09:44:40', '<!-- wp:paragraph -->\r\n<p>&nbsp;</p>\r\n<!-- /wp:paragraph -->\r\n\r\n<!-- wp:paragraph -->\r\n<p>&nbsp;</p>\r\n<!-- /wp:paragraph -->\r\n\r\n<!-- wp:paragraph -->\r\n<p>&nbsp;</p>\r\n<!-- /wp:paragraph -->', 'Trang chủ', '', 'inherit', 'closed', 'closed', '', '12-revision-v1', '', '', '2021-08-26 16:44:40', '2021-08-26 09:44:40', '', 12, 'http://wordpress.local/GCO/maytracdiasaoviet/?p=362', 0, 'revision', '', 0),
(363, 1, '2021-08-26 16:58:40', '2021-08-26 09:58:40', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'Sản phẩm bán chạy', '', 'publish', 'closed', 'closed', '', 'field_61276570f0891', '', '', '2021-08-26 16:58:40', '2021-08-26 09:58:40', '', 138, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&p=363', 16, 'acf-field', '', 0),
(364, 1, '2021-08-26 16:58:40', '2021-08-26 09:58:40', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Tiêu đề', 'home_product_buy_hot_title', 'publish', 'closed', 'closed', '', 'field_61276583f0892', '', '', '2021-08-26 16:58:40', '2021-08-26 09:58:40', '', 138, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&p=364', 17, 'acf-field', '', 0);
INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(365, 1, '2021-08-26 16:58:40', '2021-08-26 09:58:40', 'a:12:{s:4:\"type\";s:12:\"relationship\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"post_type\";a:1:{i:0;s:7:\"product\";}s:8:\"taxonomy\";s:0:\"\";s:7:\"filters\";a:1:{i:0;s:6:\"search\";}s:8:\"elements\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:13:\"return_format\";s:6:\"object\";}', 'Chọn sản phẩm', 'home_product_buy_hot_select', 'publish', 'closed', 'closed', '', 'field_6127658df0893', '', '', '2021-08-26 16:58:40', '2021-08-26 09:58:40', '', 138, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&p=365', 18, 'acf-field', '', 0),
(366, 1, '2021-08-26 16:59:24', '2021-08-26 09:59:24', '<!-- wp:paragraph -->\r\n<p>&nbsp;</p>\r\n<!-- /wp:paragraph -->\r\n\r\n<!-- wp:paragraph -->\r\n<p>&nbsp;</p>\r\n<!-- /wp:paragraph -->\r\n\r\n<!-- wp:paragraph -->\r\n<p>&nbsp;</p>\r\n<!-- /wp:paragraph -->', 'Trang chủ', '', 'inherit', 'closed', 'closed', '', '12-revision-v1', '', '', '2021-08-26 16:59:24', '2021-08-26 09:59:24', '', 12, 'http://wordpress.local/GCO/maytracdiasaoviet/?p=366', 0, 'revision', '', 0),
(367, 1, '2021-08-26 17:07:48', '2021-08-26 10:07:48', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:4:\"full\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Ảnh 1', 'taxonomy_product_image_ads_one', 'publish', 'closed', 'closed', '', 'field_6127674015cdf', '', '', '2021-08-26 17:36:03', '2021-08-26 10:36:03', '', 231, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&#038;p=367', 1, 'acf-field', '', 0),
(368, 1, '2021-08-26 17:07:48', '2021-08-26 10:07:48', 'a:7:{s:4:\"type\";s:3:\"url\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:8:\"https://\";}', 'Đường dẫn ảnh 1', 'taxonomy_product_image_ads_one_url', 'publish', 'closed', 'closed', '', 'field_6127679415ce1', '', '', '2021-08-26 17:08:47', '2021-08-26 10:08:47', '', 231, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&#038;p=368', 2, 'acf-field', '', 0),
(369, 1, '2021-08-26 17:07:48', '2021-08-26 10:07:48', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:4:\"full\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Ảnh 2', 'taxonomy_product_image_ads_two', 'publish', 'closed', 'closed', '', 'field_6127676515ce0', '', '', '2021-08-26 17:36:03', '2021-08-26 10:36:03', '', 231, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&#038;p=369', 3, 'acf-field', '', 0),
(370, 1, '2021-08-26 17:07:48', '2021-08-26 10:07:48', 'a:7:{s:4:\"type\";s:3:\"url\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:8:\"https://\";}', 'Đường dẫn ảnh 2', 'taxonomy_product_image_ads_two_url', 'publish', 'closed', 'closed', '', 'field_612767c615ce2', '', '', '2021-08-26 17:08:47', '2021-08-26 10:08:47', '', 231, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&#038;p=370', 4, 'acf-field', '', 0),
(371, 1, '2021-08-26 17:11:25', '2021-08-26 10:11:25', '', 'banner4', '', 'inherit', 'open', 'closed', '', 'banner4', '', '', '2021-08-26 17:11:25', '2021-08-26 10:11:25', '', 12, 'http://wordpress.local/GCO/maytracdiasaoviet/wp-content/uploads/2021/08/banner4.jpg', 0, 'attachment', 'image/jpeg', 0),
(372, 1, '2021-08-26 17:11:26', '2021-08-26 10:11:26', '', 'banner5', '', 'inherit', 'open', 'closed', '', 'banner5', '', '', '2021-08-26 17:11:26', '2021-08-26 10:11:26', '', 12, 'http://wordpress.local/GCO/maytracdiasaoviet/wp-content/uploads/2021/08/banner5.jpg', 0, 'attachment', 'image/jpeg', 0),
(373, 1, '2021-08-26 17:11:28', '2021-08-26 10:11:28', '', 'banner6', '', 'inherit', 'open', 'closed', '', 'banner6', '', '', '2021-08-26 17:11:28', '2021-08-26 10:11:28', '', 12, 'http://wordpress.local/GCO/maytracdiasaoviet/wp-content/uploads/2021/08/banner6.jpg', 0, 'attachment', 'image/jpeg', 0),
(374, 1, '2021-08-26 17:11:29', '2021-08-26 10:11:29', '', 'banner7', '', 'inherit', 'open', 'closed', '', 'banner7', '', '', '2021-08-26 17:11:29', '2021-08-26 10:11:29', '', 12, 'http://wordpress.local/GCO/maytracdiasaoviet/wp-content/uploads/2021/08/banner7.jpg', 0, 'attachment', 'image/jpeg', 0),
(375, 1, '2021-08-26 17:11:30', '2021-08-26 10:11:30', '', 'banner9', '', 'inherit', 'open', 'closed', '', 'banner9', '', '', '2021-08-26 17:11:30', '2021-08-26 10:11:30', '', 12, 'http://wordpress.local/GCO/maytracdiasaoviet/wp-content/uploads/2021/08/banner9.jpg', 0, 'attachment', 'image/jpeg', 0),
(376, 1, '2021-08-26 17:11:32', '2021-08-26 10:11:32', '', 'banner10', '', 'inherit', 'open', 'closed', '', 'banner10', '', '', '2021-08-26 17:11:32', '2021-08-26 10:11:32', '', 12, 'http://wordpress.local/GCO/maytracdiasaoviet/wp-content/uploads/2021/08/banner10.jpg', 0, 'attachment', 'image/jpeg', 0),
(377, 1, '2021-08-26 17:12:34', '2021-08-26 10:12:34', '<!-- wp:paragraph -->\r\n<p>&nbsp;</p>\r\n<!-- /wp:paragraph -->\r\n\r\n<!-- wp:paragraph -->\r\n<p>&nbsp;</p>\r\n<!-- /wp:paragraph -->\r\n\r\n<!-- wp:paragraph -->\r\n<p>&nbsp;</p>\r\n<!-- /wp:paragraph -->', 'Trang chủ', '', 'inherit', 'closed', 'closed', '', '12-revision-v1', '', '', '2021-08-26 17:12:34', '2021-08-26 10:12:34', '', 12, 'http://wordpress.local/GCO/maytracdiasaoviet/?p=377', 0, 'revision', '', 0),
(378, 1, '2021-08-26 17:13:24', '2021-08-26 10:13:24', '<!-- wp:paragraph -->\r\n<p>&nbsp;</p>\r\n<!-- /wp:paragraph -->\r\n\r\n<!-- wp:paragraph -->\r\n<p>&nbsp;</p>\r\n<!-- /wp:paragraph -->\r\n\r\n<!-- wp:paragraph -->\r\n<p>&nbsp;</p>\r\n<!-- /wp:paragraph -->', 'Trang chủ', '', 'inherit', 'closed', 'closed', '', '12-revision-v1', '', '', '2021-08-26 17:13:24', '2021-08-26 10:13:24', '', 12, 'http://wordpress.local/GCO/maytracdiasaoviet/?p=378', 0, 'revision', '', 0),
(379, 1, '2021-08-27 10:43:27', '2021-08-27 03:43:27', '<div class=\"single-product-form-quote-title\">Báo Giá Máy</div>\r\n<div class=\"single-product-form-quote-desc\">Quý khách vui lòng cung cấp thông tin dưới đây để nhận báo giá!</div>\r\n\r\n<div class=\"form-group\">\r\n<label for=\"your-name\">Họ và tên (*)</label>\r\n    [text* your-name id:your-name class:form-control]\r\n</div>\r\n<div class=\"form-group\">\r\n<label for=\"your-phone\">Số điện thoại (*)</label>\r\n    [tel* your-phone id:your-phone class:form-control]\r\n</div>\r\n<div class=\"form-group\">\r\n<label for=\"your-email\">Email (*)</label>\r\n    [email* your-email id:your-email class:form-control]\r\n</div>\r\n<div class=\"form-group\">\r\n<label for=\"your-type\">Loại máy cần nhận báo giá (*)</label>\r\n    [text* your-type id:your-type class:form-control]\r\n</div>\r\n\r\n[submit class:btn class:w-100 class:bold class:buy-btn \"Yêu cầu gửi báo giá\"]\n1\n[_site_title] \"[your-subject]\"\n[_site_title] <wordpress@wordpress.local>\n[_site_admin_email]\nGửi đến từ: [your-name] <[your-email]>\r\nSố điện thoại: [your-phone]\r\nLoại máy cần nhận báo giá: [your-type]\r\n\r\n-- \r\nEmail này được gửi đến từ form liên hệ của website [_site_title] ([_site_url])\nReply-To: [your-email]\n\n\n\n\n[_site_title] \"[your-subject]\"\n[_site_title] <wordpress@wordpress.local>\n[your-email]\nNội dung thông điệp:\r\n[your-message]\r\n\r\n-- \r\nEmail này được gửi đến từ form liên hệ của website [_site_title] ([_site_url])\nReply-To: [_site_admin_email]\n\n\n\nXin cảm ơn, form đã được gửi thành công.\nCó lỗi xảy ra trong quá trình gửi. Xin vui lòng thử lại hoặc liên hệ người quản trị website.\nCó một hoặc nhiều mục nhập có lỗi. Vui lòng kiểm tra và thử lại.\nCó lỗi xảy ra trong quá trình gửi. Xin vui lòng thử lại hoặc liên hệ người quản trị website.\nBạn phải chấp nhận điều khoản trước khi gửi form.\nMục này là bắt buộc.\nNhập quá số kí tự cho phép.\nNhập ít hơn số kí tự tối thiểu.\nTải file lên không thành công.\nBạn không được phép tải lên file theo định dạng này.\nFile kích thước quá lớn.\nTải file lên không thành công.\nĐịnh dạng ngày tháng không hợp lệ.\nNgày này trước ngày sớm nhất được cho phép.\nNgày này quá ngày gần nhất được cho phép.\nĐịnh dạng số không hợp lệ.\nCon số nhỏ hơn số nhỏ nhất cho phép.\nCon số lớn hơn số lớn nhất cho phép.\nCâu trả lời chưa đúng.\nĐịa chỉ e-mail không hợp lệ.\nURL không hợp lệ.\nSố điện thoại không hợp lệ.', 'Đăng ký nhận báo giá', '', 'publish', 'closed', 'closed', '', 'dang-ky-nhan-bao-gia', '', '', '2021-08-27 11:06:44', '2021-08-27 04:06:44', '', 0, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=wpcf7_contact_form&#038;p=379', 0, 'wpcf7_contact_form', '', 0),
(380, 1, '2021-08-27 10:47:25', '2021-08-27 03:47:25', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'Form đăng ký nhận báo giá', 'form_dang_ky_nhận_bao_gia', 'publish', 'closed', 'closed', '', 'field_61285ff1dfcaa', '', '', '2021-09-06 18:03:37', '2021-09-06 11:03:37', '', 101, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&#038;p=380', 31, 'acf-field', '', 0),
(381, 1, '2021-08-27 10:47:25', '2021-08-27 03:47:25', 'a:11:{s:4:\"type\";s:11:\"post_object\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"post_type\";a:1:{i:0;s:18:\"wpcf7_contact_form\";}s:8:\"taxonomy\";s:0:\"\";s:10:\"allow_null\";i:0;s:8:\"multiple\";i:0;s:13:\"return_format\";s:2:\"id\";s:2:\"ui\";i:1;}', 'Chọn Form', 'single_product_form_quote', 'publish', 'closed', 'closed', '', 'field_61286003dfcab', '', '', '2021-09-06 18:03:37', '2021-09-06 11:03:37', '', 101, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&#038;p=381', 32, 'acf-field', '', 0),
(383, 1, '2021-09-03 14:15:59', '2021-09-03 07:15:59', ' ', '', '', 'publish', 'closed', 'closed', '', '383', '', '', '2021-09-03 14:15:59', '2021-09-03 07:15:59', '', 26, 'http://wordpress.local/GCO/maytracdiasaoviet/?p=383', 2, 'nav_menu_item', '', 0),
(384, 1, '2021-09-03 14:15:59', '2021-09-03 07:15:59', ' ', '', '', 'publish', 'closed', 'closed', '', '384', '', '', '2021-09-03 14:15:59', '2021-09-03 07:15:59', '', 26, 'http://wordpress.local/GCO/maytracdiasaoviet/?p=384', 3, 'nav_menu_item', '', 0),
(385, 1, '2021-09-06 17:58:33', '2021-09-06 10:58:33', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Khẩu hiệu', 'customer_slogan', 'publish', 'closed', 'closed', '', 'field_6135f44287cf2', '', '', '2021-09-06 17:58:33', '2021-09-06 10:58:33', '', 101, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&p=385', 4, 'acf-field', '', 0),
(386, 1, '2021-09-06 18:03:36', '2021-09-06 11:03:36', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'Header', '_copy', 'publish', 'closed', 'closed', '', 'field_6135f5423fc6d', '', '', '2021-09-06 18:03:36', '2021-09-06 11:03:36', '', 101, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&p=386', 9, 'acf-field', '', 0),
(387, 1, '2021-09-06 18:03:36', '2021-09-06 11:03:36', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Dịch vụ', 'h_service', 'publish', 'closed', 'closed', '', 'field_6135f55e3fc6e', '', '', '2021-09-06 18:03:59', '2021-09-06 11:03:59', '', 101, 'http://wordpress.local/GCO/maytracdiasaoviet/?post_type=acf-field&#038;p=387', 10, 'acf-field', '', 0),
(388, 1, '2021-12-30 10:16:16', '0000-00-00 00:00:00', '', 'Lưu bản nháp tự động', '', 'auto-draft', 'open', 'open', '', '', '', '', '2021-12-30 10:16:16', '0000-00-00 00:00:00', '', 0, 'http://wordpress.local/GCO/maytracdiasaoviet/?p=388', 0, 'post', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_termmeta`
--

CREATE TABLE `wp_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_termmeta`
--

INSERT INTO `wp_termmeta` (`meta_id`, `term_id`, `meta_key`, `meta_value`) VALUES
(1, 25, 'product_count_product_cat', '0'),
(2, 26, 'order', '3'),
(3, 26, 'display_type', ''),
(4, 26, 'thumbnail_id', '329'),
(5, 27, 'order', '6'),
(6, 27, 'display_type', ''),
(7, 27, 'thumbnail_id', '0'),
(8, 28, 'order', '5'),
(9, 28, 'display_type', ''),
(10, 28, 'thumbnail_id', '0'),
(11, 29, 'order', '4'),
(12, 29, 'display_type', ''),
(13, 29, 'thumbnail_id', '0'),
(14, 30, 'order', '7'),
(15, 30, 'display_type', ''),
(16, 30, 'thumbnail_id', '0'),
(17, 26, 'product_count_product_cat', '4'),
(18, 28, 'product_count_product_cat', '2'),
(19, 29, 'product_count_product_cat', '1'),
(20, 27, 'product_count_product_cat', '3'),
(21, 30, 'product_count_product_cat', '2'),
(22, 31, 'order', '2'),
(23, 31, 'display_type', ''),
(24, 31, 'thumbnail_id', '0'),
(25, 31, 'product_count_product_cat', '5'),
(26, 25, 'order', '1');

-- --------------------------------------------------------

--
-- Table structure for table `wp_terms`
--

CREATE TABLE `wp_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_terms`
--

INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Tin tức', 'tintuc', 0),
(2, 'Tin tức nổi bật', 'tin-tuc-noi-bat', 0),
(5, 'Thẻ của Bài viết 2', 'the-cua-bai-viet-2', 0),
(6, 'Thẻ của Bài viết 1', 'the-cua-bai-viet-1', 0),
(7, 'Menu chính', 'menu-chinh', 0),
(8, 'Chuyên mục dịch vụ 1', 'chuyen-muc-dich-vu-1', 0),
(9, 'Danh mục sản phẩm', 'danh-muc-san-pham', 0),
(10, 'Giới thiệu', 'gioi-thieu', 0),
(11, 'Chính sách', 'chinh-sach', 0),
(12, 'simple', 'simple', 0),
(13, 'grouped', 'grouped', 0),
(14, 'variable', 'variable', 0),
(15, 'external', 'external', 0),
(16, 'exclude-from-search', 'exclude-from-search', 0),
(17, 'exclude-from-catalog', 'exclude-from-catalog', 0),
(18, 'featured', 'featured', 0),
(19, 'outofstock', 'outofstock', 0),
(20, 'rated-1', 'rated-1', 0),
(21, 'rated-2', 'rated-2', 0),
(22, 'rated-3', 'rated-3', 0),
(23, 'rated-4', 'rated-4', 0),
(24, 'rated-5', 'rated-5', 0),
(25, 'Chưa phân loại', 'chua-phan-loai', 0),
(26, 'Máy thủy bình', 'may-thuy-binh', 0),
(27, 'Máy toàn đạc điện tử', 'may-toan-dac-dien-tu', 0),
(28, 'Leica', 'leica', 0),
(29, 'Myzox', 'myzox', 0),
(30, 'Leica', 'leica-may-toan-dac-dien-tu', 0),
(31, 'Máy Định Vị GPS RTK', 'may-dinh-vi-gps-rtk', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_relationships`
--

CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_term_relationships`
--

INSERT INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(29, 2, 0),
(29, 6, 0),
(31, 2, 0),
(31, 5, 0),
(55, 7, 0),
(57, 7, 0),
(64, 2, 0),
(71, 1, 0),
(73, 1, 0),
(101, 1, 0),
(117, 1, 0),
(125, 1, 0),
(138, 1, 0),
(166, 1, 0),
(236, 1, 0),
(256, 7, 0),
(266, 7, 0),
(281, 10, 0),
(282, 10, 0),
(283, 11, 0),
(284, 11, 0),
(297, 1, 0),
(310, 12, 0),
(310, 18, 0),
(310, 23, 0),
(310, 26, 0),
(310, 27, 0),
(310, 28, 0),
(310, 29, 0),
(310, 30, 0),
(310, 31, 0),
(314, 7, 0),
(317, 7, 0),
(319, 12, 0),
(319, 18, 0),
(319, 26, 0),
(319, 27, 0),
(319, 28, 0),
(319, 31, 0),
(320, 12, 0),
(320, 18, 0),
(320, 31, 0),
(321, 7, 0),
(322, 9, 0),
(323, 9, 0),
(326, 12, 0),
(326, 18, 0),
(326, 26, 0),
(326, 31, 0),
(327, 12, 0),
(327, 26, 0),
(327, 27, 0),
(327, 30, 0),
(327, 31, 0),
(383, 9, 0),
(384, 9, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_taxonomy`
--

CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_term_taxonomy`
--

INSERT INTO `wp_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', 'Mô tả của Chuyên mục 3', 0, 1),
(2, 2, 'category', 'Mô tả của Chuyên mục 1', 0, 3),
(5, 5, 'post_tag', '', 0, 1),
(6, 6, 'post_tag', '', 0, 1),
(7, 7, 'nav_menu', '', 0, 7),
(8, 8, 'services-cat', '', 0, 0),
(9, 9, 'nav_menu', '', 0, 4),
(10, 10, 'nav_menu', '', 0, 2),
(11, 11, 'nav_menu', '', 0, 2),
(12, 12, 'product_type', '', 0, 5),
(13, 13, 'product_type', '', 0, 0),
(14, 14, 'product_type', '', 0, 0),
(15, 15, 'product_type', '', 0, 0),
(16, 16, 'product_visibility', '', 0, 0),
(17, 17, 'product_visibility', '', 0, 0),
(18, 18, 'product_visibility', '', 0, 4),
(19, 19, 'product_visibility', '', 0, 0),
(20, 20, 'product_visibility', '', 0, 0),
(21, 21, 'product_visibility', '', 0, 0),
(22, 22, 'product_visibility', '', 0, 0),
(23, 23, 'product_visibility', '', 0, 1),
(24, 24, 'product_visibility', '', 0, 0),
(25, 25, 'product_cat', '', 0, 0),
(26, 26, 'product_cat', '', 0, 4),
(27, 27, 'product_cat', '', 0, 3),
(28, 28, 'product_cat', '', 26, 2),
(29, 29, 'product_cat', '', 26, 1),
(30, 30, 'product_cat', '', 27, 2),
(31, 31, 'product_cat', '', 0, 5);

-- --------------------------------------------------------

--
-- Table structure for table `wp_usermeta`
--

CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_usermeta`
--

INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'admin'),
(2, 1, 'first_name', 'Tiệp'),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'wp_capabilities', 'a:13:{s:13:\"administrator\";b:1;s:26:\"wpcf_custom_post_type_view\";b:1;s:26:\"wpcf_custom_post_type_edit\";b:1;s:33:\"wpcf_custom_post_type_edit_others\";b:1;s:25:\"wpcf_custom_taxonomy_view\";b:1;s:25:\"wpcf_custom_taxonomy_edit\";b:1;s:32:\"wpcf_custom_taxonomy_edit_others\";b:1;s:22:\"wpcf_custom_field_view\";b:1;s:22:\"wpcf_custom_field_edit\";b:1;s:29:\"wpcf_custom_field_edit_others\";b:1;s:25:\"wpcf_user_meta_field_view\";b:1;s:25:\"wpcf_user_meta_field_edit\";b:1;s:32:\"wpcf_user_meta_field_edit_others\";b:1;}'),
(13, 1, 'wp_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', ''),
(15, 1, 'show_welcome_panel', '0'),
(17, 1, 'wp_dashboard_quick_press_last_post_id', '388'),
(18, 1, 'community-events-location', 'a:1:{s:2:\"ip\";s:9:\"127.0.0.0\";}'),
(19, 1, 'wp_user-settings', 'libraryContent=browse&posts_list_mode=list&hidetb=1&mfold=o&editor=tinymce'),
(20, 1, 'wp_user-settings-time', '1629891642'),
(21, 1, 'managenav-menuscolumnshidden', 'a:4:{i:0;s:11:\"link-target\";i:1;s:15:\"title-attribute\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";}'),
(22, 1, 'metaboxhidden_nav-menus', 'a:1:{i:0;s:12:\"add-post_tag\";}'),
(23, 1, 'toolset_admin_notices_manager', 'a:1:{s:17:\"dismissed-notices\";a:1:{s:18:\"types-3-0-features\";b:1;}}'),
(24, 1, 'closedpostboxes_dashboard', 'a:0:{}'),
(25, 1, 'metaboxhidden_dashboard', 'a:3:{i:0;s:21:\"dashboard_site_health\";i:1;s:21:\"dashboard_quick_press\";i:2;s:17:\"dashboard_primary\";}'),
(26, 1, 'meta-box-order_dashboard', 'a:4:{s:6:\"normal\";s:41:\"dashboard_site_health,dashboard_right_now\";s:4:\"side\";s:58:\"dashboard_quick_press,dashboard_primary,dashboard_activity\";s:7:\"column3\";s:0:\"\";s:7:\"column4\";s:0:\"\";}'),
(27, 1, '_types_feedback_dont_show_until', '1621960643'),
(28, 1, 'manageedit-postcolumnshidden', 'a:2:{i:0;s:4:\"tags\";i:1;s:8:\"comments\";}'),
(29, 1, 'edit_post_per_page', '20'),
(30, 1, 'manageedit-categorycolumnshidden', 'a:1:{i:0;s:11:\"description\";}'),
(31, 1, 'edit_category_per_page', '20'),
(32, 1, 'closedpostboxes_post', 'a:0:{}'),
(33, 1, 'metaboxhidden_post', 'a:7:{i:0;s:16:\"tagsdiv-post_tag\";i:1;s:12:\"revisionsdiv\";i:2;s:13:\"trackbacksdiv\";i:3;s:16:\"commentstatusdiv\";i:4;s:11:\"commentsdiv\";i:5;s:7:\"slugdiv\";i:6;s:9:\"authordiv\";}'),
(34, 1, 'manageedit-pagecolumnshidden', 'a:1:{i:0;s:8:\"comments\";}'),
(35, 1, 'edit_page_per_page', '20'),
(36, 1, 'nav_menu_recently_edited', '9'),
(37, 1, 'manageedit-servicecolumnshidden', 'a:1:{i:0;s:8:\"comments\";}'),
(38, 1, 'edit_service_per_page', '20'),
(39, 1, 'closedpostboxes_toplevel_page_theme-settings', 'a:0:{}'),
(40, 1, 'metaboxhidden_toplevel_page_theme-settings', 'a:0:{}'),
(41, 1, 'session_tokens', 'a:1:{s:64:\"d338478dbbe251ab3b46a586aec9d7a786e44687ef96802d39ac226340bc3a04\";a:4:{s:10:\"expiration\";i:1642043764;s:2:\"ip\";s:9:\"127.0.0.1\";s:2:\"ua\";s:121:\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36\";s:5:\"login\";i:1640834164;}}'),
(42, 1, 'wpcf7_hide_welcome_panel_on', 'a:1:{i:0;s:3:\"5.4\";}'),
(43, 1, 'manageedit-productcolumnshidden', 'a:2:{i:0;s:3:\"sku\";i:1;s:11:\"product_tag\";}'),
(44, 1, 'edit_product_per_page', '20'),
(45, 1, '_woocommerce_tracks_anon_id', 'woo:4NyVc1kLSjfIWvMcEvRJhkWN'),
(46, 1, 'last_update', '1629801999'),
(47, 1, 'woocommerce_admin_activity_panel_inbox_last_read', '1629800828484'),
(48, 1, 'wc_last_active', '1640822400'),
(50, 1, 'billing_first_name', 'Tiệp'),
(51, 1, 'billing_address_1', 'Hà Nội'),
(52, 1, 'billing_email', 'tiepnguyen220194@gmail.com'),
(53, 1, 'billing_phone', '0359117322'),
(54, 1, 'shipping_method', ''),
(55, 1, '_order_count', '1'),
(56, 1, '_last_order', '312'),
(57, 1, '_woocommerce_persistent_cart_1', 'a:1:{s:4:\"cart\";a:2:{s:32:\"a666587afda6e89aec274a3657558a27\";a:11:{s:3:\"key\";s:32:\"a666587afda6e89aec274a3657558a27\";s:10:\"product_id\";i:326;s:12:\"variation_id\";i:0;s:9:\"variation\";a:0:{}s:8:\"quantity\";i:1;s:9:\"data_hash\";s:32:\"b5c1d5ca8bae6d4896cf1807cdf763f0\";s:13:\"line_tax_data\";a:2:{s:8:\"subtotal\";a:0:{}s:5:\"total\";a:0:{}}s:13:\"line_subtotal\";d:950000;s:17:\"line_subtotal_tax\";i:0;s:10:\"line_total\";d:950000;s:8:\"line_tax\";i:0;}s:32:\"06eb61b839a0cefee4967c67ccb099dc\";a:11:{s:3:\"key\";s:32:\"06eb61b839a0cefee4967c67ccb099dc\";s:10:\"product_id\";i:310;s:12:\"variation_id\";i:0;s:9:\"variation\";a:0:{}s:8:\"quantity\";i:2;s:9:\"data_hash\";s:32:\"b5c1d5ca8bae6d4896cf1807cdf763f0\";s:13:\"line_tax_data\";a:2:{s:8:\"subtotal\";a:0:{}s:5:\"total\";a:0:{}}s:13:\"line_subtotal\";d:1900000;s:17:\"line_subtotal_tax\";i:0;s:10:\"line_total\";d:1900000;s:8:\"line_tax\";i:0;}}}'),
(58, 1, 'meta-box-order_product', 'a:4:{s:15:\"acf_after_title\";s:0:\"\";s:4:\"side\";s:84:\"submitdiv,product_catdiv,tagsdiv-product_tag,postimagediv,woocommerce-product-images\";s:6:\"normal\";s:80:\"woocommerce-product-data,acf-group_612621c53aab5,slugdiv,postexcerpt,commentsdiv\";s:8:\"advanced\";s:0:\"\";}'),
(59, 1, 'screen_layout_product', '2');

-- --------------------------------------------------------

--
-- Table structure for table `wp_users`
--

CREATE TABLE `wp_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_users`
--

INSERT INTO `wp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'admin', '$P$BoObrfsFAJIyYfoa.Dhvh07.B.KKg90', 'admin', 'tiepnguyen220194@gmail.com', 'http://wordpress.local/GCO/maytracdiasaoviet', '2020-12-07 09:44:42', '', 0, 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `wp_wc_admin_notes`
--

CREATE TABLE `wp_wc_admin_notes` (
  `note_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `locale` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `title` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `content_data` longtext COLLATE utf8mb4_unicode_520_ci,
  `status` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `source` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_reminder` datetime DEFAULT NULL,
  `is_snoozable` tinyint(1) NOT NULL DEFAULT '0',
  `layout` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `image` varchar(200) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `icon` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'info'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_wc_admin_notes`
--

INSERT INTO `wp_wc_admin_notes` (`note_id`, `name`, `type`, `locale`, `title`, `content`, `content_data`, `status`, `source`, `date_created`, `date_reminder`, `is_snoozable`, `layout`, `image`, `is_deleted`, `icon`) VALUES
(1, 'wayflyer_q3_2021', 'marketing', 'en_US', 'Grow your revenue with Wayflyer financing and analytics', 'Flexible financing tailored to your needs by <a href=\"https://woocommerce.com/products/wayflyer/\">Wayflyer</a> – one fee, no interest rates, penalties, equity, or personal guarantees. Based on your store\'s performance, Wayflyer can provide the financing you need to grow and the analytical insights to help you spend it.', '{}', 'pending', 'woocommerce.com', '2021-08-24 10:21:43', NULL, 0, 'plain', '', 0, 'info'),
(2, 'eu_vat_changes_2021', 'marketing', 'en_US', 'Get your business ready for the new EU tax regulations', 'On July 1, 2021, new taxation rules will come into play when the <a href=\"https://ec.europa.eu/taxation_customs/business/vat/modernising-vat-cross-border-ecommerce_en\">European Union (EU) Value-Added Tax (VAT) eCommerce package</a> takes effect.<br /><br />The new regulations will impact virtually every B2C business involved in cross-border eCommerce trade with the EU.<br /><br />We therefore recommend <a href=\"https://woocommerce.com/posts/new-eu-vat-regulations\">familiarizing yourself with the new updates</a>, and consult with a tax professional to ensure your business is following regulations and best practice.', '{}', 'pending', 'woocommerce.com', '2021-08-24 10:21:43', NULL, 0, 'plain', '', 0, 'info'),
(3, 'paypal_ppcp_gtm_2021', 'marketing', 'en_US', 'Offer more options with the new PayPal', 'Get the latest PayPal extension for a full suite of payment methods with extensive currency and country coverage.', '{}', 'pending', 'woocommerce.com', '2021-08-24 10:21:43', NULL, 0, 'plain', '', 0, 'info'),
(4, 'facebook_pixel_api_2021', 'marketing', 'en_US', 'Improve the performance of your Facebook ads', 'Enable Facebook Pixel and Conversions API through the latest version of Facebook for WooCommerce for improved measurement and ad targeting capabilities.', '{}', 'pending', 'woocommerce.com', '2021-08-24 10:21:43', NULL, 0, 'plain', '', 0, 'info'),
(5, 'facebook_ec_2021', 'marketing', 'en_US', 'Sync your product catalog with Facebook to help boost sales', 'A single click adds all products to your Facebook Business Page shop. Product changes are automatically synced, with the flexibility to control which products are listed.', '{}', 'pending', 'woocommerce.com', '2021-08-24 10:21:43', NULL, 0, 'plain', '', 0, 'info'),
(6, 'ecomm-need-help-setting-up-your-store', 'info', 'en_US', 'Need help setting up your Store?', 'Schedule a free 30-min <a href=\"https://wordpress.com/support/concierge-support/\">quick start session</a> and get help from our specialists. We’re happy to walk through setup steps, show you around the WordPress.com dashboard, troubleshoot any issues you may have, and help you the find the features you need to accomplish your goals for your site.', '{}', 'pending', 'woocommerce.com', '2021-08-24 10:21:43', NULL, 0, 'plain', '', 0, 'info'),
(7, 'woocommerce-services', 'info', 'en_US', 'WooCommerce Shipping & Tax', 'WooCommerce Shipping &amp; Tax helps get your store “ready to sell” as quickly as possible. You create your products. We take care of tax calculation, payment processing, and shipping label printing! Learn more about the extension that you just installed.', '{}', 'pending', 'woocommerce.com', '2021-08-24 10:21:43', NULL, 0, 'plain', '', 0, 'info'),
(8, 'ecomm-unique-shopping-experience', 'info', 'en_US', 'For a shopping experience as unique as your customers', 'Product Add-Ons allow your customers to personalize products while they’re shopping on your online store. No more follow-up email requests—customers get what they want, before they’re done checking out. Learn more about this extension that comes included in your plan.', '{}', 'pending', 'woocommerce.com', '2021-08-24 10:21:43', NULL, 0, 'plain', '', 0, 'info'),
(9, 'wc-admin-getting-started-in-ecommerce', 'info', 'en_US', 'Getting Started in eCommerce - webinar', 'We want to make eCommerce and this process of getting started as easy as possible for you. Watch this webinar to get tips on how to have our store up and running in a breeze.', '{}', 'pending', 'woocommerce.com', '2021-08-24 10:21:43', NULL, 0, 'plain', '', 0, 'info'),
(10, 'your-first-product', 'info', 'en_US', 'Your first product', 'That\'s huge! You\'re well on your way to building a successful online store — now it’s time to think about how you\'ll fulfill your orders.<br /><br />Read our shipping guide to learn best practices and options for putting together your shipping strategy. And for WooCommerce stores in the United States, you can print discounted shipping labels via USPS with <a href=\"https://href.li/?https://woocommerce.com/shipping\" target=\"_blank\">WooCommerce Shipping</a>.', '{}', 'unactioned', 'woocommerce.com', '2021-08-25 10:27:14', NULL, 0, 'plain', '', 0, 'info'),
(11, 'wc-square-apple-pay-boost-sales', 'marketing', 'en_US', 'Boost sales with Apple Pay', 'Now that you accept Apple Pay® with Square you can increase conversion rates by letting your customers know that Apple Pay® is available. Here’s a marketing guide to help you get started.', '{}', 'pending', 'woocommerce.com', '2021-08-24 10:21:43', NULL, 0, 'plain', '', 0, 'info'),
(12, 'wc-square-apple-pay-grow-your-business', 'marketing', 'en_US', 'Grow your business with Square and Apple Pay ', 'Now more than ever, shoppers want a fast, simple, and secure online checkout experience. Increase conversion rates by letting your customers know that you now accept Apple Pay®.', '{}', 'pending', 'woocommerce.com', '2021-08-24 10:21:43', NULL, 0, 'plain', '', 0, 'info'),
(13, 'wcpay-apple-pay-is-now-available', 'marketing', 'en_US', 'Apple Pay is now available with WooCommerce Payments!', 'Increase your conversion rate by offering a fast and secure checkout with <a href=\"https://woocommerce.com/apple-pay/?utm_source=inbox&amp;utm_medium=product&amp;utm_campaign=wcpay_applepay\" target=\"_blank\">Apple Pay</a>®. It’s free to get started with <a href=\"https://woocommerce.com/payments/?utm_source=inbox&amp;utm_medium=product&amp;utm_campaign=wcpay_applepay\" target=\"_blank\">WooCommerce Payments</a>.', '{}', 'pending', 'woocommerce.com', '2021-08-24 10:21:43', NULL, 0, 'plain', '', 0, 'info'),
(14, 'wcpay-apple-pay-boost-sales', 'marketing', 'en_US', 'Boost sales with Apple Pay', 'Now that you accept Apple Pay® with WooCommerce Payments you can increase conversion rates by letting your customers know that Apple Pay® is available. Here’s a marketing guide to help you get started.', '{}', 'pending', 'woocommerce.com', '2021-08-24 10:21:43', NULL, 0, 'plain', '', 0, 'info'),
(15, 'wcpay-apple-pay-grow-your-business', 'marketing', 'en_US', 'Grow your business with WooCommerce Payments and Apple Pay', 'Now more than ever, shoppers want a fast, simple, and secure online checkout experience. Increase conversion rates by letting your customers know that you now accept Apple Pay®.', '{}', 'pending', 'woocommerce.com', '2021-08-24 10:21:43', NULL, 0, 'plain', '', 0, 'info'),
(16, 'wc-admin-optimizing-the-checkout-flow', 'info', 'en_US', 'Optimizing the checkout flow', 'It\'s crucial to get your store\'s checkout as smooth as possible to avoid losing sales. Let\'s take a look at how you can optimize the checkout experience for your shoppers.', '{}', 'pending', 'woocommerce.com', '2021-08-24 10:21:43', NULL, 0, 'plain', '', 0, 'info'),
(17, 'wc-admin-first-five-things-to-customize', 'info', 'en_US', 'The first 5 things to customize in your store', 'Deciding what to start with first is tricky. To help you properly prioritize, we\'ve put together this short list of the first few things you should customize in WooCommerce.', '{}', 'unactioned', 'woocommerce.com', '2021-08-26 10:22:39', NULL, 0, 'plain', '', 0, 'info'),
(18, 'wc-payments-qualitative-feedback', 'info', 'en_US', 'WooCommerce Payments setup - let us know what you think', 'Congrats on enabling WooCommerce Payments for your store. Please share your feedback in this 2 minute survey to help us improve the setup process.', '{}', 'pending', 'woocommerce.com', '2021-08-24 10:21:43', NULL, 0, 'plain', '', 0, 'info'),
(19, 'share-your-feedback-on-paypal', 'info', 'en_US', 'Share your feedback on PayPal', 'Share your feedback in this 2 minute survey about how we can make the process of accepting payments more useful for your store.', '{}', 'pending', 'woocommerce.com', '2021-08-24 10:21:43', NULL, 0, 'plain', '', 0, 'info'),
(20, 'wcpay_instant_deposits_gtm_2021', 'marketing', 'en_US', 'Get paid within minutes – Instant Deposits for WooCommerce Payments', 'Stay flexible with immediate access to your funds when you need them – including nights, weekends, and holidays. With <a href=\"https://woocommerce.com/products/woocommerce-payments/?utm_source=inbox&amp;utm_medium=product&amp;utm_campaign=wcpay_instant_deposits\">WooCommerce Payments\'</a> new Instant Deposits feature, you’re able to transfer your earnings to a debit card within minutes.', '{}', 'pending', 'woocommerce.com', '2021-08-24 10:21:43', NULL, 0, 'plain', '', 0, 'info'),
(21, 'google_listings_and_ads_install', 'marketing', 'en_US', 'Drive traffic and sales with Google', 'Reach online shoppers to drive traffic and sales for your store by showcasing products across Google, for free or with ads.', '{}', 'pending', 'woocommerce.com', '2021-08-24 10:21:43', NULL, 0, 'plain', '', 0, 'info'),
(22, 'wc-subscriptions-security-update-3-0-15', 'info', 'en_US', 'WooCommerce Subscriptions security update!', 'We recently released an important security update to WooCommerce Subscriptions. To ensure your site\'s data is protected, please upgrade <strong>WooCommerce Subscriptions to version 3.0.15</strong> or later.<br /><br />Click the button below to view and update to the latest Subscriptions version, or log in to <a href=\"https://woocommerce.com/my-dashboard\">WooCommerce.com Dashboard</a> and navigate to your <strong>Downloads</strong> page.<br /><br />We recommend always using the latest version of WooCommerce Subscriptions, and other software running on your site, to ensure maximum security.<br /><br />If you have any questions we are here to help — just <a href=\"https://woocommerce.com/my-account/create-a-ticket/\">open a ticket</a>.', '{}', 'pending', 'woocommerce.com', '2021-08-24 10:21:43', NULL, 0, 'plain', '', 0, 'info'),
(23, 'woocommerce-core-update-5-4-0', 'info', 'en_US', 'Update to WooCommerce 5.4.1 now', 'WooCommerce 5.4.1 addresses a checkout issue discovered in WooCommerce 5.4. We recommend upgrading to WooCommerce 5.4.1 as soon as possible.', '{}', 'pending', 'woocommerce.com', '2021-08-24 10:21:43', NULL, 0, 'plain', '', 0, 'info'),
(24, 'wcpay-promo-2020-11', 'marketing', 'en_US', 'wcpay-promo-2020-11', 'wcpay-promo-2020-11', '{}', 'pending', 'woocommerce.com', '2021-08-24 10:21:43', NULL, 0, 'plain', '', 0, 'info'),
(25, 'wcpay-promo-2020-12', 'marketing', 'en_US', 'wcpay-promo-2020-12', 'wcpay-promo-2020-12', '{}', 'pending', 'woocommerce.com', '2021-08-24 10:21:43', NULL, 0, 'plain', '', 0, 'info'),
(26, 'wcpay-promo-2021-6-incentive-1', 'marketing', 'en_US', 'Simplify the payments process for you and your customers with WooCommerce Payments', 'With <a href=\"https://woocommerce.com/payments/?utm_medium=notification&amp;utm_source=product&amp;utm_campaign=wcpay601\">WooCommerce Payments</a>, you can securely accept all major cards, Apple Pay®, and recurring revenue in over 100 currencies.\n				Built into your store’s WooCommerce dashboard, track cash flow and manage all of your transactions in one place – with no setup costs or monthly fees.\n				<br /><br />\n				By clicking \"Get WooCommerce Payments,\" you agree to the <a href=\"https://wordpress.com/tos/?utm_medium=notification&amp;utm_source=product&amp;utm_campaign=wcpay601\">Terms of Service</a>\n				and acknowledge you have read the <a href=\"https://automattic.com/privacy/\">Privacy Policy</a>.\n				', '{}', 'pending', 'woocommerce.com', '2021-08-24 10:21:43', NULL, 0, 'plain', '', 0, 'info'),
(27, 'wcpay-promo-2021-6-incentive-2', 'marketing', 'en_US', 'Simplify the payments process for you and your customers with WooCommerce Payments', 'With <a href=\"https://woocommerce.com/payments/?utm_medium=notification&amp;utm_source=product&amp;utm_campaign=wcpay601\">WooCommerce Payments</a>, you can securely accept all major cards, Apple Pay®, and recurring revenue in over 100 currencies.\n				Built into your store’s WooCommerce dashboard, track cash flow and manage all of your transactions in one place – with no setup costs or monthly fees.\n				<br /><br />\n				By clicking \"Get WooCommerce Payments,\" you agree to the <a href=\"https://wordpress.com/tos/?utm_medium=notification&amp;utm_source=product&amp;utm_campaign=wcpay601\">Terms of Service</a>\n				and acknowledge you have read the <a href=\"https://automattic.com/privacy/\">Privacy Policy</a>.\n				', '{}', 'pending', 'woocommerce.com', '2021-08-24 10:21:43', NULL, 0, 'plain', '', 0, 'info'),
(28, 'ppxo-pps-upgrade-paypal-payments-1', 'info', 'en_US', 'Get the latest PayPal extension for WooCommerce', 'Heads up! There\'s a new PayPal on the block!<br /><br />Now is a great time to upgrade to our latest <a href=\"https://woocommerce.com/products/woocommerce-paypal-payments/\" target=\"_blank\">PayPal extension</a> to continue to receive support and updates with PayPal.<br /><br />Get access to a full suite of PayPal payment methods, extensive currency and country coverage, and pay later options with the all-new PayPal extension for WooCommerce.', '{}', 'unactioned', 'woocommerce.com', '2021-08-24 10:21:43', NULL, 0, 'plain', '', 0, 'info'),
(29, 'ppxo-pps-upgrade-paypal-payments-2', 'info', 'en_US', 'Upgrade your PayPal experience!', 'We\'ve developed a whole new <a href=\"https://woocommerce.com/products/woocommerce-paypal-payments/\" target=\"_blank\">PayPal extension for WooCommerce</a> that combines the best features of our many PayPal extensions into just one extension.<br /><br />Get access to a full suite of PayPal payment methods, extensive currency and country coverage, offer subscription and recurring payments, and the new PayPal pay later options.<br /><br />Start using our latest PayPal today to continue to receive support and updates.', '{}', 'pending', 'woocommerce.com', '2021-08-24 10:21:43', NULL, 0, 'plain', '', 0, 'info'),
(30, 'woocommerce-core-sqli-july-2021-need-to-update', 'update', 'en_US', 'Action required: Critical vulnerabilities in WooCommerce', 'In response to a critical vulnerability identified on July 13, 2021, we are working with the WordPress Plugins Team to deploy software updates to stores running WooCommerce (versions 3.3 to 5.5) and the WooCommerce Blocks feature plugin (versions 2.5 to 5.5).<br /><br />Our investigation into this vulnerability is ongoing, but <strong>we wanted to let you know now about the importance of updating immediately</strong>.<br /><br />For more information on which actions you should take, as well as answers to FAQs, please urgently review our blog post detailing this issue.', '{}', 'pending', 'woocommerce.com', '2021-08-24 10:21:43', NULL, 0, 'plain', '', 0, 'info'),
(31, 'woocommerce-blocks-sqli-july-2021-need-to-update', 'update', 'en_US', 'Action required: Critical vulnerabilities in WooCommerce Blocks', 'In response to a critical vulnerability identified on July 13, 2021, we are working with the WordPress Plugins Team to deploy software updates to stores running WooCommerce (versions 3.3 to 5.5) and the WooCommerce Blocks feature plugin (versions 2.5 to 5.5).<br /><br />Our investigation into this vulnerability is ongoing, but <strong>we wanted to let you know now about the importance of updating immediately</strong>.<br /><br />For more information on which actions you should take, as well as answers to FAQs, please urgently review our blog post detailing this issue.', '{}', 'pending', 'woocommerce.com', '2021-08-24 10:21:43', NULL, 0, 'plain', '', 0, 'info'),
(32, 'woocommerce-core-sqli-july-2021-store-patched', 'update', 'en_US', 'Solved: Critical vulnerabilities patched in WooCommerce', 'In response to a critical vulnerability identified on July 13, 2021, we worked with the WordPress Plugins Team to deploy software updates to stores running WooCommerce (versions 3.3 to 5.5) and the WooCommerce Blocks feature plugin (versions 2.5 to 5.5).<br /><br /><strong>Your store has been updated to the latest secure version(s)</strong>. For more information and answers to FAQs, please review our blog post detailing this issue.', '{}', 'pending', 'woocommerce.com', '2021-08-24 10:21:43', NULL, 0, 'plain', '', 0, 'info'),
(33, 'woocommerce-blocks-sqli-july-2021-store-patched', 'update', 'en_US', 'Solved: Critical vulnerabilities patched in WooCommerce Blocks', 'In response to a critical vulnerability identified on July 13, 2021, we worked with the WordPress Plugins Team to deploy software updates to stores running WooCommerce (versions 3.3 to 5.5) and the WooCommerce Blocks feature plugin (versions 2.5 to 5.5).<br /><br /><strong>Your store has been updated to the latest secure version(s)</strong>. For more information and answers to FAQs, please review our blog post detailing this issue.', '{}', 'pending', 'woocommerce.com', '2021-08-24 10:21:43', NULL, 0, 'plain', '', 0, 'info'),
(34, 'wc-refund-returns-page', 'info', 'en_US', 'Setup a Refund and Returns Policy page to boost your store\'s credibility.', 'We have created a sample draft Refund and Returns Policy page for you. Please have a look and update it to fit your store.', '{}', 'unactioned', 'woocommerce-core', '2021-08-24 10:21:45', NULL, 0, 'plain', '', 0, 'info'),
(35, 'wc-admin-wc-helper-connection', 'info', 'en_US', 'Connect to WooCommerce.com', 'Connect to get important product notifications and updates.', '{}', 'unactioned', 'woocommerce-admin', '2021-08-24 10:21:45', NULL, 0, 'plain', '', 0, 'info'),
(36, 'wc-admin-onboarding-email-marketing', 'info', 'en_US', 'Sign up for tips, product updates, and inspiration', 'We\'re here for you - get tips, product updates and inspiration straight to your email box', '{}', 'unactioned', 'woocommerce-admin', '2021-08-24 10:21:45', NULL, 0, 'plain', '', 0, 'info'),
(37, 'wc-admin-learn-more-about-variable-products', 'info', 'en_US', 'Learn more about variable products', 'Variable products are a powerful product type that lets you offer a set of variations on a product, with control over prices, stock, image and more for each variation. They can be used for a product like a shirt, where you can offer a large, medium and small and in different colors.', '{}', 'unactioned', 'woocommerce-admin', '2021-08-24 10:31:39', NULL, 0, 'plain', '', 0, 'info'),
(38, 'wc-admin-orders-milestone', 'info', 'en_US', 'First order received', 'Congratulations on getting your first order! Now is a great time to learn how to manage your orders.', '{}', 'unactioned', 'woocommerce-admin', '2021-08-24 16:01:47', NULL, 0, 'plain', '', 0, 'info'),
(39, 'wc-admin-choosing-a-theme', 'marketing', 'en_US', 'Choosing a theme?', 'Check out the themes that are compatible with WooCommerce and choose one aligned with your brand and business needs.', '{}', 'unactioned', 'woocommerce-admin', '2021-08-25 10:27:09', NULL, 0, 'plain', '', 0, 'info'),
(40, 'wc-admin-insight-first-product-and-payment', 'survey', 'en_US', 'Insight', 'More than 80% of new merchants add the first product and have at least one payment method set up during the first week.<br><br>Do you find this type of insight useful?', '{}', 'unactioned', 'woocommerce-admin', '2021-08-25 10:27:09', NULL, 0, 'plain', '', 0, 'info'),
(41, 'wc-admin-mobile-app', 'info', 'en_US', 'Install Woo mobile app', 'Install the WooCommerce mobile app to manage orders, receive sales notifications, and view key metrics — wherever you are.', '{}', 'unactioned', 'woocommerce-admin', '2021-08-26 10:22:38', NULL, 0, 'plain', '', 0, 'info'),
(42, 'wc-admin-customizing-product-catalog', 'info', 'en_US', 'How to customize your product catalog', 'You want your product catalog and images to look great and align with your brand. This guide will give you all the tips you need to get your products looking great in your store.', '{}', 'unactioned', 'woocommerce-admin', '2021-08-26 10:22:38', NULL, 0, 'plain', '', 0, 'info'),
(43, 'wc-admin-marketing-intro', 'info', 'en_US', 'Kết nối với khách hàng tiềm năng', 'Tăng khách hàng tiềm năng và số lượng đơn hàng với các công cụ marketing dành cho WooCommerce.', '{}', 'unactioned', 'woocommerce-admin', '2021-08-30 08:23:12', NULL, 0, 'plain', '', 0, 'info'),
(44, 'wc-admin-usage-tracking-opt-in', 'info', 'en_US', 'Help WooCommerce improve with usage tracking', 'Gathering usage data allows us to improve WooCommerce. Your store will be considered as we evaluate new features, judge the quality of an update, or determine if an improvement makes sense. You can always visit the <a href=\"http://wordpress.local/GCO/maytracdiasaoviet/wp-admin/admin.php?page=wc-settings&#038;tab=advanced&#038;section=woocommerce_com\" target=\"_blank\">Settings</a> and choose to stop sharing data. <a href=\"https://woocommerce.com/usage-tracking\" target=\"_blank\">Read more</a> about what data we collect.', '{}', 'unactioned', 'woocommerce-admin', '2021-09-03 07:15:31', NULL, 0, 'plain', '', 0, 'info'),
(45, 'wc-admin-insight-first-sale', 'survey', 'en_US', 'Did you know?', 'A WooCommerce powered store needs on average 31 days to get the first sale. You\'re on the right track! Do you find this type of insight useful?', '{}', 'unactioned', 'woocommerce-admin', '2021-09-03 07:15:31', NULL, 0, 'plain', '', 0, 'info'),
(46, 'mercadopago_q3_2021_EN', 'marketing', 'en_US', 'Get paid with Mercado Pago Checkout', 'Latin America\'s leading payment processor is now available for WooCommerce stores. Securely accept debit and credit cards, cash, bank transfers, and installment payments – backed by exclusive fraud prevention tools.', '{}', 'pending', 'woocommerce.com', '2021-09-03 07:15:32', NULL, 0, 'plain', '', 0, 'info'),
(47, 'wc-admin-real-time-order-alerts', 'info', 'en_US', 'Get real-time order alerts anywhere', 'Get notifications about store activity, including new orders and product reviews directly on your mobile devices with the Woo app.', '{}', 'unactioned', 'woocommerce-admin', '2021-11-24 10:32:50', NULL, 0, 'plain', '', 0, 'info');

-- --------------------------------------------------------

--
-- Table structure for table `wp_wc_admin_note_actions`
--

CREATE TABLE `wp_wc_admin_note_actions` (
  `action_id` bigint(20) UNSIGNED NOT NULL,
  `note_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `query` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `is_primary` tinyint(1) NOT NULL DEFAULT '0',
  `actioned_text` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `nonce_action` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `nonce_name` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_wc_admin_note_actions`
--

INSERT INTO `wp_wc_admin_note_actions` (`action_id`, `note_id`, `name`, `label`, `query`, `status`, `is_primary`, `actioned_text`, `nonce_action`, `nonce_name`) VALUES
(37, 34, 'notify-refund-returns-page', 'Edit page', 'http://wordpress.local/GCO/dogiadung/wp-admin/post.php?post=309&action=edit', 'actioned', 0, '', NULL, NULL),
(38, 35, 'connect', 'Kết nối', '?page=wc-addons&section=helper', 'unactioned', 0, '', NULL, NULL),
(39, 36, 'yes-please', 'Đồng ý!', 'https://woocommerce.us8.list-manage.com/subscribe/post?u=2c1434dc56f9506bf3c3ecd21&amp;id=13860df971&amp;SIGNUPPAGE=plugin', 'actioned', 0, '', NULL, NULL),
(112, 37, 'learn-more', 'Tìm hiểu thêm', 'https://docs.woocommerce.com/document/variable-product/?utm_source=inbox', 'actioned', 0, '', NULL, NULL),
(113, 38, 'learn-more', 'Tìm hiểu thêm', 'https://docs.woocommerce.com/document/managing-orders/?utm_source=inbox', 'actioned', 0, '', NULL, NULL),
(114, 39, 'visit-the-theme-marketplace', 'Visit the theme marketplace', 'https://woocommerce.com/product-category/themes/?utm_source=inbox', 'actioned', 0, '', NULL, NULL),
(115, 40, 'affirm-insight-first-product-and-payment', 'Có', '', 'actioned', 0, 'Thanks for your feedback', NULL, NULL),
(116, 40, 'affirm-insight-first-product-and-payment', 'Không', '', 'actioned', 0, 'Thanks for your feedback', NULL, NULL),
(153, 41, 'learn-more', 'Tìm hiểu thêm', 'https://woocommerce.com/mobile/', 'actioned', 0, '', NULL, NULL),
(154, 42, 'day-after-first-product', 'Tìm hiểu thêm', 'https://docs.woocommerce.com/document/woocommerce-customizer/?utm_source=inbox', 'actioned', 0, '', NULL, NULL),
(191, 43, 'open-marketing-hub', 'Open marketing hub', 'http://wordpress.local/GCO/dogiadung/wp-admin/admin.php?page=wc-admin&path=/marketing', 'actioned', 0, '', NULL, NULL),
(264, 44, 'tracking-opt-in', 'Activate usage tracking', '', 'actioned', 1, '', NULL, NULL),
(265, 45, 'affirm-insight-first-sale', 'Có', '', 'actioned', 0, 'Thanks for your feedback', NULL, NULL),
(266, 45, 'deny-insight-first-sale', 'Không', '', 'actioned', 0, 'Thanks for your feedback', NULL, NULL),
(378, 46, 'mercadopago_q3_2021_EN', 'Free download', 'https://woocommerce.com/products/mercado-pago-checkout/?utm_source=inbox&utm_medium=product&utm_campaign=mercadopago_q3_2021_EN', 'actioned', 1, '', NULL, NULL),
(487, 1, 'wayflyer_q3_2021', 'Get funded', 'https://woocommerce.com/products/wayflyer/', 'actioned', 1, '', NULL, NULL),
(488, 2, 'eu_vat_changes_2021', 'Learn more about the EU tax regulations', 'https://woocommerce.com/posts/new-eu-vat-regulations', 'actioned', 1, '', NULL, NULL),
(489, 3, 'open_wc_paypal_payments_product_page', 'Learn more', 'https://woocommerce.com/products/woocommerce-paypal-payments/', 'actioned', 1, '', NULL, NULL),
(490, 4, 'upgrade_now_facebook_pixel_api', 'Upgrade now', 'plugin-install.php?tab=plugin-information&plugin=&section=changelog', 'actioned', 1, '', NULL, NULL),
(491, 5, 'learn_more_facebook_ec', 'Learn more', 'https://woocommerce.com/products/facebook/', 'unactioned', 1, '', NULL, NULL),
(492, 6, 'set-up-concierge', 'Schedule free session', 'https://wordpress.com/me/concierge', 'actioned', 1, '', NULL, NULL),
(493, 7, 'learn-more', 'Learn more', 'https://docs.woocommerce.com/document/woocommerce-shipping-and-tax/?utm_source=inbox', 'unactioned', 1, '', NULL, NULL),
(494, 8, 'learn-more-ecomm-unique-shopping-experience', 'Learn more', 'https://docs.woocommerce.com/document/product-add-ons/?utm_source=inbox', 'actioned', 1, '', NULL, NULL),
(495, 9, 'watch-the-webinar', 'Watch the webinar', 'https://youtu.be/V_2XtCOyZ7o', 'actioned', 1, '', NULL, NULL),
(496, 10, 'learn-more', 'Learn more', 'https://woocommerce.com/posts/ecommerce-shipping-solutions-guide/?utm_source=inbox', 'actioned', 1, '', NULL, NULL),
(497, 11, 'boost-sales-marketing-guide', 'See marketing guide', 'https://developer.apple.com/apple-pay/marketing/?utm_source=inbox&utm_campaign=square-boost-sales', 'actioned', 1, '', NULL, NULL),
(498, 12, 'grow-your-business-marketing-guide', 'See marketing guide', 'https://developer.apple.com/apple-pay/marketing/?utm_source=inbox&utm_campaign=square-grow-your-business', 'actioned', 1, '', NULL, NULL),
(499, 13, 'add-apple-pay', 'Add Apple Pay', '/admin.php?page=wc-settings&tab=checkout&section=woocommerce_payments', 'actioned', 1, '', NULL, NULL),
(500, 13, 'learn-more', 'Learn more', 'https://docs.woocommerce.com/document/payments/apple-pay/?utm_source=inbox&utm_medium=product&utm_campaign=wcpay_applepay', 'actioned', 1, '', NULL, NULL),
(501, 14, 'boost-sales-marketing-guide', 'See marketing guide', 'https://developer.apple.com/apple-pay/marketing/?utm_source=inbox&utm_campaign=wcpay-boost-sales', 'actioned', 1, '', NULL, NULL),
(502, 15, 'grow-your-business-marketing-guide', 'See marketing guide', 'https://developer.apple.com/apple-pay/marketing/?utm_source=inbox&utm_campaign=wcpay-grow-your-business', 'actioned', 1, '', NULL, NULL),
(503, 16, 'optimizing-the-checkout-flow', 'Learn more', 'https://woocommerce.com/posts/optimizing-woocommerce-checkout?utm_source=inbox', 'actioned', 1, '', NULL, NULL),
(504, 17, 'learn-more', 'Learn more', 'https://woocommerce.com/posts/first-things-customize-woocommerce/?utm_source=inbox', 'unactioned', 1, '', NULL, NULL),
(505, 18, 'qualitative-feedback-from-new-users', 'Share feedback', 'https://automattic.survey.fm/wc-pay-new', 'actioned', 1, '', NULL, NULL),
(506, 19, 'share-feedback', 'Share feedback', 'http://automattic.survey.fm/paypal-feedback', 'unactioned', 1, '', NULL, NULL),
(507, 20, 'learn-more', 'Learn about Instant Deposits eligibility', 'https://docs.woocommerce.com/document/payments/instant-deposits/?utm_source=inbox&utm_medium=product&utm_campaign=wcpay_instant_deposits', 'actioned', 1, '', NULL, NULL),
(508, 21, 'get-started', 'Get started', 'https://woocommerce.com/products/google-listings-and-ads', 'actioned', 1, '', NULL, NULL),
(509, 22, 'update-wc-subscriptions-3-0-15', 'View latest version', 'http://wordpress.local/GCO/maytracdiasaoviet/wp-admin/admin.php?page=wc-admin&page=wc-addons&section=helper', 'actioned', 1, '', NULL, NULL),
(510, 23, 'update-wc-core-5-4-0', 'How to update WooCommerce', 'https://docs.woocommerce.com/document/how-to-update-woocommerce/', 'actioned', 1, '', NULL, NULL),
(511, 26, 'get-woo-commerce-payments', 'Get WooCommerce Payments', 'admin.php?page=wc-admin&action=setup-woocommerce-payments', 'actioned', 1, '', NULL, NULL),
(512, 27, 'get-woocommerce-payments', 'Get WooCommerce Payments', 'admin.php?page=wc-admin&action=setup-woocommerce-payments', 'actioned', 1, '', NULL, NULL),
(513, 28, 'ppxo-pps-install-paypal-payments-1', 'View upgrade guide', 'https://docs.woocommerce.com/document/woocommerce-paypal-payments/paypal-payments-upgrade-guide/', 'actioned', 1, '', NULL, NULL),
(514, 29, 'ppxo-pps-install-paypal-payments-2', 'View upgrade guide', 'https://docs.woocommerce.com/document/woocommerce-paypal-payments/paypal-payments-upgrade-guide/', 'actioned', 1, '', NULL, NULL),
(515, 30, 'learn-more', 'Learn more', 'https://woocommerce.com/posts/critical-vulnerability-detected-july-2021/?utm_source=inbox_note&utm_medium=product&utm_campaign=vulnerability_comms', 'unactioned', 1, '', NULL, NULL),
(516, 30, 'dismiss', 'Dismiss', '', 'actioned', 0, '', NULL, NULL),
(517, 31, 'learn-more', 'Learn more', 'https://woocommerce.com/posts/critical-vulnerability-detected-july-2021/?utm_source=inbox_note&utm_medium=product&utm_campaign=vulnerability_comms', 'unactioned', 1, '', NULL, NULL),
(518, 31, 'dismiss', 'Dismiss', '', 'actioned', 0, '', NULL, NULL),
(519, 32, 'learn-more', 'Learn more', 'https://woocommerce.com/posts/critical-vulnerability-detected-july-2021/?utm_source=inbox_note&utm_medium=product&utm_campaign=vulnerability_comms', 'unactioned', 1, '', NULL, NULL),
(520, 32, 'dismiss', 'Dismiss', '', 'actioned', 0, '', NULL, NULL),
(521, 33, 'learn-more', 'Learn more', 'https://woocommerce.com/posts/critical-vulnerability-detected-july-2021/?utm_source=inbox_note&utm_medium=product&utm_campaign=vulnerability_comms', 'unactioned', 1, '', NULL, NULL),
(522, 33, 'dismiss', 'Dismiss', '', 'actioned', 0, '', NULL, NULL),
(523, 47, 'learn-more', 'Tìm hiểu thêm', 'https://woocommerce.com/mobile/?utm_source=inbox', 'actioned', 0, '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wp_wc_category_lookup`
--

CREATE TABLE `wp_wc_category_lookup` (
  `category_tree_id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_wc_category_lookup`
--

INSERT INTO `wp_wc_category_lookup` (`category_tree_id`, `category_id`) VALUES
(25, 25),
(26, 26),
(26, 28),
(26, 29),
(27, 27),
(27, 30),
(28, 28),
(29, 29),
(30, 30),
(31, 31);

-- --------------------------------------------------------

--
-- Table structure for table `wp_wc_customer_lookup`
--

CREATE TABLE `wp_wc_customer_lookup` (
  `customer_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `username` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `first_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `date_last_active` timestamp NULL DEFAULT NULL,
  `date_registered` timestamp NULL DEFAULT NULL,
  `country` char(2) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `postcode` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `city` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `state` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_wc_customer_lookup`
--

INSERT INTO `wp_wc_customer_lookup` (`customer_id`, `user_id`, `username`, `first_name`, `last_name`, `email`, `date_last_active`, `date_registered`, `country`, `postcode`, `city`, `state`) VALUES
(1, 1, 'admin', 'Tiệp', '', 'tiepnguyen220194@gmail.com', '2021-12-29 17:00:00', '2020-12-07 02:44:42', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `wp_wc_download_log`
--

CREATE TABLE `wp_wc_download_log` (
  `download_log_id` bigint(20) UNSIGNED NOT NULL,
  `timestamp` datetime NOT NULL,
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `user_ip_address` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_wc_order_coupon_lookup`
--

CREATE TABLE `wp_wc_order_coupon_lookup` (
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `coupon_id` bigint(20) NOT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `discount_amount` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_wc_order_product_lookup`
--

CREATE TABLE `wp_wc_order_product_lookup` (
  `order_item_id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `variation_id` bigint(20) UNSIGNED NOT NULL,
  `customer_id` bigint(20) UNSIGNED DEFAULT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `product_qty` int(11) NOT NULL,
  `product_net_revenue` double NOT NULL DEFAULT '0',
  `product_gross_revenue` double NOT NULL DEFAULT '0',
  `coupon_amount` double NOT NULL DEFAULT '0',
  `tax_amount` double NOT NULL DEFAULT '0',
  `shipping_amount` double NOT NULL DEFAULT '0',
  `shipping_tax_amount` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_wc_order_product_lookup`
--

INSERT INTO `wp_wc_order_product_lookup` (`order_item_id`, `order_id`, `product_id`, `variation_id`, `customer_id`, `date_created`, `product_qty`, `product_net_revenue`, `product_gross_revenue`, `coupon_amount`, `tax_amount`, `shipping_amount`, `shipping_tax_amount`) VALUES
(1, 312, 310, 0, 1, '2021-08-24 17:46:40', 1, 950000, 950000, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_wc_order_stats`
--

CREATE TABLE `wp_wc_order_stats` (
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `parent_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_created_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `num_items_sold` int(11) NOT NULL DEFAULT '0',
  `total_sales` double NOT NULL DEFAULT '0',
  `tax_total` double NOT NULL DEFAULT '0',
  `shipping_total` double NOT NULL DEFAULT '0',
  `net_total` double NOT NULL DEFAULT '0',
  `returning_customer` tinyint(1) DEFAULT NULL,
  `status` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `customer_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_wc_order_stats`
--

INSERT INTO `wp_wc_order_stats` (`order_id`, `parent_id`, `date_created`, `date_created_gmt`, `num_items_sold`, `total_sales`, `tax_total`, `shipping_total`, `net_total`, `returning_customer`, `status`, `customer_id`) VALUES
(312, 0, '2021-08-24 17:46:40', '2021-08-24 10:46:40', 1, 950000, 0, 0, 950000, 0, 'wc-processing', 1);

-- --------------------------------------------------------

--
-- Table structure for table `wp_wc_order_tax_lookup`
--

CREATE TABLE `wp_wc_order_tax_lookup` (
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_id` bigint(20) UNSIGNED NOT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `shipping_tax` double NOT NULL DEFAULT '0',
  `order_tax` double NOT NULL DEFAULT '0',
  `total_tax` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_wc_product_meta_lookup`
--

CREATE TABLE `wp_wc_product_meta_lookup` (
  `product_id` bigint(20) NOT NULL,
  `sku` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT '',
  `virtual` tinyint(1) DEFAULT '0',
  `downloadable` tinyint(1) DEFAULT '0',
  `min_price` decimal(19,4) DEFAULT NULL,
  `max_price` decimal(19,4) DEFAULT NULL,
  `onsale` tinyint(1) DEFAULT '0',
  `stock_quantity` double DEFAULT NULL,
  `stock_status` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT 'instock',
  `rating_count` bigint(20) DEFAULT '0',
  `average_rating` decimal(3,2) DEFAULT '0.00',
  `total_sales` bigint(20) DEFAULT '0',
  `tax_status` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT 'taxable',
  `tax_class` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_wc_product_meta_lookup`
--

INSERT INTO `wp_wc_product_meta_lookup` (`product_id`, `sku`, `virtual`, `downloadable`, `min_price`, `max_price`, `onsale`, `stock_quantity`, `stock_status`, `rating_count`, `average_rating`, `total_sales`, `tax_status`, `tax_class`) VALUES
(310, '', 0, 0, '950000.0000', '950000.0000', 1, NULL, 'instock', 1, '4.00', 1, 'taxable', ''),
(319, '', 0, 0, '2000000.0000', '2000000.0000', 0, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(320, '', 0, 0, '0.0000', '0.0000', 0, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(326, '', 0, 0, '950000.0000', '950000.0000', 1, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(327, '', 0, 0, '1000000.0000', '1000000.0000', 0, NULL, 'instock', 0, '0.00', 0, 'taxable', '');

-- --------------------------------------------------------

--
-- Table structure for table `wp_wc_reserved_stock`
--

CREATE TABLE `wp_wc_reserved_stock` (
  `order_id` bigint(20) NOT NULL,
  `product_id` bigint(20) NOT NULL,
  `stock_quantity` double NOT NULL DEFAULT '0',
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `expires` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_wc_tax_rate_classes`
--

CREATE TABLE `wp_wc_tax_rate_classes` (
  `tax_rate_class_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_wc_tax_rate_classes`
--

INSERT INTO `wp_wc_tax_rate_classes` (`tax_rate_class_id`, `name`, `slug`) VALUES
(1, 'Reduced rate', 'reduced-rate'),
(2, 'Zero rate', 'zero-rate');

-- --------------------------------------------------------

--
-- Table structure for table `wp_wc_webhooks`
--

CREATE TABLE `wp_wc_webhooks` (
  `webhook_id` bigint(20) UNSIGNED NOT NULL,
  `status` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `name` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `delivery_url` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `secret` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `topic` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_created_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `api_version` smallint(4) NOT NULL,
  `failure_count` smallint(10) NOT NULL DEFAULT '0',
  `pending_delivery` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_api_keys`
--

CREATE TABLE `wp_woocommerce_api_keys` (
  `key_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `description` varchar(200) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `permissions` varchar(10) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `consumer_key` char(64) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `consumer_secret` char(43) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `nonces` longtext COLLATE utf8mb4_unicode_520_ci,
  `truncated_key` char(7) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `last_access` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_attribute_taxonomies`
--

CREATE TABLE `wp_woocommerce_attribute_taxonomies` (
  `attribute_id` bigint(20) UNSIGNED NOT NULL,
  `attribute_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `attribute_label` varchar(200) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `attribute_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `attribute_orderby` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `attribute_public` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_downloadable_product_permissions`
--

CREATE TABLE `wp_woocommerce_downloadable_product_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `download_id` varchar(36) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `order_key` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `user_email` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `downloads_remaining` varchar(9) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `access_granted` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access_expires` datetime DEFAULT NULL,
  `download_count` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_log`
--

CREATE TABLE `wp_woocommerce_log` (
  `log_id` bigint(20) UNSIGNED NOT NULL,
  `timestamp` datetime NOT NULL,
  `level` smallint(4) NOT NULL,
  `source` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `message` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `context` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_order_itemmeta`
--

CREATE TABLE `wp_woocommerce_order_itemmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `order_item_id` bigint(20) UNSIGNED NOT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_woocommerce_order_itemmeta`
--

INSERT INTO `wp_woocommerce_order_itemmeta` (`meta_id`, `order_item_id`, `meta_key`, `meta_value`) VALUES
(1, 1, '_product_id', '310'),
(2, 1, '_variation_id', '0'),
(3, 1, '_qty', '1'),
(4, 1, '_tax_class', ''),
(5, 1, '_line_subtotal', '950000'),
(6, 1, '_line_subtotal_tax', '0'),
(7, 1, '_line_total', '950000'),
(8, 1, '_line_tax', '0'),
(9, 1, '_line_tax_data', 'a:2:{s:5:\"total\";a:0:{}s:8:\"subtotal\";a:0:{}}');

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_order_items`
--

CREATE TABLE `wp_woocommerce_order_items` (
  `order_item_id` bigint(20) UNSIGNED NOT NULL,
  `order_item_name` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `order_item_type` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `order_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_woocommerce_order_items`
--

INSERT INTO `wp_woocommerce_order_items` (`order_item_id`, `order_item_name`, `order_item_type`, `order_id`) VALUES
(1, 'Sản phẩm 1', 'line_item', 312);

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_payment_tokenmeta`
--

CREATE TABLE `wp_woocommerce_payment_tokenmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `payment_token_id` bigint(20) UNSIGNED NOT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_payment_tokens`
--

CREATE TABLE `wp_woocommerce_payment_tokens` (
  `token_id` bigint(20) UNSIGNED NOT NULL,
  `gateway_id` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `token` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `type` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_sessions`
--

CREATE TABLE `wp_woocommerce_sessions` (
  `session_id` bigint(20) UNSIGNED NOT NULL,
  `session_key` char(32) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `session_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `session_expiry` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_woocommerce_sessions`
--

INSERT INTO `wp_woocommerce_sessions` (`session_id`, `session_key`, `session_value`, `session_expiry`) VALUES
(1, '1', 'a:7:{s:4:\"cart\";s:834:\"a:2:{s:32:\"a666587afda6e89aec274a3657558a27\";a:11:{s:3:\"key\";s:32:\"a666587afda6e89aec274a3657558a27\";s:10:\"product_id\";i:326;s:12:\"variation_id\";i:0;s:9:\"variation\";a:0:{}s:8:\"quantity\";i:1;s:9:\"data_hash\";s:32:\"b5c1d5ca8bae6d4896cf1807cdf763f0\";s:13:\"line_tax_data\";a:2:{s:8:\"subtotal\";a:0:{}s:5:\"total\";a:0:{}}s:13:\"line_subtotal\";d:950000;s:17:\"line_subtotal_tax\";i:0;s:10:\"line_total\";d:950000;s:8:\"line_tax\";i:0;}s:32:\"06eb61b839a0cefee4967c67ccb099dc\";a:11:{s:3:\"key\";s:32:\"06eb61b839a0cefee4967c67ccb099dc\";s:10:\"product_id\";i:310;s:12:\"variation_id\";i:0;s:9:\"variation\";a:0:{}s:8:\"quantity\";i:2;s:9:\"data_hash\";s:32:\"b5c1d5ca8bae6d4896cf1807cdf763f0\";s:13:\"line_tax_data\";a:2:{s:8:\"subtotal\";a:0:{}s:5:\"total\";a:0:{}}s:13:\"line_subtotal\";d:1900000;s:17:\"line_subtotal_tax\";i:0;s:10:\"line_total\";d:1900000;s:8:\"line_tax\";i:0;}}\";s:11:\"cart_totals\";s:405:\"a:15:{s:8:\"subtotal\";s:7:\"2850000\";s:12:\"subtotal_tax\";i:0;s:14:\"shipping_total\";s:1:\"0\";s:12:\"shipping_tax\";i:0;s:14:\"shipping_taxes\";a:0:{}s:14:\"discount_total\";i:0;s:12:\"discount_tax\";i:0;s:19:\"cart_contents_total\";s:7:\"2850000\";s:17:\"cart_contents_tax\";i:0;s:19:\"cart_contents_taxes\";a:0:{}s:9:\"fee_total\";s:1:\"0\";s:7:\"fee_tax\";i:0;s:9:\"fee_taxes\";a:0:{}s:5:\"total\";s:7:\"2850000\";s:9:\"total_tax\";d:0;}\";s:15:\"applied_coupons\";s:6:\"a:0:{}\";s:22:\"coupon_discount_totals\";s:6:\"a:0:{}\";s:26:\"coupon_discount_tax_totals\";s:6:\"a:0:{}\";s:21:\"removed_cart_contents\";s:6:\"a:0:{}\";s:8:\"customer\";s:804:\"a:27:{s:2:\"id\";s:1:\"1\";s:13:\"date_modified\";s:25:\"2021-08-24T10:46:39+00:00\";s:8:\"postcode\";s:0:\"\";s:4:\"city\";s:0:\"\";s:9:\"address_1\";s:9:\"Hà Nội\";s:7:\"address\";s:9:\"Hà Nội\";s:9:\"address_2\";s:0:\"\";s:5:\"state\";s:0:\"\";s:7:\"country\";s:2:\"VN\";s:17:\"shipping_postcode\";s:0:\"\";s:13:\"shipping_city\";s:0:\"\";s:18:\"shipping_address_1\";s:0:\"\";s:16:\"shipping_address\";s:0:\"\";s:18:\"shipping_address_2\";s:0:\"\";s:14:\"shipping_state\";s:0:\"\";s:16:\"shipping_country\";s:2:\"VN\";s:13:\"is_vat_exempt\";s:0:\"\";s:19:\"calculated_shipping\";s:0:\"\";s:10:\"first_name\";s:6:\"Tiệp\";s:9:\"last_name\";s:0:\"\";s:7:\"company\";s:0:\"\";s:5:\"phone\";s:10:\"0359117322\";s:5:\"email\";s:26:\"tiepnguyen220194@gmail.com\";s:19:\"shipping_first_name\";s:0:\"\";s:18:\"shipping_last_name\";s:0:\"\";s:16:\"shipping_company\";s:0:\"\";s:14:\"shipping_phone\";s:0:\"\";}\";}', 1641006965);

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_shipping_zones`
--

CREATE TABLE `wp_woocommerce_shipping_zones` (
  `zone_id` bigint(20) UNSIGNED NOT NULL,
  `zone_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `zone_order` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_shipping_zone_locations`
--

CREATE TABLE `wp_woocommerce_shipping_zone_locations` (
  `location_id` bigint(20) UNSIGNED NOT NULL,
  `zone_id` bigint(20) UNSIGNED NOT NULL,
  `location_code` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `location_type` varchar(40) COLLATE utf8mb4_unicode_520_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_shipping_zone_methods`
--

CREATE TABLE `wp_woocommerce_shipping_zone_methods` (
  `zone_id` bigint(20) UNSIGNED NOT NULL,
  `instance_id` bigint(20) UNSIGNED NOT NULL,
  `method_id` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `method_order` bigint(20) UNSIGNED NOT NULL,
  `is_enabled` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_tax_rates`
--

CREATE TABLE `wp_woocommerce_tax_rates` (
  `tax_rate_id` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_country` varchar(2) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `tax_rate_state` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `tax_rate` varchar(8) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `tax_rate_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `tax_rate_priority` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_compound` int(1) NOT NULL DEFAULT '0',
  `tax_rate_shipping` int(1) NOT NULL DEFAULT '1',
  `tax_rate_order` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_class` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_tax_rate_locations`
--

CREATE TABLE `wp_woocommerce_tax_rate_locations` (
  `location_id` bigint(20) UNSIGNED NOT NULL,
  `location_code` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `tax_rate_id` bigint(20) UNSIGNED NOT NULL,
  `location_type` varchar(40) COLLATE utf8mb4_unicode_520_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `wp_actionscheduler_actions`
--
ALTER TABLE `wp_actionscheduler_actions`
  ADD PRIMARY KEY (`action_id`),
  ADD KEY `hook` (`hook`),
  ADD KEY `status` (`status`),
  ADD KEY `scheduled_date_gmt` (`scheduled_date_gmt`),
  ADD KEY `args` (`args`),
  ADD KEY `group_id` (`group_id`),
  ADD KEY `last_attempt_gmt` (`last_attempt_gmt`),
  ADD KEY `claim_id` (`claim_id`),
  ADD KEY `claim_id_status_scheduled_date_gmt` (`claim_id`,`status`,`scheduled_date_gmt`);

--
-- Indexes for table `wp_actionscheduler_claims`
--
ALTER TABLE `wp_actionscheduler_claims`
  ADD PRIMARY KEY (`claim_id`),
  ADD KEY `date_created_gmt` (`date_created_gmt`);

--
-- Indexes for table `wp_actionscheduler_groups`
--
ALTER TABLE `wp_actionscheduler_groups`
  ADD PRIMARY KEY (`group_id`),
  ADD KEY `slug` (`slug`(191));

--
-- Indexes for table `wp_actionscheduler_logs`
--
ALTER TABLE `wp_actionscheduler_logs`
  ADD PRIMARY KEY (`log_id`),
  ADD KEY `action_id` (`action_id`),
  ADD KEY `log_date_gmt` (`log_date_gmt`);

--
-- Indexes for table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_comments`
--
ALTER TABLE `wp_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10)),
  ADD KEY `woo_idx_comment_type` (`comment_type`);

--
-- Indexes for table `wp_links`
--
ALTER TABLE `wp_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Indexes for table `wp_options`
--
ALTER TABLE `wp_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`),
  ADD KEY `autoload` (`autoload`);

--
-- Indexes for table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_posts`
--
ALTER TABLE `wp_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Indexes for table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_terms`
--
ALTER TABLE `wp_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Indexes for table `wp_term_relationships`
--
ALTER TABLE `wp_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Indexes for table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Indexes for table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_users`
--
ALTER TABLE `wp_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- Indexes for table `wp_wc_admin_notes`
--
ALTER TABLE `wp_wc_admin_notes`
  ADD PRIMARY KEY (`note_id`);

--
-- Indexes for table `wp_wc_admin_note_actions`
--
ALTER TABLE `wp_wc_admin_note_actions`
  ADD PRIMARY KEY (`action_id`),
  ADD KEY `note_id` (`note_id`);

--
-- Indexes for table `wp_wc_category_lookup`
--
ALTER TABLE `wp_wc_category_lookup`
  ADD PRIMARY KEY (`category_tree_id`,`category_id`);

--
-- Indexes for table `wp_wc_customer_lookup`
--
ALTER TABLE `wp_wc_customer_lookup`
  ADD PRIMARY KEY (`customer_id`),
  ADD UNIQUE KEY `user_id` (`user_id`),
  ADD KEY `email` (`email`);

--
-- Indexes for table `wp_wc_download_log`
--
ALTER TABLE `wp_wc_download_log`
  ADD PRIMARY KEY (`download_log_id`),
  ADD KEY `permission_id` (`permission_id`),
  ADD KEY `timestamp` (`timestamp`);

--
-- Indexes for table `wp_wc_order_coupon_lookup`
--
ALTER TABLE `wp_wc_order_coupon_lookup`
  ADD PRIMARY KEY (`order_id`,`coupon_id`),
  ADD KEY `coupon_id` (`coupon_id`),
  ADD KEY `date_created` (`date_created`);

--
-- Indexes for table `wp_wc_order_product_lookup`
--
ALTER TABLE `wp_wc_order_product_lookup`
  ADD PRIMARY KEY (`order_item_id`),
  ADD KEY `order_id` (`order_id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `customer_id` (`customer_id`),
  ADD KEY `date_created` (`date_created`);

--
-- Indexes for table `wp_wc_order_stats`
--
ALTER TABLE `wp_wc_order_stats`
  ADD PRIMARY KEY (`order_id`),
  ADD KEY `date_created` (`date_created`),
  ADD KEY `customer_id` (`customer_id`),
  ADD KEY `status` (`status`(191));

--
-- Indexes for table `wp_wc_order_tax_lookup`
--
ALTER TABLE `wp_wc_order_tax_lookup`
  ADD PRIMARY KEY (`order_id`,`tax_rate_id`),
  ADD KEY `tax_rate_id` (`tax_rate_id`),
  ADD KEY `date_created` (`date_created`);

--
-- Indexes for table `wp_wc_product_meta_lookup`
--
ALTER TABLE `wp_wc_product_meta_lookup`
  ADD PRIMARY KEY (`product_id`),
  ADD KEY `virtual` (`virtual`),
  ADD KEY `downloadable` (`downloadable`),
  ADD KEY `stock_status` (`stock_status`),
  ADD KEY `stock_quantity` (`stock_quantity`),
  ADD KEY `onsale` (`onsale`),
  ADD KEY `min_max_price` (`min_price`,`max_price`);

--
-- Indexes for table `wp_wc_reserved_stock`
--
ALTER TABLE `wp_wc_reserved_stock`
  ADD PRIMARY KEY (`order_id`,`product_id`);

--
-- Indexes for table `wp_wc_tax_rate_classes`
--
ALTER TABLE `wp_wc_tax_rate_classes`
  ADD PRIMARY KEY (`tax_rate_class_id`),
  ADD UNIQUE KEY `slug` (`slug`(191));

--
-- Indexes for table `wp_wc_webhooks`
--
ALTER TABLE `wp_wc_webhooks`
  ADD PRIMARY KEY (`webhook_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `wp_woocommerce_api_keys`
--
ALTER TABLE `wp_woocommerce_api_keys`
  ADD PRIMARY KEY (`key_id`),
  ADD KEY `consumer_key` (`consumer_key`),
  ADD KEY `consumer_secret` (`consumer_secret`);

--
-- Indexes for table `wp_woocommerce_attribute_taxonomies`
--
ALTER TABLE `wp_woocommerce_attribute_taxonomies`
  ADD PRIMARY KEY (`attribute_id`),
  ADD KEY `attribute_name` (`attribute_name`(20));

--
-- Indexes for table `wp_woocommerce_downloadable_product_permissions`
--
ALTER TABLE `wp_woocommerce_downloadable_product_permissions`
  ADD PRIMARY KEY (`permission_id`),
  ADD KEY `download_order_key_product` (`product_id`,`order_id`,`order_key`(16),`download_id`),
  ADD KEY `download_order_product` (`download_id`,`order_id`,`product_id`),
  ADD KEY `order_id` (`order_id`),
  ADD KEY `user_order_remaining_expires` (`user_id`,`order_id`,`downloads_remaining`,`access_expires`);

--
-- Indexes for table `wp_woocommerce_log`
--
ALTER TABLE `wp_woocommerce_log`
  ADD PRIMARY KEY (`log_id`),
  ADD KEY `level` (`level`);

--
-- Indexes for table `wp_woocommerce_order_itemmeta`
--
ALTER TABLE `wp_woocommerce_order_itemmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `order_item_id` (`order_item_id`),
  ADD KEY `meta_key` (`meta_key`(32));

--
-- Indexes for table `wp_woocommerce_order_items`
--
ALTER TABLE `wp_woocommerce_order_items`
  ADD PRIMARY KEY (`order_item_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `wp_woocommerce_payment_tokenmeta`
--
ALTER TABLE `wp_woocommerce_payment_tokenmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `payment_token_id` (`payment_token_id`),
  ADD KEY `meta_key` (`meta_key`(32));

--
-- Indexes for table `wp_woocommerce_payment_tokens`
--
ALTER TABLE `wp_woocommerce_payment_tokens`
  ADD PRIMARY KEY (`token_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `wp_woocommerce_sessions`
--
ALTER TABLE `wp_woocommerce_sessions`
  ADD PRIMARY KEY (`session_id`),
  ADD UNIQUE KEY `session_key` (`session_key`);

--
-- Indexes for table `wp_woocommerce_shipping_zones`
--
ALTER TABLE `wp_woocommerce_shipping_zones`
  ADD PRIMARY KEY (`zone_id`);

--
-- Indexes for table `wp_woocommerce_shipping_zone_locations`
--
ALTER TABLE `wp_woocommerce_shipping_zone_locations`
  ADD PRIMARY KEY (`location_id`),
  ADD KEY `location_id` (`location_id`),
  ADD KEY `location_type_code` (`location_type`(10),`location_code`(20));

--
-- Indexes for table `wp_woocommerce_shipping_zone_methods`
--
ALTER TABLE `wp_woocommerce_shipping_zone_methods`
  ADD PRIMARY KEY (`instance_id`);

--
-- Indexes for table `wp_woocommerce_tax_rates`
--
ALTER TABLE `wp_woocommerce_tax_rates`
  ADD PRIMARY KEY (`tax_rate_id`),
  ADD KEY `tax_rate_country` (`tax_rate_country`),
  ADD KEY `tax_rate_state` (`tax_rate_state`(2)),
  ADD KEY `tax_rate_class` (`tax_rate_class`(10)),
  ADD KEY `tax_rate_priority` (`tax_rate_priority`);

--
-- Indexes for table `wp_woocommerce_tax_rate_locations`
--
ALTER TABLE `wp_woocommerce_tax_rate_locations`
  ADD PRIMARY KEY (`location_id`),
  ADD KEY `tax_rate_id` (`tax_rate_id`),
  ADD KEY `location_type_code` (`location_type`(10),`location_code`(20));

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wp_actionscheduler_actions`
--
ALTER TABLE `wp_actionscheduler_actions`
  MODIFY `action_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `wp_actionscheduler_claims`
--
ALTER TABLE `wp_actionscheduler_claims`
  MODIFY `claim_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `wp_actionscheduler_groups`
--
ALTER TABLE `wp_actionscheduler_groups`
  MODIFY `group_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `wp_actionscheduler_logs`
--
ALTER TABLE `wp_actionscheduler_logs`
  MODIFY `log_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `wp_comments`
--
ALTER TABLE `wp_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `wp_links`
--
ALTER TABLE `wp_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_options`
--
ALTER TABLE `wp_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2166;

--
-- AUTO_INCREMENT for table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1500;

--
-- AUTO_INCREMENT for table `wp_posts`
--
ALTER TABLE `wp_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=389;

--
-- AUTO_INCREMENT for table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `wp_terms`
--
ALTER TABLE `wp_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT for table `wp_users`
--
ALTER TABLE `wp_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wp_wc_admin_notes`
--
ALTER TABLE `wp_wc_admin_notes`
  MODIFY `note_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `wp_wc_admin_note_actions`
--
ALTER TABLE `wp_wc_admin_note_actions`
  MODIFY `action_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=524;

--
-- AUTO_INCREMENT for table `wp_wc_customer_lookup`
--
ALTER TABLE `wp_wc_customer_lookup`
  MODIFY `customer_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wp_wc_download_log`
--
ALTER TABLE `wp_wc_download_log`
  MODIFY `download_log_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_wc_tax_rate_classes`
--
ALTER TABLE `wp_wc_tax_rate_classes`
  MODIFY `tax_rate_class_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `wp_wc_webhooks`
--
ALTER TABLE `wp_wc_webhooks`
  MODIFY `webhook_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_woocommerce_api_keys`
--
ALTER TABLE `wp_woocommerce_api_keys`
  MODIFY `key_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_woocommerce_attribute_taxonomies`
--
ALTER TABLE `wp_woocommerce_attribute_taxonomies`
  MODIFY `attribute_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_woocommerce_downloadable_product_permissions`
--
ALTER TABLE `wp_woocommerce_downloadable_product_permissions`
  MODIFY `permission_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_woocommerce_log`
--
ALTER TABLE `wp_woocommerce_log`
  MODIFY `log_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_woocommerce_order_itemmeta`
--
ALTER TABLE `wp_woocommerce_order_itemmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `wp_woocommerce_order_items`
--
ALTER TABLE `wp_woocommerce_order_items`
  MODIFY `order_item_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wp_woocommerce_payment_tokenmeta`
--
ALTER TABLE `wp_woocommerce_payment_tokenmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_woocommerce_payment_tokens`
--
ALTER TABLE `wp_woocommerce_payment_tokens`
  MODIFY `token_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_woocommerce_sessions`
--
ALTER TABLE `wp_woocommerce_sessions`
  MODIFY `session_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wp_woocommerce_shipping_zones`
--
ALTER TABLE `wp_woocommerce_shipping_zones`
  MODIFY `zone_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_woocommerce_shipping_zone_locations`
--
ALTER TABLE `wp_woocommerce_shipping_zone_locations`
  MODIFY `location_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_woocommerce_shipping_zone_methods`
--
ALTER TABLE `wp_woocommerce_shipping_zone_methods`
  MODIFY `instance_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_woocommerce_tax_rates`
--
ALTER TABLE `wp_woocommerce_tax_rates`
  MODIFY `tax_rate_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_woocommerce_tax_rate_locations`
--
ALTER TABLE `wp_woocommerce_tax_rate_locations`
  MODIFY `location_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `wp_wc_download_log`
--
ALTER TABLE `wp_wc_download_log`
  ADD CONSTRAINT `fk_wp_wc_download_log_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `wp_woocommerce_downloadable_product_permissions` (`permission_id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
